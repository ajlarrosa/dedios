<?php

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class VarsExtension extends AbstractExtension{ 

    
     public function getFilters()
    {
        return [
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
        ];
    }
    
  

    public function jsonDecode($str) {
        return json_decode($str);
    }

}
