<?php

namespace App\Entity;

use App\LocalidadRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\LocalidadRepository::class)
 * @ORM\Table(name="localidad")
 * @UniqueEntity(
 * 		fields = {"descripcion","provincia"},
 *                          errorPath="descripcion",
 * 			message = "The localty already exists for this province."
 * 		) 
 */
class Localidad {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Provincia", inversedBy="localidades")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255,nullable=false)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "La descripción no puede superar los 255 caracteres"
     * )   
     * @ORM\OrderBy({"descripcion" = "ASC"})     * 
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cp;

    /**
     * @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="localidad")
     */
    protected $clientesproveedores;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * *******************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->clientesproveedores = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getFullDescription() {
        return $this->getDescripcion().', '.$this->provincia->getDescripcion();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCp(): ?int {
        return $this->cp;
    }

    public function setCp(?int $cp): self {
        $this->cp = $cp;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getProvincia(): ?Provincia {
        return $this->provincia;
    }

    public function setProvincia(?Provincia $provincia): self {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * @return Collection|ClienteProveedor[]
     */
    public function getClientesproveedores(): Collection {
        return $this->clientesproveedores;
    }

    public function addClientesproveedore(ClienteProveedor $clientesproveedore): self {
        if (!$this->clientesproveedores->contains($clientesproveedore)) {
            $this->clientesproveedores[] = $clientesproveedore;
            $clientesproveedore->setLocalidad($this);
        }

        return $this;
    }

    public function removeClientesproveedore(ClienteProveedor $clientesproveedore): self {
        if ($this->clientesproveedores->contains($clientesproveedore)) {
            $this->clientesproveedores->removeElement($clientesproveedore);
            // set the owning side to null (unless already changed)
            if ($clientesproveedore->getLocalidad() === $this) {
                $clientesproveedore->setLocalidad(null);
            }
        }

        return $this;
    }

}
