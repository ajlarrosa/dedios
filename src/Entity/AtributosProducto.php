<?php

namespace App\Entity;

use App\Entity\Medida;
use App\Repository\AtributosProductoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\AtributosProductoRepository::class)
 * @ORM\Table(name="atributos_producto")
 */
class AtributosProducto {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="atributosProducto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Medida", inversedBy="atributosProducto")
     * @ORM\JoinColumn(name="medida_id", referencedColumnName="id", nullable=true)
     */
    protected $medida;

    /**
     * @ORM\ManyToOne(targetEntity="Color", inversedBy="atributosProducto")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id", nullable=true)
     */
    protected $color;

    
    /**
     * @ORM\ManyToOne(targetEntity="Letra", inversedBy="atributosProducto")
     * @ORM\JoinColumn(name="letra_id", referencedColumnName="id", nullable=true)
     */
    protected $letra;
    
    /**
     * @ORM\OneToMany(targetEntity="PrecioMedidaProducto", mappedBy="atributoProducto", cascade={"persist", "remove"})
     */
    protected $preciosMedidaProducto;

    public function __construct() {
        $this->preciosMedidaProducto = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getMedida(): ?Medida {
        return $this->medida;
    }

    public function setMedida(Medida $medida): self {
        $this->medida = $medida;

        return $this;
    }

    public function getColor(): ?Color {
        return $this->color;
    }

    public function setColor(Color $color): self {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|PrecioMedidaProducto[]
     */
    public function getPreciosMedidaProducto(): Collection
    {
        return $this->preciosMedidaProducto;
    }

    public function addPreciosMedidaProducto(PrecioMedidaProducto $preciosMedidaProducto): self
    {
        if (!$this->preciosMedidaProducto->contains($preciosMedidaProducto)) {
            $this->preciosMedidaProducto[] = $preciosMedidaProducto;
            $preciosMedidaProducto->setAtributoProducto($this);
        }

        return $this;
    }

    public function removePreciosMedidaProducto(PrecioMedidaproducto $preciosMedidaProducto): self
    {
        if ($this->preciosMedidaProducto->contains($preciosMedidaProducto)) {
            $this->preciosMedidaProducto->removeElement($preciosMedidaProducto);
            // set the owning side to null (unless already changed)
            if ($preciosMedidaProducto->getAtributoProducto() === $this) {
                $preciosMedidaProducto->setAtributoProducto(null);
            }
        }

        return $this;
    }

    public function getLetra(): ?Letra
    {
        return $this->letra;
    }

    public function setLetra(?Letra $letra): self
    {
        $this->letra = $letra;

        return $this;
    }

}
