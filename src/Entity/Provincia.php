<?php

namespace App\Entity;

use App\ProvinciaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ProvinciaRepository::class)
 * @ORM\Table(name="provincia")
 * @UniqueEntity(
 * 		fields = {"descripcion"},
 *                   errorPath="descripcion",
 * 			message = "The province already exists."
 * 		) 
 */
class Provincia {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=false)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "La descripción no puede superar los 255 caracteres"
     * )   
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Localidad", mappedBy="provincia")
     */
    protected $localidades;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->localidades = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|Localidad[]
     */
    public function getLocalidades(): Collection {
        return $this->localidades;
    }

    public function addLocalidade(Localidad $localidade): self {
        if (!$this->localidades->contains($localidade)) {
            $this->localidades[] = $localidade;
            $localidade->setProvincia($this);
        }

        return $this;
    }

    public function removeLocalidade(Localidad $localidade): self {
        if ($this->localidades->contains($localidade)) {
            $this->localidades->removeElement($localidade);
            // set the owning side to null (unless already changed)
            if ($localidade->getProvincia() === $this) {
                $localidade->setProvincia(null);
            }
        }

        return $this;
    }

}
