<?php

namespace App\Entity;

use App\Repository\OrdenPagoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Moneda;

/**
 * @ORM\Entity(repositoryClass=App\Repository\OrdenPagoRepository::class)
 * @ORM\Table(name="ordenpago")
 */
class OrdenPago extends Moneda {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC")
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=true)
     * */
    private $movimientocc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $importe = 0;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="reciboCliente", cascade={"persist"})
     */
    protected $cheques;

    /**
     * @ORM\OneToMany(targetEntity="Cheque", mappedBy="ordenPago", cascade={"persist"})
     */
    protected $chequesOrdenPago;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->cheques = new ArrayCollection();
        $this->chequesOrdenPago = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'Orden Pago Nro. ' . $this->id;
    }

    public function getTotal() {
        if ($this->moneda == self::COD_PESOS) {
            $total = $this->importe + $this->getTotalCheques();
        } else {
            $total = $this->importe;
        }
        return $total;
    }

    public function getTotalCheques() {
        $total=0;
        foreach ($this->chequesOrdenPago as $cheque) {
            $total = $total + $cheque->getImporte();
        }
        return $total;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImporte(): ?float {
        return $this->importe;
    }

    public function setImporte(float $importe): self {
        $this->importe = $importe;

        return $this;
    }

    public function getObservacion(): ?string {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self {
        $this->observacion = $observacion;

        return $this;
    }

    public function getMovimientocc(): ?MovimientoCC {
        return $this->movimientocc;
    }

    public function setMovimientocc(?MovimientoCC $movimientocc): self {
        $this->movimientocc = $movimientocc;

        return $this;
    }

    /**
     * @return Collection|Cheque[]
     */
    public function getCheques(): Collection {
        return $this->cheques;
    }

    public function addCheque(Cheque $cheque): self {
        if (!$this->cheques->contains($cheque)) {
            $this->cheques[] = $cheque;
            $cheque->setEstado($cheque::COD_USADO);            
            $cheque->setReciboCliente($this);
        }

        return $this;
    }

    public function removeCheque(Cheque $cheque): self {
        if ($this->cheques->contains($cheque)) {
            $this->cheques->removeElement($cheque);
            $cheque->setEstado($cheque::COD_ACTIVO);
            // set the owning side to null (unless already changed)
            if ($cheque->getReciboCliente() === $this) {
                $cheque->setReciboCliente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cheque[]
     */
    public function getChequesOrdenPago(): Collection {
        return $this->chequesOrdenPago;
    }

    public function addChequesOrdenPago(Cheque $chequesOrdenPago): self {
        if (!$this->chequesOrdenPago->contains($chequesOrdenPago)) {
            $this->chequesOrdenPago[] = $chequesOrdenPago;
            $chequesOrdenPago->setEstado($chequesOrdenPago::COD_USADO);
            $chequesOrdenPago->setOrdenPago($this);
        }

        return $this;
    }

    public function removeChequesOrdenPago(Cheque $chequesOrdenPago): self {
        if ($this->chequesOrdenPago->contains($chequesOrdenPago)) {
            $this->chequesOrdenPago->removeElement($chequesOrdenPago);
            $chequesOrdenPago->setEstado($chequesOrdenPago::COD_ACTIVO);
            // set the owning side to null (unless already changed)
            if ($chequesOrdenPago->getOrdenPago() === $this) {
                $chequesOrdenPago->setOrdenPago(null);
            }
        }

        return $this;
    }

}
