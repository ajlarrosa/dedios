<?php

namespace App\Entity;

use App\Repository\UserRepository;
use App\Entity\Common\Common;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Tienda\Pedido;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\Tienda\DireccionEnvio;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
 *     fields={"username"},
 *     errorPath="username",    
 *     message="The username is already used. Select another one."
 * )
 * @UniqueEntity(
 *     fields={"mail"},
 *     errorPath="mail",    
 *     message="The mail is already used. Select another one."
 * )
 */
class User extends Common implements UserInterface {

    CONST COD_ADMIN = 'ADMINISTRADOR';
    CONST COD_USER = 'USUARIO';
    CONST COD_USER_MAYORISTA = 'USUARIO MAYORISTA';
    CONST COD_USER_MINORISTA = 'USUARIO MINORISTA';
    CONST ROLE_USER_MAYORISTA = 'ROLE_USER_MAYORISTA';
    CONST ROLE_USER_MINORISTA = 'ROLE_USER_MINORISTA';
    CONST COD_OPERADOR = 'OPERADOR';
    CONST COD_JEWELRY = 'JOYERIA';
    CONST COD_RESELLER = 'REVENDEDOR';
    CONST JEWELRY = 'Jewelry';
    CONST RESELLER = 'Reseller';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CondicionFiscal", inversedBy="users")
     * @ORM\JoinColumn(name="condicionfiscal_id", referencedColumnName="id", nullable=true)     
     */
    protected $condicionFiscal;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="users")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=true)     
     */
    protected $cliente;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)  
     */
    protected $apellido;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    protected $dni;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    protected $marcaComercial;

    /**
     * @ORM\Column(type="string", length=10,nullable=true)
     */
    protected $joyeriaRevendedora;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $web;

    /**
     * @ORM\Column(type="string", length=100,nullable=false)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $signature;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expires;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $aprobado = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Pedido", mappedBy="user" )
     */
    protected $pedidos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\DireccionEnvio", mappedBy="user")
     */
    protected $direccionesEnvio;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->pedidos = new ArrayCollection();
        $this->direccionesEnvio = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getUsername();
    }

    public function getDireccion($tipoDireccion) {
        $direccionesEnvio = $this->getDireccionesEnvio();
        foreach ($direccionesEnvio as $direccionEnvio) {
            if ($direccionEnvio->getTipoDireccion() == $tipoDireccion) {
                return $direccionEnvio;
            }
        }
        return false;
    }

    /*
     * Get Tipos Direccion
     */

    public function getTiposDireccion() {
        $tiposDireccion = [];
        foreach ($this->getDireccionesEnvio() as $direccionEnvio) {
            if ($direccionEnvio->getEnabled()) {
                $tiposDireccion[] = $direccionEnvio->getTipoDireccion();
            }
        }
        return $tiposDireccion;
    }

    public function addRoleMinorista() {
        $this->setRoles(['ROLE_USER_MINORISTA']);       
        return $this;
    }

    public function getRolesHtml() {
        $html = '';
        foreach ($this->getRoles() as $role) {
            switch ($role) {
                case 'ROLE_ADMIN':
                    $html .= '<li>' . self::COD_ADMIN . '</li>';
                    break;
                case 'ROLE_OPERADOR':
                    $html .= '<li>' . self::COD_OPERADOR . '</li>';
                    break;
                case 'ROLE_USER_MAYORISTA':
                    $html .= '<li>' . self::COD_USER_MAYORISTA . '</li>';
                    break;
                case 'ROLE_USER_MINORISTA':
                    $html .= '<li>' . self::COD_USER_MINORISTA . '</li>';
                    break;
            }
        }

        return $html;
    }

    public function getJoyeriaRevendedorString() {
        if ($this->joyeriaRevendedora == self::COD_JEWELRY) {
            return self::JEWELRY;
        }
        if ($this->joyeriaRevendedora == self::COD_RESELLER) {
            return self::COD_RESELLER;
        }
        return '';
    }

    public function hasRole($rol): string {
        return (in_array($rol, $this->getRoles())) ? true : false;        
    }

    public function getId(): ?int {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return (string) $this->username;
    }

    public function setUsername(string $username): self {
        $this->username = $username;

        return $this;
    }

    public function getRoles(): array {
        $roles = explode(",", $this->roles);
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(array $roles): self {
        $this->roles = implode(",", $roles);
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return (string) $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt() {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getMail(): ?string {
        return $this->mail;
    }

    public function setMail(string $mail): self {
        $this->mail = $mail;

        return $this;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setUser($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getUser() === $this) {
                $pedido->setUser(null);
            }
        }

        return $this;
    }

    public function isVerified(): bool {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getTelefono(): ?string {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self {
        $this->telefono = $telefono;

        return $this;
    }

    public function getIsVerified(): ?bool {
        return $this->isVerified;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function setDni(?string $dni): self {
        $this->dni = $dni;

        return $this;
    }

    public function getCuit(): ?string {
        return $this->cuit;
    }

    public function setCuit(?string $cuit): self {
        $this->cuit = $cuit;

        return $this;
    }

    public function getMarcaComercial(): ?string {
        return $this->marcaComercial;
    }

    public function setMarcaComercial(?string $marcaComercial): self {
        $this->marcaComercial = $marcaComercial;

        return $this;
    }

    public function getJoyeriaRevendedora(): ?string {
        return $this->joyeriaRevendedora;
    }

    public function setJoyeriaRevendedora(?string $joyeriaRevendedora): self {
        $this->joyeriaRevendedora = $joyeriaRevendedora;

        return $this;
    }

    public function getWeb(): ?string {
        return $this->web;
    }

    public function setWeb(?string $web): self {
        $this->web = $web;

        return $this;
    }

    public function getCondicionFiscal(): ?CondicionFiscal {
        return $this->condicionFiscal;
    }

    public function setCondicionFiscal(?CondicionFiscal $condicionFiscal): self {
        $this->condicionFiscal = $condicionFiscal;

        return $this;
    }

    public function getSignature(): ?string {
        return $this->signature;
    }

    public function setSignature(?string $signature): self {
        $this->signature = $signature;

        return $this;
    }

    public function getToken(): ?string {
        return $this->token;
    }

    public function setToken(?string $token): self {
        $this->token = $token;

        return $this;
    }

    public function getExpires(): ?\DateTimeInterface {
        return $this->expires;
    }

    public function setExpires(?\DateTimeInterface $expires): self {
        $this->expires = $expires;

        return $this;
    }

    public function getCliente(): ?ClienteProveedor {
        return $this->cliente;
    }

    public function setCliente(?ClienteProveedor $cliente): self {
        $this->cliente = $cliente;

        return $this;
    }

    public function getAprobado(): ?bool {
        return $this->aprobado;
    }

    public function setAprobado(bool $aprobado): self {
        $this->aprobado = $aprobado;

        return $this;
    }

    /**
     * @return Collection|DireccionEnvio[]
     */
    public function getDireccionesEnvio(): Collection {
        return $this->direccionesEnvio;
    }

    public function addDireccionesEnvio(DireccionEnvio $direccionesEnvio): self {
        if (!$this->direccionesEnvio->contains($direccionesEnvio)) {
            $this->direccionesEnvio[] = $direccionesEnvio;
            $direccionesEnvio->setUser($this);
        }

        return $this;
    }

    public function removeDireccionesEnvio(DireccionEnvio $direccionesEnvio): self {
        if ($this->direccionesEnvio->contains($direccionesEnvio)) {
            $this->direccionesEnvio->removeElement($direccionesEnvio);
            // set the owning side to null (unless already changed)
            if ($direccionesEnvio->getUser() === $this) {
                $direccionesEnvio->setUser(null);
            }
        }

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

}
