<?php

namespace App\Entity\Common;

class Email {
   
    /*
     * Atributos
     */   
    protected $title;
    protected $parameters;    
    protected $template;    
    protected $sendTo;

    /*
     * Constructor
     */
    function __construct() {
        $this->parameters = [];
    }

    /*
     * Métodos
     */
    function getTitle() {
        return $this->title;
    }

    function getParameters() {
        return $this->parameters;
    }

    function getTemplate() {
        return $this->template;
    }

    function getSendTo() {
        return $this->sendTo;
    }

    function setTitle($title): void {
        $this->title = $title;
    }

    function setParameters($parameters): void {
        $this->parameters = $parameters;
    }

    function setTemplate($template): void {
        $this->template = $template;
    }

    function setSendTo($sendTo): void {
        $this->sendTo = $sendTo;
    }


}
