<?php

namespace App\Entity\Common;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
abstract class Common {
    /*
     * Constantes
     */

    const ACTIVO = 'ENABLED';
    const INACTIVO = 'DISABLED';

    /*
     * Atributos
     */

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * @ORM\Column(type="boolean", nullable=false , options={"default" : 1})
     */
    protected $enabled = true;

    /*
     * Métodos
     */

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {
        if (!$this->getCreated()) {
            $this->created = new \DateTime();
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate() {
        $this->updated = new \DateTime();
    }

    public function getEnabledString() {
        if ($this->enabled) {
            return self::ACTIVO;
        } else {
            return self::INACTIVO;
        }
    }

    public function getEnabled(): ?bool {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getUpdated() {
        return $this->updated;
    }

    public function getSlug() {
        if (isset($this->descripcion)) {
            $text = $this->descripcion;
            $text = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove;', $text);
            $text = str_replace("/", "-", $text);

            $text = str_replace(" ", "_", $text);
            $text = str_replace('"', "", $text);
            $text = str_replace("(", "", $text);
            $text = str_replace(")", "", $text);
            $text = str_replace("&", "", $text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        }
        return false;
    }

}
