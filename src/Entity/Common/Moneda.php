<?php

namespace App\Entity\Common;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
abstract class Moneda extends Common {
    /*
     * Constantes
     */

    const PESOS = 'Pesos';
    const DOLARES = 'Dolares';
    const SIMBOLO_PESOS = '$';
    const SIMBOLO_DOLARES = 'u$s';
    const ISO_PESOS = 'ARS';
    const ISO_DOLARES = 'USD';
    const COD_PESOS = 1;
    const COD_DOLARES = 2;

    /*
     * Atributos
     */

    /**
     * @ORM\Column(type="integer", options={"default" : 1})
     */
    protected $moneda = 1;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     *     
     */
    protected $oro = 0;

    /**
     * @ORM\Column(type="float", nullable=true, options={"default" : 0})
     */
    protected $plata = 0;

    public function getMonedaString() {
        if ($this->moneda == self::COD_PESOS) {
            return self::PESOS;
        } else {
            return self::DOLARES;
        }
    }

    public function getMonedaSimbolo() {
        if ($this->moneda == self::COD_PESOS) {
            return self::SIMBOLO_PESOS;
        } else {
            return self::SIMBOLO_DOLARES;
        }
    }

    public function getMoneda(): ?string {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self {
        $this->moneda = $moneda;

        return $this;
    }

    public function getOro(): ?float {
        return $this->oro;
    }

    public function setOro(?float $oro): self {
        $this->oro = $oro;

        return $this;
    }

    public function getPlata(): ?float {
        return $this->plata;
    }

    public function setPlata(?float $plata): self {
        $this->plata = $plata;

        return $this;
    }
    
    abstract function getTotal();

}
