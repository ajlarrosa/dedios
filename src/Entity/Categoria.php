<?php

namespace App\Entity;

use App\Entity\Tienda\CategoriaGrupo;
use App\Entity\Tienda\Configuracion;
use App\Entity\Tienda\Cupon;
use App\Entity\Tienda\Menu;
use App\Entity\Tienda\Promotion;
use App\Repository\CategoriaRepository;
use App\Entity\Common\Common;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\CategoriaRepository::class)
 * @ORM\Table(name="categoria")
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The category already exists."
 * )
 * @UniqueEntity(
 *     fields={"sigla"},
 *     errorPath="sigla",
 *     message="The initials already exists."
 * )

 */
class Categoria extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=100, unique=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "El título de web no puede superar los 100 caracteres"
     * )    
     */
    protected $titleWeb;

    /**
     * @ORM\Column(type="string",length=3,nullable=false,unique=true)     
     * @Assert\Length(
     *     min = 1, 
     *     max = 3,     
     *      maxMessage = "La sigla debe contener 1 caracteres",
     *      maxMessage = "La sigla debe contener 3 caracteres"
     * )    
     */
    protected $sigla;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen;

    /**
     * @ORM\OneToMany(targetEntity="Categoriasubcategoria", mappedBy="categoria")
     */
    protected $categoriasubcategorias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\CategoriaGrupo", mappedBy="categoria")
     */
    protected $categoriasGrupo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="categoria1")
     */
    protected $menu1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="categoria2")
     */
    protected $menu2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="categoriaMenu")
     */
    protected $menuCategoria;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="categoria1")
     */
    protected $configuracion1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="categoria2")
     */
    protected $configuracion2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="categoria3")
     */
    protected $configuracion3;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Promotion", mappedBy="categoria")
     */
    protected $promotions;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Cupon", mappedBy="categoria")
     */
    protected $cupones;
    
    protected $rootDir = 'uploads/categoria';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->categoriasubcategorias = new ArrayCollection();
        $this->categoriasGrupo = new ArrayCollection();
        $this->menu1 = new ArrayCollection();
        $this->menu2 = new ArrayCollection();
        $this->menuCategoria = new ArrayCollection();
        $this->configuracion1 = new ArrayCollection();
        $this->configuracion2 = new ArrayCollection();
        $this->configuracion3 = new ArrayCollection();
        $this->promotions = new ArrayCollection();
         $this->cupones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getType() {
        return 'categoria';
    }
    public function getTitleMenu() {
        if (!is_null($this->titleWeb) && trim($this->titleWeb != '')) {
            return $this->titleWeb;
        } else {
            return $this->getDescripcion();
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|Categoriasubcategoria[]
     */
    public function getCategoriasubcategorias(): Collection {
        return $this->categoriasubcategorias;
    }

    public function addCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if (!$this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias[] = $categoriasubcategoria;
            $categoriasubcategoria->setCategoria($this);
        }

        return $this;
    }

    public function removeCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if ($this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias->removeElement($categoriasubcategoria);
            // set the owning side to null (unless already changed)
            if ($categoriasubcategoria->getCategoria() === $this) {
                $categoriasubcategoria->setCategoria(null);
            }
        }

        return $this;
    }

    public function getSigla(): ?string {
        return $this->sigla;
    }

    public function setSigla(string $sigla): self {
        $this->sigla = $sigla;

        return $this;
    }

    /**
     * @return Collection|CategoriaGrupo[]
     */
    public function getCategoriasGrupo(): Collection {
        return $this->categoriasGrupo;
    }

    public function addCategoriasGrupo(CategoriaGrupo $categoriasGrupo): self {
        if (!$this->categoriasGrupo->contains($categoriasGrupo)) {
            $this->categoriasGrupo[] = $categoriasGrupo;
            $categoriasGrupo->setCategoria($this);
        }

        return $this;
    }

    public function removeCategoriasGrupo(CategoriaGrupo $categoriasGrupo): self {
        if ($this->categoriasGrupo->contains($categoriasGrupo)) {
            $this->categoriasGrupo->removeElement($categoriasGrupo);
            // set the owning side to null (unless already changed)
            if ($categoriasGrupo->getCategoria() === $this) {
                $categoriasGrupo->setCategoria(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu1(): Collection {
        return $this->menu1;
    }

    public function addMenu1(Menu $menu1): self {
        if (!$this->menu1->contains($menu1)) {
            $this->menu1[] = $menu1;
            $menu1->setCategoria1($this);
        }

        return $this;
    }

    public function removeMenu1(Menu $menu1): self {
        if ($this->menu1->contains($menu1)) {
            $this->menu1->removeElement($menu1);
            // set the owning side to null (unless already changed)
            if ($menu1->getCategoria1() === $this) {
                $menu1->setCategoria1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu2(): Collection {
        return $this->menu2;
    }

    public function addMenu2(Menu $menu2): self {
        if (!$this->menu2->contains($menu2)) {
            $this->menu2[] = $menu2;
            $menu2->setCategoria2($this);
        }

        return $this;
    }

    public function removeMenu2(Menu $menu2): self {
        if ($this->menu2->contains($menu2)) {
            $this->menu2->removeElement($menu2);
            // set the owning side to null (unless already changed)
            if ($menu2->getCategoria2() === $this) {
                $menu2->setCategoria2(null);
            }
        }

        return $this;
    }

    public function getImagen() {
        return $this->imagen != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagen : '';
    }

    public function setImagen(string $imagen): self {
        $this->imagen = $imagen;

        return $this;
    }

    public function getTitleWeb(): ?string {
        return $this->titleWeb;
    }

    public function setTitleWeb(?string $titleWeb): self {
        $this->titleWeb = $titleWeb;

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenuCategoria(): Collection {
        return $this->menuCategoria;
    }

    public function addMenuCategorium(Menu $menuCategorium): self {
        if (!$this->menuCategoria->contains($menuCategorium)) {
            $this->menuCategoria[] = $menuCategorium;
            $menuCategorium->setCategoriaMenu($this);
        }

        return $this;
    }

    public function removeMenuCategorium(Menu $menuCategorium): self {
        if ($this->menuCategoria->contains($menuCategorium)) {
            $this->menuCategoria->removeElement($menuCategorium);
            // set the owning side to null (unless already changed)
            if ($menuCategorium->getCategoriaMenu() === $this) {
                $menuCategorium->setCategoriaMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion1(): Collection {
        return $this->configuracion1;
    }

    public function addConfiguracion1(Configuracion $configuracion1): self {
        if (!$this->configuracion1->contains($configuracion1)) {
            $this->configuracion1[] = $configuracion1;
            $configuracion1->setCategoria1($this);
        }

        return $this;
    }

    public function removeConfiguracion1(Configuracion $configuracion1): self {
        if ($this->configuracion1->contains($configuracion1)) {
            $this->configuracion1->removeElement($configuracion1);
            // set the owning side to null (unless already changed)
            if ($configuracion1->getCategoria1() === $this) {
                $configuracion1->setCategoria1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion2(): Collection {
        return $this->configuracion2;
    }

    public function addConfiguracion2(Configuracion $configuracion2): self {
        if (!$this->configuracion2->contains($configuracion2)) {
            $this->configuracion2[] = $configuracion2;
            $configuracion2->setCategoria2($this);
        }

        return $this;
    }

    public function removeConfiguracion2(Configuracion $configuracion2): self {
        if ($this->configuracion2->contains($configuracion2)) {
            $this->configuracion2->removeElement($configuracion2);
            // set the owning side to null (unless already changed)
            if ($configuracion2->getCategoria2() === $this) {
                $configuracion2->setCategoria2(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion3(): Collection
    {
        return $this->configuracion3;
    }

    public function addConfiguracion3(Configuracion $configuracion3): self
    {
        if (!$this->configuracion3->contains($configuracion3)) {
            $this->configuracion3[] = $configuracion3;
            $configuracion3->setCategoria3($this);
        }

        return $this;
    }

    public function removeConfiguracion3(Configuracion $configuracion3): self
    {
        if ($this->configuracion3->removeElement($configuracion3)) {
            // set the owning side to null (unless already changed)
            if ($configuracion3->getCategoria3() === $this) {
                $configuracion3->setCategoria3(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Promotion[]
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    public function addPromotion(Promotion $promotion): self
    {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setCategoria($this);
        }

        return $this;
    }

    public function removePromotion(Promotion $promotion): self
    {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getCategoria() === $this) {
                $promotion->setCategoria(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cupon[]
     */
    public function getCupones(): Collection
    {
        return $this->cupones;
    }

    public function addCupone(Cupon $cupone): self
    {
        if (!$this->cupones->contains($cupone)) {
            $this->cupones[] = $cupone;
            $cupone->setCategoria($this);
        }

        return $this;
    }

    public function removeCupone(Cupon $cupone): self
    {
        if ($this->cupones->removeElement($cupone)) {
            // set the owning side to null (unless already changed)
            if ($cupone->getCategoria() === $this) {
                $cupone->setCategoria(null);
            }
        }

        return $this;
    }
    
}
