<?php

namespace App\Entity;

use App\Repository\PrecioMedidaProductoRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\PrecioMedidaProductoRepository::class)
 * @ORM\Table(name="precio_medida_producto")
 */
class PrecioMedidaProducto extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AtributosProducto", inversedBy="preciosMedidaProducto")
     * @ORM\JoinColumn(name="atributoproducto_id", referencedColumnName="id", nullable=false)
     */
    protected $atributoProducto;

    /**
     * @ORM\ManyToOne(targetEntity="Precio", inversedBy="preciosMedidaProducto")
     * @ORM\JoinColumn(name="precio_id", referencedColumnName="id", nullable=false)
     */
    protected $precio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAtributoProducto(): ?AtributosProducto
    {
        return $this->atributoProducto;
    }

    public function setAtributoProducto(?AtributosProducto $atributoProducto): self
    {
        $this->atributoProducto = $atributoProducto;

        return $this;
    }

    public function getPrecio(): ?Precio
    {
        return $this->precio;
    }

    public function setPrecio(?Precio $precio): self
    {
        $this->precio = $precio;

        return $this;
    }


}
