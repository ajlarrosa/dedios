<?php

namespace App\Entity;

use App\Repository\CondicionFiscalRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\CondicionFiscalRepository::class)
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The promo already exists."
 * )
 */
class CondicionFiscal extends Common{      
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Description cannot exceed {{ limit }} characters"
     * )   
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="condicionFiscal")
     */
    protected $users;
    
    /**
     * @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="condicionFiscal")
     */
    protected $clientes;
    
 
    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->users = new ArrayCollection();
        $this->clientes = new ArrayCollection();
    }

    /* *********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCondicionFsical($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getCondicionFsical() === $this) {
                $user->setCondicionFsical(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClienteProveedor[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(ClienteProveedor $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setCondicionFsical($this);
        }

        return $this;
    }

    public function removeCliente(ClienteProveedor $cliente): self
    {
        if ($this->clientes->contains($cliente)) {
            $this->clientes->removeElement($cliente);
            // set the owning side to null (unless already changed)
            if ($cliente->getCondicionFsical() === $this) {
                $cliente->setCondicionFsical(null);
            }
        }

        return $this;
    }
  
    
}
