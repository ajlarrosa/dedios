<?php

namespace App\Entity;

use App\ConsignacionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ConsignacionRepository::class)
 * @ORM\Table(name="consignacion")
 */
class Consignacion {

     /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaVencimiento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\OneToMany(targetEntity="ProductoConsignacion", mappedBy="consignacion", cascade={"persist"})   
     */
    protected $productosConsignacion;

       /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="consignaciones")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaprecio_id", referencedColumnName="id")
     */
    protected $listaPrecio;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;


    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosConsignacion = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFechaVencimiento(): ?\DateTimeInterface
    {
        return $this->fechaVencimiento;
    }

    public function setFechaVencimiento(?\DateTimeInterface $fechaVencimiento): self
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(?float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getBonificacion(): ?float
    {
        return $this->bonificacion;
    }

    public function setBonificacion(?float $bonificacion): self
    {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(?float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|ProductoConsignacion[]
     */
    public function getProductosConsignacion(): Collection
    {
        return $this->productosConsignacion;
    }

    public function addProductosConsignacion(ProductoConsignacion $productosConsignacion): self
    {
        if (!$this->productosConsignacion->contains($productosConsignacion)) {
            $this->productosConsignacion[] = $productosConsignacion;
            $productosConsignacion->setConsignacion($this);
        }

        return $this;
    }

    public function removeProductosConsignacion(ProductoConsignacion $productosConsignacion): self
    {
        if ($this->productosConsignacion->contains($productosConsignacion)) {
            $this->productosConsignacion->removeElement($productosConsignacion);
            // set the owning side to null (unless already changed)
            if ($productosConsignacion->getConsignacion() === $this) {
                $productosConsignacion->setConsignacion(null);
            }
        }

        return $this;
    }

   

    public function getClienteProveedor(): ?ClienteProveedor
    {
        return $this->clienteProveedor;
    }

    public function setClienteProveedor(?ClienteProveedor $clienteProveedor): self
    {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    public function getListaPrecio(): ?ListaPrecio
    {
        return $this->listaPrecio;
    }

    public function setListaPrecio(?ListaPrecio $listaPrecio): self
    {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

   
}
