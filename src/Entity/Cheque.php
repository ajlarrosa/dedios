<?php

namespace App\Entity;

use App\Repository\ChequeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ChequeRepository::class)
 * @ORM\Table(name="cheque")
 */
class Cheque {

    const ACTIVO = 'Active';
    const ELIMINADO = 'Eliminated';
    const RECHAZADO = 'Rejected';
    const USADO = 'Used';
    const COD_ACTIVO = 'A';
    const COD_ELIMINADO = 'E';
    const COD_RECHAZADO = 'R';
    const COD_USADO = 'U';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaEmision;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaCobro;

    /**
     * @ORM\Column(type="float")
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $nroCheque;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firmantes;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $conciliacion;

    /**
     * @ORM\ManyToOne(targetEntity="ReciboCliente", inversedBy="cheques",cascade={"persist"})
     * @ORM\JoinColumn(name="recibocliente_id", referencedColumnName="id", nullable=true)
     */
    protected $reciboCliente;

    /**
     * @ORM\ManyToOne(targetEntity="OrdenPago", inversedBy="chequesOrdenPago",cascade={"persist"})
     * @ORM\JoinColumn(name="ordenpago_id", referencedColumnName="id", nullable=true)
     */
    protected $ordenPago;

    /**
     * @ORM\ManyToOne(targetEntity="Banco", inversedBy="cheques")
     * @ORM\JoinColumn(name="banco_id", referencedColumnName="id", nullable=true)
     */
    protected $banco;

    /**
     * @ORM\ManyToOne(targetEntity="TipoCheque", inversedBy="cheques")
     * @ORM\JoinColumn(name="tipocheque_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoCheque;

    /**
     * @ORM\Column(type="string",length=1, nullable=false)
     */
    protected $estado = self::COD_ACTIVO;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fecha = new \DateTime;
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return "Cheque Nro. ".$this->nroCheque." - Importe ".$this->importe." - Fec. Cobro ".date_format($this->fechaCobro,'d-m-Y')." - Tipo ".$this->tipoCheque;
    }

    public function getEstadoString(): ?string {
        switch ($this->estado) {
            case self::COD_ACTIVO:
                return self::ACTIVO;
            case self::COD_ELIMINADO:
                return self::ELIMINADO;
            case self::COD_RECHAZADO:
                return self::RECHAZADO;
            case self::COD_USADO:
                return self::USADO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFechaEmision(): ?\DateTimeInterface {
        return $this->fechaEmision;
    }

    public function setFechaEmision(?\DateTimeInterface $fechaEmision): self {
        $this->fechaEmision = $fechaEmision;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFechaCobro(): ?\DateTimeInterface {
        return $this->fechaCobro;
    }

    public function setFechaCobro(?\DateTimeInterface $fechaCobro): self {
        $this->fechaCobro = $fechaCobro;

        return $this;
    }

    public function getImporte(): ?float {
        return $this->importe;
    }

    public function setImporte(?float $importe): self {
        $this->importe = $importe;

        return $this;
    }

    public function getNroCheque(): ?string {
        return $this->nroCheque;
    }

    public function setNroCheque(string $nroCheque): self {
        $this->nroCheque = $nroCheque;

        return $this;
    }

    public function getCuit(): ?string {
        return $this->cuit;
    }

    public function setCuit(?string $cuit): self {
        $this->cuit = $cuit;

        return $this;
    }

    public function getFirmantes(): ?string {
        return $this->firmantes;
    }

    public function setFirmantes(?string $firmantes): self {
        $this->firmantes = $firmantes;

        return $this;
    }

    public function getConciliacion(): ?string {
        return $this->conciliacion;
    }

    public function setConciliacion(?string $conciliacion): self {
        $this->conciliacion = $conciliacion;

        return $this;
    }

    public function getEstado(): ?string {
        return $this->estado;
    }

    public function setEstado(string $estado): self {
        $this->estado = $estado;

        return $this;
    }

    public function getReciboCliente(): ?ReciboCliente {
        return $this->reciboCliente;
    }

    public function setReciboCliente(?ReciboCliente $reciboCliente): self {
        $this->reciboCliente = $reciboCliente;

        return $this;
    }

    public function getOrdenPago(): ?OrdenPago {
        return $this->ordenPago;
    }

    public function setOrdenPago(?OrdenPago $ordenPago): self {
        $this->ordenPago = $ordenPago;

        return $this;
    }

    public function getBanco(): ?Banco {
        return $this->banco;
    }

    public function setBanco(?Banco $banco): self {
        $this->banco = $banco;

        return $this;
    }

    public function getTipoCheque(): ?TipoCheque {
        return $this->tipoCheque;
    }

    public function setTipoCheque(?TipoCheque $tipoCheque): self {
        $this->tipoCheque = $tipoCheque;

        return $this;
    }

}
