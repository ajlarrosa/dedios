<?php

namespace App\Entity;

use App\Repository\RecordatorioRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=App\Repository\RecordatorioRepository::class) 
 * @ORM\Table(name="recordatorio")
 */
class Recordatorio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;
    
    /**
     * @ORM\OneToMany(targetEntity="DetalleRecordatorio", mappedBy="recordatorio", cascade={"persist", "remove"} )
     */
    protected $detallesrecordatorio;
    
    /**********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->detallesrecordatorio = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }
    
        /**
     * @return Collection|DetalleRecordatorio[]
     */
    public function getDetallesrecordatorio(): Collection {
        return $this->detallesrecordatorio;
    }

    public function addDetallesrecordatorio(DetalleRecordatorio $detallerecordatorio): self {
        if (!$this->detallesrecordatorio->contains($detallesrecordatorio)) {
            $this->detallesrecordatorio[] = $detallerecordatorio;
            $detallerecordatorio->setRecordatorio($this);
        }

        return $this;
    }

    public function removeDetallesrecordatorio(DetalleRecordatorio $detallerecordatorio): self {
        if ($this->detallesrecordatorio->contains($detallerecordatorio)) {
            $this->detallesrecordatorio->removeElement($detallerecordatorio);
            // set the owning side to null (unless already changed)
            if ($detallerecordatorio->getPedido() === $this) {
                $detallerecordatorio->setPedido(null);
            }
        }

        return $this;
    }
}
