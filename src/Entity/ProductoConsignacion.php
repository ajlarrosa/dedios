<?php

namespace App\Entity;

use App\ProductoConsignacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ProductoConsignacionRepository::class)
 * @ORM\Table(name="productoconsignacion")
 */
class ProductoConsignacion {

      /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="float", length=100)
     */
    protected $precio;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosConsignacion")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")  
     * @ORM\OrderBy({"descripcion" = "ASC"}) 
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Consignacion", inversedBy="productosConsignacion", cascade={"persist"})
     * @ORM\JoinColumn(name="consignacion_id", referencedColumnName="id")
     */
    protected $consignacion;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCantidad(): ?float
    {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getConsignacion(): ?Consignacion
    {
        return $this->consignacion;
    }

    public function setConsignacion(?Consignacion $consignacion): self
    {
        $this->consignacion = $consignacion;

        return $this;
    }

   

}
