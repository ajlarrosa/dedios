<?php

namespace App\Entity;

use App\Repository\ClienteProveedorRepository;
use App\Entity\Common\Common;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ClienteProveedorRepository::class)
 * @ORM\Table(name="clienteproveedor")
 * @UniqueEntity(
 *     fields={"razonSocial"},
 *     errorPath="razonSocial",
 *     message="The customer/provider already exists."
 * )
 */
class ClienteProveedor extends Common {

    const CLIENTE = 'Cliente';
    const PROVEEDOR = 'Proveedor';
    const CLIENTE_COD = '1';
    const PROVEEDOR_COD = '2';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CondicionFiscal", inversedBy="clientes")
     * @ORM\JoinColumn(name="condicionfiscal_id", referencedColumnName="id", nullable=true)     
     */
    protected $condicionFiscal;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $razonSocial;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $telefonoLocal;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $celular;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $clienteProveedor = self::CLIENTE_COD;

    /**
     * @ORM\OneToMany(targetEntity="MovimientoCC", mappedBy="clienteProveedor")
     */
    protected $movimientoscc;

    /**
     * @ORM\OneToMany(targetEntity="Consignacion", mappedBy="clienteProveedor")
     */
    protected $consignaciones;

    /**
     * @ORM\ManyToOne(targetEntity="Localidad", inversedBy="clientesproveedores")
     * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=30,nullable=true)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $saldoInicialPesos = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $saldoInicialDolares = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $saldoInicialAg = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $saldoInicialAu = 0;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="cliente")
     */
    protected $users;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $latitud;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $longitud;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $visible = false;
     
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Pedido", mappedBy="store")
     */
    protected $pedidos;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->movimientoscc = new ArrayCollection();
        $this->consignaciones = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getRazonSocial();
    }

    public function getStore() {
        $localidad = '';
        $direccion = '';
        $telefonoLocal = '';
        if ($this->localidad) {
            $localidad = ' <strong>Localidad:</strong>' . $this->localidad;
        }
        if ($this->direccion) {
            $direccion = ' <strong>Dirección: </strong>' . $this->direccion;
        }
        if ($this->telefonoLocal) {
            $telefonoLocal = ' <strong>Tel.: </strong>' . $this->telefonoLocal;
        }

        return '<strong>' . $this->getRazonSocial() . '</strong> -' . $localidad . $direccion . $telefonoLocal;
    }

    /**
     * Get clienteProveedor
     *
     * @return string 
     */
    public function getClienteProveedorString() {
        if ($this->clienteProveedor == self::CLIENTE_COD) {
            return self::CLIENTE;
        }
        if ($this->clienteProveedor == self::CLIENTE_PROD) {
            return self::PROVEEDOR;
        }
    }

    public function getSaldoInicial($moneda) {
        if (ListaPrecio::COD_PESOS == $moneda) {
            return $this->saldoInicialPesos;
        } else {
            return $this->saldoInicialDolares;
        }
    }

    public function getSaldoInicialMetal($metal) {
        if (Metal::GOLD == $metal) {
            return $this->saldoInicialAu;
        } else {
            return $this->saldoInicialAg;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getRazonSocial(): ?string {
        return $this->razonSocial;
    }

    public function setRazonSocial(string $razonSocial): self {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    public function getDireccion(): ?string {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCelular(): ?string {
        return $this->celular;
    }

    public function setCelular(string $celular): self {
        $this->celular = $celular;

        return $this;
    }

    public function getMail(): ?string {
        return $this->mail;
    }

    public function setMail(string $mail): self {
        $this->mail = $mail;

        return $this;
    }

    public function getClienteProveedor(): ?string {
        return $this->clienteProveedor;
    }

    public function setClienteProveedor(string $clienteProveedor): self {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    public function getCuit(): ?string {
        return $this->cuit;
    }

    public function setCuit(?string $cuit): self {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * @return Collection|MovimientoCC[]
     */
    public function getMovimientoscc(): Collection {
        return $this->movimientoscc;
    }

    public function addMovimientoscc(MovimientoCC $movimientoscc): self {
        if (!$this->movimientoscc->contains($movimientoscc)) {
            $this->movimientoscc[] = $movimientoscc;
            $movimientoscc->setClienteProveedor($this);
        }

        return $this;
    }

    public function removeMovimientoscc(MovimientoCC $movimientoscc): self {
        if ($this->movimientoscc->contains($movimientoscc)) {
            $this->movimientoscc->removeElement($movimientoscc);
            // set the owning side to null (unless already changed)
            if ($movimientoscc->getClienteProveedor() === $this) {
                $movimientoscc->setClienteProveedor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Consignacion[]
     */
    public function getConsignaciones(): Collection {
        return $this->consignaciones;
    }

    public function addConsignacione(Consignacion $consignacione): self {
        if (!$this->consignaciones->contains($consignacione)) {
            $this->consignaciones[] = $consignacione;
            $consignacione->setClienteProveedor($this);
        }

        return $this;
    }

    public function removeConsignacione(Consignacion $consignacione): self {
        if ($this->consignaciones->contains($consignacione)) {
            $this->consignaciones->removeElement($consignacione);
            // set the owning side to null (unless already changed)
            if ($consignacione->getClienteProveedor() === $this) {
                $consignacione->setClienteProveedor(null);
            }
        }

        return $this;
    }

    public function getLocalidad(): ?Localidad {
        return $this->localidad;
    }

    public function setLocalidad(?Localidad $localidad): self {
        $this->localidad = $localidad;

        return $this;
    }

    public function getSaldoInicialPesos(): ?float {
        return $this->saldoInicialPesos;
    }

    public function setSaldoInicialPesos(float $saldoInicialPesos): self {
        $this->saldoInicialPesos = $saldoInicialPesos;

        return $this;
    }

    public function getSaldoInicialDolares(): ?float {
        return $this->saldoInicialDolares;
    }

    public function setSaldoInicialDolares(float $saldoInicialDolares): self {
        $this->saldoInicialDolares = $saldoInicialDolares;

        return $this;
    }

    public function getSaldoInicialAg(): ?float {
        return $this->saldoInicialAg;
    }

    public function setSaldoInicialAg(float $saldoInicialAg): self {
        $this->saldoInicialAg = $saldoInicialAg;

        return $this;
    }

    public function getSaldoInicialAu(): ?float {
        return $this->saldoInicialAu;
    }

    public function setSaldoInicialAu(float $saldoInicialAu): self {
        $this->saldoInicialAu = $saldoInicialAu;

        return $this;
    }

    public function getCondicionFiscal(): ?CondicionFiscal {
        return $this->condicionFiscal;
    }

    public function setCondicionFiscal(?CondicionFiscal $condicionFiscal): self {
        $this->condicionFiscal = $condicionFiscal;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection {
        return $this->users;
    }

    public function addUser(User $user): self {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCliente($this);
        }

        return $this;
    }

    public function removeUser(User $user): self {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getCliente() === $this) {
                $user->setCliente(null);
            }
        }

        return $this;
    }

    public function getLatitud(): ?string {
        return $this->latitud;
    }

    public function setLatitud(?string $latitud): self {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string {
        return $this->longitud;
    }

    public function setLongitud(?string $longitud): self {
        $this->longitud = $longitud;

        return $this;
    }

    public function getVisible(): ?bool {
        return $this->visible;
    }

    public function setVisible(bool $visible): self {
        $this->visible = $visible;

        return $this;
    }

    public function getTelefonoLocal(): ?string {
        return $this->telefonoLocal;
    }

    public function setTelefonoLocal(?string $telefonoLocal): self {
        $this->telefonoLocal = $telefonoLocal;

        return $this;
    }

}
