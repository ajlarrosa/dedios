<?php

namespace App\Entity;

use App\Entity\Tienda\Cupon;
use App\Entity\Tienda\DetallePedido;
use App\Entity\Tienda\Menu;
use App\Entity\Tienda\ProductoGrupo;
use App\Entity\Common\Common;
use App\Entity\Tienda\ProductoLinea;
use App\Entity\Tienda\ProductosLineaProducto;
use App\Entity\Tienda\Promotion;
use App\ProductoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ProductoRepository::class)
 * @ORM\Table(name="producto")
 */
class Producto extends Common {

    const VISIBLE = 'Visible on the web';
    const NO_VISIBLE = 'No visible on the web';
    const NOVEDAD = 'It is a novelty';
    const NO_NOVEDAD = 'It is not a novelty';
    const DESTACADO = 'It is a standout';
    const NO_DESTACADO = 'It is not a standout';
    const PESOS = '$';
    const DOLAR = 'u$s';
    const COD_PESOS = '1';
    const COD_DOLAR = '2';
    const UNIDAD = 'Unidades';
    const GRAMO_PLATA = 'Gramos de plata';
    const GRAMO_ORO = 'Gramos de oro';
    const COD_UNIDAD = 'U';
    const COD_GRAMO_PLATA = 'P';
    const COD_GRAMO_ORO = 'O';
    const GRABABLE = 'It is recordable';
    const NO_GRABABLE = 'It is not recordable';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)    
     */
    protected $descripcionampliada;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *       message = "Debe agregar una descripción."
     *              )
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=25,nullable=true)
     */
    protected $descripcionMenu;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *       message = "Debe indicar el stock."
     *              )
     */
    protected $stock;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $plata = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $oro = 0;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $moneda;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @Assert\NotBlank(
     *       message = "Debe agregar un costo."
     *              )
     */
    protected $costo;

    /**
     * @ORM\Column(type="string", length=50,  unique=true)
     */
    protected $codigo;

    /**
     * @ORM\ManyToOne(targetEntity="Categoriasubcategoria", inversedBy="productos")
     * @ORM\JoinColumn(name="categoriasubcategoria_id", referencedColumnName="id")     
     */
    protected $categoriasubcategoria;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="producto", cascade={"persist", "remove"})
     */
    protected $precios;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="producto")
     */
    protected $productosFactura;

    /**
     * @ORM\OneToMany(targetEntity="ImagenProducto", mappedBy="producto", cascade={"persist", "remove"})
     * @ORM\OrderBy({"pos" = "ASC"})
     */
    protected $imagenesproducto;

    /**
     * @ORM\OneToMany(targetEntity="ProductoConsignacion", mappedBy="producto", cascade={"remove"})     
     */
    protected $productosConsignacion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $tipoStock = 'U'; //Valores Posibles U O P

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(message = "El campo no puede estar en blanco.")
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $visible = true;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $novedad = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $destacado = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $orden = 100000;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $esGrabable = false;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $maxGrabado = 0;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)   
     */
    protected $tipoMedida;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\ProductosLineaProducto", mappedBy="producto")
     */
    protected $productosLineaProducto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\ProductoGrupo", mappedBy="producto")
     */
    protected $productosGrupo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="producto1")
     */
    protected $menu1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="producto2")
     */
    protected $menu2;

    /**
     * @ORM\OneToMany(targetEntity="AtributosProducto", mappedBy="producto")
     */
    protected $atributosProducto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\DetallePedido", mappedBy="producto")
     */
    protected $detallespedido;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Promotion", mappedBy="producto")
     */
    protected $promotions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Cupon", mappedBy="producto")
     */
    protected $cupones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\ProductoLinea", mappedBy="producto")
     */
    protected $productosLinea;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->precios = new ArrayCollection();
        $this->productosFactura = new ArrayCollection();
        $this->imagenesproducto = new ArrayCollection();
        $this->productosConsignacion = new ArrayCollection();
        $this->productosGrupo = new ArrayCollection();
        $this->productosLineaProducto = new ArrayCollection();
        $this->productosLinea = new ArrayCollection();
        $this->promotions = new ArrayCollection();
        $this->menu1 = new ArrayCollection();
        $this->menu2 = new ArrayCollection();
        $this->atributosProducto = new ArrayCollection();
        $this->detallespedido = new ArrayCollection();
        $this->cupones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getCodigo() . ' - ' . $this->getDescripcion();
    }

    public function getTitleMenu() {
        return (is_null($this->descripcionMenu)) ? $this->codigo : $this->descripcionMenu;
    }

    public function hasImages() {
        foreach ($this->imagenesproducto as $imagenProducto) {
            if ($imagenProducto->getEnabled() == TRUE) {
                return true;
            }
        }
        return false;
    }

    /*
     * Has images by product
     */

    public function hasPrice($listaPrecioId) {
        foreach ($this->precios as $precio) {
            if ($precio->getActivo() == TRUE && $precio->getListaPrecio()->getId() == $listaPrecioId) {
                return true;
            }
        }
        return false;
    }

    public function getPrecio($listaPrecioId) {
        foreach ($this->precios as $precio) {
            if ($precio->getActivo() == TRUE && $precio->getListaPrecio()->getId() == $listaPrecioId) {
                return $precio->getValor();
            }
        }
        return '';
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaSimbolo() {
        if ($this->moneda == self::COD_PESOS) {
            return self::PESOS;
        }

        if ($this->moneda == self::COD_DOLAR) {
            return self::DOLAR;
        }
    }

    public function getTipoStockString() {
        switch ($this->getTipoStock()) {
            case self::COD_UNIDAD:
                return self::UNIDAD;
            case self::COD_GRAMO_PLATA:
                return self::GRAMO_PLATA;
            case self::COD_GRAMO_ORO:
                return self::GRAMO_ORO;
            default:
                return '';
        }
    }

    public function getVisibleString() {
        if ($this->visible) {
            return self::VISIBLE;
        }
        return self::NO_VISIBLE;
    }

    public function getDestacadoString() {
        if ($this->destacado) {
            return self::DESTACADO;
        }
        return self::NO_DESTACADO;
    }

    public function getNovedadString() {
        if ($this->novedad) {
            return self::NOVEDAD;
        }
        return self::NO_NOVEDAD;
    }

    public function getEsGrabableString() {
        if ($this->esGrabable) {
            return self::GRABABLE;
        }
        return self::NO_GRABABLE;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcionampliada(): ?string {
        return $this->descripcionampliada;
    }

    public function setDescripcionampliada(?string $descripcionampliada): self {
        $this->descripcionampliada = $descripcionampliada;

        return $this;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getStock(): ?float {
        return $this->stock;
    }

    public function setStock(float $stock): self {
        $this->stock = $stock;

        return $this;
    }

    public function getPlata(): ?float {
        return $this->plata;
    }

    public function setPlata(?float $plata): self {
        $this->plata = $plata;

        return $this;
    }

    public function getOro(): ?float {
        return $this->oro;
    }

    public function setOro(?float $oro): self {
        $this->oro = $oro;

        return $this;
    }

    public function getMoneda(): ?string {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self {
        $this->moneda = $moneda;

        return $this;
    }

    public function getCosto(): ?float {
        return $this->costo;
    }

    public function setCosto(float $costo): self {
        $this->costo = $costo;

        return $this;
    }

    public function getCodigo(): ?string {
        return $this->codigo;
    }

    public function setCodigo(?string $codigo): self {
        $this->codigo = $codigo;

        return $this;
    }

    public function getTipoStock(): ?string {
        return $this->tipoStock;
    }

    public function setTipoStock(string $tipoStock): self {
        $this->tipoStock = $tipoStock;

        return $this;
    }

    public function getVisible(): ?bool {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self {
        $this->visible = $visible;

        return $this;
    }

    public function getNovedad(): ?bool {
        return $this->novedad;
    }

    public function setNovedad(?bool $novedad): self {
        $this->novedad = $novedad;

        return $this;
    }

    public function getDestacado(): ?bool {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self {
        $this->destacado = $destacado;

        return $this;
    }

    public function getDescuento(): ?int {
        return $this->descuento;
    }

    public function setDescuento(?int $descuento): self {
        $this->descuento = $descuento;

        return $this;
    }

    public function getOrden(): ?int {
        return $this->orden;
    }

    public function setOrden(?int $orden): self {
        $this->orden = $orden;

        return $this;
    }

    public function getCategoriasubcategoria(): ?Categoriasubcategoria {
        return $this->categoriasubcategoria;
    }

    public function setCategoriasubcategoria(?Categoriasubcategoria $categoriasubcategoria): self {
        $this->categoriasubcategoria = $categoriasubcategoria;

        return $this;
    }

    /**
     * @return Collection|Precio[]
     */
    public function getPrecios(): Collection {
        return $this->precios;
    }

    public function addPrecio(Precio $precio): self {
        if (!$this->precios->contains($precio)) {
            $this->precios[] = $precio;
            $precio->setProducto($this);
        }

        return $this;
    }

    public function removePrecio(Precio $precio): self {
        if ($this->precios->contains($precio)) {
            $this->precios->removeElement($precio);
            // set the owning side to null (unless already changed)
            if ($precio->getProducto() === $this) {
                $precio->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoFactura[]
     */
    public function getProductosFactura(): Collection {
        return $this->productosFactura;
    }

    public function addProductosFactura(ProductoFactura $productosFactura): self {
        if (!$this->productosFactura->contains($productosFactura)) {
            $this->productosFactura[] = $productosFactura;
            $productosFactura->setProducto($this);
        }

        return $this;
    }

    public function removeProductosFactura(ProductoFactura $productosFactura): self {
        if ($this->productosFactura->contains($productosFactura)) {
            $this->productosFactura->removeElement($productosFactura);
            // set the owning side to null (unless already changed)
            if ($productosFactura->getProducto() === $this) {
                $productosFactura->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ImagenProducto[]
     */
    public function getImagenesproducto(): Collection {
        return $this->imagenesproducto;
    }

    public function addImagenesproducto(ImagenProducto $imagenesproducto): self {
        if (!$this->imagenesproducto->contains($imagenesproducto)) {
            $this->imagenesproducto[] = $imagenesproducto;
            $imagenesproducto->setProducto($this);
        }

        return $this;
    }

    public function removeImagenesproducto(ImagenProducto $imagenesproducto): self {
        if ($this->imagenesproducto->contains($imagenesproducto)) {
            $this->imagenesproducto->removeElement($imagenesproducto);
            // set the owning side to null (unless already changed)
            if ($imagenesproducto->getProducto() === $this) {
                $imagenesproducto->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoConsignacion[]
     */
    public function getProductosConsignacion(): Collection {
        return $this->productosConsignacion;
    }

    public function addProductosConsignacion(ProductoConsignacion $productosConsignacion): self {
        if (!$this->productosConsignacion->contains($productosConsignacion)) {
            $this->productosConsignacion[] = $productosConsignacion;
            $productosConsignacion->setProducto($this);
        }

        return $this;
    }

    public function removeProductosConsignacion(ProductoConsignacion $productosConsignacion): self {
        if ($this->productosConsignacion->contains($productosConsignacion)) {
            $this->productosConsignacion->removeElement($productosConsignacion);
            // set the owning side to null (unless already changed)
            if ($productosConsignacion->getProducto() === $this) {
                $productosConsignacion->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoGrupo[]
     */
    public function getProductosGrupo(): Collection {
        return $this->productosGrupo;
    }

    public function addProductosGrupo(ProductoGrupo $productosGrupo): self {
        if (!$this->productosGrupo->contains($productosGrupo)) {
            $this->productosGrupo[] = $productosGrupo;
            $productosGrupo->setProducto($this);
        }

        return $this;
    }

    public function removeProductosGrupo(ProductoGrupo $productosGrupo): self {
        if ($this->productosGrupo->contains($productosGrupo)) {
            $this->productosGrupo->removeElement($productosGrupo);
            // set the owning side to null (unless already changed)
            if ($productosGrupo->getProducto() === $this) {
                $productosGrupo->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductosLineaProducto[]
     */
    public function getProductosLineaProducto(): Collection {
        return $this->productosLineaProducto;
    }

    public function addProductosLineaProducto(ProductosLineaProducto $productosLineaProducto): self {
        if (!$this->productosLineaProducto->contains($productosLineaProducto)) {
            $this->productosLineaProducto[] = $productosLineaProducto;
            $productosLineaProducto->setProducto($this);
        }

        return $this;
    }

    public function removeProductosLineaProducto(ProductosLineaProducto $productosLineaProducto): self {
        if ($this->productosLineaProducto->contains($productosLineaProducto)) {
            $this->productosLineaProducto->removeElement($productosLineaProducto);
            // set the owning side to null (unless already changed)
            if ($productosLineaProducto->getProducto() === $this) {
                $productosLineaProducto->setProducto(null);
            }
        }

        return $this;
    }

    public function getMenu1(): ?Menu {
        return $this->menu1;
    }

    public function setMenu1(?Menu $menu1): self {
        $this->menu1 = $menu1;

        // set (or unset) the owning side of the relation if necessary
        $newProducto1 = null === $menu1 ? null : $this;
        if ($menu1->getProducto1() !== $newProducto1) {
            $menu1->setProducto1($newProducto1);
        }

        return $this;
    }

    public function getMenu2(): ?Menu {
        return $this->menu2;
    }

    public function setMenu2(?Menu $menu2): self {
        $this->menu2 = $menu2;

        // set (or unset) the owning side of the relation if necessary
        $newProducto2 = null === $menu2 ? null : $this;
        if ($menu2->getProducto2() !== $newProducto2) {
            $menu2->setProducto2($newProducto2);
        }

        return $this;
    }

    public function addMenu1(Menu $menu1): self {
        if (!$this->menu1->contains($menu1)) {
            $this->menu1[] = $menu1;
            $menu1->setProducto1($this);
        }

        return $this;
    }

    public function removeMenu1(Menu $menu1): self {
        if ($this->menu1->contains($menu1)) {
            $this->menu1->removeElement($menu1);
            // set the owning side to null (unless already changed)
            if ($menu1->getProducto1() === $this) {
                $menu1->setProducto1(null);
            }
        }

        return $this;
    }

    public function addMenu2(Menu $menu2): self {
        if (!$this->menu2->contains($menu2)) {
            $this->menu2[] = $menu2;
            $menu2->setProducto2($this);
        }

        return $this;
    }

    public function removeMenu2(Menu $menu2): self {
        if ($this->menu2->contains($menu2)) {
            $this->menu2->removeElement($menu2);
            // set the owning side to null (unless already changed)
            if ($menu2->getProducto2() === $this) {
                $menu2->setProducto2(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AtributosProducto[]
     */
    public function getAtributosProducto(): Collection {
        return $this->atributosProducto;
    }

    public function addAtributosProducto(AtributosProducto $atributoProducto): self {

        if (!$this->atributosProducto->contains($atributoProducto)) {
            $this->atributosProducto[] = $atributoProducto;
            $atributoProducto->setProducto($this);
        }

        return $this;
    }

    public function removeAtributosProducto(AtributosProducto $atributoProducto): self {
        if ($this->atributosProducto->contains($atributoProducto)) {
            $this->atributosProducto->removeElement($atributoProducto);
            // set the owning side to null (unless already changed)
            if ($atributoProducto->getProducto() === $this) {
                $atributoProducto->setProducto(null);
            }
        }

        return $this;
    }

    public function getMedidas($enabled = null) {
        $medidas = [];

        foreach ($this->atributosProducto as $atributo) {
            if (is_null($atributo->getMedida())) {
                continue;
            }

            if (is_null($enabled)) {
                $medidas[] = $atributo->getMedida();
                continue;
            }

            if ($atributo->getMedida()->getEnabled() == $enabled) {
                $medidas[] = $atributo->getMedida();
            }
        }
        return $medidas;
    }

    public function getColores($enabled = null) {
        $colores = array();
        foreach ($this->atributosProducto as $atributo) {
            if ($atributo->getColor() != null) {
                if (is_null($enabled)) {
                    $colores[] = $atributo->getColor();
                } else {
                    if ($atributo->getColor()->getEnabled() == $enabled) {
                        $colores[] = $atributo->getColor();
                    }
                }
            }
        }
        return $colores;
    }

    public function getLetras($enabled = null) {
        $letras = array();
        foreach ($this->atributosProducto as $atributo) {
            if ($atributo->getLetra() != null) {
                if (is_null($enabled)) {
                    $letras[] = $atributo->getLetra();
                } else {
                    if ($atributo->getLetra()->getEnabled() == $enabled) {
                        $letras[] = $atributo->getLetra();
                    }
                }
            }
        }
        return $letras;
    }

    public function getDescripcionMenu(): ?string {
        return $this->descripcionMenu;
    }

    public function setDescripcionMenu(?string $descripcionMenu): self {
        $this->descripcionMenu = $descripcionMenu;

        return $this;
    }

    /**
     * @return Collection|DetallePedido[]
     */
    public function getDetallespedido(): Collection {
        return $this->detallespedido;
    }

    public function addDetallespedido(DetallePedido $detallespedido): self {
        if (!$this->detallespedido->contains($detallespedido)) {
            $this->detallespedido[] = $detallespedido;
            $detallespedido->setProducto($this);
        }

        return $this;
    }

    public function removeDetallespedido(DetallePedido $detallespedido): self {
        if ($this->detallespedido->contains($detallespedido)) {
            $this->detallespedido->removeElement($detallespedido);
            // set the owning side to null (unless already changed)
            if ($detallespedido->getProducto() === $this) {
                $detallespedido->setProducto(null);
            }
        }

        return $this;
    }

    public function getEsGrabable(): ?bool {
        return $this->esGrabable;
    }

    public function setEsGrabable(?bool $esGrabable): self {
        $this->esGrabable = $esGrabable;

        return $this;
    }

    public function getMaxGrabado(): ?float {
        return $this->maxGrabado;
    }

    public function setMaxGrabado(float $maxGrabado): self {
        $this->maxGrabado = $maxGrabado;

        return $this;
    }

    public function getTipoMedida(): ?string {
        return $this->tipoMedida;
    }

    public function setTipoMedida(?string $tipoMedida): self {
        $this->tipoMedida = $tipoMedida;

        return $this;
    }

    /**
     * @return Collection|Promotion[]
     */
    public function getPromotions(): Collection {
        return $this->promotions;
    }

    public function addPromotion(Promotion $promotion): self {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setProducto($this);
        }

        return $this;
    }

    public function removePromotion(Promotion $promotion): self {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getProducto() === $this) {
                $promotion->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cupon[]
     */
    public function getCupones(): Collection {
        return $this->cupones;
    }

    public function addCupone(Cupon $cupone): self {
        if (!$this->cupones->contains($cupone)) {
            $this->cupones[] = $cupone;
            $cupone->setProducto($this);
        }

        return $this;
    }

    public function removeCupone(Cupon $cupone): self {
        if ($this->cupones->removeElement($cupone)) {
            // set the owning side to null (unless already changed)
            if ($cupone->getProducto() === $this) {
                $cupone->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoLinea[]
     */
    public function getProductosLinea(): Collection {
        return $this->productosLinea;
    }

    public function addProductosLinea(ProductoLinea $productosLinea): self {
        if (!$this->productosLinea->contains($productosLinea)) {
            $this->productosLinea[] = $productosLinea;
            $productosLinea->setProducto($this);
        }

        return $this;
    }

    public function removeProductosLinea(ProductoLinea $productosLinea): self {
        if ($this->productosLinea->removeElement($productosLinea)) {
            // set the owning side to null (unless already changed)
            if ($productosLinea->getProducto() === $this) {
                $productosLinea->setProducto(null);
            }
        }

        return $this;
    }
    
    public function isInLineaProducto(int $lineaProductoId): bool{        
        return in_array($lineaProductoId,$this->getIdsLineaProducto());        
    }
    
    public function getIdsLineaProducto(): array{
        $idsLineaProducto = [];
        
        foreach ($this->productosLineaProducto as $lineaProducto){          
            $idsLineaProducto[] = $lineaProducto->getLineaProducto()->getId();
        }  
    
        return $idsLineaProducto;
    }

}
