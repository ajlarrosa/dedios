<?php

namespace App\Entity;

use App\ListaPrecioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ListaPrecioRepository::class)
 * @ORM\Table(name="listaprecio")
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The list already exists."
 * )
 */
class ListaPrecio {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';
    const PESOS = '$';
    const DOLARES = 'u$s';
    const COD_PESOS = '1';
    const COD_DOLARES = '2';
    const LISTA_PRECIO_MINORISTA_WEB = 1;
    const LISTA_PRECIO_MAYORISTA_WEB = 3;    

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    protected $moneda;

    /**
     * @ORM\Column(type="float")
     */
    protected $precioGrabado=0;
    
    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="listaPrecio")
     */
    protected $precios;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->precios = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion() . ' - ' . $this->getMonedaSimbolo();
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    
    public function getMonedaSimbolo() {
        if ($this->moneda == self::COD_PESOS) {
            return self::PESOS;
        }
        if ($this->moneda == self::COD_DOLARES) {
            return self::DOLARES;
        }
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getMoneda(): ?string {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self {
        $this->moneda = $moneda;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|Precio[]
     */
    public function getPrecios(): Collection {
        return $this->precios;
    }

    public function addPrecio(Precio $precio): self {
        if (!$this->precios->contains($precio)) {
            $this->precios[] = $precio;
            $precio->setListaPrecio($this);
        }

        return $this;
    }

    public function removePrecio(Precio $precio): self {
        if ($this->precios->contains($precio)) {
            $this->precios->removeElement($precio);
            // set the owning side to null (unless already changed)
            if ($precio->getListaPrecio() === $this) {
                $precio->setListaPrecio(null);
            }
        }

        return $this;
    }

    public function getPrecioGrabado(): ?float
    {
        return $this->precioGrabado;
    }

    public function setPrecioGrabado(float $precioGrabado): self
    {
        $this->precioGrabado = $precioGrabado;

        return $this;
    }

}
