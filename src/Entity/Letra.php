<?php

namespace App\Entity;

use App\Entity\Common\Common;
use App\Repository\LetraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\LetraRepository::class) 
 * @ORM\Table(name="letra")
 */
class Letra extends Common {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)   
     * @Assert\Length(
     *      min = 1,
     *      max = 20,
     *      minMessage = "La descripción debe tener al menos de {{ limit }} caracteres",
     *      maxMessage = "La descripción no puede superar los {{ limit }} caracteres"
     * ) 
     */
    protected $descripcion;
   
    /**
     * @ORM\OneToMany(targetEntity="AtributosProducto", mappedBy="letra", cascade={"persist", "remove"})
     */
    protected $atributosProducto;
    
    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
         $this->atributosProducto = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

     public function getLetraString() {
        return 'Letra: ' . $this->getDescripcion();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|AtributosProducto[]
     */
    public function getAtributosProducto(): Collection {
        return $this->atributosProducto;
    }

    public function addAtributosProducto(AtributosProducto $atributoProducto): self {
        if (!$this->atributosProducto->contains($atributoProducto)) {
            $this->atributosProducto[] = $atributoProducto;
            $atributoProducto->setLetra($this);
        }

        return $this;
    }

    public function removeAtributosProducto(AtributosProducto $atributoProducto): self {
        if ($this->atributosProducto->contains($atributoProducto)) {
            $this->atributosProducto->removeElement($atributoProducto);
            // set the owning side to null (unless already changed)
            if ($atributoProducto->getLetra() === $this) {
                $atributoProducto->setLetra(null);
            }
        }

        return $this;
    }
 
}
