<?php

namespace App\Entity;

use App\NotaDebitoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Moneda;

/**
 * @ORM\Entity(repositoryClass=App\Repository\NotaDebitoRepository::class)
 * @ORM\Table(name="notadebito")
 */
class NotaDebito extends Moneda {

  
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC", cascade={"persist"})
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=true)
     * */
    private $movimientocc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;



    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $importe = 0;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

   
    
    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
          
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'Nota de Débito Nro. ' . $this->id;
    }

       public function getTotal(){
        return $this->importe;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }


    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getMovimientocc(): ?MovimientoCC
    {
        return $this->movimientocc;
    }

    public function setMovimientocc(?MovimientoCC $movimientocc): self
    {
        $this->movimientocc = $movimientocc;

        return $this;
    }

}
