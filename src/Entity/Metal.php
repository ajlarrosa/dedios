<?php

namespace App\Entity;

use App\MetalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=App\Repository\MetalRepository::class)
 * @ORM\Table(name="metal")
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The metal already exists."
 * )
 */
class Metal {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';
    const GOLD = 'ORO';
    const SILVER = 'SILVERS';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Categoriasubcategoria", mappedBy="metal")
     */
    protected $categoriasubcategorias;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->categoriasubcategorias = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|Categoriasubcategoria[]
     */
    public function getCategoriasubcategorias(): Collection {
        return $this->categoriasubcategorias;
    }

    public function addCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if (!$this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias[] = $categoriasubcategoria;
            $categoriasubcategoria->setMetal($this);
        }

        return $this;
    }

    public function removeCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if ($this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias->removeElement($categoriasubcategoria);
            // set the owning side to null (unless already changed)
            if ($categoriasubcategoria->getMetal() === $this) {
                $categoriasubcategoria->setMetal(null);
            }
        }

        return $this;
    }

}
