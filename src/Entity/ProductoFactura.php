<?php

namespace App\Entity;

use App\ProductoDocumentoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ProductoFacturaRepository::class)
 * @ORM\Table(name="productofactura")
 */
class ProductoFactura {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="productosFactura")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Factura", inversedBy="productosFactura",cascade={"persist"})
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=false)
     */
    protected $factura;

    /**
     * @ORM\Column(type="float")
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="string",length=300,nullable=true)     
     * @Assert\Length(
     *      max = 300,     
     *      maxMessage = "La descripción no puede superar los 300 caracteres"
     * )     
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float")
     */
    protected $precio;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->id;
    }

    public function getImporte() {
        return round($this->cantidad * $this->precio, 2);
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCantidad(): ?float {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float {
        return $this->precio;
    }

    public function setPrecio(float $precio): self {
        $this->precio = $precio;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getFactura(): ?Factura {
        return $this->factura;
    }

    public function setFactura(?Factura $factura): self {
        $this->factura = $factura;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

}
