<?php

namespace App\Entity;

use App\MetalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\PrecioRepository::class)
 * @ORM\Table(name="precio") 
 * @UniqueEntity(
 * 		fields = {"producto","listaPrecio","precioReferencia"},
 *             errorPath="listaPrecio",
 * 		message = "Ya existe un precio para este producto en esta lista"
 * 		)
 */
class Precio {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="precios")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", nullable=false)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio", inversedBy="precios")
     * @ORM\JoinColumn(name="listaPrecio_id", referencedColumnName="id", nullable=false)
     */
    protected $listaPrecio;

    /**
     * @ORM\ManyToOne(targetEntity="Precio", inversedBy="precios")
     * @ORM\JoinColumn(name="precio_id", referencedColumnName="id", nullable=true)
     */
    protected $precioReferencia;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     * 		message = "Debe poner un precio."
     * 		)
     */
    protected $valor;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /**
     * @ORM\OneToMany(targetEntity="PrecioMedidaProducto", mappedBy="precio", cascade={"persist", "remove"})
     */
    protected $preciosMedidaProducto;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="precioReferencia", cascade={"persist", "remove"})
     */
    protected $precios;

    public function __construct() {
        $this->preciosMedidaProducto = new ArrayCollection();
        $this->precios = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->id;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getValor(): ?float {
        return $this->valor;
    }

    public function setValor(float $valor): self {
        $this->valor = $valor;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getListaPrecio(): ?ListaPrecio {
        return $this->listaPrecio;
    }

    public function setListaPrecio(?ListaPrecio $listaPrecio): self {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

    /**
     * @return Collection|PrecioMedidaproducto[]
     */
    public function getPreciosMedidaProducto(): Collection {
        return $this->preciosMedidaProducto;
    }

    public function addPreciosMedidaProducto(PrecioMedidaProducto $preciosMedidaProducto): self {
        if (!$this->preciosMedidaProducto->contains($preciosMedidaProducto)) {
            $this->preciosMedidaProducto[] = $preciosMedidaProducto;
            $preciosMedidaProducto->setAtributoProducto($this);
        }

        return $this;
    }

    public function removePreciosMedidaProducto(PrecioMedidaProducto $preciosMedidaProducto): self {
        if ($this->preciosMedidaProducto->contains($preciosMedidaProducto)) {
            $this->preciosMedidaProducto->removeElement($preciosMedidaProducto);
            // set the owning side to null (unless already changed)
            if ($preciosMedidaProducto->getAtributoProducto() === $this) {
                $preciosMedidaProducto->setAtributoProducto(null);
            }
        }

        return $this;
    }

    public function getPrecioReferencia(): ?self
    {
        return $this->precioReferencia;
    }

    public function setPrecioReferencia(?self $precioReferencia): self
    {
        $this->precioReferencia = $precioReferencia;

        return $this;
    }

    /**
     * @return Collection|Precio[]
     */
    public function getPrecios(): Collection
    {
        return $this->precios;
    }

    public function addPrecio(Precio $precio): self
    {
        if (!$this->precios->contains($precio)) {
            $this->precios[] = $precio;
            $precio->setPrecioReferencia($this);
        }

        return $this;
    }

    public function removePrecio(Precio $precio): self
    {
        if ($this->precios->contains($precio)) {
            $this->precios->removeElement($precio);
            // set the owning side to null (unless already changed)
            if ($precio->getPrecioReferencia() === $this) {
                $precio->setPrecioReferencia(null);
            }
        }

        return $this;
    }

}
