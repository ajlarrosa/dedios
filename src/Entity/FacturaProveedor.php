<?php

namespace App\Entity;

use App\FacturaProveedorRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Moneda;

/**
 * @ORM\Entity(repositoryClass=App\Repository\FacturaProveedorRepository::class)
 * @ORM\Table(name="facturaproveedor")
 */
class FacturaProveedor extends Moneda {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="TipoGasto", inversedBy="facturasProveedor")
     * @ORM\JoinColumn(name="tipogasto_id", referencedColumnName="id", nullable=true)     
     */
    protected $tipoGasto;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC")
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=true)
     * */
    private $movimientocc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $importe = 0;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $bonificacion = 0;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $observacion;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {

    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'Factura Nro. ' . $this->id;
    }


    public function getTotal() {
        $total = round($this->getImporte() - $this->getDescuentoValue() - $this->bonificacion, 2);
        return $total;
    }

    public function getDescuentoValue() {
        return $this->getImporte() * $this->descuento / 100;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImporte(): ?float {
        return $this->importe;
    }

    public function setImporte(float $importe): self {
        $this->importe = $importe;

        return $this;
    }

    public function getObservacion(): ?string {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self {
        $this->observacion = $observacion;

        return $this;
    }


    public function getTipoGasto(): ?TipoGasto {
        return $this->tipoGasto;
    }

    public function setTipoGasto(?TipoGasto $tipoGasto): self {
        $this->tipoGasto = $tipoGasto;

        return $this;
    }

    public function getMovimientocc(): ?MovimientoCC {
        return $this->movimientocc;
    }

    public function setMovimientocc(?MovimientoCC $movimientocc): self {
        $this->movimientocc = $movimientocc;

        return $this;
    }

    public function getDescuento(): ?float {
        return $this->descuento;
    }

    public function setDescuento(float $descuento): self {
        $this->descuento = $descuento;

        return $this;
    }

    public function getBonificacion(): ?float {
        return $this->bonificacion;
    }

    public function setBonificacion(float $bonificacion): self {
        $this->bonificacion = $bonificacion;

        return $this;
    }

}
