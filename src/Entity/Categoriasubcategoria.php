<?php

namespace App\Entity;

use App\Entity\Tienda\CategoriasubcategoriaGrupo;
use App\Repository\CategoriasubcategoriaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\CategoriasubcategoriaRepository::class)
 * @ORM\Table(name="categoriasubcategoria")
 * @UniqueEntity(
 *     fields={"categoria", "subcategoria","metal"},
 *     errorPath="categoria",
 *     message="The relation already exists."
 * )
 */
class Categoriasubcategoria {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    protected $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Subcategoria", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Metal", inversedBy="categoriasubcategorias")
     * @ORM\JoinColumn(name="metal_id", referencedColumnName="id", nullable=true)
     */
    protected $metal;

    /**
     * @ORM\OneToMany(targetEntity="Producto", mappedBy="categoriasubcategoria")
     */
    protected $productos;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "El título de web no puede superar los 100 caracteres"
     * )    
     */
    protected $titleWeb;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;
  
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\CategoriasubcategoriaGrupo", mappedBy="categoriasubcategoria")
     */
    protected $categoriasubcategoriasGrupo;
    
    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productos = new ArrayCollection();
        $this->categoriasubcategoriasGrupo = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->categoria . ' - ' . $this->subcategoria . ' - ' . $this->metal;
    }
    
     public function getType() {
        return 'categoriasubcategoria';
    }

    public function getTitleMenu() {
        return $this->subcategoria->getDescripcion();
    }
    

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getCategoria(): ?Categoria {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self {
        $this->categoria = $categoria;

        return $this;
    }

    public function getSubcategoria(): ?Subcategoria {
        return $this->subcategoria;
    }

    public function setSubcategoria(?Subcategoria $subcategoria): self {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    public function getMetal(): ?Metal {
        return $this->metal;
    }

    public function setMetal(?Metal $metal): self {
        $this->metal = $metal;

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setCategoriasubcategoria($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getCategoriasubcategoria() === $this) {
                $producto->setCategoriasubcategoria(null);
            }
        }

        return $this;
    }

    public function getTitleWeb(): ?string {
        return $this->titleWeb;
    }

    public function setTitleWeb(?string $titleWeb): self {
        $this->titleWeb = $titleWeb;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return Collection|CategoriasubcategoriaGrupo[]
     */
    public function getCategoriasubcategoriasGrupo(): Collection
    {
        return $this->categoriasubcategoriasGrupo;
    }

    public function addCategoriasubcategoriasGrupo(CategoriasubcategoriaGrupo $categoriasubcategoriasGrupo): self
    {
        if (!$this->categoriasubcategoriasGrupo->contains($categoriasubcategoriasGrupo)) {
            $this->categoriasubcategoriasGrupo[] = $categoriasubcategoriasGrupo;
            $categoriasubcategoriasGrupo->setCategoriasubcategoria($this);
        }

        return $this;
    }

    public function removeCategoriasubcategoriasGrupo(CategoriasubcategoriaGrupo $categoriasubcategoriasGrupo): self
    {
        if ($this->categoriasubcategoriasGrupo->contains($categoriasubcategoriasGrupo)) {
            $this->categoriasubcategoriasGrupo->removeElement($categoriasubcategoriasGrupo);
            // set the owning side to null (unless already changed)
            if ($categoriasubcategoriasGrupo->getCategoriasubcategoria() === $this) {
                $categoriasubcategoriasGrupo->setCategoriasubcategoria(null);
            }
        }

        return $this;
    }

}
