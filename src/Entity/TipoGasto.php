<?php

namespace App\Entity;

use App\TipoGstoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\ListaPrecioRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass=App\Repository\TipoGastoRepository::class)
 * @ORM\Table(name="tipogasto")
  * @UniqueEntity(
 *     fields={"nombre"},
 *     errorPath="nombre",
 *     message="The expense type already exists."
 * )
 */
class TipoGasto {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /**
     * @ORM\OneToMany(targetEntity="FacturaProveedor", mappedBy="tipoGasto")
     */
    protected $facturasProveedor;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->facturasProveedor = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre();
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|FacturaProveedor[]
     */
    public function getFacturasProveedor(): Collection
    {
        return $this->facturasProveedor;
    }

    public function addFacturasProveedor(FacturaProveedor $facturasProveedor): self
    {
        if (!$this->facturasProveedor->contains($facturasProveedor)) {
            $this->facturasProveedor[] = $facturasProveedor;
            $facturasProveedor->setTipoGasto($this);
        }

        return $this;
    }

    public function removeFacturasProveedor(FacturaProveedor $facturasProveedor): self
    {
        if ($this->facturasProveedor->contains($facturasProveedor)) {
            $this->facturasProveedor->removeElement($facturasProveedor);
            // set the owning side to null (unless already changed)
            if ($facturasProveedor->getTipoGasto() === $this) {
                $facturasProveedor->setTipoGasto(null);
            }
        }

        return $this;
    }

    

}
