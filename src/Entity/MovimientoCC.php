<?php

namespace App\Entity;

use App\MovimientoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\MovimientoRepository::class)
 * @ORM\Table(name="movimientocc")
 */
class MovimientoCC {

    CONST FACTURA = 'Invoice';
    CONST FACTURA_PROVEEDOR = 'Invoice provider';
    CONST NOTA_CREDITO = 'Credit note';
    CONST NOTA_DEBITO = 'Debit note';
    CONST RECIBO_CLIENTE = 'Client receipt';
    CONST ORDEN_PAGO = 'Pay order';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="movimientoscc")
     * @ORM\JoinColumn(name="clienteProveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\OneToOne(targetEntity="Factura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
     * */
    private $factura;

    /**
     * @ORM\OneToOne(targetEntity="FacturaProveedor")
     * @ORM\JoinColumn(name="facturaproveedor_id", referencedColumnName="id", nullable=true)
     * */
    private $facturaProveedor;

    /**
     * @ORM\OneToOne(targetEntity="NotaDebito")
     * @ORM\JoinColumn(name="notadebito_id", referencedColumnName="id", nullable=true)
     * */
    private $notaDebito;

    /**
     * @ORM\OneToOne(targetEntity="NotaCredito")
     * @ORM\JoinColumn(name="notacredito_id", referencedColumnName="id", nullable=true)
     * */
    private $notaCredito;

    /**
     * @ORM\OneToOne(targetEntity="ReciboCliente")
     * @ORM\JoinColumn(name="recibocliente_id", referencedColumnName="id", nullable=true)
     * */
    private $reciboCliente;

    /**
     * @ORM\OneToOne(targetEntity="OrdenPago")
     * @ORM\JoinColumn(name="ordenpago_id", referencedColumnName="id", nullable=true)
     * */
    private $ordenPago;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fecha = new \Datetime;
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDocumentName();
    }

    public function getDocumentName() {
        if (!is_null($this->factura)) {
            return self::FACTURA;
        }
        if (!is_null($this->facturaProveedor)) {
            return self::FACTURA_PROVEEDOR;
        }
        if (!is_null($this->notaCredito)) {
            return self::NOTA_CREDITO;
        }
        if (!is_null($this->notaDebito)) {
            return self::NOTA_DEBITO;
        }
        if (!is_null($this->ordenPago)) {
            return self::ORDEN_PAGO;
        }
        if (!is_null($this->reciboCliente)) {
            return self::RECIBO_CLIENTE;
        }
    }

    public function getDocument() {
        if (!is_null($this->factura)) {
            return $this->factura;
        }
        if (!is_null($this->facturaProveedor)) {
            return $this->facturaProveedor;
        }
        if (!is_null($this->notaCredito)) {
            return $this->notaCredito;
        }
        if (!is_null($this->notaDebito)) {
            return $this->notaDebito;
        }
        if (!is_null($this->ordenPago)) {
            return $this->ordenPago;
        }
        if (!is_null($this->reciboCliente)) {
            return $this->reciboCliente;
        }
    }

    public function getBaseDocumentPath() {
        if (!is_null($this->factura)) {
            return 'factura';
        }
        if (!is_null($this->facturaProveedor)) {
            return 'facturaproveedor';
        }
        if (!is_null($this->notaCredito)) {
            return 'nota_credito';
        }
        if (!is_null($this->notaDebito)) {
            return 'nota_debito';
        }
        if (!is_null($this->ordenPago)) {
            return 'ordenpago';
        }
        if (!is_null($this->reciboCliente)) {
            return 'recibocliente';
        }
    }

    public function isDebit() {
        switch ($this->getDocumentName()) {
            case self::FACTURA:
                return true;
            case self::FACTURA_PROVEEDOR:
                return false;
            case self::ORDEN_PAGO:
                return true;
            case self::NOTA_CREDITO:
                if ($this->clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {
                    return false;
                } else {
                    return true;
                }
            case self::NOTA_DEBITO:
                if ($this->clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {
                    return true;
                } else {
                    return false;
                }
            case self::RECIBO_CLIENTE:
                return false;
        }
    }

    public function isPesos() {
        $document = $this->getDocument();
        if ($this->getDocumentName() == self::FACTURA) {
            if ($document->getListaPrecio()->getMoneda() == ListaPrecio::COD_PESOS) {
                return true;
            }
            return false;
        }
        if ($document->getMoneda() == FacturaProveedor::COD_PESOS) {
            return true;
        }
        return false;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

    public function getClienteProveedor(): ?ClienteProveedor {
        return $this->clienteProveedor;
    }

    public function setClienteProveedor(?ClienteProveedor $clienteProveedor): self {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    public function getFactura(): ?Factura {
        return $this->factura;
    }

    public function setFactura(?Factura $factura): self {
        $this->factura = $factura;

        return $this;
    }

    public function getFacturaProveedor(): ?FacturaProveedor {
        return $this->facturaProveedor;
    }

    public function setFacturaProveedor(?FacturaProveedor $facturaProveedor): self {
        $this->facturaProveedor = $facturaProveedor;

        return $this;
    }

    public function getNotaDebito(): ?NotaDebito {
        return $this->notaDebito;
    }

    public function setNotaDebito(?NotaDebito $notaDebito): self {
        $this->notaDebito = $notaDebito;

        return $this;
    }

    public function getNotaCredito(): ?NotaCredito {
        return $this->notaCredito;
    }

    public function setNotaCredito(?NotaCredito $notaCredito): self {
        $this->notaCredito = $notaCredito;

        return $this;
    }

    public function getReciboCliente(): ?ReciboCliente {
        return $this->reciboCliente;
    }

    public function setReciboCliente(?ReciboCliente $reciboCliente): self {
        $this->reciboCliente = $reciboCliente;

        return $this;
    }

    public function getOrdenPago(): ?OrdenPago {
        return $this->ordenPago;
    }

    public function setOrdenPago(?OrdenPago $ordenPago): self {
        $this->ordenPago = $ordenPago;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

}
