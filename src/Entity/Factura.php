<?php

namespace App\Entity;

use App\FacturaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Moneda;

/**
 * @ORM\Entity(repositoryClass=App\Repository\FacturaRepository::class)
 * @ORM\Table(name="factura")
 */
class Factura extends Moneda {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="MovimientoCC")
     * @ORM\JoinColumn(name="movimientocc_id", referencedColumnName="id", nullable=false)
     * */
    private $movimientocc;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaPrecio_id", referencedColumnName="id")
     */
    protected $listaPrecio;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $bonificacion = 0;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="factura", cascade={"persist", "remove"} )
     */
    protected $productosFactura;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosFactura = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return 'Factura Nro. ' . $this->id;
    }


    public function getTotal() {
        $total = round($this->getSubtotal() -  $this->getDescuentoValue() - $this->bonificacion, 2);
        return $total;
    }

    public function getDescuentoValue() {
        return $this->getSubtotal() * $this->descuento / 100;
    }

    public function getSubtotal() {
        $subTotal = 0;
        foreach ($this->productosFactura as $productoFactura) {
            $subTotal = $subTotal + $productoFactura->getImporte();
        }
        return $subTotal;
    }

    public function getId(): ?int {
        return $this->id;
    }


    public function getDescuento(): ?float {
        return $this->descuento;
    }

    public function setDescuento(float $descuento): self {
        $this->descuento = $descuento;

        return $this;
    }

    public function getBonificacion(): ?float {
        return $this->bonificacion;
    }

    public function setBonificacion(float $bonificacion): self {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    public function getObservacion(): ?string {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self {
        $this->observacion = $observacion;

        return $this;
    }

    public function getMovimientocc(): ?MovimientoCC {
        return $this->movimientocc;
    }

    public function setMovimientocc(?MovimientoCC $movimientocc): self {
        $this->movimientocc = $movimientocc;

        return $this;
    }

    public function getListaPrecio(): ?ListaPrecio {
        return $this->listaPrecio;
    }

    public function setListaPrecio(?ListaPrecio $listaPrecio): self {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

    /**
     * @return Collection|ProductoFactura[]
     */
    public function getProductosFactura(): Collection {
        return $this->productosFactura;
    }

    public function addProductosFactura(ProductoFactura $productosFactura): self {
        if (!$this->productosFactura->contains($productosFactura)) {
            $this->productosFactura[] = $productosFactura;
            $productosFactura->setFactura($this);
        }

        return $this;
    }

    public function removeProductosFactura(ProductoFactura $productosFactura): self {
        if ($this->productosFactura->contains($productosFactura)) {
            $this->productosFactura->removeElement($productosFactura);
            // set the owning side to null (unless already changed)
            if ($productosFactura->getFactura() === $this) {
                $productosFactura->setFactura(null);
            }
        }

        return $this;
    }

}
