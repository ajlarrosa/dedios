<?php

namespace App\Entity;

use App\Entity\Tienda\Configuracion;
use App\Entity\Tienda\Cupon;
use App\Entity\Tienda\Menu;
use App\Entity\Tienda\Promotion;
use App\SubcategoriaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Common\Common;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\SubcategoriaRepository::class)
 * @ORM\Table(name="subcategoria")
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The subcategory already exists."
 * )
 */
class Subcategoria extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "El título de web no puede superar los 100 caracteres"
     * )    
     */
    protected $titleWeb;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen;

    /**
     * @ORM\OneToMany(targetEntity="Categoriasubcategoria", mappedBy="subcategoria")
     */
    protected $categoriasubcategorias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="subcategoria1")
     */
    protected $menu1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="subcategoria2")
     */
    protected $menu2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="subcategoriaMenu")
     */
    protected $menuSubcategoria;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="subcategoria1")
     */
    protected $configuracion1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="subcategoria2")
     */
    protected $configuracion2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="subcategoria3")
     */
    protected $configuracion3;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Promotion", mappedBy="subcategoria")
     */
    protected $promotions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Cupon", mappedBy="subcategoria")
     */
    protected $cupones;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->categoriasubcategorias = new ArrayCollection();

        $this->menu1 = new ArrayCollection();
        $this->menu2 = new ArrayCollection();
        $this->menuSubcategoria = new ArrayCollection();
        $this->configuracion1 = new ArrayCollection();
        $this->configuracion2 = new ArrayCollection();
        $this->configuracion3 = new ArrayCollection();
        $this->promotions = new ArrayCollection();
        $this->cupones = new ArrayCollection();
    }

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getType() {
        return 'subcategoria';
    }

    public function getTitleMenu() {
        return (!is_null($this->titleWeb) && trim($this->titleWeb != ''))  ? $this->titleWeb : $this->getDescripcion();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return Collection|Categoriasubcategoria[]
     */
    public function getCategoriasubcategorias(): Collection {
        return $this->categoriasubcategorias;
    }

    public function addCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if (!$this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias[] = $categoriasubcategoria;
            $categoriasubcategoria->setSubcategoria($this);
        }

        return $this;
    }

    public function removeCategoriasubcategoria(Categoriasubcategoria $categoriasubcategoria): self {
        if ($this->categoriasubcategorias->contains($categoriasubcategoria)) {
            $this->categoriasubcategorias->removeElement($categoriasubcategoria);
            // set the owning side to null (unless already changed)
            if ($categoriasubcategoria->getSubcategoria() === $this) {
                $categoriasubcategoria->setSubcategoria(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu1(): Collection {
        return $this->menu1;
    }

    public function addMenu1(Menu $menu1): self {
        if (!$this->menu1->contains($menu1)) {
            $this->menu1[] = $menu1;
            $menu1->setSubcategoria1($this);
        }

        return $this;
    }

    public function removeMenu1(Menu $menu1): self {
        if ($this->menu1->contains($menu1)) {
            $this->menu1->removeElement($menu1);
            // set the owning side to null (unless already changed)
            if ($menu1->getSubcategoria1() === $this) {
                $menu1->setSubcategoria1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu2(): Collection {
        return $this->menu2;
    }

    public function addMenu2(Menu $menu2): self {
        if (!$this->menu2->contains($menu2)) {
            $this->menu2[] = $menu2;
            $menu2->setSubcategoria2($this);
        }

        return $this;
    }

    public function removeMenu2(Menu $menu2): self {
        if ($this->menu2->contains($menu2)) {
            $this->menu2->removeElement($menu2);
            // set the owning side to null (unless already changed)
            if ($menu2->getSubcategoria2() === $this) {
                $menu2->setSubcategoria2(null);
            }
        }

        return $this;
    }

    public function getImagen(): ?string {
        return $this->imagen;
    }

    public function setImagen(string $imagen): self {
        $this->imagen = $imagen;

        return $this;
    }

    public function getTitleWeb(): ?string {
        return $this->titleWeb;
    }

    public function setTitleWeb(?string $titleWeb): self {
        $this->titleWeb = $titleWeb;

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenuSubcategoria(): Collection {
        return $this->menuSubcategoria;
    }

    public function addMenuSubcategorium(Menu $menuSubcategorium): self {
        if (!$this->menuSubcategoria->contains($menuSubcategorium)) {
            $this->menuSubcategoria[] = $menuSubcategorium;
            $menuSubcategorium->setSubcategoriaMenu($this);
        }

        return $this;
    }

    public function removeMenuSubcategorium(Menu $menuSubcategorium): self {
        if ($this->menuSubcategoria->contains($menuSubcategorium)) {
            $this->menuSubcategoria->removeElement($menuSubcategorium);
            // set the owning side to null (unless already changed)
            if ($menuSubcategorium->getSubcategoriaMenu() === $this) {
                $menuSubcategorium->setSubcategoriaMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion1(): Collection {
        return $this->configuracion1;
    }

    public function addConfiguracion1(Configuracion $configuracion1): self {
        if (!$this->configuracion1->contains($configuracion1)) {
            $this->configuracion1[] = $configuracion1;
            $configuracion1->setSubcategoria1($this);
        }

        return $this;
    }

    public function removeConfiguracion1(Configuracion $configuracion1): self {
        if ($this->configuracion1->contains($configuracion1)) {
            $this->configuracion1->removeElement($configuracion1);
            // set the owning side to null (unless already changed)
            if ($configuracion1->getSubcategoria1() === $this) {
                $configuracion1->setSubcategoria1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion2(): Collection {
        return $this->configuracion2;
    }

    public function addConfiguracion2(Configuracion $configuracion2): self {
        if (!$this->configuracion2->contains($configuracion2)) {
            $this->configuracion2[] = $configuracion2;
            $configuracion2->setSubcategoria2($this);
        }

        return $this;
    }

    public function removeConfiguracion2(Configuracion $configuracion2): self {
        if ($this->configuracion2->contains($configuracion2)) {
            $this->configuracion2->removeElement($configuracion2);
            // set the owning side to null (unless already changed)
            if ($configuracion2->getSubcategoria2() === $this) {
                $configuracion2->setSubcategoria2(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion3(): Collection {
        return $this->configuracion3;
    }

    public function addConfiguracion3(Configuracion $configuracion3): self {
        if (!$this->configuracion3->contains($configuracion3)) {
            $this->configuracion3[] = $configuracion3;
            $configuracion3->setSubcategoria3($this);
        }

        return $this;
    }

    public function removeConfiguracion3(Configuracion $configuracion3): self {
        if ($this->configuracion3->contains($configuracion3)) {
            $this->configuracion3->removeElement($configuracion3);
            // set the owning side to null (unless already changed)
            if ($configuracion3->getSubcategoria3() === $this) {
                $configuracion3->setSubcategoria3(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Promotion[]
     */
    public function getPromotions(): Collection {
        return $this->promotions;
    }

    public function addPromotion(Promotion $promotion): self {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setSubcategoria($this);
        }

        return $this;
    }

    public function removePromotion(Promotion $promotion): self {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getSubcategoria() === $this) {
                $promotion->setSubcategoria(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cupon[]
     */
    public function getCupones(): Collection
    {
        return $this->cupones;
    }

    public function addCupone(Cupon $cupone): self
    {
        if (!$this->cupones->contains($cupone)) {
            $this->cupones[] = $cupone;
            $cupone->setSubcategoria($this);
        }

        return $this;
    }

    public function removeCupone(Cupon $cupone): self
    {
        if ($this->cupones->removeElement($cupone)) {
            // set the owning side to null (unless already changed)
            if ($cupone->getSubcategoria() === $this) {
                $cupone->setSubcategoria(null);
            }
        }

        return $this;
    }

}
