<?php

namespace App\Entity\Tienda;

use App\Entity\Tienda\LineaProducto;
use App\Repository\Tienda\LineaProductoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\LineaProductoRepository::class)
 */
class LineaProducto extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Description cannot exceed {{ limit }} characters"
     * )   
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string",length=100,nullable=true)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "El título de web no puede superar los 100 caracteres"
     * )    
     */
    protected $titleWeb;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\ProductosLineaProducto", mappedBy="lineaProducto")
     */
    protected $productosLineaProducto;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\ProductoLinea", mappedBy="lineaProducto")
     */
    protected $productosLinea;

    /**
     * @ORM\OneToMany(targetEntity="LineaProductoGrupo", mappedBy="lineaProducto")
     */
    protected $lineaProductosGrupo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="lineaProducto1")
     */
    protected $menu1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="lineaProducto2")
     */
    protected $menu2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Menu", mappedBy="lineaProductoMenu")
     */
    protected $menuLineaProducto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="lineaProducto1")
     */
    protected $configuracion1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="lineaProducto2")
     */
    protected $configuracion2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Configuracion", mappedBy="lineaProducto3")
     */
    protected $configuracion3;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Promotion", mappedBy="lineaProducto")
     */
    protected $promotions;
     /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Cupon", mappedBy="lineaProducto")
     */
    protected $cupones;
    
    
    protected $rootDir = 'uploads/linea_producto';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosLineaProducto = new ArrayCollection();
        $this->productosLinea = new ArrayCollection();
        $this->lineaProductosGrupo = new ArrayCollection();
        $this->menu1 = new ArrayCollection();
        $this->menu2 = new ArrayCollection();
        $this->menuLineaProducto = new ArrayCollection();
        $this->configuracion1 = new ArrayCollection();
        $this->configuracion2 = new ArrayCollection();
        $this->configuracion3 = new ArrayCollection();
        $this->promotions = new ArrayCollection();
         $this->cupones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getType() {
        return 'lineaProducto';
    }

    /*
     * 
     */

    public function getTitleMenu() {
        if (!is_null($this->titleWeb) && trim($this->titleWeb != '')) {
            return $this->titleWeb;
        } else {
            return $this->getDescripcion();
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

   public function getImagen() {
        return $this->imagen != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagen : '';
    }

    public function setImagen(string $imagen): self {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return Collection|ProductosLineaProducto[]
     */
    public function getProductosLineaProducto(): Collection {
        return $this->productosLineaProducto;
    }

    public function addProductosLineaProducto(ProductosLineaProducto $productosLineaProducto): self {
        if (!$this->productosLineaProducto->contains($productosLineaProducto)) {
            $this->productosLineaProducto[] = $productosLineaProducto;
            $productosLineaProducto->setLineaProducto($this);
        }

        return $this;
    }

    public function removeProductosLineaProducto(ProductosLineaProducto $productosLineaProducto): self {
        if ($this->productosLineaProducto->contains($productosLineaProducto)) {
            $this->productosLineaProducto->removeElement($productosLineaProducto);
            // set the owning side to null (unless already changed)
            if ($productosLineaProducto->getLineaProducto() === $this) {
                $productosLineaProducto->setLineaProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LineaProductoGrupo[]
     */
    public function getLineaProductosGrupo(): Collection {
        return $this->lineaProductosGrupo;
    }

    public function addLineaProductosGrupo(LineaProductoGrupo $lineaProductosGrupo): self {
        if (!$this->lineaProductosGrupo->contains($lineaProductosGrupo)) {
            $this->lineaProductosGrupo[] = $lineaProductosGrupo;
            $lineaProductosGrupo->setLineaProducto($this);
        }

        return $this;
    }

    public function removeLineaProductosGrupo(LineaProductoGrupo $lineaProductosGrupo): self {
        if ($this->lineaProductosGrupo->contains($lineaProductosGrupo)) {
            $this->lineaProductosGrupo->removeElement($lineaProductosGrupo);
            // set the owning side to null (unless already changed)
            if ($lineaProductosGrupo->getLineaProducto() === $this) {
                $lineaProductosGrupo->setLineaProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu1(): Collection {
        return $this->menu1;
    }

    public function addMenu1(Menu $menu1): self {
        if (!$this->menu1->contains($menu1)) {
            $this->menu1[] = $menu1;
            $menu1->setLineaProducto1($this);
        }

        return $this;
    }

    public function removeMenu1(Menu $menu1): self {
        if ($this->menu1->contains($menu1)) {
            $this->menu1->removeElement($menu1);
            // set the owning side to null (unless already changed)
            if ($menu1->getLineaProducto1() === $this) {
                $menu1->setLineaProducto1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenu2(): Collection {
        return $this->menu2;
    }

    public function addMenu2(Menu $menu2): self {
        if (!$this->menu2->contains($menu2)) {
            $this->menu2[] = $menu2;
            $menu2->setLineaProducto2($this);
        }

        return $this;
    }

    public function removeMenu2(Menu $menu2): self {
        if ($this->menu2->contains($menu2)) {
            $this->menu2->removeElement($menu2);
            // set the owning side to null (unless already changed)
            if ($menu2->getLineaProducto2() === $this) {
                $menu2->setLineaProducto2(null);
            }
        }

        return $this;
    }

    public function getTitleWeb(): ?string {
        return $this->titleWeb;
    }

    public function setTitleWeb(?string $titleWeb): self {
        $this->titleWeb = $titleWeb;

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenuLineaProducto(): Collection {
        return $this->menuLineaProducto;
    }

    public function addMenuLineaProducto(Menu $menuLineaProducto): self {
        if (!$this->menuLineaProducto->contains($menuLineaProducto)) {
            $this->menuLineaProducto[] = $menuLineaProducto;
            $menuLineaProducto->setLineaProductoMenu($this);
        }

        return $this;
    }

    public function removeMenuLineaProducto(Menu $menuLineaProducto): self {
        if ($this->menuLineaProducto->contains($menuLineaProducto)) {
            $this->menuLineaProducto->removeElement($menuLineaProducto);
            // set the owning side to null (unless already changed)
            if ($menuLineaProducto->getLineaProductoMenu() === $this) {
                $menuLineaProducto->setLineaProductoMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion1(): Collection {
        return $this->configuracion1;
    }

    public function addConfiguracion1(Configuracion $configuracion1): self {
        if (!$this->configuracion1->contains($configuracion1)) {
            $this->configuracion1[] = $configuracion1;
            $configuracion1->setLineaProducto1($this);
        }

        return $this;
    }

    public function removeConfiguracion1(Configuracion $configuracion1): self {
        if ($this->configuracion1->contains($configuracion1)) {
            $this->configuracion1->removeElement($configuracion1);
            // set the owning side to null (unless already changed)
            if ($configuracion1->getLineaProducto1() === $this) {
                $configuracion1->setLineaProducto1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion2(): Collection {
        return $this->configuracion2;
    }

    public function addConfiguracion2(Configuracion $configuracion2): self {
        if (!$this->configuracion2->contains($configuracion2)) {
            $this->configuracion2[] = $configuracion2;
            $configuracion2->setLineaProducto2($this);
        }

        return $this;
    }

    public function removeConfiguracion2(Configuracion $configuracion2): self {
        if ($this->configuracion2->contains($configuracion2)) {
            $this->configuracion2->removeElement($configuracion2);
            // set the owning side to null (unless already changed)
            if ($configuracion2->getLineaProducto2() === $this) {
                $configuracion2->setLineaProducto2(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguracion3(): Collection {
        return $this->configuracion3;
    }

    public function addConfiguracion3(Configuracion $configuracion3): self {
        if (!$this->configuracion3->contains($configuracion3)) {
            $this->configuracion3[] = $configuracion3;
            $configuracion3->setLineaProducto3($this);
        }

        return $this;
    }

    public function removeConfiguracion3(Configuracion $configuracion3): self {
        if ($this->configuracion3->contains($configuracion3)) {
            $this->configuracion3->removeElement($configuracion3);
            // set the owning side to null (unless already changed)
            if ($configuracion3->getLineaProducto3() === $this) {
                $configuracion3->setLineaProducto3(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Promotion[]
     */
    public function getPromotions(): Collection
    {
        return $this->promotions;
    }

    public function addPromotion(Promotion $promotion): self
    {
        if (!$this->promotions->contains($promotion)) {
            $this->promotions[] = $promotion;
            $promotion->setLineaProducto($this);
        }

        return $this;
    }

    public function removePromotion(Promotion $promotion): self
    {
        if ($this->promotions->removeElement($promotion)) {
            // set the owning side to null (unless already changed)
            if ($promotion->getLineaProducto() === $this) {
                $promotion->setLineaProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cupon[]
     */
    public function getCupones(): Collection
    {
        return $this->cupones;
    }

    public function addCupone(Cupon $cupone): self
    {
        if (!$this->cupones->contains($cupone)) {
            $this->cupones[] = $cupone;
            $cupone->setLineaProducto($this);
        }

        return $this;
    }

    public function removeCupone(Cupon $cupone): self
    {
        if ($this->cupones->removeElement($cupone)) {
            // set the owning side to null (unless already changed)
            if ($cupone->getLineaProducto() === $this) {
                $cupone->setLineaProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoLinea[]
     */
    public function getProductosLinea(): Collection
    {
        return $this->productosLinea;
    }

    public function addProductosLinea(ProductoLinea $productosLinea): self
    {
        if (!$this->productosLinea->contains($productosLinea)) {
            $this->productosLinea[] = $productosLinea;
            $productosLinea->setLineaProducto($this);
        }

        return $this;
    }

    public function removeProductosLinea(ProductoLinea $productosLinea): self
    {
        if ($this->productosLinea->removeElement($productosLinea)) {
            // set the owning side to null (unless already changed)
            if ($productosLinea->getLineaProducto() === $this) {
                $productosLinea->setLineaProducto(null);
            }
        }

        return $this;
    }

}
