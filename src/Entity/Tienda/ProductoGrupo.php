<?php

namespace App\Entity\Tienda;

use App\Entity\Producto;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\ProductoGrupoRepository::class)
 */
class ProductoGrupo extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="productosGrupo")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")     
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="productosGrupo")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")     
     */
    protected $grupo;

    public function getItem() {
        return $this->producto;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getGrupo(): ?Grupo {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self {
        $this->grupo = $grupo;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self {
        $this->created = $created;

        return $this;
    }

}
