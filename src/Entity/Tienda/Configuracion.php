<?php

namespace App\Entity\Tienda;

use App\Entity\Categoria;
use App\Entity\Subcategoria;
use App\Repository\Tienda\ConfiguracionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;


/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\ConfiguracionRepository::class)
 */
class Configuracion extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $logo;

    /**
     * @ORM\Column(type="integer")    
     */
    protected $monedaMinorista;

    /**
     * @ORM\Column(type="integer")    
     */
    protected $monedaMayorista;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSlider1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSlider2;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSlider3;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSliderMobile1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSliderMobile2;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagenMainSliderMobile3;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $videoMainSlider;

    /**
     * @ORM\Column(type="blob", nullable=true)    
     */
    protected $imagenResultado;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $costoFijoShipNow = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Promocion", inversedBy="configuraciones", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="promocion_id", referencedColumnName="id", nullable=false)
     */
    protected $promocion;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="configuracion")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    protected $menus;

    /**
     * @ORM\OneToMany(targetEntity="SeccionConfiguracion", mappedBy="configuracion")
     * @ORM\OrderBy({"posicion" = "ASC"})
     */
    protected $seccionConfiguracion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="configuracion1")
     * @ORM\JoinColumn(name="categoria1_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="configuracion2")
     * @ORM\JoinColumn(name="categoria2_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="configuracion3")
     * @ORM\JoinColumn(name="categoria3_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="configuracion1")
     * @ORM\JoinColumn(name="subcategoria1_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="configuracion2")
     * @ORM\JoinColumn(name="subcategoria2_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="configuracion3")
     * @ORM\JoinColumn(name="subcategoria3_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="configuracion1")
     * @ORM\JoinColumn(name="lineaproducto1_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="configuracion2")
     * @ORM\JoinColumn(name="lineaproducto2_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="configuracion3")
     * @ORM\JoinColumn(name="lineaproducto3_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto3;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\FormatoMail", mappedBy="configuracion")
     */
    protected $formatoMail;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @Assert\PositiveOrZero(message = "Solo se admiten valores positivos o cero.")
     */
    protected $demoraShipNow = 0;

    /**
     * @ORM\Column(type="integer", options={"default" = 0})
     * @Assert\PositiveOrZero(message = "Solo se admiten valores positivos o cero.")
     */
    protected $demoraGrabado = 0;

    /**
     * @ORM\Column(type="boolean", options={"default" = 1})
     */
    protected $mostrarFechaShipNow = 1;
    
    
    protected $rootDir = 'uploads/configuracion';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->menus = new ArrayCollection();
        $this->seccionConfiguracion = new ArrayCollection();
        $this->formatoMail = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return strval($this->getId());
    }

    /*
     * 
     */

    public function getImagenResultadoSrc() {
        if (!is_null($this->imagenResultado) && $this->imagenResultado != '') {
            return $this->rootDir.'/'.$this->id.'/'.stream_get_contents($this->imagenResultado);
        } else {
            return '';
        }
    }

    /*
     * Get Mostrar Fecha Ship Now String
     */

    public function getMostrarFechaShipNowString(): ?string {
        if ($this->mostrarFechaShipNow) {
            return 'Yes';
        }
        return 'No';
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getLogo() {
        return $this->logo;
    }

    public function setLogo($logo): self {
        $this->logo = $logo;

        return $this;
    }

    public function getImagenResultado() {
        return $this->imagenResultado;
    }

    public function setImagenResultado($imagenResultado): self {
        $this->imagenResultado = $imagenResultado;

        return $this;
    }

    public function getPromocion(): ?Promocion {
        return $this->promocion;
    }

    public function setPromocion(?Promocion $promocion): self {
        $this->promocion = $promocion;

        return $this;
    }

    /**
     * @return Collection|Menu[]
     */
    public function getMenus(): Collection {
        return $this->menus;
    }

    public function addMenu(Menu $menu): self {
        if (!$this->menus->contains($menu)) {
            $this->menus[] = $menu;
            $menu->setConfiguracion($this);
        }

        return $this;
    }

    public function removeMenu(Menu $menu): self {
        if ($this->menus->contains($menu)) {
            $this->menus->removeElement($menu);
            // set the owning side to null (unless already changed)
            if ($menu->getConfiguracion() === $this) {
                $menu->setConfiguracion(null);
            }
        }

        return $this;
    }

  
    /**
     * @return Collection|SeccionConfiguracion[]
     */
    public function getSeccionConfiguracion(): Collection {
        return $this->seccionConfiguracion;
    }

    public function addSeccionConfiguracion(SeccionConfiguracion $seccionConfiguracion): self {
        if (!$this->seccionConfiguracion->contains($seccionConfiguracion)) {
            $this->seccionConfiguracion[] = $seccionConfiguracion;
            $seccionConfiguracion->setConfiguracion($this);
        }

        return $this;
    }

    public function removeSeccionConfiguracion(SeccionConfiguracion $seccionConfiguracion): self {
        if ($this->seccionConfiguracion->contains($seccionConfiguracion)) {
            $this->seccionConfiguracion->removeElement($seccionConfiguracion);
            // set the owning side to null (unless already changed)
            if ($seccionConfiguracion->getConfiguracion() === $this) {
                $seccionConfiguracion->setConfiguracion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FormatoMail[]
     */
    public function getFormatoMail(): Collection {
        return $this->formatoMail;
    }

    public function addFormatoMail(FormatoMail $formatoMail): self {
        if (!$this->formatoMail->contains($formatoMail)) {
            $this->formatoMail[] = $formatoMail;
            $formatoMail->setConfiguracion($this);
        }

        return $this;
    }

    public function removeFormatoMail(FormatoMail $formatoMail): self {
        if ($this->formatoMail->contains($formatoMail)) {
            $this->formatoMail->removeElement($formatoMail);
            // set the owning side to null (unless already changed)
            if ($formatoMail->getConfiguracion() === $this) {
                $formatoMail->setConfiguracion(null);
            }
        }

        return $this;
    }

    public function getMonedaMinorista(): ?int {
        return $this->monedaMinorista;
    }

    public function setMonedaMinorista(int $monedaMinorista): self {
        $this->monedaMinorista = $monedaMinorista;

        return $this;
    }

    public function getMonedaMayorista(): ?int {
        return $this->monedaMayorista;
    }

    public function setMonedaMayorista(int $monedaMayorista): self {
        $this->monedaMayorista = $monedaMayorista;

        return $this;
    }

    public function getImagenMainSlider1(): ?string {
        return $this->imagenMainSlider1 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSlider1 : '';
    }

    public function setImagenMainSlider1(?string $imagenMainSlider1): self {
        $this->imagenMainSlider1 = $imagenMainSlider1;

        return $this;
    }

    public function getImagenMainSlider2(): ?string {
        return $this->imagenMainSlider2 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSlider2 : '';
    }

    public function setImagenMainSlider2(?string $imagenMainSlider2): self {
        $this->imagenMainSlider2 = $imagenMainSlider2;

        return $this;
    }

    public function getVideoMainSlider(): ?string {
        return $this->videoMainSlider;
    }

    public function setVideoMainSlider(?string $videoMainSlider): self {
        $this->videoMainSlider = $videoMainSlider;

        return $this;
    }

    public function getCategoria1(): ?Categoria {
        return $this->categoria1;
    }

    public function setCategoria1(?Categoria $categoria1): self {
        $this->categoria1 = $categoria1;

        return $this;
    }

    public function getCategoria2(): ?Categoria {
        return $this->categoria2;
    }

    public function setCategoria2(?Categoria $categoria2): self {
        $this->categoria2 = $categoria2;

        return $this;
    }

    public function getSubcategoria1(): ?Subcategoria {
        return $this->subcategoria1;
    }

    public function setSubcategoria1(?Subcategoria $subcategoria1): self {
        $this->subcategoria1 = $subcategoria1;

        return $this;
    }

    public function getSubcategoria2(): ?Subcategoria {
        return $this->subcategoria2;
    }

    public function setSubcategoria2(?Subcategoria $subcategoria2): self {
        $this->subcategoria2 = $subcategoria2;

        return $this;
    }

    public function getLineaProducto1(): ?LineaProducto {
        return $this->lineaProducto1;
    }

    public function setLineaProducto1(?LineaProducto $lineaProducto1): self {
        $this->lineaProducto1 = $lineaProducto1;

        return $this;
    }

    public function getLineaProducto2(): ?LineaProducto {
        return $this->lineaProducto2;
    }

    public function setLineaProducto2(?LineaProducto $lineaProducto2): self {
        $this->lineaProducto2 = $lineaProducto2;

        return $this;
    }

    public function getImagenMainSliderMobile1(): ?string {
        return $this->imagenMainSliderMobile1 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSliderMobile1 : '';
    }

    public function setImagenMainSliderMobile1(?string $imagenMainSliderMobile1): self {
        $this->imagenMainSliderMobile1 = $imagenMainSliderMobile1;

        return $this;
    }

    public function getImagenMainSliderMobile2(): ?string {
        return $this->imagenMainSliderMobile2 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSliderMobile2 : '';
    }

    public function setImagenMainSliderMobile2(?string $imagenMainSliderMobile2): self {
        $this->imagenMainSliderMobile2 = $imagenMainSliderMobile2;

        return $this;
    }

    public function getImagenMainSlider3(): ?string {
        return $this->imagenMainSlider3 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSlider3 : '';
    }

    public function setImagenMainSlider3(?string $imagenMainSlider3): self {
        $this->imagenMainSlider3 = $imagenMainSlider3;

        return $this;
    }

    public function getImagenMainSliderMobile3(): ?string {
        return $this->imagenMainSliderMobile3 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagenMainSliderMobile3 : '';
    }

    public function setImagenMainSliderMobile3(?string $imagenMainSliderMobile3): self {
        $this->imagenMainSliderMobile3 = $imagenMainSliderMobile3;

        return $this;
    }

    public function getCategoria3(): ?Categoria {
        return $this->categoria3;
    }

    public function setCategoria3(?Categoria $categoria3): self {
        $this->categoria3 = $categoria3;

        return $this;
    }

    public function getSubcategoria3(): ?Subcategoria {
        return $this->subcategoria3;
    }

    public function setSubcategoria3(?Subcategoria $subcategoria3): self {
        $this->subcategoria3 = $subcategoria3;

        return $this;
    }

    public function getLineaProducto3(): ?LineaProducto {
        return $this->lineaProducto3;
    }

    public function setLineaProducto3(?LineaProducto $lineaProducto3): self {
        $this->lineaProducto3 = $lineaProducto3;

        return $this;
    }

    public function getCostoFijoShipNow(): ?float {
        return $this->costoFijoShipNow;
    }

    public function setCostoFijoShipNow(float $costoFijoShipNow): self {
        $this->costoFijoShipNow = $costoFijoShipNow;

        return $this;
    }

    public function getDemoraShipNow() {
        return $this->demoraShipNow;
    }

    public function setDemoraShipNow($demoraShipNow): void {
        $this->demoraShipNow = $demoraShipNow;
    }

    public function getDemoraGrabado() {
        return $this->demoraGrabado;
    }

    public function setDemoraGrabado($demoraGrabado): void {
        $this->demoraGrabado = $demoraGrabado;
    }

    public function getMostrarFechaShipNow(): ?bool {
        return $this->mostrarFechaShipNow;
    }

    public function setMostrarFechaShipNow(bool $mostrarFechaShipNow): self {
        $this->mostrarFechaShipNow = $mostrarFechaShipNow;

        return $this;
    }

}
