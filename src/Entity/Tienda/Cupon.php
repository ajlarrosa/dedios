<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\CuponRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Entity\Categoria;
use App\Entity\Common\Common;
use App\Entity\Producto;
use App\Entity\Subcategoria;
use App\Entity\Tienda\LineaProducto;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=CuponRepository::class)
 * @ORM\Table(
 *      name="cupon",
 *      uniqueConstraints={@ORM\UniqueConstraint(columns={"codigo"})}
 * )
 * @UniqueEntity(
 *      fields={"codigo"},
 *      message="This code has already been used."
 * )
 */
class Cupon extends Common {

    const PORCENTAJE = 'Percentage of total';
    const MONTO = 'Fixed amount';
    const ENVIO = 'Shipment';
    const COD_PORCENTAJE = 1;
    const COD_MONTO = 2;
    const COD_ENVIO = 3;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $codigo;

    /**
     * @ORM\Column(type="integer")
     */
    private $tipoDescuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\Column(type="float", nullable=true))
     */
    private $montoFijo;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThanOrEqual(
     *     message="This value can't be lower than -1.",
     *     value=-1)
     */
    private $usos;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaDesde;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaHasta;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $montoMinimo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="cupones")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="cupones")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="cupones")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="cupones")
     * @ORM\JoinColumn(name="lineaproducto_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tienda\Pedido", mappedBy="cupon")     
     */
    protected $pedidos;

    public function __construct() {
        $this->pedidos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getCodigo();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCodigo(): ?string {
        return $this->codigo;
    }

    public function setCodigo(string $Codigo): self {
        $this->codigo = $Codigo;

        return $this;
    }

    public function getTipoDescuento() {
        switch ($this->tipoDescuento) {
            case $this::COD_PORCENTAJE:
                return $this::PORCENTAJE;
            case $this::COD_MONTO:
                return $this::MONTO;
            case $this::COD_ENVIO:
                return $this::ENVIO;
            default:
                return null;
        }
    }

    public function setTipoDescuento(int $tipoDescuento): self {
        $this->tipoDescuento = $tipoDescuento;

        return $this;
    }

    public function getUsos(): ?int {
        return $this->usos;
    }

    public function setUsos(int $usos): self {
        $this->usos = $usos;

        return $this;
    }

    public function getFechaDesde(): ?\DateTimeInterface {
        return $this->fechaDesde;
    }

    public function setFechaDesde(?\DateTimeInterface $fechaDesde): self {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    public function getFechaHasta(): ?\DateTimeInterface {
        return $this->fechaHasta;
    }

    public function setFechaHasta(?\DateTimeInterface $fechaHasta): self {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getCategoria(): ?Categoria {
        return $this->categoria;
    }

    public function getSubcategoria(): ?Subcategoria {
        return $this->subcategoria;
    }

    public function getLineaProducto(): ?LineaProducto {
        return $this->lineaProducto;
    }

    public function setCategoria($categoria): self {
        $this->categoria = $categoria;

        return $this;
    }

    public function setSubcategoria($subcategoria): self {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    public function setLineaProducto($lineaProducto): self {
        $this->lineaProducto = $lineaProducto;

        return $this;
    }

    public function getDescuento(): ?float {
        return $this->descuento;
    }

    public function setDescuento($descuento): self {
        $this->descuento = $descuento;

        return $this;
    }

    public function getMontoFijo(): ?float {
        return $this->montoFijo;
    }

    public function setMontoFijo($montoFijo): self {
        $this->montoFijo = $montoFijo;

        return $this;
    }

    public function getMontoMinimo(): ?float {
        return $this->montoMinimo;
    }

    public function setMontoMinimo($montoMinimo): self {
        $this->montoMinimo = $montoMinimo;

        return $this;
    }

    /**
     * @return Collection|Pedidos[]
     */
    public function getPedidos(): Collection {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setCupon($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);

            if ($pedido->getCupon() === $this) {
                $pedido->setCupon(null);
            }
        }

        return $this;
    }
    
    public function restarUso(){
        if ($this->usos >= 1){
            $this->usos --;
            
            return true;
        }
        
        return false;                
    }

}
