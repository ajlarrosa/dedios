<?php

namespace App\Entity\Tienda;

use App\Entity\Color;
use App\Entity\Letra;
use App\Entity\Medida;
use App\Entity\Producto;
use App\Repository\Tienda\DetallePedidoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\DetallePedidoRepository::class)
 */
class DetallePedido extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="detallespedido")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id")
     */
    protected $pedido;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="detallespedido")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Color")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    protected $color;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Letra")
     * @ORM\JoinColumn(name="letra_id", referencedColumnName="id")
     */
    protected $letra;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Medida")
     * @ORM\JoinColumn(name="medida_id", referencedColumnName="id")
     */
    protected $medida;

    /**
     * @ORM\Column(type="float",nullable=true)
     */
    protected $precio;

    /**
     * @ORM\Column(type="float")
     */
    protected $cantidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $productodesc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $grabado;

    /**
     * @ORM\Column(type="float",nullable=true)
     */
    protected $precioGrabado;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $letra = '';
        if ($this->letra) {
            $letra = ' - ' . $this->letra->getLetraString();
        }
        $color = '';
        if ($this->color) {
            $color = ' - ' . $this->color->getColorString();
        }
        $medida = '';
        if ($this->medida) {
            $medida = ' - ' . $this->medida->getMedidaString();
        }
        $grabado = '';
        if (trim($this->grabado) != '') {
            $grabado = ' - Grabado: "' . $this->grabado . '"';
        }

        return $this->productodesc . $this->getCharacteristics(' - ');
    }

    /*
     * Get charactersitics
     */

    public function getCharacteristics($separator, $grabadoDetalle = true) {
        $letra = '';
        if ($this->letra) {
            $letra = $separator . $this->letra->getLetraString();
        }
        $color = '';
        if ($this->color) {
            $color = $separator . $this->color->getColorString();
        }
        $medida = '';
        if ($this->medida) {
            $medida = $separator . $this->medida->getMedidaString();
        }
        $grabado = '';
        if (trim($this->grabado) != '') {
            if ($grabadoDetalle) {
                $grabado = $separator . 'Grabado: "' . $this->grabado . '"';
            } else {
                $grabado = $separator . 'grabado';
            }
        }
        return $medida . $color . $letra . $grabado;
    }

    /*
     * Get External Reference
     */

    public function getExternalReference() {
        return $this->getProducto()->getCodigo().'_'.$this->getCharacteristics('_', false);
    }

    public function getTotal() {
        return round($this->cantidad * $this->getPrecio(), 2);
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getPrecio(): ?float {
        if (trim($this->grabado) != '') {
            return $this->precio + $this->precioGrabado;
        }

        return $this->precio;
    }

    public function setPrecio(float $precio): self {
        $this->precio = $precio;

        return $this;
    }

    public function getCantidad(): ?float {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getProductodesc(): ?string {
        return $this->productodesc;
    }

    public function setProductodesc(?string $productodesc): self {
        $this->productodesc = $productodesc;

        return $this;
    }

    public function getPedido(): ?Pedido {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): self {
        $this->pedido = $pedido;

        return $this;
    }

    public function getProducto(): ?\App\Entity\Producto {
        return $this->producto;
    }

    public function setProducto(?\App\Entity\Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getColor(): ?Color {
        return $this->color;
    }

    public function setColor(?Color $color): self {
        $this->color = $color;

        return $this;
    }

    public function getMedida(): ?Medida {
        return $this->medida;
    }

    public function setMedida(?Medida $medida): self {
        $this->medida = $medida;

        return $this;
    }

    public function getLetra(): ?Letra {
        return $this->letra;
    }

    public function setLetra(?Letra $letra): self {
        $this->letra = $letra;

        return $this;
    }

    public function getGrabado(): ?string {
        return $this->grabado;
    }

    public function setGrabado(?string $grabado): self {
        $this->grabado = $grabado;

        return $this;
    }

    public function getPrecioGrabado(): ?float {
        return $this->precioGrabado;
    }

    public function setPrecioGrabado(?float $precioGrabado): self {
        $this->precioGrabado = $precioGrabado;

        return $this;
    }

}
