<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\MailingRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;
use App\Entity\User;
use App\Entity\Tienda\ClienteWeb;

/**
 * @ORM\Entity(repositoryClass=MailingRepository::class)
 */
class Mailing extends Common
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\ClienteWeb")
     * @ORM\JoinColumn(name="clienteweb_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteWeb;
    

    public function getId(): ?int
    {
        return $this->id;
    }
    
    
    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        
        if($this->clienteWeb!=null){
            $this->setClienteWeb(null);
        }
        
        $this->user = $user;

        return $this;
    }
    
    public function getClienteWeb(): ?ClienteWeb {
        return $this->clienteWeb;
    }

    public function setClienteWeb(?ClienteWeb $clienteWeb): self {
        
        if($this->user!=null){
            $this->setUser(null);
        }
        
        $this->clienteWeb = $clienteWeb;

        return $this;
    }

}
