<?php

namespace App\Entity\Tienda;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\FormatoMailRepository::class)
 * @UniqueEntity(
 *     fields={"type","configuracion"},
 *     errorPath="type",
 *     message="This configuration already has this mail format."
 * )
 */
class FormatoMail extends Common {

    const CONFIRM_USER = 'Confirm User';
    const CONFIRM_MINORISTA = 'Confirm Retail';
    const CONFIRM_CLIENTE_WEB = 'Confirm Web Client';
    const CONFIRM_MAYORISTA = 'Confirm Wholesaler';
    const CONFIRM_MAIL_RETAIL = 'Confirm Mail Retail';
    const CONFIRM_MAIL_WHOLESALER = 'Confirm Mail Wholesaler';
    const CONFIRM_PAYMENT_CLIENT = 'Confirm Payment Client';
    const CONFIRM_ORDER_TAKE_AWAY = 'Confirm Order Take away';
    const REMINDER_UNCONFIRMED_ORDER = 'Reminder Unconfirmed Order Payment';
    const COD_CONFIRM_USER = 1;
    const COD_CONFIRM_MINORISTA = 2;
    const COD_CONFIRM_CLIENTE_WEB = 3;
    const COD_CONFIRM_MAYORISTA = 4;
    const COD_CONFIRM_MAIL_RETAIL = 5;
    const COD_CONFIRM_PAYMENT_CLIENT = 6;
    const COD_CONFIRM_MAIL_WHOLESALER = 7;
    const COD_CONFIRM_ORDER_TAKE_AWAY = 8;
    const COD_REMINDER_UNCONFIRMED_ORDER = 9;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", options={"default" : 1})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=4000)
     */
    private $title;

    /**
     * @ORM\Column(type="blob")
     */
    private $body;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    private $footer;

    /**
     * @ORM\ManyToOne(targetEntity="Configuracion", inversedBy="formatoMail")
     * @ORM\JoinColumn(name="configuracion_id", referencedColumnName="id")     
     */
    protected $configuracion;

    public function getTypeString() {
        switch ($this->getType()) {
            case self::COD_CONFIRM_USER:
                return self::CONFIRM_USER;
                
            case self::COD_CONFIRM_MINORISTA:
                return self::CONFIRM_MINORISTA;

            case self::COD_CONFIRM_CLIENTE_WEB:
                return self::CONFIRM_CLIENTE_WEB;

            case self::COD_CONFIRM_MAYORISTA:
                return self::CONFIRM_MAYORISTA;

            case self::COD_CONFIRM_MAIL_WHOLESALER:
                return self::CONFIRM_MAIL_WHOLESALER;
                
            case self::COD_CONFIRM_MAIL_RETAIL:
                return self::CONFIRM_MAIL_RETAIL;

            case self::COD_CONFIRM_PAYMENT_CLIENT:
                return self::CONFIRM_PAYMENT_CLIENT;
                
            case self::COD_CONFIRM_ORDER_TAKE_AWAY:
                return self::CONFIRM_ORDER_TAKE_AWAY;
                
            case self::COD_REMINDER_UNCONFIRMED_ORDER:
                return self::REMINDER_UNCONFIRMED_ORDER;    
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string {
        if (is_resource($this->body)) {
            return stream_get_contents($this->body);
        } else {
            return $this->body;
        }
    }

    public function setBody(string $body): self {
        $this->body = $body;

        return $this;
    }

    public function getFooter(): ?string {
        return $this->footer;
    }

    public function setFooter(?string $footer): self {
        $this->footer = $footer;

        return $this;
    }

    public function getConfiguracion(): ?Configuracion {
        return $this->configuracion;
    }

    public function setConfiguracion(?Configuracion $configuracion): self {
        $this->configuracion = $configuracion;

        return $this;
    }

}
