<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\PromocionRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\PromocionRepository::class)
 * @UniqueEntity(
 *     fields={"descripcion"},
 *     errorPath="descripcion",
 *     message="The promo already exists."
 * )
 */
class Promocion extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Description cannot exceed {{ limit }} characters"
     * )   
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Configuracion", mappedBy="promocion")
     */
    protected $configuraciones;

    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $color;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $visibleMayorista = false;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->configuraciones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->descripcion;
    }

    public function getVisibleMayoristaString():  ?string {
        return $this->visibleMayorista ? 'Visible' : 'No visible';         
    }
    
    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getColor(): ?string {
        return $this->color;
    }

    public function setColor(string $color): self {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Configuracion[]
     */
    public function getConfiguraciones(): Collection {
        return $this->configuraciones;
    }

    public function addConfiguracion(Configuracion $configuracion): self {
        if (!$this->configuraciones->contains($configuracion)) {
            $this->configuraciones[] = $configuracion;
            $configuracion->setPromocion($this);
        }

        return $this;
    }

    public function removeConfiguracion(Configuracion $configuracion): self {
        if ($this->configuraciones->contains($configuracion)) {
            $this->configuraciones->removeElement($configuracion);
            // set the owning side to null (unless already changed)
            if ($configuracion->getPromocion() === $this) {
                $configuracion->setPromocion(null);
            }
        }

        return $this;
    }

    public function getVisibleMayorista(): ?bool
    {
        return $this->visibleMayorista;
    }

    public function setVisibleMayorista(bool $visibleMayorista): self
    {
        $this->visibleMayorista = $visibleMayorista;

        return $this;
    }

    public function addConfiguracione(Configuracion $configuracione): self
    {
        if (!$this->configuraciones->contains($configuracione)) {
            $this->configuraciones[] = $configuracione;
            $configuracione->setPromocion($this);
        }

        return $this;
    }

    public function removeConfiguracione(Configuracion $configuracione): self
    {
        if ($this->configuraciones->contains($configuracione)) {
            $this->configuraciones->removeElement($configuracione);
            // set the owning side to null (unless already changed)
            if ($configuracione->getPromocion() === $this) {
                $configuracione->setPromocion(null);
            }
        }

        return $this;
    }

}
