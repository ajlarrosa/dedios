<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\PromotionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Categoria;
use App\Entity\Common\Common;
use App\Entity\Producto;
use App\Entity\Subcategoria;
use App\Entity\Tienda\LineaProducto;

/**
 * @ORM\Entity(repositoryClass=PromotionRepository::class)
 * @ORM\Table(
 *      name="promotion",
 *      uniqueConstraints={@ORM\UniqueConstraint(columns={"nombre"})}
 * )
 * @UniqueEntity(
 *      fields={"nombre"},
 *      message="This name has already been used."
 * )
 */
class Promotion extends Common {

    const BENEFICIO_3x2 = '3x2';
    const BENEFICIO_2x1 = '2x1';
    const DESCUENTO_CANTIDAD = 'Descuento por Cantidad';
    const ENVIO_CERO = 'Envio Cero';
    const TYPE_PRODUCTO = 'PRODUCTO';
    const TYPE_CATEGORIA = 'CATEGORIA';
    const TYPE_SUBCATEGORIA = 'SUBCATEGORIA';
    const TYPE_LINEA_PRODUCTO = 'LINEA PRODUCTO';
    const TYPE_ALL = 'TODOS';
    const TYPE_GRUPO = 'GRUPO';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string",length=50)
     */
    private $tipoDescuento;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(
     *     message="This value can't be lower than 0.",
     *     value=0)
     */
    private $cantidadMinima;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaDesde;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaHasta;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $porcentaje = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="promotions")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id",nullable=true)
     */
    protected $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="promotions")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="promotions")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="promotions")
     * @ORM\JoinColumn(name="lineaproducto_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto;

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre();
    }

    /*
     * Get Types
     */
    public function getTypes() {
        $types = [];        
        if (!is_null($this->producto)) {
            $type['type']=self::TYPE_PRODUCTO;
            $type['id']= $this->producto->getId();
            $types[]=$type;            
        }
        if (!is_null($this->categoria)) {
            $type['type']= self::TYPE_CATEGORIA;
            $type['id']= $this->categoria->getId();
            $types[]=$type;
            
        }
        if (!is_null($this->subcategoria)) {
            $type['type']= self::TYPE_SUBCATEGORIA;
            $type['id']= $this->subcategoria->getId();
            $types[]=$type;            
        }
        if (!is_null($this->lineaProducto)) {
            $type['type']= self::TYPE_LINEA_PRODUCTO;
            $type['id']= $this->lineaProducto->getId();
            $types[]=$type;            
        }
        return $types;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipoDescuento(): ?string {
        return $this->tipoDescuento;
    }

    public function setTipoDescuento(string $tipoDescuento): self {
        $this->tipoDescuento = $tipoDescuento;

        return $this;
    }

    public function getCantidadMinima(): ?int {
        return $this->cantidadMinima;
    }

    public function setCantidadMinima(?int $cantidadMinima): self {
        $this->cantidadMinima = $cantidadMinima;

        return $this;
    }

    public function getFechaDesde(): ?\DateTimeInterface {
        return $this->fechaDesde;
    }

    public function setFechaDesde(?\DateTimeInterface $fechaDesde): self {
        $this->fechaDesde = $fechaDesde;

        return $this;
    }

    public function getFechaHasta(): ?\DateTimeInterface {
        return $this->fechaHasta;
    }

    public function setFechaHasta(?\DateTimeInterface $fechaHasta): self {
        $this->fechaHasta = $fechaHasta;

        return $this;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

    public function getCategoria(): ?Categoria {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self {
        $this->categoria = $categoria;

        return $this;
    }

    public function getSubcategoria(): ?Subcategoria {
        return $this->subcategoria;
    }

    public function setSubcategoria(?Subcategoria $subcategoria): self {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    public function getLineaProducto(): ?LineaProducto {
        return $this->lineaProducto;
    }

    public function setLineaProducto(?LineaProducto $lineaProducto): self {
        $this->lineaProducto = $lineaProducto;

        return $this;
    }

    public function getPorcentaje(): ?float {
        return $this->porcentaje;
    }

    public function setPorcentaje(float $porcentaje): self {
        $this->porcentaje = $porcentaje;

        return $this;
    }

}
