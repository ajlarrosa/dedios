<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\ClienteWebRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\ClienteWebRepository::class)
 */
class ClienteWeb extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     *      @Assert\Length(
     *      max = 100,     
     *      maxMessage = "E-mail cannot exceed {{ limit }} characters"
     * )   
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\Length(
     *      max = 200,     
     *      maxMessage = "Name cannot exceed {{ limit }} characters"
     * )   
     */
    protected $nombre;
    
     /**
     * @ORM\Column(type="string", length=200, nullable=false)
     * @Assert\Length(
     *      max = 200,     
     *      maxMessage = "Surname cannot exceed {{ limit }} characters"
     * )   
     */
    protected $apellido;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "Telephone number cannot exceed {{ limit }} characters"
     * )   
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(
     *      max = 50,     
     *      maxMessage = "Dni cannot exceed {{ limit }} characters"
     * )   
     */
    protected $dni;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="clienteWeb")
     */
    protected $pedidos;

    /**
     * @ORM\OneToMany(targetEntity="DireccionEnvio", mappedBy="clienteWeb")
     */
    protected $direccionesEnvio;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->pedidos = new ArrayCollection();
        $this->direccionesEnvio = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre();
    }

    public function getDireccion($tipoDireccion) {
        $direccionesEnvio = $this->getDireccionesEnvio();
        foreach ($direccionesEnvio as $direccionEnvio) {
            if ($direccionEnvio->getTipoDireccion()==$tipoDireccion){
                return $direccionEnvio;
            }            
        }
        return false;
    }
    /*
     * 
     */
    public function getTiposDireccion(){
        $tiposDireccion=[];
        foreach ($this->getDireccionesEnvio() as $direccionEnvio) {
            if ($direccionEnvio->getEnabled()){
                $tiposDireccion[]=$direccionEnvio->getTipoDireccion();
            }
        }
        return $tiposDireccion;        
    }   

    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTelefono(): ?string {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self {
        $this->telefono = $telefono;

        return $this;
    }

    public function getDni(): ?string {
        return $this->dni;
    }

    public function setDni(string $dni): self {
        $this->dni = $dni;

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setClienteWeb($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getClienteWeb() === $this) {
                $pedido->setClienteWeb(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DireccionEnvio[]
     */
    public function getDireccionesEnvio(): Collection {
        return $this->direccionesEnvio;
    }

    public function addDireccionesEnvio(DireccionEnvio $direccionesEnvio): self {
        if (!$this->direccionesEnvio->contains($direccionesEnvio)) {
            $this->direccionesEnvio[] = $direccionesEnvio;
            $direccionesEnvio->setClienteWeb($this);
        }

        return $this;
    }

    public function removeDireccionesEnvio(DireccionEnvio $direccionesEnvio): self {
        if ($this->direccionesEnvio->contains($direccionesEnvio)) {
            $this->direccionesEnvio->removeElement($direccionesEnvio);
            // set the owning side to null (unless already changed)
            if ($direccionesEnvio->getClienteWeb() === $this) {
                $direccionesEnvio->setClienteWeb(null);
            }
        }

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

}
