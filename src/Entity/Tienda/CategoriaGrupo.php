<?php

namespace App\Entity\Tienda;

use App\Entity\Categoria;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\CategoriaGrupoRepository::class)
 */
class CategoriaGrupo extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

   /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="categoriasGrupo")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")     
     */
    protected $categoria;

   /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="categoriasGrupo")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")     
     */
    protected $grupo;

    public function getItem(){
        return $this->categoria;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

}
