<?php

namespace App\Entity\Tienda;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Tienda\MercadoPagoRepository")
 * @ORM\Table(name="mercadopago")
 */
class MercadoPago extends Common{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
  
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $preference_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $collection_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $collection_status;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $external_reference;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $payment_type;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $merchant_order_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $site_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $processing_mode;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $merchant_account_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $payment_id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="Pedido", inversedBy="mercadosPagos")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id",nullable=true)
     */
    protected $pedido;

        /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $pagoImputadoCC=FALSE;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
      
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set preferenceId
     *
     * @param string $preferenceId
     *
     * @return MercadoPago
     */
    public function setPreferenceId($preferenceId) {
        $this->preference_id = $preferenceId;

        return $this;
    }

    /**
     * Get preferenceId
     *
     * @return string
     */
    public function getPreferenceId() {
        return $this->preference_id;
    }

    /**
     * Set collectionId
     *
     * @param string $collectionId
     *
     * @return MercadoPago
     */
    public function setCollectionId($collectionId) {
        $this->collection_id = $collectionId;

        return $this;
    }

    /**
     * Get collectionId
     *
     * @return string
     */
    public function getCollectionId() {
        return $this->collection_id;
    }

    /**
     * Set collectionStatus
     *
     * @param string $collectionStatus
     *
     * @return MercadoPago
     */
    public function setCollectionStatus($collectionStatus) {
        $this->collection_status = $collectionStatus;

        return $this;
    }

    /**
     * Get collectionStatus
     *
     * @return string
     */
    public function getCollectionStatus() {
        return $this->collection_status;
    }

    /**
     * Set externalReference
     *
     * @param string $externalReference
     *
     * @return MercadoPago
     */
    public function setExternalReference($externalReference) {
        $this->external_reference = $externalReference;

        return $this;
    }

    /**
     * Get externalReference
     *
     * @return string
     */
    public function getExternalReference() {
        return $this->external_reference;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return MercadoPago
     */
    public function setPaymentType($paymentType) {
        $this->payment_type = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType() {
        return $this->payment_type;
    }

    /**
     * Set merchantOrderId
     *
     * @param string $merchantOrderId
     *
     * @return MercadoPago
     */
    public function setMerchantOrderId($merchantOrderId) {
        $this->merchant_order_id = $merchantOrderId;

        return $this;
    }

    /**
     * Get merchantOrderId
     *
     * @return string
     */
    public function getMerchantOrderId() {
        return $this->merchant_order_id;
    }

    /**
     * Set siteId
     *
     * @param string $siteId
     *
     * @return MercadoPago
     */
    public function setSiteId($siteId) {
        $this->site_id = $siteId;

        return $this;
    }

    /**
     * Get siteId
     *
     * @return string
     */
    public function getSiteId() {
        return $this->site_id;
    }

    /**
     * Set processingMode
     *
     * @param string $processingMode
     *
     * @return MercadoPago
     */
    public function setProcessingMode($processingMode) {
        $this->processing_mode = $processingMode;

        return $this;
    }

    /**
     * Get processingMode
     *
     * @return string
     */
    public function getProcessingMode() {
        return $this->processing_mode;
    }

    /**
     * Set merchantAccountId
     *
     * @param string $merchantAccountId
     *
     * @return MercadoPago
     */
    public function setMerchantAccountId($merchantAccountId) {
        $this->merchant_account_id = $merchantAccountId;

        return $this;
    }

    /**
     * Get merchantAccountId
     *
     * @return string
     */
    public function getMerchantAccountId() {
        return $this->merchant_account_id;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     *
     * @return MercadoPago
     */
    public function setPaymentId($paymentId) {
        $this->payment_id = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId() {
        return $this->payment_id;
    }

    public function getPedido(): ?Pedido
    {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): self
    {
        $this->pedido = $pedido;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPagoImputadoCC(): ?bool
    {
        return $this->pagoImputadoCC;
    }

    public function setPagoImputadoCC(bool $pagoImputadoCC): self
    {
        $this->pagoImputadoCC = $pagoImputadoCC;

        return $this;
    }


}
