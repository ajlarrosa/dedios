<?php

namespace App\Entity\Tienda;

use App\Entity\Common\Common;
use App\Repository\Tienda\FormaPagoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\FormaPagoRepository::class) 
 * @ORM\Table(name="formapago")
 */
class FormaPago extends Common {

    const MERCADOPAGO_ID = 1;
    const EFECTIVO_ID = 2;
    const ARREGLA_DEDIOS_ID = 3;
    const TRANSFERENCIA_ID = 4;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)   
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "La descripción debe tener al menos de {{ limit }} caracteres",
     *      maxMessage = "La descripción no puede superar los {{ limit }} caracteres"
     * ) 
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float")
     * @Assert\PositiveOrZero       
     */
    protected $porcentajeDescuento = 0;

    /**
     * @ORM\Column(type="float")   
     * @Assert\PositiveOrZero
     */
    protected $porcentajeRecargo = 0;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="formaPago")
     */
    protected $pedidos;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->pedidos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }
    /*
     * Get Forma Pago With Discount Recharge
     */
    public function getFormaPagoWithDiscountRecharge() {
        $recargo = '';
        $descuento = '';
        
        if ($this->porcentajeRecargo > 0) {
            $recargo = '<span class="badge badge-dark">+ '.$this->porcentajeRecargo.'%</span>';
        }
        
        if ($this->porcentajeDescuento > 0) {
             $descuento= '<span class="badge badge-dark">'.$this->porcentajeDescuento.'% OFF</span>';            
        }

        if ($this->getId() == self::MERCADOPAGO_ID) {
            $descripcion = 'Tarjeta de débito/crédito (Mercado Pago)';
        } else {
            $descripcion = $this->getDescripcion();
        }

        return $descripcion . $recargo . $descuento;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPorcentajeDescuento(): ?float {
        return $this->porcentajeDescuento;
    }

    public function setPorcentajeDescuento(float $porcentajeDescuento): self {
        $this->porcentajeDescuento = $porcentajeDescuento;

        return $this;
    }

    public function getPorcentajeRecargo(): ?float {
        return $this->porcentajeRecargo;
    }

    public function setPorcentajeRecargo(float $porcentajeRecargo): self {
        $this->porcentajeRecargo = $porcentajeRecargo;

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setFormaPago($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getFormaPago() === $this) {
                $pedido->setFormaPago(null);
            }
        }

        return $this;
    }

}
