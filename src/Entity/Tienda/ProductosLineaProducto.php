<?php

namespace App\Entity\Tienda;

use App\Entity\Producto;
use App\Entity\Tienda\ProductosLineaProducto;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\ProducotsLineaProductoRepository::class)
 */
class ProductosLineaProducto extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

   /**
     * @ORM\ManyToOne(targetEntity="LineaProducto", inversedBy="productosLineaProducto")
     * @ORM\JoinColumn(name="lineaproducto_id", referencedColumnName="id")     
     */
    protected $lineaProducto;

   /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="productosLineaProducto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")     
     */
    protected $producto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLineaProducto(): ?LineaProducto
    {
        return $this->lineaProducto;
    }

    public function setLineaProducto(?LineaProducto $lineaProducto): self
    {
        $this->lineaProducto = $lineaProducto;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }



}
