<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\SeccionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=SeccionRepository::class)
 */
class Seccion extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $imagen;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $imageSize;

    /**
     * @ORM\OneToMany(targetEntity="SeccionConfiguracion", mappedBy="seccion")
     */
    protected $seccionConfiguracion;
    
    protected $rootDir = 'uploads/seccion';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->seccionConfiguracion = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getImagen() {
        return $this->imagen != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagen : '';
    }

    public function setImagen(string $imagen): self {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return Collection|SeccionConfiguracion[]
     */
    public function getSeccionConfiguracion(): Collection {
        return $this->seccionConfiguracion;
    }

    public function addSeccionConfiguracion(SeccionConfiguracion $seccionConfiguracion): self {
        if (!$this->seccionConfiguracion->contains($seccionConfiguracion)) {
            $this->seccionConfiguracion[] = $seccionConfiguracion;
            $seccionConfiguracion->setSeccion($this);
        }

        return $this;
    }

    public function removeSeccionConfiguracion(SeccionConfiguracion $seccionConfiguracion): self {
        if ($this->seccionConfiguracion->contains($seccionConfiguracion)) {
            $this->seccionConfiguracion->removeElement($seccionConfiguracion);
            // set the owning side to null (unless already changed)
            if ($seccionConfiguracion->getSeccion() === $this) {
                $seccionConfiguracion->setSeccion(null);
            }
        }

        return $this;
    }

    public function getImageSize(): ?string
    {
        return $this->imageSize;
    }

    public function setImageSize(?string $imageSize): self
    {
        $this->imageSize = $imageSize;

        return $this;
    }

}
