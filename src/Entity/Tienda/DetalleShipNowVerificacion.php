<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\DetalleShipNowVerificacionRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Recordatorio;
use App\Entity\Tienda\Pedido;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\DetalleShipNowVerificacionRepository::class) 
 * @ORM\Table(name="detalleshipnowverificacion")
 */
class DetalleShipNowVerificacion {

    const ERROR = 'Error en el envío';
    const OK = 'Envío exitoso';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ShipNowVerificacion", inversedBy="detallesShipNowVerificacion")
     * @ORM\JoinColumn(name="ship_now_verificacion_id", referencedColumnName="id")
     */
    private $shipNowVerificacion;

    /**
     * @ORM\ManyToOne(targetEntity="Pedido")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id", nullable=false)
     * */
    protected $pedido;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $resultado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResultado(): ?string
    {
        return $this->resultado;
    }

    public function setResultado(?string $resultado): self
    {
        $this->resultado = $resultado;

        return $this;
    }

    public function getShipNowVerificacion(): ?ShipNowVerificacion
    {
        return $this->shipNowVerificacion;
    }

    public function setShipNowVerificacion(?ShipNowVerificacion $shipNowVerificacion): self
    {
        $this->shipNowVerificacion = $shipNowVerificacion;

        return $this;
    }

    public function getPedido(): ?Pedido
    {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): self
    {
        $this->pedido = $pedido;

        return $this;
    }


}
