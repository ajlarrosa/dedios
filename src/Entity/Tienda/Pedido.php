<?php

namespace App\Entity\Tienda;

use App\Entity\ClienteProveedor;
use App\Entity\ShipNow\ShipNow;
use App\Entity\User;
use App\Repository\Tienda\PedidoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\PedidoRepository::class)
 */
class Pedido extends Common {    
    const INICIADO = 'Iniciado';
    const PENDIENTE = 'Pendiente';
    const ACTIVO = 'Activo';
    const ENTREGADO = 'Entregado';
    const NO_ENTREGADO = 'No Entregado';
    const CERRADO = 'Cerrado';
    const ABANDONADO = 'Abandonado';
    const SHIP_NOW = 'Ship now';
    const STORE = 'Store';
    const OFFICE = 'Office';
    const ARREGLAR_ENVIO = 'Arrange delivery';        

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="pedidos", fetch="EAGER")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\ClienteWeb", inversedBy="pedidos", fetch="EAGER")
     * @ORM\JoinColumn(name="clienteweb_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteWeb;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)     
     */
    protected $tipoEnvio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClienteProveedor", inversedBy="pedidos")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="id", nullable=true)
     */
    protected $store;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\DireccionEnvio", inversedBy="pedidos", fetch="EAGER")
     * @ORM\JoinColumn(name="direccionenvio_id", referencedColumnName="id")
     */
    protected $direccionEnvio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\DireccionEnvio", fetch="EAGER")
     * @ORM\JoinColumn(name="direccionfacturacion_id", referencedColumnName="id", nullable=true)
     */
    protected $direccionFacturacion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\Cupon", inversedBy="pedidos", fetch="EAGER")
     * @ORM\JoinColumn(name="cupon_id", referencedColumnName="id", nullable=true)
     */
    protected $cupon;    
    
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $costoEnvio;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $recargo = 0;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $observacion;

    /**
     * @ORM\ManyToOne(targetEntity="FormaPago", inversedBy="pedidos")
     * @ORM\JoinColumn(name="formapago_id", referencedColumnName="id", nullable=true)
     */
    protected $formaPago;

    /**
     * @ORM\OneToMany(targetEntity="DetallePedido", mappedBy="pedido", cascade={"persist", "remove"} )
     */
    protected $detallespedido;

    /**
     * @ORM\OneToMany(targetEntity="MercadoPago", mappedBy="pedido")
     */
    protected $mercadosPagos;

    /**
     * @ORM\Column(type="string", length=20,nullable=false)
     */
    protected $estado = self::PENDIENTE;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $pagado = FALSE;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $hash;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $shipNowServiceId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShipNow\ShipNow", mappedBy="pedido")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    protected $shipsNow;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 0})
     */
    protected $envioGenerado = FALSE;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $deviceCheckout;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    protected $recordatoriosEnviados = 0;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $ultimoRecordatorio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaPagado;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $descripcionPromocion;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    protected $montoPromocion = 0;

    public function __construct() {                        
        $this->tipoEnvio = (is_null($this->getUser()) || $this->getUser()->hasRole('ROLE_USER_MINORISTA')) ? self::OFFICE : $this->tipoEnvio = self::ARREGLAR_ENVIO;
        
        $this->detallespedido = new ArrayCollection();
        $this->mercadosPagos = new ArrayCollection();
        $this->shipsNow = new ArrayCollection();
    }

    /**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        $this->id;
    }
    
    public function getExternalReference() {
        return is_null($this->getFecha()) ? $this->getId() : $this->getId() . ' - ' . $this->getFecha()->format('d-m-Y H:i:s');
    }

    public function getValorDescuento() {
        return round(($this->getTotal() - $this->montoPromocion) * ( $this->descuento / 100));
    }

    public function getValorRecargo() {
        return round(($this->getTotal() - $this->montoPromocion) * ( $this->recargo / 100));
    }

    public function getTotal() {
        $total = 0;
        foreach ($this->detallespedido as $detallePedido) {
            $total = $total + $detallePedido->getTotal();
        }

        return $total;
    }

    public function getEntity() {
        return ($this->user) ? $this->user : $this->clienteWeb;
    }

    public function getEmail() {
        return ($this->user) ? $this->user->getMail() : $this->clienteWeb->getEMail();
    }

    public function getNombre() {
        return ($this->user) ? $this->user->getNombre() : $this->clienteWeb->getNombre();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCostoEnvio(): ?float {
        return $this->costoEnvio;
    }

    public function setCostoEnvio(?float $costoEnvio): self {
        $this->costoEnvio = $costoEnvio;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

    public function getObservacion(): ?string {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self {
        $this->observacion = $observacion;

        return $this;
    }

    public function getEstado(): ?string {
        return $this->estado;
    }

    public function setEstado(string $estado): self {
        $this->estado = $estado;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|DetallePedido[]
     */
    public function getDetallespedido(): Collection {
        return $this->detallespedido;
    }

    public function addDetallespedido(DetallePedido $detallespedido): self {
        if (!$this->detallespedido->contains($detallespedido)) {
            $this->detallespedido[] = $detallespedido;
            $detallespedido->setPedido($this);
        }

        return $this;
    }

    public function removeDetallespedido(DetallePedido $detallespedido): self {
        if ($this->detallespedido->contains($detallespedido)) {
            $this->detallespedido->removeElement($detallespedido);
            // set the owning side to null (unless already changed)
            if ($detallespedido->getPedido() === $this) {
                $detallespedido->setPedido(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MercadoPago[]
     */
    public function getMercadosPagos(): Collection {
        return $this->mercadosPagos;
    }

    public function addMercadosPago(MercadoPago $mercadosPago): self {
        if (!$this->mercadosPagos->contains($mercadosPago)) {
            $this->mercadosPagos[] = $mercadosPago;
            $mercadosPago->setPedido($this);
        }

        return $this;
    }

    public function removeMercadosPago(MercadoPago $mercadosPago): self {
        if ($this->mercadosPagos->contains($mercadosPago)) {
            $this->mercadosPagos->removeElement($mercadosPago);
            // set the owning side to null (unless already changed)
            if ($mercadosPago->getPedido() === $this) {
                $mercadosPago->setPedido(null);
            }
        }

        return $this;
    }

    public function getFormaPago(): ?FormaPago {
        return $this->formaPago;
    }

    public function setFormaPago(?FormaPago $formaPago): self {
        $this->formaPago = $formaPago;

        return $this;
    }

    public function getDescuento(): ?float {
        return $this->descuento;
    }

    public function setDescuento(float $descuento): self {
        $this->descuento = $descuento;

        return $this;
    }

    public function getRecargo(): ?float {
        return $this->recargo;
    }

    public function setRecargo(float $recargo): self {
        $this->recargo = $recargo;

        return $this;
    }

    public function getPagado(): ?bool {
        return $this->pagado;
    }

    public function setPagado(bool $pagado): self {
        $this->pagado = $pagado;

        return $this;
    }

    public function getClienteWeb(): ?ClienteWeb {
        return $this->clienteWeb;
    }

    public function setClienteWeb(?ClienteWeb $clienteWeb): self {
        $this->clienteWeb = $clienteWeb;

        return $this;
    }

    public function getStore(): ?ClienteProveedor {
        return $this->store;
    }

    public function setStore(?ClienteProveedor $store): self {
        $this->store = $store;

        return $this;
    }

    public function getTipoEnvio(): ?string {
        return $this->tipoEnvio;
    }

    public function setTipoEnvio(string $tipoEnvio): self {
        $this->tipoEnvio = $tipoEnvio;

        return $this;
    }

    public function getDireccionEnvio(): ?DireccionEnvio {
        return $this->direccionEnvio;
    }

    public function setDireccionEnvio(?DireccionEnvio $direccionEnvio): self {
        $this->direccionEnvio = $direccionEnvio;

        return $this;
    }

    public function getDireccionFacturacion(): ?DireccionEnvio {
        return $this->direccionFacturacion;
    }

    public function setDireccionFacturacion(?DireccionEnvio $direccionFacturacion): self {
        $this->direccionFacturacion = $direccionFacturacion;

        return $this;
    }

    public function getCupon(): ?Cupon {
        return $this->cupon;
    }

    public function setCupon(?Cupon $cupon): self {
        $this->cupon = $cupon;

        return $this;
    }    
    
    public function getHash(): ?string {
        return $this->hash;
    }

    public function setHash(?string $hash): self {
        $this->hash = $hash;

        return $this;
    }

    public function getShipNowServiceId(): ?int {
        return $this->shipNowServiceId;
    }

    public function setShipNowServiceId(?int $shipNowServiceId): self {
        $this->shipNowServiceId = $shipNowServiceId;

        return $this;
    }

    /**
     * @return Collection|ShipNow[]
     */
    public function getShipsNow(): Collection {
        return $this->shipsNow;
    }

    public function addShipsNow(ShipNow $shipsNow): self {
        if (!$this->shipsNow->contains($shipsNow)) {
            $this->shipsNow[] = $shipsNow;
            $shipsNow->setPedido($this);
        }

        return $this;
    }

    public function removeShipsNow(ShipNow $shipsNow): self {
        if ($this->shipsNow->contains($shipsNow)) {
            $this->shipsNow->removeElement($shipsNow);
            // set the owning side to null (unless already changed)
            if ($shipsNow->getPedido() === $this) {
                $shipsNow->setPedido(null);
            }
        }

        return $this;
    }

    public function getEnvioGenerado(): ?bool {
        return $this->envioGenerado;
    }

    public function setEnvioGenerado(bool $envioGenerado): self {
        $this->envioGenerado = $envioGenerado;

        return $this;
    }

    public function getDeviceCheckout() {
        return $this->deviceCheckout;
    }

    public function setDeviceCheckout($deviceCheckout): void {
        $this->deviceCheckout = $deviceCheckout;
    }

    public function getRecordatoriosEnviados() {
        return $this->recordatoriosEnviados;
    }

    public function getUltimoRecordatorio() {
        return $this->ultimoRecordatorio;
    }

    public function setRecordatoriosEnviados($recordatoriosEnviados): void {
        $this->recordatoriosEnviados = $recordatoriosEnviados;
    }

    public function setUltimoRecordatorio($ultimoRecordatorio): void {
        $this->ultimoRecordatorio = $ultimoRecordatorio;
    }

    public function getFechaPagado(): ?\DateTimeInterface {
        return $this->fechaPagado;
    }

    public function setFechaPagado(\DateTimeInterface $fechaPagado): self {
        $this->fechaPagado = $fechaPagado;

        return $this;
    }

    public function getDescripcionPromocion(): ?string {
        return $this->descripcionPromocion;
    }

    public function setDescripcionPromocion(?string $descripcionPromocion): self {
        $this->descripcionPromocion = $descripcionPromocion;

        return $this;
    }

    public function getMontoPromocion(): ?float {
        return $this->montoPromocion;
    }

    public function setMontoPromocion(float $montoPromocion): self {
        $this->montoPromocion = $montoPromocion;

        return $this;
    }

    public function getDireccionEnvioByType(string $type): ?DireccionEnvio {
        foreach ($this->getEntity()->getDireccionesEnvio() as $direccionEnvio) {
            if ($direccionEnvio->getTipoDireccion() === $type) {
                return $direccionEnvio;
            }
        }
        
        return null;
    }

    public function getDireccionFacturacionActiva(): ?DireccionEnvio {        
        return is_null($this->getDireccionFacturacion()) ? $this->getDireccionEnvioByType(DireccionEnvio::CASA) : $this->getDireccionFacturacion();
    }

}
