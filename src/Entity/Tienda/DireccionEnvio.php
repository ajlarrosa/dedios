<?php

namespace App\Entity\Tienda;

use App\Entity\User;
use App\Repository\Tienda\DireccionEnvioRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\DireccionEnvioRepository::class)
 */
class DireccionEnvio extends Common {

    const CASA = 'Casa';
    const OFICINA = 'Oficina';
    const DIRECCION_1 = 'Dirección 1';
    const DIRECCION_2 = 'Dirección 2';
    const DIRECCION_3 = 'Dirección 3';
    const DIRECCION_FACTURACION  = 'Dirección Facturación';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="direccionesEnvio")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\ClienteWeb", inversedBy="direccionesEnvio")
     * @ORM\JoinColumn(name="clienteweb_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteWeb;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(
     *      max = 50,     
     *      maxMessage = "Type of adress cannot exceed {{ limit }} characters"
     * )   
     */
    protected $tipo_direccion = self::CASA;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      max = 100,     
     *      maxMessage = "Street number cannot exceed {{ limit }} characters"
     * )   
     */
    protected $calle_numero;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Street cannot exceed {{ limit }} characters"
     * )   
     */
    protected $calle;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Province cannot exceed {{ limit }} characters"
     * )   
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "Locality cannot exceed {{ limit }} characters"
     * )   
     */
    protected $localidad;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $departamento;

    /**
     * @ORM\Column(type="integer")
     */
    protected $zipCode;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $post_office_id;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     * @Assert\Length(
     *      max = 50,     
     *      maxMessage = "Floot cannot exceed {{ limit }} characters"
     * )   
     */
    protected $piso;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     * @Assert\Length(
     *      max = 50,     
     *      maxMessage = "Functional unit cannot exceed {{ limit }} characters"
     * )   
     */
    protected $unidadFuncional;

    /**
      * @ORM\Column(type="string", length=255)
     */
    protected $provinciaId;

    /**
      * @ORM\Column(type="string", length=255)
     */
    protected $localidadId;

    /**
      * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $departamentoId;

    /**
      * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $calleId;

    /**
     * @ORM\OneToMany(targetEntity="Pedido", mappedBy="direccionEnvio")
     */
    protected $pedidos;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->pedidos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getAdrressOneLine();        
    }
    
    /*
     * Get Adress in one Line
     */
    public function getAdrressOneLine(){
        $direccion=$this->calle . " N°" . $this->calle_numero;
        
        if (!is_null($this->piso)){
            $direccion .=" - Piso " . $this->piso;
        }
        
        if (!is_null($this->unidadFuncional)){
            $direccion .=" Unidad Funcional " . $this->unidadFuncional;
        }
        
        $direccion .= " - " . $this->provincia ;
        
        if (!is_null($this->departamento)){
            $direccion .=", " . $this->departamento;
        }
        
        $direccion .= ", " . $this->localidad . ", CP" . $this->zipCode;
        
        return $direccion;             
    }
    
    public function getId(): ?int {
        return $this->id;
    }

    public function getCalleNumero(): ?string {
        return $this->calle_numero;
    }

    public function setCalleNumero(string $calle_numero): self {
        $this->calle_numero = $calle_numero;

        return $this;
    }

    public function getCalle(): ?string {
        return $this->calle;
    }

    public function setCalle(string $calle): self {
        $this->calle = $calle;

        return $this;
    }

    public function getProvincia(): ?string {
        return $this->provincia;
    }

    public function setProvincia(string $provincia): self {
        $this->provincia = $provincia;

        return $this;
    }

    public function getLocalidad(): ?string {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self {
        $this->localidad = $localidad;

        return $this;
    }

    public function getDepartamento(): ?string {
        return $this->departamento;
    }

    public function setDepartamento(?string $departamento): self {
        $this->departamento = $departamento;

        return $this;
    }

    public function getZipCode(): ?int {
        return $this->zipCode;
    }

    public function setZipCode(int $zipCode): self {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return Collection|Pedido[]
     */
    public function getPedidos(): Collection {
        return $this->pedidos;
    }

    public function addPedido(Pedido $pedido): self {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos[] = $pedido;
            $pedido->setClienteWeb($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): self {
        if ($this->pedidos->contains($pedido)) {
            $this->pedidos->removeElement($pedido);
            // set the owning side to null (unless already changed)
            if ($pedido->getClienteWeb() === $this) {
                $pedido->setClienteWeb(null);
            }
        }

        return $this;
    }

    public function getPiso(): ?string {
        return $this->piso;
    }

    public function setPiso(?string $piso): self {
        $this->piso = $piso;

        return $this;
    }

    public function getUnidadFuncional(): ?string {
        return $this->unidadFuncional;
    }

    public function setUnidadFuncional(?string $unidadFuncional): self {
        $this->unidadFuncional = $unidadFuncional;

        return $this;
    }

    public function getTipoDireccion(): ?string {
        return $this->tipo_direccion;
    }

    public function setTipoDireccion(string $tipo_direccion): self {
        $this->tipo_direccion = $tipo_direccion;

        return $this;
    }

    public function getPostOfficeId(): ?int {
        return $this->post_office_id;
    }

    public function setPostOfficeId(?int $post_office_id): self {
        $this->post_office_id = $post_office_id;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getClienteWeb(): ?ClienteWeb {
        return $this->clienteWeb;
    }

    public function setClienteWeb(?ClienteWeb $clienteWeb): self {
        $this->clienteWeb = $clienteWeb;

        return $this;
    }

    public function getProvinciaId(): ?string
    {
        return $this->provinciaId;
    }

    public function setProvinciaId(string $provinciaId): self
    {
        $this->provinciaId = $provinciaId;

        return $this;
    }

    public function getLocalidadId(): ?string
    {
        return $this->localidadId;
    }

    public function setLocalidadId(string $localidadId): self
    {
        $this->localidadId = $localidadId;

        return $this;
    }

    public function getDepartamentoId(): ?string
    {
        return $this->departamentoId;
    }

    public function setDepartamentoId(?string $departamentoId): self
    {
        $this->departamentoId = $departamentoId;

        return $this;
    }

    public function getCalleId(): ?string
    {
        return $this->calleId;
    }

    public function setCalleId(?string $calleId): self
    {
        $this->calleId = $calleId;

        return $this;
    }
   
}
