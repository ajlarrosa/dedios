<?php

namespace App\Entity\Tienda;

use App\Entity\Producto;
use App\Repository\Tienda\ProductoLineaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\ProductoLineaRepository::class)
 */
class ProductoLinea {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="LineaProducto", inversedBy="productosLinea")
     * @ORM\JoinColumn(name="lineaproducto_id", referencedColumnName="id")
     */
    protected $lineaProducto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="productosLinea")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    protected $producto;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
      
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->id();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getLineaProducto(): ?LineaProducto
    {
        return $this->lineaProducto;
    }

    public function setLineaProducto(?LineaProducto $lineaProducto): self
    {
        $this->lineaProducto = $lineaProducto;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

  
}
