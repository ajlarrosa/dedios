<?php

namespace App\Entity\Tienda;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\LineaProductoGrupoRepository::class)
 */
class LineaProductoGrupo extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

   /**
     * @ORM\ManyToOne(targetEntity="LineaProducto", inversedBy="lineaProductosGrupo")
     * @ORM\JoinColumn(name="lineaproducto_id", referencedColumnName="id")     
     */
    protected $lineaProducto;

   /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="lineaProductosGrupo")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")     
     */
    protected $grupo;


    public function getItem(){
        return $this->lineaProducto;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }


    public function getLineaProducto(): ?LineaProducto
    {
        return $this->lineaProducto;
    }

    public function setLineaProducto(LineaProducto $lineaProducto): self
    {
        $this->lineaProducto = $lineaProducto;

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setGrupo(Grupo $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }


}
