<?php

namespace App\Entity\Tienda;

use App\Entity\Categoria;
use App\Entity\Producto;
use App\Entity\Subcategoria;
use App\Repository\Tienda\SeccionConfiguracionRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=SeccionConfiguracionRepository::class)
 */
class SeccionConfiguracion extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Seccion", inversedBy="seccionConfiguracion")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id")     
     */
    protected $seccion;

    /**
     * @ORM\ManyToOne(targetEntity="Configuracion", inversedBy="seccionConfiguracion")
     * @ORM\JoinColumn(name="configuracion_id", referencedColumnName="id")     
     */
    protected $configuracion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria")
     * @ORM\JoinColumn(name="subcategoria1_id", referencedColumnName="id",nullable=true)     
     */
    protected $subcategoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria")
     * @ORM\JoinColumn(name="subcategoria2_id", referencedColumnName="id",nullable=true)     
     */
    protected $subcategoria2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria")
     * @ORM\JoinColumn(name="categoria1_id", referencedColumnName="id",nullable=true)     
     */
    protected $categoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria")
     * @ORM\JoinColumn(name="categoria2_id", referencedColumnName="id",nullable=true)     
     */
    protected $categoria2;

    /**
     * @ORM\ManyToOne(targetEntity="LineaProducto")
     * @ORM\JoinColumn(name="lineaproducto1_id", referencedColumnName="id",nullable=true)     
     */
    protected $lineaProducto1;

    /**
     * @ORM\ManyToOne(targetEntity="LineaProducto")
     * @ORM\JoinColumn(name="lineaproducto2_id", referencedColumnName="id",nullable=true)     
     */
    protected $lineaproducto2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto1_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto2_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto3_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto4_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto5_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto")
     * @ORM\JoinColumn(name="producto6_id", referencedColumnName="id",nullable=true)     
     */
    protected $producto6;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $imagen2;

    /**
     * @ORM\Column(type="integer", options={"default" : 1000})
     */
    protected $posicion = 1000;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $titulo1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $titulo2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $subtitulo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $descripcion;
    
    
    protected $rootDir = 'uploads/seccion_config';
    

    public function getId(): ?int {
        return $this->id;
    }

    public function getImagen1() {
        return $this->imagen1 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagen1 : '';
    }

    public function setImagen1(string $imagen1): self {
        $this->imagen1 = $imagen1;

        return $this;
    }
    
    public function getImagen2() {
        return $this->imagen2 != '' ? $this->rootDir.'/'.$this->id.'/'.$this->imagen2 : '';
    }

    public function setImagen2(string $imagen2): self {
        $this->imagen2 = $imagen2;

        return $this;
    }

    public function getPosicion(): ?int {
        return $this->posicion;
    }

    public function setPosicion(int $posicion): self {
        $this->posicion = $posicion;

        return $this;
    }

    public function getTitulo1(): ?string {
        return $this->titulo1;
    }

    public function setTitulo1(?string $titulo1): self {
        $this->titulo1 = $titulo1;

        return $this;
    }

    public function getTitulo2(): ?string {
        return $this->titulo2;
    }

    public function setTitulo2(?string $titulo2): self {
        $this->titulo2 = $titulo2;

        return $this;
    }

    public function getSubtitulo(): ?string {
        return $this->subtitulo;
    }

    public function setSubtitulo(?string $subtitulo): self {
        $this->subtitulo = $subtitulo;

        return $this;
    }

    public function getSeccion(): ?Seccion {
        return $this->seccion;
    }

    public function setSeccion(?Seccion $seccion): self {
        $this->seccion = $seccion;

        return $this;
    }

    public function getConfiguracion(): ?Configuracion {
        return $this->configuracion;
    }

    public function setConfiguracion(?Configuracion $configuracion): self {
        $this->configuracion = $configuracion;

        return $this;
    }

    public function getSubcategoria1(): ?Subcategoria {
        return $this->subcategoria1;
    }

    public function setSubcategoria1(?Subcategoria $subcategoria1): self {
        $this->subcategoria1 = $subcategoria1;

        return $this;
    }

    public function getSubcategoria2(): ?Subcategoria {
        return $this->subcategoria2;
    }

    public function setSubcategoria2(?Subcategoria $subcategoria2): self {
        $this->subcategoria2 = $subcategoria2;

        return $this;
    }

    public function getCategoria1(): ?Categoria {
        return $this->categoria1;
    }

    public function setCategoria1(?Categoria $categoria1): self {
        $this->categoria1 = $categoria1;

        return $this;
    }

    public function getCategoria2(): ?Categoria {
        return $this->categoria2;
    }

    public function setCategoria2(?Categoria $categoria2): self {
        $this->categoria2 = $categoria2;

        return $this;
    }

    public function getLineaProducto1(): ?LineaProducto {
        return $this->lineaProducto1;
    }

    public function setLineaProducto1(?LineaProducto $lineaProducto1): self {
        $this->lineaProducto1 = $lineaProducto1;

        return $this;
    }

    public function getLineaproducto2(): ?LineaProducto {
        return $this->lineaproducto2;
    }

    public function setLineaproducto2(?LineaProducto $lineaproducto2): self {
        $this->lineaproducto2 = $lineaproducto2;

        return $this;
    }

    public function getProducto1(): ?Producto {
        return $this->producto1;
    }

    public function setProducto1(?Producto $producto1): self {
        $this->producto1 = $producto1;

        return $this;
    }

    public function getProducto2(): ?Producto {
        return $this->producto2;
    }

    public function setProducto2(?Producto $producto2): self {
        $this->producto2 = $producto2;

        return $this;
    }

    public function getProducto3(): ?Producto {
        return $this->producto3;
    }

    public function setProducto3(?Producto $producto3): self {
        $this->producto3 = $producto3;

        return $this;
    }

    public function getProducto4(): ?Producto {
        return $this->producto4;
    }

    public function setProducto4(?Producto $producto4): self {
        $this->producto4 = $producto4;

        return $this;
    }

    public function getProducto5(): ?Producto {
        return $this->producto5;
    }

    public function setProducto5(?Producto $producto5): self {
        $this->producto5 = $producto5;

        return $this;
    }

    public function getProducto6(): ?Producto {
        return $this->producto6;
    }

    public function setProducto6(?Producto $producto6): self {
        $this->producto6 = $producto6;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

}
