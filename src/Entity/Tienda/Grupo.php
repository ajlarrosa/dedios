<?php

namespace App\Entity\Tienda;

use App\Repository\Tienda\GrupoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Tienda\Menu;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=GrupoRepository::class)
 */
class Grupo extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="grupos")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")     
     */
    protected $menu;

    /**
     * @ORM\OneToMany(targetEntity="LineaProductoGrupo", mappedBy="grupo")
     */
    protected $lineaProductosGrupo;

    /**
     * @ORM\OneToMany(targetEntity="CategoriaGrupo", mappedBy="grupo")
     */
    protected $categoriasGrupo;

    /**
     * @ORM\OneToMany(targetEntity="CategoriasubcategoriaGrupo", mappedBy="grupo")
     */
    protected $categoriasubcategoriasGrupo;

    /**
     * @ORM\OneToMany(targetEntity="ProductoGrupo", mappedBy="grupo")
     */
    protected $productosGrupo;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosGrupo = new ArrayCollection();
        $this->categoriasubcategoriasGrupo = new ArrayCollection();
        $this->categoriasGrupo = new ArrayCollection();
        $this->lineaProductosGrupo = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getListaActiva() {
        if (sizeof($this->productosGrupo) > 0) {
            return $this->productosGrupo;
        }
        if (sizeof($this->categoriasubcategoriasGrupo) > 0) {
            return $this->categoriasubcategoriasGrupo;
        }
        if (sizeof($this->categoriasGrupo) > 0) {
            return $this->categoriasGrupo;
        }
        if (sizeof($this->lineaProductosGrupo) > 0) {
            return $this->lineaProductosGrupo;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getMenu(): ?Menu {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self {
        $this->menu = $menu;

        return $this;
    }

    /**
     * @return Collection|LineaProductoGrupo[]
     */
    public function getLineaProductosGrupo(): Collection {
        return $this->lineaProductosGrupo;
    }

    public function addLineaProductosGrupo(LineaProductoGrupo $lineaProductosGrupo): self {
        if (!$this->lineaProductosGrupo->contains($lineaProductosGrupo)) {
            $this->lineaProductosGrupo[] = $lineaProductosGrupo;
            $lineaProductosGrupo->setGrupo($this);
        }

        return $this;
    }

    public function removeLineaProductosGrupo(LineaProductoGrupo $lineaProductosGrupo): self {
        if ($this->lineaProductosGrupo->contains($lineaProductosGrupo)) {
            $this->lineaProductosGrupo->removeElement($lineaProductosGrupo);
            // set the owning side to null (unless already changed)
            if ($lineaProductosGrupo->getGrupo() === $this) {
                $lineaProductosGrupo->setGrupo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategoriaGrupo[]
     */
    public function getCategoriasGrupo(): Collection {
        return $this->categoriasGrupo;
    }

    public function addCategoriasGrupo(CategoriaGrupo $categoriasGrupo): self {
        if (!$this->categoriasGrupo->contains($categoriasGrupo)) {
            $this->categoriasGrupo[] = $categoriasGrupo;
            $categoriasGrupo->setGrupo($this);
        }

        return $this;
    }

    public function removeCategoriasGrupo(CategoriaGrupo $categoriasGrupo): self {
        if ($this->categoriasGrupo->contains($categoriasGrupo)) {
            $this->categoriasGrupo->removeElement($categoriasGrupo);
            // set the owning side to null (unless already changed)
            if ($categoriasGrupo->getGrupo() === $this) {
                $categoriasGrupo->setGrupo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductoGrupo[]
     */
    public function getProductosGrupo(): Collection {
        return $this->productosGrupo;
    }

    public function addProductosGrupo(ProductoGrupo $productosGrupo): self {
        if (!$this->productosGrupo->contains($productosGrupo)) {
            $this->productosGrupo[] = $productosGrupo;
            $productosGrupo->setGrupo($this);
        }

        return $this;
    }

    public function removeProductosGrupo(ProductoGrupo $productosGrupo): self {
        if ($this->productosGrupo->contains($productosGrupo)) {
            $this->productosGrupo->removeElement($productosGrupo);
            // set the owning side to null (unless already changed)
            if ($productosGrupo->getGrupo() === $this) {
                $productosGrupo->setGrupo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CategoriasubcategoriaGrupo[]
     */
    public function getCategoriasubcategoriasGrupo(): Collection
    {
        return $this->categoriasubcategoriasGrupo;
    }

    public function addCategoriasubcategoriasGrupo(CategoriasubcategoriaGrupo $categoriasubcategoriasGrupo): self
    {
        if (!$this->categoriasubcategoriasGrupo->contains($categoriasubcategoriasGrupo)) {
            $this->categoriasubcategoriasGrupo[] = $categoriasubcategoriasGrupo;
            $categoriasubcategoriasGrupo->setGrupo($this);
        }

        return $this;
    }

    public function removeCategoriasubcategoriasGrupo(CategoriasubcategoriaGrupo $categoriasubcategoriasGrupo): self
    {
        if ($this->categoriasubcategoriasGrupo->contains($categoriasubcategoriasGrupo)) {
            $this->categoriasubcategoriasGrupo->removeElement($categoriasubcategoriasGrupo);
            // set the owning side to null (unless already changed)
            if ($categoriasubcategoriasGrupo->getGrupo() === $this) {
                $categoriasubcategoriasGrupo->setGrupo(null);
            }
        }

        return $this;
    }

}
