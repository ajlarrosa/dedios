<?php

namespace App\Entity\Tienda;

use App\Entity\Categoria;
use App\Entity\Categoriasubcategoria;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\CategoriasubcategoriaGrupoRepository::class)
 */
class CategoriasubcategoriaGrupo extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoriasubcategoria", inversedBy="categoriasubcategoriasGrupo")
     * @ORM\JoinColumn(name="subcategoria_id", referencedColumnName="id")     
     */
    protected $categoriasubcategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="categoriasubcategoriasGrupo")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")     
     */
    protected $grupo;

    public function getItem() {
        return $this->categoriasubcategoria;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getGrupo(): ?Grupo {
        return $this->grupo;
    }

    public function setGrupo(?Grupo $grupo): self {
        $this->grupo = $grupo;

        return $this;
    }

    public function getCategoriasubcategoria(): ?Categoriasubcategoria
    {
        return $this->categoriasubcategoria;
    }

    public function setCategoriasubcategoria(?Categoriasubcategoria $categoriasubcategoria): self
    {
        $this->categoriasubcategoria = $categoriasubcategoria;

        return $this;
    }


}
