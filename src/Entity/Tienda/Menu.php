<?php

namespace App\Entity\Tienda;

use App\Entity\Categoria;
use App\Entity\Common\Common;
use App\Entity\Producto;
use App\Entity\Subcategoria;
use App\Repository\Tienda\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Tienda\MenuRepository::class) 
 * @ORM\Table(name="menu")
 */
class Menu extends Common {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=15)   
     * @Assert\Length(
     *      min = 2,
     *      max = 15,
     *      minMessage = "La descripción debe tener al menos de {{ limit }} caracteres",
     *      maxMessage = "La descripción no puede superar los {{ limit }} caracteres"
     * ) 
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $imagen;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)       
     * @Assert\Length(   
     *      max = 100,     
     *      maxMessage = "La descripción no puede superar los {{ limit }} caracteres"
     * )  
     */
    protected $texto1;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)    
     * @Assert\Length(     
     *      max = 100,
     *      maxMessage = "La descripción no puede superar los {{ limit }} caracteres"
     * )      
     */
    protected $texto2;
       
    /**
     * @ORM\ManyToOne(targetEntity="Configuracion", inversedBy="menus")
     * @ORM\JoinColumn(name="configuracion_id", referencedColumnName="id")     
     */
    protected $configuracion;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="menu1")
     * @ORM\JoinColumn(name="producto1_id", referencedColumnName="id",nullable=true)
     */
    protected $producto1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="menu2")
     * @ORM\JoinColumn(name="producto2_id", referencedColumnName="id",nullable=true)
     */
    protected $producto2;
    
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="menu1")
     * @ORM\JoinColumn(name="categoria1_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="menu2")
     * @ORM\JoinColumn(name="categoria2_id", referencedColumnName="id",nullable=true)
     */
    protected $categoria2;
    
        /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="menuCategoria")
     * @ORM\JoinColumn(name="categoriamenu_id", referencedColumnName="id",nullable=true)
     */
    protected $categoriaMenu;

    
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="menu1")
     * @ORM\JoinColumn(name="subcategoria1_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="menu2")
     * @ORM\JoinColumn(name="subcategoria2_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoria2;
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subcategoria", inversedBy="menuSubcategoria")
     * @ORM\JoinColumn(name="subcategoriamenu_id", referencedColumnName="id",nullable=true)
     */
    protected $subcategoriaMenu;
    
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="menu1")
     * @ORM\JoinColumn(name="lineaproducto1_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="menu2")
     * @ORM\JoinColumn(name="lineaproducto2_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProducto2;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\LineaProducto", inversedBy="menuLineaProducto")
     * @ORM\JoinColumn(name="lineaproductomenu_id", referencedColumnName="id",nullable=true)
     */
    protected $lineaProductoMenu;
       
    /**
     * @ORM\OneToMany(targetEntity="Grupo", mappedBy="menu")
     * @Assert\Count(max=3)
     * @Assert\Count(min=1)
     */
    protected $grupos;
    
     /**
     * @ORM\Column(type="integer", nullable=false, options={"default" : 100}))
     */
    protected $orden=100;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->grupos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getId() {
        return $this->id;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function getTexto1() {
        return $this->texto1;
    }

    public function getTexto2() {
        return $this->texto2;
    }

    public function getConfiguracion() {
        return $this->configuracion;
    }

    public function getCategoria1() {
        return $this->categoria1;
    }

    public function getCategoria2() {
        return $this->categoria2;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setImagen($imagen) {
        $this->imagen = $imagen;
    }

    public function setTexto1($texto1) {
        $this->texto1 = $texto1;
    }

    public function setTexto2($texto2) {
        $this->texto2 = $texto2;
    }

    public function setConfiguracion($configuracion) {
        $this->configuracion = $configuracion;
    }

    /**
     * @return Collection|Grupo[]
     */
    public function getGrupos(): Collection {
        return $this->grupos;
    }

    public function addGrupo(Grupo $grupo): self {
        if (!$this->grupos->contains($grupo)) {
            $this->grupos[] = $grupo;
            $grupo->setMenu($this);
        }

        return $this;
    }

    public function removeGrupo(Grupo $grupo): self {
        if ($this->grupos->contains($grupo)) {
            $this->grupos->removeElement($grupo);
            // set the owning side to null (unless already changed)
            if ($grupo->getMenu() === $this) {
                $grupo->setMenu(null);
            }
        }

        return $this;
    }

    public function getProducto1(): ?Producto
    {
        return $this->producto1;
    }

    public function setProducto1(?Producto $producto1): self
    {
        $this->producto1 = $producto1;

        return $this;
    }

    public function getProducto2(): ?Producto
    {
        return $this->producto2;
    }

    public function setProducto2(?Producto $producto2): self
    {
        $this->producto2 = $producto2;

        return $this;
    }

    public function setCategoria1(?Categoria $categoria1): self
    {
        $this->categoria1 = $categoria1;

        return $this;
    }

    public function setCategoria2(?Categoria $categoria2): self
    {
        $this->categoria2 = $categoria2;

        return $this;
    }

    public function getSubcategoria1(): ?Subcategoria
    {
        return $this->subcategoria1;
    }

    public function setSubcategoria1(?Subcategoria $subcategoria1): self
    {
        $this->subcategoria1 = $subcategoria1;

        return $this;
    }

    public function getSubcategoria2(): ?Subcategoria
    {
        return $this->subcategoria2;
    }

    public function setSubcategoria2(?Subcategoria $subcategoria2): self
    {
        $this->subcategoria2 = $subcategoria2;

        return $this;
    }

    public function getLineaProducto1(): ?LineaProducto
    {
        return $this->lineaProducto1;
    }

    public function setLineaProducto1(?LineaProducto $lineaProducto1): self
    {
        $this->lineaProducto1 = $lineaProducto1;

        return $this;
    }

    public function getLineaProducto2(): ?LineaProducto
    {
        return $this->lineaProducto2;
    }

    public function setLineaProducto2(?LineaProducto $lineaProducto2): self
    {
        $this->lineaProducto2 = $lineaProducto2;

        return $this;
    }

    public function getCategoriaMenu(): ?Categoria
    {
        return $this->categoriaMenu;
    }

    public function setCategoriaMenu(?Categoria $categoriaMenu): self
    {
        $this->categoriaMenu = $categoriaMenu;

        return $this;
    }

    public function getSubcategoriaMenu(): ?Subcategoria
    {
        return $this->subcategoriaMenu;
    }

    public function setSubcategoriaMenu(?Subcategoria $subcategoriaMenu): self
    {
        $this->subcategoriaMenu = $subcategoriaMenu;

        return $this;
    }

    public function getLineaProductoMenu(): ?LineaProducto
    {
        return $this->lineaProductoMenu;
    }

    public function setLineaProductoMenu(?LineaProducto $lineaProductoMenu): self
    {
        $this->lineaProductoMenu = $lineaProductoMenu;

        return $this;
    }

    public function getOrden()
    {
        return $this->orden;
    }

    public function setOrden($orden): self
    {
        $this->orden = $orden;

        return $this;
    }

}
