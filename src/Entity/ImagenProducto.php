<?php

namespace App\Entity;

use App\ImagenProductoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ImagenProductoRepository::class)
 * @ORM\Table(name="imagenproducto")
 */
class ImagenProducto extends Common {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="imagenesproducto")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id", nullable=true)
     */
    protected $producto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(type="integer")
     */
    protected $pos;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    public function getAbsolutePath() {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath() {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->producto->getId() . "/" . $this->path;
    }

    protected function getUploadDir() {
        return 'uploads/documents/productos';
    }

    protected function getUploadRootDir() {
        return __DIR__ . '/../../../../dedios/' . $this->getUploadDir();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDescripcion(): ?string {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPath(): ?string {
        return $this->path;
    }

    public function setPath(?string $path): self {
        $this->path = $path;

        return $this;
    }

    public function getPos(): ?int {
        return $this->pos;
    }

    public function setPos(int $pos): self {
        $this->pos = $pos;

        return $this;
    }

    public function getProducto(): ?Producto {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self {
        $this->producto = $producto;

        return $this;
    }

}
