<?php

namespace App\Entity;

use App\CotizacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\CotizacionRepository::class)
 * @ORM\Table(name="cotizacion")
 */
class Cotizacion {

    const ACTIVO = 'ACTIVE';
    const NO_ACTIVO = 'INACTIVE';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $dolar;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $oro;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $plata;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fecha = new \DateTime('NOW');
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return strval($this->getId());
    }

    public function getActivoString() {
        if ($this->activo) {
            return self::ACTIVO;
        } else {
            return self::NO_ACTIVO;
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getDolar(): ?float {
        return $this->dolar;
    }

    public function setDolar(float $dolar): self {
        $this->dolar = $dolar;

        return $this;
    }

    public function getOro(): ?float {
        return $this->oro;
    }

    public function setOro(float $oro): self {
        $this->oro = $oro;

        return $this;
    }

    public function getPlata(): ?float {
        return $this->plata;
    }

    public function setPlata(float $plata): self {
        $this->plata = $plata;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self {
        $this->fecha = $fecha;

        return $this;
    }

    public function getActivo(): ?bool {
        return $this->activo;
    }

    public function setActivo(bool $activo): self {
        $this->activo = $activo;

        return $this;
    }

}
