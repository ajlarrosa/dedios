<?php

namespace App\Entity;

use App\Repository\DetalleRecordatorioRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Recordatorio;
use App\Entity\Tienda\Pedido;

/**
 * @ORM\Entity(repositoryClass=App\Repository\DetalleRecordatorioRepository::class) 
 * @ORM\Table(name="detallerecordatorio")
 */
class DetalleRecordatorio
{
    const ERROR = 'Error en el envío';
    const OK = 'Envío exitoso';
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Recordatorio", inversedBy="detallesrecordatorio")
     * @ORM\JoinColumn(name="recordatorio_id", referencedColumnName="id")
     */
    private $recordatorio;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\Pedido")
     * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id", nullable=false)
     * */
    protected $pedido;
    
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $resultado;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResultado(): ?string
    {
        return $this->resultado;
    }

    public function setResultado(?string $resultado): self
    {
        $this->resultado = $resultado;

        return $this;
    }
    
    public function getRecordatorio(): ?Recordatorio {
        return $this->recordatorio;
    }

    public function setRecordatorio(?Recordatorio $recordatorio): self {
        $this->recordatorio = $recordatorio;

        return $this;
    }
    
    
    public function getPedido(): ?Pedido {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): self {
        $this->pedido = $pedido;

        return $this;
    }

}
