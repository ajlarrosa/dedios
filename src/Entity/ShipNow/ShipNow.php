<?php

namespace App\Entity\ShipNow;

use App\Entity\Tienda\Pedido;
use App\Repository\ShipNow\ShipNowRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\Common;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ShipNow\ShipNowRepository::class)
 */
class ShipNow extends Common { 

const STATUS_AWAITING_PAYMENT = 'awaiting_payment';
const STATUS_ON_HOLD = 'on_hold';
const STATUS_CANCELLED = 'cancelled';
const STATUS_AWAITING_SHIPMENT = 'awaiting_shipment';
const STATUS_FILTERED = 'filtered';
const STATUS_SHIPPED = 'shipped';
const STATUS_NOT_DELIVERED = 'not_delivered';
const STATUS_DELIVERED = 'delivered';
const STATUS_RETURN = 'return';
const STATUS_READY_TO_PICK = 'ready_to_pick';
const STATUS_PICKING_LIST = 'picking_list';
const STATUS_READY_TO_PACK = 'ready_to_pack';
const STATUS_READY_TO_SHIP = 'ready_to_ship';

/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
protected $id;

/**
 * @ORM\Column(type="text")     
 */
protected $respuesta;

/**
 * @ORM\ManyToOne(targetEntity="App\Entity\Tienda\Pedido", inversedBy="shipsNow")
 * @ORM\JoinColumn(name="pedido_id", referencedColumnName="id", nullable=true)
 */
protected $pedido;

/* * ********************************
 * __construct
 *
 * 
 * ******************************** */

public function __construct() {

}

public function getId(): ?int
{
return $this->id;
}

public function getRespuesta(): ?string
{
return $this->respuesta;
}

public function setRespuesta(string $respuesta): self
{
$this->respuesta = $respuesta;

return $this;
}

public function getPedido(): ?Pedido
{
return $this->pedido;
}

public function setPedido(?Pedido $pedido): self
{
$this->pedido = $pedido;

return $this;
}

}
