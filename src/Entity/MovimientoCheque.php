<?php

namespace App\Entity;

use App\MovimientoChequeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\MovimientoChequeRepository::class)
 * @ORM\Table(name="movimientocheque")
 */
class MovimientoCheque {

    CONST ALTA_MANUAL = 'ALTA MANUAL';
    CONST PAGO = 'PAGO';
    CONST COBRANZA = 'COBRANZA';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cheque")
     * @ORM\JoinColumn(name="cheque_id", referencedColumnName="id", nullable=false)
     */
    protected $cheque;

    /**
     * @ORM\Column(type="string")
     */
    protected $tipoMovimiento;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaMovimiento;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->fechaMovimiento = new \DateTime();
    }

    public function __toString() {
        if ($this->tipoMovimiento == self::ALTA_MANUAL) {
            return self::ALTA_MANUAL;
        }
        if ($this->tipoMovimiento == self::PAGO) {
            return 'RECIBO PROV. ' . $this->cheque->getPago()->getId();
        }
        if ($this->tipoMovimiento == self::COBRANZA) {
            return 'RECIBO PROV. ' . $this->cheque->getPago()->getId();
        }
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getTipoMovimiento(): ?string {
        return $this->tipoMovimiento;
    }

    public function setTipoMovimiento(string $tipoMovimiento): self {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

    public function getFechaMovimiento(): ?\DateTimeInterface {
        return $this->fechaMovimiento;
    }

    public function setFechaMovimiento(?\DateTimeInterface $fechaMovimiento): self {
        $this->fechaMovimiento = $fechaMovimiento;

        return $this;
    }

    public function getCheque(): ?Cheque {
        return $this->cheque;
    }

    public function setCheque(?Cheque $cheque): self {
        $this->cheque = $cheque;

        return $this;
    }

}
