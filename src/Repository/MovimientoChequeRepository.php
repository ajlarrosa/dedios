<?php

namespace App\Repository;

use App\Entity\MovimientoCheque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MovimientoCheque|null find($id, $lockMode = null, $lockVersion = null)
 * @method MovimientoCheque|null findOneBy(array $criteria, array $orderBy = null)
 * @method MovimientoCheque[]    findAll()
 * @method MovimientoCheque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovimientoChequeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovimientoCheque::class);
    }

    // /**
    //  * @return MovimientoCheque[] Returns an array of MovimientoCheque objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MovimientoCheque
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
