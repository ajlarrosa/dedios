<?php

namespace App\Repository;

use App\Entity\Localidad;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Localidad|null find($id, $lockMode = null, $lockVersion = null)
 * @method Localidad|null findOneBy(array $criteria, array $orderBy = null)
 * @method Localidad[]    findAll()
 * @method Localidad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalidadRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Localidad::class);
    }

    /*
     * Filtros de búsqueda de bancos
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }
        if (isset($filtros['provincia']) && $filtros['provincia'] != '') {
            $query->andWhere('b.provincia =:provincia ')
                    ->setParameter(':provincia', '%' . $filtros['provincia'] . '%');
        }
        $query->orderBy('b.descripcion', 'ASC');
        return $query;
    }

}
