<?php

namespace App\Repository;

use App\Entity\ReciboCliente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReciboCliente|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReciboCliente|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReciboCliente[]    findAll()
 * @method ReciboCliente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReciboClienteRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ReciboCliente::class);
    }

    /*
     * Filtros de búsqueda de facturas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');

        if (isset($filtros['nroRecibo']) && $filtros['nroRecibo'] != '') {
            $query->andWhere('b.id = :nroRecibo ')
                    ->setParameter(':nroRecibo', $filtros['nroRecibo']);
        }
        if (isset($filtros['cliente']) && $filtros['cliente'] != '') {
            $query->Join('b.movimientocc', 'mcc');
            $query->andWhere('mcc.clienteProveedor  = :cliente ')
                    ->setParameter(':cliente', $filtros['cliente']);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        if (isset($filtros['fechaDesdeCreated']) && $filtros['fechaDesdeCreated'] != '') {
            $fechaDesdeCreated = new \DateTime($filtros['fechaDesdeCreated']);
            $query->andWhere('b.created >= :fechaDesdeCreated ')
                    ->setParameter(':fechaDesdeCreated', $fechaDesdeCreated);
        }

        if (isset($filtros['fechaHastaCreated']) && $filtros['fechaHastaCreated'] != '') {
            $fechaHastaCreated = new \DateTime($filtros['fechaHastaCreated'] . ' 23:59');
            $query->andWhere(' :fechaHastaCreated >= b.creted')
                    ->setParameter(':fechaHastaCreated', $fechaHastaCreated);
        }


        $query->orderBy('b.id', 'DESC');
        return $query;
    }

}
