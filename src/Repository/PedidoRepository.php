<?php

namespace App\Repository;

use App\Entity\Tienda\Pedido;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PedidoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Pedido::class);
    }

    /*
     * Filtros de búsqueda de productos
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('p')
                ->where('p.estado != :estado')
                ->setParameter(':estado', 'Pendiente');

        if (isset($filtros['id']) && $filtros['id'] != '') {
            $query->andWhere('p.id = :id')
                    ->setParameter(':id', $filtros['id']);
        }

        if (isset($filtros['estado']) && $filtros['estado'] != '') {
            $query->andWhere('p.estado = :estadoFiltrado')
                    ->setParameter(':estadoFiltrado', $filtros['estado']);
        }

        if (isset($filtros['cliente']) && $filtros['cliente'] != '') {
            $query->join('p.user', 'u');
            $query->andWhere('u.cliente = :cliente')
                    ->setParameter(':cliente', $filtros['cliente']);
        }

        if (isset($filtros['clienteWeb']) && $filtros['clienteWeb'] != '') {
            $query->join('p.clienteWeb', 'cw');
            $query->andWhere('cw.nombre like :clienteWeb')
                    ->setParameter(':clienteWeb', '%' . $filtros['clienteWeb'] . '%');
        }

        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('p.created >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= p.created')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        if (isset($filtros['pagado']) && $filtros['pagado'] != '') {
            $query->andWhere('p.pagado = :pagado')
                    ->setParameter(':pagado', $filtros['pagado']);
        }
        
        if (isset($filtros['fechaPagoDesde']) && $filtros['fechaPagoDesde'] != '') {
            $fechaPagoDesde = new \DateTime($filtros['fechaPagoDesde']);
            $query->andWhere('p.fechaPagado >= :fechaPagoDesde ')
                    ->andWhere('p.pagado = 1')
                    ->setParameter(':fechaPagoDesde', $fechaPagoDesde);
        }
     
        if (isset($filtros['fechaPagoHasta']) && $filtros['fechaPagoHasta'] != '') {
            $fechaPagoHasta = new \DateTime($filtros['fechaPagoHasta'] . ' 23:59');
            $query->andWhere(' :fechaPagoHasta >= p.fechaPagado')
                    ->andWhere('p.pagado = 1')
                    ->setParameter(':fechaPagoHasta', $fechaPagoHasta);
        }
        
        if (isset($filtros['tipoCliente']) && $filtros['tipoCliente']!=''){
            
            if ($filtros['tipoCliente']=='mayorista'){                
                $query->Join('p.user', 'u');
                $query->andWhere('u.roles like :tipoCliente')
                        ->setParameter(':tipoCliente', '%ROLE_USER_MAYORISTA%');
            }else {
                $query->leftJoin('p.user', 'u');
                $query->andWhere('u.roles like :tipoCliente or u.id is null ')
                        ->setParameter(':tipoCliente', '%ROLE_USER_MINORISTA%');
            }
        }

        $query->orderBy('p.fecha', 'DESC');
        return $query;
    }

    /*
     * Traer Pendientes de pago
     */

    public function traerPendientesPago() {
        $beforeYesterday = new \DateTime('2 days ago');

        $dia = $beforeYesterday->format('d');
        $mes = $beforeYesterday->format('m');
        $anio = $beforeYesterday->format('Y');

        $query = $this->createQueryBuilder('p')
                ->where('p.pagado = :pagado')
                ->setParameter(':pagado', false)
                ->andWhere('DAY(p.fecha) = :dia')
                ->setParameter(':dia', $dia)
                ->andWhere('MONTH(p.fecha) = :mes')
                ->setParameter(':mes', $mes)
                ->andWhere('YEAR(p.fecha) = :anio')
                ->setParameter(':anio', $anio)
                ->andWhere('p.ultimoRecordatorio IS NULL')
                ->andWhere('p.estado = :estado')
                ->setParameter(':estado', Pedido::ACTIVO);

        return $query->getQuery()->getResult();
    }

    /*
     * Traer Pendientes de entrega
     */

    public function findByPedidosPendientesEntrega() {

        $query = $this->createQueryBuilder('p');
        $query->select('p')
                ->where($query->expr()->notIn("p.estado", ['Cerrado', 'Entregado', 'No entregado']))
                ->join('p.shipsNow', 's')
                ->andWhere('p.tipoEnvio = :tipoEnvio')
                ->setParameter(':tipoEnvio', 'Ship now')
                ->groupBy('p');

        return $query->getQuery()->getResult();
    }

    /*
     * Count pedidos
     */

    public function countPedidos() {

        $fechaDesde= new \DateTime('TODAY');
        $fechaHasta = new \DateTime('NOW');

        $query = $this->createQueryBuilder('p');
        $query->select('count(p)')
                ->andWhere('p.estado != :estado')
                ->setParameter(':estado', 'Pendiente')
                ->andWhere(' :fechaHasta >= p.fecha')
                ->setParameter(':fechaHasta', $fechaHasta)
        ->andWhere(' :fechaDesde <= p.fecha')
                ->setParameter(':fechaDesde', $fechaDesde);

        $result['hoy']=$query->getQuery()->getSingleScalarResult();
        
        $sevenDays = new \DateTime('7 days ago');
        
        $query = $this->createQueryBuilder('p');
        $query->select('count(p)')
                ->andWhere('p.estado != :estado')
                ->setParameter(':estado', 'Pendiente')
                ->andWhere(' :fechaHasta >= p.fecha')
                ->setParameter(':fechaHasta', $fechaHasta)
        ->andWhere(' :fechaDesde <= p.fecha')
                ->setParameter(':fechaDesde', $sevenDays);
        
        $result['7']=$query->getQuery()->getSingleScalarResult();
        
        $thirtyDays = new \DateTime('30 days ago');

        $query = $this->createQueryBuilder('p');
        $query->select('count(p)')
                ->andWhere('p.estado != :estado')
                ->setParameter(':estado', 'Pendiente')
                ->andWhere(' :fechaHasta >= p.fecha')
                ->setParameter(':fechaHasta', $fechaHasta)
        ->andWhere(' :fechaDesde <= p.fecha')
                ->setParameter(':fechaDesde', $thirtyDays);
        
        $result['30']=$query->getQuery()->getSingleScalarResult();
        
        return $result;
    }

}
