<?php

namespace App\Repository;

use App\Entity\CondicionFiscal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CondicionFiscal|null find($id, $lockMode = null, $lockVersion = null)
 * @method CondicionFiscal|null findOneBy(array $criteria, array $orderBy = null)
 * @method CondicionFiscal[]    findAll()
 * @method CondicionFiscal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CondicionFiscalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CondicionFiscal::class);
    }
  /*
     * Filtros de búsqueda de bancos
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('p');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('p.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('p.descripcion', 'ASC');
        return $query;
    }
}
