<?php

namespace App\Repository;

use App\Entity\Cotizacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cotizacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cotizacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cotizacion[]    findAll()
 * @method Cotizacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CotizacionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Cotizacion::class);
    }

    /*
     * Filtros de búsqueda de cotización
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b')->where('1=1');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha <= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'].' 23:59');           
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        $query->orderBy('b.id', 'DESC');

        return $query;
    }

}
