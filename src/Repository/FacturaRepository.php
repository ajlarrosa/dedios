<?php

namespace App\Repository;

use App\Entity\Factura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Factura|null find($id, $lockMode = null, $lockVersion = null)
 * @method Factura|null findOneBy(array $criteria, array $orderBy = null)
 * @method Factura[]    findAll()
 * @method Factura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturaRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Factura::class);
    }

    /*
     * Filtros de búsqueda de facturas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');

        if (isset($filtros['nroFactura']) && $filtros['nroFactura'] != '') {
            $query->andWhere('b.id = :nroFactura ')
                    ->setParameter(':nroFactura', $filtros['nroFactura']);
        }
        if (isset($filtros['cliente']) && $filtros['cliente'] != '') {
            $query->Join('b.movimientocc', 'mcc');
            $query->andWhere('mcc.clienteProveedor  = :cliente ')
                    ->setParameter(':cliente', $filtros['cliente']);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.created >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.created')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        $query->orderBy('b.id', 'DESC');
        return $query;
    }

}
