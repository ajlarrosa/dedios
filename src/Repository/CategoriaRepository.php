<?php

namespace App\Repository;

use App\Entity\Categoria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categoria|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categoria|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categoria[]    findAll()
 * @method Categoria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Categoria::class);
    }

    /*
     * Filtros de búsqueda de categoría
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('b.descripcion', 'ASC');
        return $query;
    }
    
    /*
     * Filtros de búsqueda de categoría
     */

    public function search($search) {
        $query = $this->createQueryBuilder('b');
        $query->where('b.enabled = :activo')
              ->setParameter(':activo', 1)
              ->andWhere('lower(b.descripcion) = :search')
              ->setParameter(':search', strtolower($search));

        return $query->getQuery()
                    ->setMaxResults(1)
                    ->getResult();
    }

}
