<?php

namespace App\Repository;

use App\Entity\DetalleRecordatorio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetalleRecordatorio|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleRecordatorio|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleRecordatorio[]    findAll()
 * @method DetalleRecordatorio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleRecordatorioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleRecordatorio::class);
    }
    
}
