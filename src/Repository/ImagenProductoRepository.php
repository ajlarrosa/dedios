<?php

namespace App\Repository;

use App\Entity\ImagenProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImagenProducto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImagenProducto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImagenProducto[]    findAll()
 * @method ImagenProducto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagenProductoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ImagenProducto::class);
    }

    /*
     * Filtros de búsqueda de imagenes de productos
     */

    public function findFirstImage($productId) {

        $query = $this->createQueryBuilder('i')
                ->where('i.producto = :productoId ')
                ->setParameter(':productoId', $productId)
                ->andWhere('i.enabled = 1 ')
                ->setMaxResults(1)
                ->orderBy('i.pos', 'ASC');

        return $query->getQuery()->getOneOrNullResult();
    }


}
