<?php

namespace App\Repository;

use App\Entity\Subcategoria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Subcategoria|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subcategoria|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subcategoria[]    findAll()
 * @method Subcategoria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubcategoriaRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Subcategoria::class);
    }

    /*
     * Filtros de búsqueda de subcategoría
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        if ((isset($filtros['categoria']) && $filtros['categoria'] != '') || (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '')) {
            $query->Join('App\Entity\Categoriasubcategoria', 'cs', 'WITH', 'cs.subcategoria = b.id');
        }

        //Categoria
        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('cs.categoria = :categoria')
                    ->setParameter(':categoria', $filtros['categoria']);
        }

        //Línea de Producto
        if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {
            $query->Join('App\Entity\Producto', 'p', 'WITH', 'p.categoriasubcategoria = cs.id');
            $query->Join('App\Entity\Tienda\ProductosLineaProducto', 'plp', 'WITH', 'plp.producto = p.id');
            $query->andWhere('plp.lineaProducto = :lineaProducto')
                    ->setParameter(':lineaProducto', $filtros['lineaProducto']);
        }

        if (isset($filtros['order']) && $filtros['order'] != '') {
            if ($filtros['order'] == 'subcategoria') {
                $query->join('cs.subcategoria', 's');
                $query->orderBy('s.descripcion', 'ASC');
            }
        } else {
            $query->orderBy('b.descripcion', 'ASC');
        }

        return $query;
    }

    /*
     * Get Query Filter
     */

    public function getQueryFilter($filtros) {
        $query = $this->filter($filtros);
        return $query->getQuery()->getResult();
    }

    /*
     * Filtros de búsqueda de categoría
     */

    public function search($search) {
        $query = $this->createQueryBuilder('b');
        $query->where('b.enabled = :activo')
              ->setParameter(':activo', 1)
              ->andWhere('lower(b.descripcion) = :search')
              ->setParameter(':search', strtolower($search));

        return $query->getQuery()
                    ->setMaxResults(1)
                    ->getResult();
    }
    
}
