<?php

namespace App\Repository;

use App\Entity\Recordatorio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Recordatorio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recordatorio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recordatorio[]    findAll()
 * @method Recordatorio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordatorioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recordatorio::class);
    }

    /*
     * Filtros de búsqueda de facturas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('r');

        
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('r.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= r.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        $query->orderBy('r.id', 'DESC');
        return $query;
    }
}
