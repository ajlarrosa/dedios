<?php

namespace App\Repository;

use App\Entity\Provincia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Provincia|null find($id, $lockMode = null, $lockVersion = null)
 * @method Provincia|null findOneBy(array $criteria, array $orderBy = null)
 * @method Provincia[]    findAll()
 * @method Provincia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProvinciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Provincia::class);
    }
/*
     * Filtros de búsqueda de provincias
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('b.descripcion', 'ASC');
        return $query;
    }
}
