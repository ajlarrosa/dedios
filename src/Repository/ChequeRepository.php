<?php

namespace App\Repository;

use App\Entity\Cheque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cheque|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cheque|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cheque[]    findAll()
 * @method Cheque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChequeRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Cheque::class);
    }

    /*
     * Filtros de búsqueda de cheques
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');

        if (isset($filtros['nroCheque']) && $filtros['nroCheque'] != '') {
            $query->andWhere('b.nroCheque = :nroCheque ')
                    ->setParameter(':nroCheque', $filtros['nroCheque']);
        }
        if (isset($filtros['tipoCheque']) && $filtros['tipoCheque'] != '') {
            $query->andWhere('b.tipoCheque = :tipoCheque ')
                    ->setParameter(':tipoCheque', $filtros['tipoCheque']);
        }
        if (isset($filtros['banco']) && $filtros['banco'] != '') {
            $query->andWhere('b.banco = :banco ')
                    ->setParameter(':banco', $filtros['banco']);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        if (isset($filtros['fechaDesdeEmision']) && $filtros['fechaDesdeEmision'] != '') {
            $fechaDesdeEmision = new \DateTime($filtros['fechaDesdeEmision']);
            $query->andWhere('b.fechaEmision >= :fechaDesdeEmision ')
                    ->setParameter(':fechaDesdeEmision', $fechaDesdeEmision);
        }

        if (isset($filtros['fechaHastaEmision']) && $filtros['fechaHastaEmision'] != '') {
            $fechaHastaEmision = new \DateTime($filtros['fechaHastaEmision'] . ' 23:59');
            $query->andWhere(' :fechaHastaEmision >= b.fechaEmision')
                    ->setParameter(':fechaHastaEmision', $fechaHastaEmision);
        }
        if (isset($filtros['fechaDesdeCobro']) && $filtros['fechaDesdeCobro'] != '') {
            $fechaDesdeCobro = new \DateTime($filtros['fechaDesdeCobro']);
            $query->andWhere('b.fechaCobro >= :fechaDesdeCobro ')
                    ->setParameter(':fechaDesdeCobro', $fechaDesdeCobro);
        }

        if (isset($filtros['fechaHastaCobro']) && $filtros['fechaHastaCobro'] != '') {
            $fechaHastaCobro = new \DateTime($filtros['fechaHastaCobro'] . ' 23:59');
            $query->andWhere(' :fechaHastaCobro >= b.fechaCobro')
                    ->setParameter(':fechaHastaCobro', $fechaHastaCobro);
        }
        if (isset($filtros['reciboCliente']) && $filtros['reciboCliente'] != '') {
            $query->andWhere('b.reciboCliente = :reciboCliente ')
                    ->setParameter(':reciboCliente', $filtros['reciboCliente']);
        }
        if (isset($filtros['ordenPago']) && $filtros['ordenPago'] != '') {
            $query->andWhere('b.ordenPago = :ordenPago ')
                    ->setParameter(':ordenPago', $filtros['ordenPago']);
        }

        $query->orderBy('b.id', 'DESC');
        return $query;
    }

}
