<?php

namespace App\Repository;

use App\Entity\PrecioMedidaProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PrecioMedidaProductoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, PrecioMedidaProducto::class);
    }

    public function precioFinal($filtros) {
        $query = $this->createQueryBuilder('a')
                ->join('a.atributoProducto', 'ap')
                ->join('a.precio', 'p')
                ->andWhere('p.activo = 1')
                ->andWhere('ap.producto = :producto')
                ->setParameter(':producto', $filtros['producto'])
                ->andWhere('ap.medida = :medida')
                ->setParameter(':medida', $filtros['medida'])
                ->andWhere('p.listaPrecio = :listaPrecio')
                ->setParameter(':listaPrecio', $filtros['listaPrecio'])
                ->andWhere('p.precioReferencia is not null')
                ->andWhere('a.enabled = 1');

        return $query->getQuery()->getResult();
    }

}
