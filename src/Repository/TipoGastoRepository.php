<?php

namespace App\Repository;

use App\Entity\TipoGasto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TipoGasto|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoGasto|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoGasto[]    findAll()
 * @method TipoGasto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoGastoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoGasto::class);
    }

   /*
     * Filtros de búsqueda de productos
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.nombre LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('b.nombre', 'ASC');
        return $query;
    }
}
