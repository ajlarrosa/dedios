<?php

namespace App\Repository;

use App\Entity\MovimientoCC;
use App\Entity\Metal;
use App\Entity\ClienteProveedor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class CajaRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, MovimientoCC::class);
    }

    /*
     * Filtros de búsqueda de movimientos de cuenta corriente
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b')
                ->leftJoin('b.ordenPago', 'op')
                ->leftJoin('b.reciboCliente', 'rc')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        
        $query->andWhere('b.factura is null')
              ->andWhere('b.facturaProveedor is null')
              ->andWhere('b.notaCredito is null')
              ->andWhere('b.notaDebito is null');

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha >= :fechaDesde')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }
        if (isset($filtros['numero']) && $filtros['numero'] != '') {
            $query->andWhere('op.id = :numero or rc.id = :numero')
                    ->setParameter(':numero', $filtros['numero']);
        }
        if (isset($filtros['tipoDocumento']) && $filtros['tipoDocumento'] != '') {
            if($filtros['tipoDocumento']=='OP'){
                $query->andWhere('b.ordenPago is not null')
                      ->andWhere('b.reciboCliente is null');
            }elseif($filtros['tipoDocumento']=='RC'){
                $query->andWhere('b.ordenPago is null')
                      ->andWhere('b.reciboCliente is not null');
            }
        }
        
        $query->orderBy('b.id', 'DESC');
        return $query;
    }

    /*
     * Get Sumas
     */

    public function getSumas(MovimientoCC $movimiento, $nombreMovimiento, $moneda) {
        $query = $this->createQueryBuilder('b')->select('SUM(f.importe) as importe')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1)
                ->andWhere('b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());

        $query->andWhere('b.id <= :idDocumento');
        $query->setParameter(':idDocumento', $movimiento->getId());
        switch ($nombreMovimiento) {
            case MovimientoCC::ORDEN_PAGO:
                $query->join('b.ordenPago', 'f');

                break;
            case MovimientoCC::NOTA_CREDITO:
                $query->join('b.notaCredito', 'f');
                break;
            case MovimientoCC::NOTA_DEBITO:
                $query->join('b.notaDebito', 'f');
                break;
            case MovimientoCC::RECIBO_CLIENTE:
                $query->join('b.reciboCliente', 'f');
                break;
        }
        $query->andWhere('f.moneda = :moneda')
                ->setParameter(':moneda', $moneda);
        return $query->getQuery()->getSingleScalarResult();
    }

    /*
     * Get Sumas Metales
     */

    public function getSumasMetal(MovimientoCC $movimiento, $nombreMovimiento, $metal) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1)
                ->andWhere('b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());
        if ($metal == Metal::GOLD) {
            $query->select('SUM(f.oro) as importe');
        }

        if ($metal == Metal::SILVER) {
            $query->select('SUM(f.plata) as importe');
        }


        $query->andWhere('b.id <= :idDocumento');
        $query->setParameter(':idDocumento', $movimiento->getId());
        switch ($nombreMovimiento) {
            case MovimientoCC::ORDEN_PAGO:
                $query->join('b.ordenPago', 'f');
                break;
            case MovimientoCC::NOTA_CREDITO:
                $query->join('b.notaCredito', 'f');
                break;
            case MovimientoCC::NOTA_DEBITO:
                $query->join('b.notaDebito', 'f');
                break;
            case MovimientoCC::RECIBO_CLIENTE:
                $query->join('b.reciboCliente', 'f');
                break;
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    /*
     * Get facturas
     */

    public function getFacturas(MovimientoCC $movimiento, $moneda) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        $query->join('b.factura', 'f');
        $query->andWhere(' b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());
        $query->join('f.listaPrecio', 'l');
        $query->andWhere('l.moneda = :moneda')
                ->setParameter(':moneda', $moneda);

        $query->andWhere('b.id <= :idDocumento');
        $query->setParameter(':idDocumento', $movimiento->getId());

        return $query->getQuery()->getResult();
    }

    /*
     * Get facturas maetal
     */

    public function getFacturasMetal(MovimientoCC $movimiento, $metal) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        $query->join('b.factura', 'f');
        $query->andWhere(' b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());

        if ($metal == Metal::GOLD) {
            $query->andWhere(' f.oro > 0');
        }
        if ($metal == Metal::SILVER) {
            $query->andWhere(' f.plata > 0');
        }

        $query->andWhere('b.id <= :idDocumento');
        $query->setParameter(':idDocumento', $movimiento->getId());

        return $query->getQuery()->getResult();
    }

    /*
     * Get facturas proveedor
     */

    public function getFacturasProveedor(MovimientoCC $movimiento, $moneda) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        $query->join('b.facturaProveedor', 'f');
        $query->andWhere(' b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());

        $query->andWhere('f.moneda = :moneda')
                ->setParameter(':moneda', $moneda);

        if ($movimiento->getFacturaProveedor() !== null) {
            $query->andWhere('f.id <= :idFactura')
                    ->setParameter(':idFactura', $movimiento->getFacturaProveedor()->getId());
        }
        return $query->getQuery()->getResult();
    }

    /*
     * Get facturas proveedor metal
     */

    public function getFacturasProveedorMetal(MovimientoCC $movimiento, $metal) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        $query->join('b.facturaProveedor', 'f');
        $query->andWhere(' b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $movimiento->getClienteProveedor());

        if ($metal == Metal::GOLD) {
            $query->andWhere(' f.oro > 0');
        }
        if ($metal == Metal::SILVER) {
            $query->andWhere(' f.plata > 0');
        }
        if ($movimiento->getFacturaProveedor() !== null) {
            $query->andWhere('f.id <= :idFactura')
                    ->setParameter(':idFactura', $movimiento->getFacturaProveedor()->getId());
        }
        return $query->getQuery()->getResult();
    }

    /*
     * Get last movement
     */

    public function getLastMovimiento(ClienteProveedor $clienteProveedor) {
        $query = $this->createQueryBuilder('b')
                ->Where('b.activo = :activo ')
                ->setParameter(':activo', 1);
        $query->andWhere(' b.clienteProveedor = :clienteProveedor')
                ->setParameter(':clienteProveedor', $clienteProveedor);
        return $query->orderBy('b.id', 'DESC')
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getOneOrNullResult();
    }

}
