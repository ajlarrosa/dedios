<?php

namespace App\Repository;

use App\Entity\Medida;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Medida|null find($id, $lockMode = null, $lockVersion = null)
 * @method Medida|null findOneBy(array $criteria, array $orderBy = null)
 * @method Medida[]    findAll()
 * @method Medida[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedidaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Medida::class);
    }

     /*
     * Filtros de búsqueda de medidas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('p');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('p.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('p.descripcion', 'ASC');
        return $query;
    }

}
