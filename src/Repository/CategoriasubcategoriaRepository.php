<?php

namespace App\Repository;

use App\Entity\Categoriasubcategoria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categoriasubcategoria|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categoriasubcategoria|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categoriasubcategoria[]    findAll()
 * @method Categoriasubcategoria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriasubcategoriaRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Categoriasubcategoria::class);
    }

    /*
     * Filtros de búsqueda de bancos
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('b.categoria =:categoria ')
                    ->setParameter(':categoria', $filtros['categoria']);
        }
        if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
            $query->andWhere('b.subcategoria =:subcategoria ')
                    ->setParameter(':subcategoria', $filtros['subcategoria']);
        }
        if (isset($filtros['metal']) && $filtros['metal'] != '') {
            $query->andWhere('b.metal =:metal ')
                    ->setParameter(':metal', $filtros['metal']);
        }
        
            $query->orderBy('b.id', 'ASC');
      
        return $query;
    }

 

}
