<?php

namespace App\Repository;

use App\Entity\NotaDebito;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NotaDebito|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotaDebito|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotaDebito[]    findAll()
 * @method NotaDebito[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotaDebitoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotaDebito::class);
    }

    // /**
    //  * @return NotaDebito[] Returns an array of NotaDebito objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotaDebito
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
