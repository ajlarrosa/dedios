<?php

namespace App\Repository;

use App\Entity\Precio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PrecioRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Precio::class);
    }

    /*
     * Predictiva de precios
     */

    public function predictiva($search, $idLista) {
        $query = $this->createQueryBuilder('p');
        $query->join('p.producto', 'pr');

        $query->where('p.activo = :activo')
                ->setParameter(':activo', true);
        $query->andWhere('p.listaPrecio = :listaPrecio')
                ->setParameter(':listaPrecio', $idLista);
        $query->andWhere('pr.codigo <> :general')
                ->setParameter(':general', '000');
        $query->andWhere('pr.codigo LIKE :search OR  pr.descripcion LIKE :search')
                ->setParameter(':search', '%' . $search . '%');

        $query->orderBy('pr.descripcion', 'DESC');
        $query->setMaxResults(30);
        return $query->getQuery()->getResult();
    }

    /*
     * get Max Prize
     */

    public function getMaxPrice($filtros) {

        $query = $this->createQueryBuilder('p');
        $query->select(' MAX(p.valor) AS max_price')
                ->join('p.producto', 'pr')
                ->andWhere('p.precioReferencia is null')
                ->where('p.activo = :activo')
                ->setParameter(':activo', 1);

        //Precios
        if ((isset($filtros['precios']) && $filtros['precios'] != '')) {
            $query->andWhere('p.listaPrecio = :listaPrecio')
                    ->setParameter(':listaPrecio', $filtros['precios']);
        }
        //Lista de precio
        if (isset($filtros['listaPrecio']) && $filtros['listaPrecio'] != '') {
            $query->andWhere('p.listaPrecio = :listaPrecio')
                    ->setParameter(':listaPrecio', $filtros['listaPrecio']);
        }

        if ((isset($filtros['categoria']) && $filtros['categoria'] != '') || (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '')) {
            $query->join('pr.categoriasubcategoria', 'cs');
        }

        //Categoría
        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('cs.categoria = :categoria')
                    ->setParameter(':categoria', $filtros['categoria']);
        }
        
        //Subcategoría        
        if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
            $query->andWhere('cs.subcategoria = :subcategoria')
                    ->setParameter(':subcategoria', $filtros['subcategoria']);
        }

        //Línea de Producto
        if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {
            $query->Join('App\Entity\Tienda\ProductosLineaProducto', 'plp', 'WITH', 'plp.producto = pr.id');
            $query->andWhere('plp.lineaProducto = :lineaProducto')
                    ->setParameter(':lineaProducto', $filtros['lineaProducto']);
        }

        //Palabras Clave
        if (isset($filtros['palabrasClave']) && $filtros['palabrasClave'] != '') {
            $search = $filtros['palabrasClave'];
            $search0 = $search[0];

            $queryAux = '';

            if (strlen($search0) > 2) {

                $queryAux = 'pr.codigo LIKE :word0 OR pr.descripcion LIKE :word0 ';

                $query->setParameter(':word0', '%' . $search0 . '%');

                array_shift($search);
            }
            $i = 1;
            foreach ($search as $word) {                
                if (strlen($word) > 2) {
                    if ($queryAux !== ''){
                        $queryAux .= ' OR ';
                    }
                    
                    $queryAux .= ' pr.codigo LIKE :word' . $i . ' OR pr.descripcion LIKE :word' . $i . ' ';
                    $query->setParameter(':word' . $i, '%' . $word . '%');
                    $i++;
                }
            }

            if ($queryAux != '') {
                $query->andWhere($queryAux);
            }
        }

        if (isset($filtros['visible']) && $filtros['visible'] != '') {
            $query->andWhere('pr.visible = :visible')
                    ->setParameter(':visible', $filtros['visible']);
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    /*
     * find precio By
     */

    public function findPreciosBy($filtros) {
        $query = $this->createQueryBuilder('p')
                ->where('p.activo = :activo')
                ->setParameter(':activo', $filtros['activo']);
        if (!isset($filtros['referencia']) || $filtros['referencia'] === true) {
            $query->andWhere('p.precioReferencia is null');
        }
        if (isset($filtros['precioReferencia']) && $filtros['precioReferencia'] != '') {
            $query->andWhere('p.precioReferencia = :precioReferencia')
                    ->setParameter(':precioReferencia', $filtros['precioReferencia']);
        }

        $query->andWhere('p.producto = :producto')
                ->setParameter(':producto', $filtros['producto']);

        if (isset($filtros['listaPrecio']) && $filtros['listaPrecio'] != '') {
            $query->andWhere('p.listaPrecio = :listaPrecio')
                    ->setParameter(':listaPrecio', $filtros['listaPrecio']);
        }

        $query->orderBy('p.id');

        return $query->getQuery()->getResult();
    }

    /*
     * Get Precio Base
     */

    public function getPrecioBase($filtros) {

        $query = $this->createQueryBuilder('p')
                ->where('p.producto = :producto')
                ->setParameter(':producto', $filtros['producto'])
                ->andWhere('p.listaPrecio = :listaPrecio')
                ->setParameter(':listaPrecio', $filtros['listaPrecio'])
                ->andWhere('p.activo = :activo')
                ->setParameter(':activo', 1)
                ->andWhere('p.precioReferencia is null');

        return $query->getQuery()->getOneOrNullResult();
    }

}
