<?php

namespace App\Repository;

use App\Entity\Producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Producto[]    findAll()
 * @method Producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Producto::class);
    }

    /*
     * Filtros de búsqueda de productos
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('p')
                ->where('p.codigo != :codigo000')
                ->setParameter(':codigo000', '000')
                ->leftJoin('p.categoriasubcategoria', 'cs');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['metal']) && $filtros['metal'] != '') {
            $query->andWhere('cs.metal = :metal')
                    ->setParameter(':metal', $filtros['metal']);
        }
        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('cs.categoria = :categoria')
                    ->setParameter(':categoria', $filtros['categoria']);
        }

        if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
            $query->andWhere('cs.subcategoria = :subcategoria')
                    ->setParameter(':subcategoria', $filtros['subcategoria']);
        }
        if (isset($filtros['finder']) && $filtros['finder'] != '') {
            $query->andWhere('p.codigo LIKE :finder OR p.descripcion LIKE :finder ')
                    ->setParameter(':finder', '%' . $filtros['finder'] . '%');
        }

        $query->orderBy('p.codigo', 'ASC');
        return $query;
    }

    /*
     * Filtros de búsqueda de productos
     */

    public function search($search) {
        $filters = 0;
        $query = $this->createQueryBuilder('p')
                ->where('p.codigo != :codigo000')
                ->setParameter(':codigo000', '000')
                ->andWhere('p.enabled = :activo')
                ->setParameter(':activo', true);

        $search0 = $search[0];

        if (strlen($search0) > 2) {
            $query->andWhere('p.codigo LIKE :word0 OR p.descripcion LIKE :word0')
                    ->setParameter(':word0', '%' . $search0 . '%');
            array_shift($search);
            $filters++;
        }
        $i = 1;
        foreach ($search as $word) {
            if (strlen($word) > 2) {
                $query->orWhere('p.codigo LIKE :word' . $i . ' OR p.descripcion LIKE :word' . $i)
                        ->setParameter(':word' . $i, '%' . $word . '%');
                $i++;
                $filters++;
            }
        }
        $query->orderBy('p.codigo', 'ASC');

        if ($filters > 0) {
            return $query->getQuery()->getResult();
        } else {
            return [];
        }
    }

    /*
     * Get Max Id
     */

    public function getMaxId() {
        return $this->createQueryBuilder('e')
                        ->select('MAX(e.id)')
                        ->getQuery()
                        ->getSingleScalarResult();
    }

    /*
     * Get Productos x Predictiva
     */

    public function getProductoPredictiva($search) {
        $query = $this->createQueryBuilder('p')
                ->andWhere('p.enabled = :activo')
                ->setParameter(':activo', '1')
                ->andWhere('p.descripcion LIKE :search')
                ->orWhere('p.codigo LIKE :search')
                ->setParameter(':search', '%' . $search . '%')
                ->orderBy('p.descripcion', 'ASC');

        return $query->getQuery()->getResult();
    }

    /*
     * Get Productos Tienda
     */

    public function getProductos($filtros) {
        $expr = $this->_em->getExpressionBuilder();

        $sub = $this->createQueryBuilder('a')
                ->select('a.id')
                ->Join('App\Entity\Precio', 'pre', 'WITH', 'pre.producto = a.id')
                ->Join('App\Entity\ImagenProducto', 'i', 'WITH', 'i.producto = a.id')
                ->andWhere('i.enabled = :enabled')
                ->andWhere('pre.listaPrecio = :listaPrecio')
                ->andWhere('pre.activo = 1')
                ->andWhere('a.enabled = :enabled')
                ->andWhere('a.visible = :visible')
                ->andWhere('pre.precioReferencia is null');

        if (isset($filtros['price']) && $filtros['price'] != '') {
            $rangos = explode('-', $filtros['price']);
            $min = intval($rangos[0]);
            $sub->andWhere('pre.valor >= :min');

            if (sizeof($rangos) > 1) {
                $max = intval($rangos[1]);
                $sub->andWhere('pre.valor <= :max');
            }
        }

        $query = $this->createQueryBuilder('p')
                ->where($expr->in('p.id', $sub->getDql()))
                ->setParameter(':enabled', true)
                ->setParameter(':visible', '1');

        //CantShow
        if (isset($filtros['cantShow']) && $filtros['cantShow'] != '') {
            $query->setMaxResults($filtros['cantShow']);
        }

        //Precio
        if (isset($filtros['price']) && $filtros['price'] != '') {
            $query->setParameter(':min', $min);

            if (sizeof($rangos) > 1) {
                $query->setParameter(':max', $max);
            }
        }

        if ((isset($filtros['categoria']) && $filtros['categoria'] != '') || (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '')) {
            $query->join('p.categoriasubcategoria', 'cs');
        }

        //Categoría
        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('cs.categoria = :categoria')
                    ->setParameter(':categoria', $filtros['categoria']);
        }

        //Subcategoría
        if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
            $query->andWhere('cs.subcategoria = :subcategoria')
                    ->setParameter(':subcategoria', $filtros['subcategoria']);
        }

        //Línea de Producto
        if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {
            $query->Join('App\Entity\Tienda\ProductosLineaProducto', 'plp', 'WITH', 'plp.producto = p.id');
            $query->andWhere('plp.lineaProducto = :lineaProducto')
                    ->setParameter(':lineaProducto', $filtros['lineaProducto']);
        }

        if (isset($filtros['from']) && $filtros['from'] != '') {
            $query->setFirstResult($filtros['from']);
        }

        //Palabras Clave
        if (isset($filtros['palabrasClave']) && $filtros['palabrasClave'] != '') {
            $search = $filtros['palabrasClave'];
            $search0 = $search[0];
            $queryAux = '';
            if (strlen($search0) > 2) {

                $queryAux = 'p.codigo LIKE :word0 OR p.descripcion LIKE :word0 ';

                $query->setParameter(':word0', '%' . $search0 . '%');

                array_shift($search);
            }
            $i = 1;
            foreach ($search as $key => $word) {

                if (strlen($word) > 2) {
                    if ($queryAux !== '') {
                        $queryAux .= ' OR ';
                    }
                    $queryAux .= 'p.codigo LIKE :word' . $i . ' OR p.descripcion LIKE :word' . $i . ' ';
                    $query->setParameter(':word' . $i, '%' . $word . '%');
                    $i++;
                }
            }

            if ($queryAux != '') {
                $query->andWhere($queryAux);
            }
        }

        if (!empty($filtros['orderPrecio'])) {
            $query->Join('App\Entity\Precio', 'pre2', 'WITH', 'pre2.producto = p.id')
                    ->andWhere('pre2.listaPrecio = :listaPrecio')
                    ->andWhere('pre2.precioReferencia is null')
                    ->orderBy('pre2.valor', str_replace("#", "", $filtros['orderPrecio']))
                    ->addOrderBy('p.orden', 'ASC');
        } else {
            $query->orderBy('p.orden', 'ASC');
        }
        $query->setParameter(':listaPrecio', $filtros['precios']);


        return $query->getQuery()->getResult();
    }

    /*
     * Get Count Productos Tienda
     */

    public function getCountProductos($filtros) {
        $expr = $this->_em->getExpressionBuilder();

        $sub = $this->createQueryBuilder('a')
                ->select('a.id')
                ->Join('App\Entity\Precio', 'pre', 'WITH', 'pre.producto = a.id')
                ->Join('App\Entity\ImagenProducto', 'i', 'WITH', 'i.producto = a.id')
                ->andWhere('i.enabled = :enabled')
                ->andWhere('pre.listaPrecio = :listaPrecio')
                ->andWhere('pre.activo = 1')
                ->andWhere('a.enabled = :enabled')
                ->andWhere('a.visible = :visible');

        if (isset($filtros['price']) && $filtros['price'] != '') {
            $rangos = explode('-', $filtros['price']);
            $min = intval($rangos[0]);
            $sub->andWhere('pre.valor >= :min');

            if (sizeof($rangos) > 1) {
                $max = intval($rangos[1]);
                $sub->andWhere('pre.valor <= :max');
            }
        }

        $query = $this->createQueryBuilder('p')
                ->select('COUNT(p.id)')
                ->where($expr->in('p.id', $sub->getDql()))
                ->andWhere('p.enabled = :enabled')
                ->setParameter(':enabled', '1')
                ->andWhere('p.visible = :visible')
                ->setParameter(':visible', '1')
                ->setParameter(':listaPrecio', $filtros['precios']);


        //Precio
        if (isset($filtros['price']) && $filtros['price'] != '') {
            $query->setParameter(':min', $min);

            if (sizeof($rangos) > 1) {
                $query->setParameter(':max', $max);
            }
        }

        if ((isset($filtros['categoria']) && $filtros['categoria'] != '') || (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '')) {
            $query->join('p.categoriasubcategoria', 'cs');
        }

        //Categoría
        if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
            $query->andWhere('cs.categoria = :categoria')
                    ->setParameter(':categoria', $filtros['categoria']);
        }

        //Subcategoría
        if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
            $query->andWhere('cs.subcategoria = :subcategoria')
                    ->setParameter(':subcategoria', $filtros['subcategoria']);
        }

        //Línea de Producto
        if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {
            $query->Join('App\Entity\Tienda\ProductosLineaProducto', 'plp', 'WITH', 'plp.producto = p.id');
            $query->andWhere('plp.lineaProducto = :lineaProducto')
                    ->setParameter(':lineaProducto', $filtros['lineaProducto']);
        }

        //Palabras Clave
        if (isset($filtros['palabrasClave']) && $filtros['palabrasClave'] != '') {
            $search = $filtros['palabrasClave'];
            $search0 = $search[0];
            $queryAux = '';
            if (strlen($search0) > 2) {

                $queryAux = 'p.codigo LIKE :word0 OR p.descripcion LIKE :word0 ';

                $query->setParameter(':word0', '%' . $search0 . '%');

                array_shift($search);
            }
            $i = 1;
            foreach ($search as $word) {
                if (strlen($word) > 2) {
                    if ($queryAux !== '') {
                        $queryAux .= 'OR';
                    }
                    $queryAux .= ' p.codigo LIKE :word' . $i . ' OR p.descripcion LIKE :word' . $i . ' ';
                    $query->setParameter(':word' . $i, '%' . $word . '%');
                    $i++;
                }
            }

            if ($queryAux != '') {
                $query->andWhere($queryAux);
            }
        }

        return $query->getQuery()->getSingleScalarResult();
    }

}
