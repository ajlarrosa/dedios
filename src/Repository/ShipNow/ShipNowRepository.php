<?php

namespace App\Repository\ShipNow;

use App\Entity\ShipNow\ShipNow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ShipNowRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ShipNow::class);
    }


}
