<?php

namespace App\Repository;

use App\Entity\Metal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Metal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metal[]    findAll()
 * @method Metal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetalRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Metal::class);
    }

    /*
     * Filtros de búsqueda de categoría
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('b.descripcion', 'ASC');
        return $query;
    }

}
