<?php

namespace App\Repository;

use App\Entity\ClienteProveedor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClienteProveedor|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClienteProveedor|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClienteProveedor[]    findAll()
 * @method ClienteProveedor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ClienteProveedor::class);
    }

    /*
     * Filtros de búsqueda de clientes
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        $query->andWhere('b.clienteProveedor = :cliente')
                ->setParameter(':cliente', ClienteProveedor::CLIENTE_COD);

        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['razonSocial']) && $filtros['razonSocial'] != '') {
            $query->andWhere('b.razonSocial LIKE :razonSocial ')
                    ->setParameter(':razonSocial', '%' . $filtros['razonSocial'] . '%');
        }
        if (isset($filtros['mail']) && $filtros['mail'] != '') {
            $query->andWhere('b.mail LIKE :mail ')
                    ->setParameter(':mail', '%' . $filtros['mail'] . '%');
        }
        if (isset($filtros['localidad']) && $filtros['localidad'] != '') {
            $query->andWhere('b.localidad = :localidad ')
                    ->setParameter(':localidad', $filtros['localidad']);
        }
        if (isset($filtros['provincia']) && $filtros['provincia'] != '') {
            $query->join('b.localidad', 'l');
            $query->andWhere('l.provincia = :provincia ')
                    ->setParameter(':provincia', $filtros['provincia']);
        }

        $query->orderBy('b.razonSocial', 'ASC');
        return $query;
    }

    /*
     * Get Stores
     */

    public function getStores() {
        $query = $this->createQueryBuilder('b');
        $query->andWhere('b.clienteProveedor = :cliente')
                ->setParameter(':cliente', ClienteProveedor::CLIENTE_COD)
                ->andWhere('b.enabled = :activo')
                ->setParameter(':activo', true)
                ->andWhere('b.visible = :visible')
                ->setParameter(':visible', true);

        $query->orderBy('b.razonSocial', 'ASC');
        return $query->getQuery()->getResult();
    }

}
