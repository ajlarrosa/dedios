<?php

namespace App\Repository;

use App\Entity\AtributosProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AtributosProducto|null find($id, $lockMode = null, $lockVersion = null)
 * @method AtributosProducto|null findOneBy(array $criteria, array $orderBy = null)
 * @method AtributosProducto[]    findAll()
 * @method AtributosProducto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AtributosProductoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, AtributosProducto::class);
    }

    
}
