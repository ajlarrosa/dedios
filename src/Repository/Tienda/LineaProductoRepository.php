<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\LineaProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LineaProducto|null find($id, $lockMode = null, $lockVersion = null)
 * @method LineaProducto|null findOneBy(array $criteria, array $orderBy = null)
 * @method LineaProducto[]    findAll()
 * @method LineaProducto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineaProductoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LineaProducto::class);
    }

     /*
     * Filtros de búsqueda de promociones
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('lp');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('lp.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('lp.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('lp.descripcion', 'ASC');
        return $query;
    }
    
    /*
     * Filtros de búsqueda de linea de productos
     */
    public function search($search) {
        $query = $this->createQueryBuilder('b');
        $query->where('b.enabled = :activo')
              ->setParameter(':activo', 1)
              ->andWhere('lower(b.descripcion) = :search')
              ->setParameter(':search', strtolower($search));

        return $query->getQuery()
                    ->setMaxResults(1)
                    ->getResult();
    }
    
}
