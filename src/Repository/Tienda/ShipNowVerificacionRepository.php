<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\ShipNowVerificacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ShipNowVerificacionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ShipNowVerificacion::class);
    }

    /*
     * Filtros de búsqueda de Ship Now Verificacion
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('r');

        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('r.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= r.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        $query->orderBy('r.id', 'DESC');
        return $query;
    }
}
