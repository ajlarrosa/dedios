<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\DireccionEnvio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DireccionEnvio|null find($id, $lockMode = null, $lockVersion = null)
 * @method DireccionEnvio|null findOneBy(array $criteria, array $orderBy = null)
 * @method DireccionEnvio[]    findAll()
 * @method DireccionEnvio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DireccionEnvioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DireccionEnvio::class);
    }

    // /**
    //  * @return DireccionEnvio[] Returns an array of DireccionEnvio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DireccionEnvio
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
