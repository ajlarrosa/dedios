<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Mailing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mailing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mailing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mailing[]    findAll()
 * @method Mailing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mailing::class);
    }

    /*
     * Query filter
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('m');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('m.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('m.created >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'].' 23:59');           
            $query->andWhere(':fechaHasta >= m.created')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        

        $query->orderBy('m.id', 'DESC');
        return $query;
    }
    
    
    public function buscarMail($email) {
        $query = $this->createQueryBuilder('m')
                ->leftJoin('m.clienteWeb', 'c')
                ->leftJoin('m.user', 'u');
        $query->andWhere('m.clienteWeb IS NOT NULL AND c.email = :email')
              ->orWhere('m.user IS NOT NULL AND u.mail = :email')  
              ->setParameter(':email', $email);
        
        return $query->getQuery()->getResult();
    }
}
