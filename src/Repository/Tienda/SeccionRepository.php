<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Seccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Seccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Seccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Seccion[]    findAll()
 * @method Seccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeccionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Seccion::class);
    }

    /*
     * Filtros de búsqueda de secciones
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('s');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('s.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('s.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('s.descripcion', 'ASC');
        return $query;
    }

}
