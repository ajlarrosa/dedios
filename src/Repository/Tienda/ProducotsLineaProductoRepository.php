<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\ProductosLineaProducto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductosLineaProducto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductosLineaProducto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductosLineaProducto[]    findAll()
 * @method ProductosLineaProducto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProducotsLineaProductoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductosLineaProducto::class);
    }

    // /**
    //  * @return ProductosLineaProducto[] Returns an array of ProductosLineaProducto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductosLineaProducto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
