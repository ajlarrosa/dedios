<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\LineaProductoGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LineaProductoGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method LineaProductoGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method LineaProductoGrupo[]    findAll()
 * @method LineaProductoGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineaProductoGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LineaProductoGrupo::class);
    }

    // /**
    //  * @return LineaProductoGrupo[] Returns an array of LineaProductoGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LineaProductoGrupo
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
