<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\FormaPago;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FormaPago|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormaPago|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormaPago[]    findAll()
 * @method FormaPago[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormaPagoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FormaPago::class);
    }

    /*
     * Filtros de búsqueda de formas de pago
     */

    public function filter($filtros) {
        $query = $this->queryFilter($filtros);
        return $query;
    }

    /*
     * Filtros de búsqueda de formas de pago
     */

    public function filterResults($filtros,$order='ASC') {
        $query = $this->queryFilter($filtros,$order);
        return $query->getQuery()->getResult();
    }
    /*
     * Query filter
     */
    private function queryFilter($filtros,$order='ASC') {
        $query = $this->createQueryBuilder('p');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('p.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        if (isset($filtros['formasPago']) && $filtros['formasPago'] != '') {
            $query->andWhere('p.id in (' . $filtros['formasPago'] . ')');
        }

        $query->orderBy('p.descripcion', $order);
                
        return $query;
    }

}
