<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\SubcategoriaGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubcategoriaGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubcategoriaGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubcategoriaGrupo[]    findAll()
 * @method SubcategoriaGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubcategoriaGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubcategoriaGrupo::class);
    }

    // /**
    //  * @return SubcategoriaGrupo[] Returns an array of SubcategoriaGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubcategoriaGrupo
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
