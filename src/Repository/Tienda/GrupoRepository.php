<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Grupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Grupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grupo[]    findAll()
 * @method Grupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Grupo::class);
    }

    /*
     * Filtros de búsqueda de grupos
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('g');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('g.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('g.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('g.descripcion', 'ASC');
        return $query;
    }
}
