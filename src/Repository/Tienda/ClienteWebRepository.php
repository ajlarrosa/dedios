<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\ClienteWeb;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClienteWeb|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClienteWeb|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClienteWeb[]    findAll()
 * @method ClienteWeb[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteWebRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClienteWeb::class);
    }

        /*
     * Filtros de búsqueda de clientes
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.created')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.created >= :fechaDesde')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }
        
        if (isset($filtros['nombre']) && $filtros['nombre'] != '') {
            $query->andWhere('b.nombre LIKE :nombre ')
                    ->setParameter(':nombre', '%' . $filtros['nombre'] . '%');
        }
        
        if (isset($filtros['mail']) && $filtros['mail'] != '') {
            $query->andWhere('b.email LIKE :mail ')
                    ->setParameter(':mail', '%' . $filtros['mail'] . '%');
        }
        
        if (isset($filtros['telefono']) && $filtros['telefono'] != '') {
            $query->andWhere('b.telefono LIKE :telefono ')
                    ->setParameter(':telefono', '%' . $filtros['telefono'] . '%');
        }
        $query->orderBy('b.id', 'ASC');
        return $query;
    }
}
