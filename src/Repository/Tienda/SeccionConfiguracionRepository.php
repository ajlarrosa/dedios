<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\SeccionConfiguracion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SeccionConfiguracion|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeccionConfiguracion|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeccionConfiguracion[]    findAll()
 * @method SeccionConfiguracion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeccionConfiguracionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeccionConfiguracion::class);
    }

    /*
     * Filtros de búsqueda de secciones de la configuracion
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('s');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('s.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('s.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('s.descripcion', 'ASC');
        return $query;
    }

}
