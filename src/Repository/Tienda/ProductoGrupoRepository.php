<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\ProductoGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductoGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoGrupo[]    findAll()
 * @method ProductoGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductoGrupo::class);
    }

    // /**
    //  * @return ProductoGrupo[] Returns an array of ProductoGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductoGrupo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
