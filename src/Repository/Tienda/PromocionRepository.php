<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Promocion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Promocion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Promocion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Promocion[]    findAll()
 * @method Promocion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromocionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Promocion::class);
    }

    /*
     * Filtros de búsqueda de promociones
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('p');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('p.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        if (isset($filtros['visibleMayorista']) && $filtros['visibleMayorista'] != '') {
            $query->andWhere('p.visibleMayorista LIKE :visibleMayorista ')
                    ->setParameter(':visibleMayorista', $filtros['visibleMayorista']);
        }

        $query->orderBy('p.descripcion', 'ASC');
        return $query;
    }

}
