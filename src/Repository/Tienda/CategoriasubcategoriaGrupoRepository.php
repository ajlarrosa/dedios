<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\CategoriasubcategoriaGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoriasubcategoriaGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriasubcategoriaGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriasubcategoriaGrupo[]    findAll()
 * @method CategoriasubcategoriaGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriasubcategoriaGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoriasubcategoriaGrupo::class);
    }

    // /**
    //  * @return CategoriasubcategoriaGrupo[] Returns an array of CategoriasubcategoriaGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoriasubcategoriaGrupo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
