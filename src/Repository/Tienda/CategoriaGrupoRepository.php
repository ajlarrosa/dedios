<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\CategoriaGrupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoriaGrupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriaGrupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriaGrupo[]    findAll()
 * @method CategoriaGrupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaGrupoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoriaGrupo::class);
    }

    // /**
    //  * @return CategoriaGrupo[] Returns an array of CategoriaGrupo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoriaGrupo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
