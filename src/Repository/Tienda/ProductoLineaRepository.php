<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\ProductoLinea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductoLinea|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoLinea|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoLinea[]    findAll()
 * @method ProductoLinea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoLineaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductoLinea::class);
    }

    // /**
    //  * @return ProductoLinea[] Returns an array of ProductoLinea objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductoLinea
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
