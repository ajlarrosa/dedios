<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\TipoImagen;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TipoImagen|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoImagen|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoImagen[]    findAll()
 * @method TipoImagen[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoImagenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoImagen::class);
    }

    // /**
    //  * @return TipoImagen[] Returns an array of TipoImagen objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoImagen
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
