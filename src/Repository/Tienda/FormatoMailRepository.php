<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\FormatoMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FomatoMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method FomatoMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method FomatoMail[]    findAll()
 * @method FomatoMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormatoMailRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FormatoMail::class);
    }

    /*
     * Filtros de búsqueda de formato mail
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('f');

        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('f.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (isset($filtros['type']) && $filtros['type'] != '') {
            $query->andWhere('f.type = :type ')
                    ->setParameter(':type', $filtros['type']);
        }

        $query->orderBy('f.id', 'DESC');
        return $query;
    }

}
