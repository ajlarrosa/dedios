<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Promotion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Promotion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Promotion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Promotion[]    findAll()
 * @method Promotion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromotionRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Promotion::class);
    }

    public function filter($filtros) {
                
        $query = $this->createQueryBuilder('p');
        
        if (!empty($filtros['activo'])) {
            $query->andWhere('p.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        //Type All
        if (isset($filtros['type']) && $filtros['type'] === 'all') {            
            $query->andWhere('p.producto is null and p.categoria is null and p.subcategoria is null and p.lineaProducto is null ');
        }

        if (isset($filtros['type']) && $filtros['type'] === 'promos') {
            $query->andWhere('p.tipoDescuento != :tipoDescuento')
                    ->setParameter(':tipoDescuento', Promotion::ENVIO_CERO);
        }

        //Nombre
        if (isset($filtros['nombre']) && $filtros['nombre'] != '') {
            $query->andWhere('p.nombre LIKE :nombre ')
                    ->setParameter(':nombre', '%' . $filtros['nombre'] . '%');
        }
        //Fecha desde
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('(p.fechaDesde IS NULL) or (:fechaDesde  >= p.fechaDesde)  ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta']);
            $query->andWhere('(p.fechaHasta IS NULL) OR (:fechaHasta <= p.fechaHasta)')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
     
        if (!empty($filtros['tipoDescuento'])) {
                        
            $query->andWhere('p.tipoDescuento = :tipoDescuento')
                    ->setParameter(':tipoDescuento', $filtros['tipoDescuento']);
            
            if ($filtros['tipoDescuento'] === Promotion::ENVIO_CERO) {
   
                $html = '';
                if (isset($filtros['producto']) && $filtros['producto'] != '') {
                    $html .= 'p.producto = :producto';
                    $query->setParameter(':producto', $filtros['producto']);
                }
                if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
                    if ($html !== '') {
                        $html .= ' or ';
                    }
                    $html .= 'p.categoria = :categoria';
                    $query->setParameter(':categoria', $filtros['categoria']);
                }
                if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
                    if ($html !== '') {
                        $html .= ' or ';
                    }
                    $html .= 'p.subcategoria = :subcategoria';
                    $query->setParameter(':subcategoria', $filtros['subcategoria']);
                }

                if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {
                    if ($html !== '') {
                        $html .= ' or ';
                    }
                    $html .= 'p.lineaProducto = :lineaProducto';
                    $query->setParameter(':lineaProducto', $filtros['lineaProducto']);
                }
                
                if ($html !== '') {
                    $query->andWhere($html);
                }
                
            } else {
                
                
                if (isset($filtros['producto']) && $filtros['producto'] != '') {
                    $query->andWhere('p.producto = :producto')
                            ->setParameter(':producto', $filtros['producto']);
                }
                
                if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
                    $query->andWhere('p.categoria = :categoria')
                            ->setParameter(':categoria', $filtros['categoria']);
                }
                
                if (isset($filtros['categoriaSubcategoria']) && $filtros['categoriaSubcategoria'] != '') {
                    $query->andWhere('p.subcategoria = :subcategoria')
                            ->setParameter(':subcategoria', $filtros['subcategoria']);
                }
                
                if (!empty($filtros['lineaProducto'])) {
                    $query->andWhere('p.lineaProducto = :lineaProducto')
                            ->setParameter(':lineaProducto', $filtros['lineaProducto']);
                }
                                                        
                if (isset($filtros['lineaProductos']) && is_array($filtros['lineaProductos'])) {  
                    if (empty($filtros['lineaProductos'])){
                        $query->andWhere('1 = 0');
                    }else {
                        $query->andWhere('p.lineaProducto in (' . implode(",", $filtros['lineaProductos']) . ')');                                                
                    }   
                }
                
            }            
        }else{
             if (isset($filtros['producto']) && $filtros['producto'] != '') {
                    $query->andWhere('p.producto = :producto')
                            ->setParameter(':producto', $filtros['producto']);
                }
                
                if (isset($filtros['categoria']) && $filtros['categoria'] != '') {
                    $query->andWhere('p.categoria = :categoria')
                            ->setParameter(':categoria', $filtros['categoria']);
                }
                
                if (isset($filtros['subcategoria']) && $filtros['subcategoria'] != '') {
                    $query->andWhere('p.subcategoria = :subcategoria')
                            ->setParameter(':subcategoria', $filtros['subcategoria']);
                }
                
                if (isset($filtros['lineaProducto']) && $filtros['lineaProducto'] != '') {                 
                    $query->andWhere('p.lineaProducto = :lineaProducto')
                            ->setParameter(':lineaProducto', $filtros['lineaProducto']);
                }
                
                if (isset($filtros['lineaProductos']) && is_array($filtros['lineaProductos'])) {   
                    if (empty($filtros['lineaProductos'])){
                        $query->andWhere('1 = 0');
                    }else {
                        $query->andWhere('p.lineaProducto in (' . implode(",", $filtros['lineaProductos']) . ')');                                                
                    }                    
                }
        }

       
        $query->orderBy('p.id', 'DESC');

        return $query;
    }

    /*
     * Filter Results
     */

    public function filterResults($filtros) {
        return $this->filter($filtros)->getQuery()->getResult();
    }

}
