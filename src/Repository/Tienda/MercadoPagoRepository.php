<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\MercadoPago;
use App\Entity\Tienda\Pedido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MercadoPagoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, MercadoPago::class);
    }

    public function filtro($filtros) {
        $query = $this->createQueryBuilder('e')->where('1=1');

        if (array_key_exists('fechaDesde', $filtros) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.created >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }
        if (array_key_exists('fechaHasta', $filtros) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.created')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        if (array_key_exists('pedidoId', $filtros) && $filtros['pedidoId'] != '') {
            $query->andWhere('e.pedido = :pedidoId');
            $query->setParameter(':pedidoId', $filtros['pedidoId']);
        }
        if (array_key_exists('cliente', $filtros) && $filtros['cliente'] != '') {
            $query->andWhere('e.cliente = :cliente');
            $query->setParameter(':cliente', $filtros['cliente']);
        }

        if (array_key_exists('paymentId', $filtros) && $filtros['paymentId'] != '') {
            $query->andWhere('e.payment_id LIKE :paymentId');
            $query->setParameter(':paymentId', '%' . $filtros['paymentId'] . '%');
        }
        if (array_key_exists('paymentType', $filtros) && $filtros['paymentType'] != '') {
            $query->andWhere('e.payment_type LIKE :paymentType');
            $query->setParameter(':paymentType', '%' . $filtros['paymentType'] . '%');
        }
        if (array_key_exists('paymentStatus', $filtros) && $filtros['paymentStatus'] != '') {
            $query->andWhere('e.status LIKE :paymentStatus');
            $query->setParameter(':paymentStatus', '%' . $filtros['paymentStatus'] . '%');
        }
        $query->orderBy('e.created', 'DESC');
        return $query->getQuery();
    }

    /*
     * Find Pagos Pendientes
     */

    public function findPendientes() {
        $query = $this->createQueryBuilder('e')
                ->join('e.pedido', 'p')
                ->andWhere('p.estado != :abandonado and '
                        . 'p.estado != :cerrado and '
                        . 'p.estado != :inactivo and '
                        . 'p.estado != :entregado and '
                        . 'p.estado != :noentregado and '
                        . '((e.status != :payment_status_approved and '
                        . 'e.status != :payment_status_cancelled and '
                        . 'e.status != :payment_status_refunded and '
                        . 'e.status != :payment_status_charged_back and '
                        . 'e.status != :notfound and '
                        . 'e.enabled = :enabled) or  e.status is null)' )
                ->setParameter(':abandonado', Pedido::ABANDONADO)                
                ->setParameter(':cerrado', Pedido::CERRADO)                
                ->setParameter(':inactivo', Pedido::INACTIVO)                
                ->setParameter(':entregado', Pedido::ENTREGADO)                               
                ->setParameter(':noentregado', Pedido::NO_ENTREGADO)                
                ->setParameter(':notfound', '404')                                
                ->setParameter(':enabled', true)
                ->setParameter(':payment_status_approved', 'approved')
                ->setParameter(':payment_status_cancelled', 'cancelled')
                ->setParameter(':payment_status_refunded', 'refunded')
                ->setParameter(':payment_status_charged_back', 'charged_back') ;                
        return $query->getQuery()->getResult();
    }

}
