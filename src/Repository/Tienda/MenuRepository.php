<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Menu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

     /*
     * Filtros de búsqueda de promociones
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('m');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('m.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('m.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        if (isset($filtros['texto']) && $filtros['texto'] != '') {
            $query->andWhere('m.texto1 LIKE :texto1 ')
                    ->setParameter(':texto1', '%' . $filtros['texto'] . '%');
            $query->orWhere('m.texto2 LIKE :texto2 ')
                    ->setParameter(':texto2', '%' . $filtros['texto'] . '%');
        }
        
        $query->orderBy('m.descripcion', 'ASC');
        return $query;
    }
}
