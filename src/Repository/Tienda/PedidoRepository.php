<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Pedido;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PedidoRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Pedido::class);
    }

    public function getMiPedido(User $user) {
        $query = $this->createQueryBuilder('e');
        $query->where('e.user = :user');
        $query->setParameter(':user', $user)
                ->andWhere('e.estado != :estadoPendiente')
                ->setParameter(':estadoPendiente', Pedido::PENDIENTE);
        $query->orderBy('e.id', 'DESC');

        return $query->getQuery()->getResult();
    }

    public function getMisPedidos(User $user) {
       
        $query = $this->createQueryBuilder('e')
                ->where('e.user = :user')
                ->setParameter(':user', $user)
                ->andWhere('e.enabled = 1');                               
                       
        $query->orderBy('e.fecha', 'DESC');

        return $query->getQuery()->getResult();
    }
    
    

}
