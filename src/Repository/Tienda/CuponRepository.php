<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Cupon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CuponRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cupon::class);
    }

    public function filter($filtros) {
        $query = $this->createQueryBuilder('c');
        if (!empty($filtros['activo'])) {
            $query->andWhere('c.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }

        if (!empty($filtros['codigo'])) {            
            $query->andWhere('c.codigo = :codigo ')
                    ->setParameter(':codigo', $filtros['codigo']);
        }

        if (!empty($filtros['fechaDesde'])) {            
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('c.fechaDesde IS NULL OR c.fechaDesde <= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (!empty($filtros['fechaHasta'])) {
            $fechaHasta = new \DateTime($filtros['fechaHasta'].' 23:59');           
            $query->andWhere('c.fechaHasta IS NULL OR c.fechaHasta >= :fechaHasta ')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }
        
        $query->orderBy('c.id', 'DESC');
        
        return $query;
    }
}
