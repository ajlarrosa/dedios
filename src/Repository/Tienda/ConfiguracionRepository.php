<?php

namespace App\Repository\Tienda;

use App\Entity\Tienda\Configuracion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Configuracion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configuracion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configuracion[]    findAll()
 * @method Configuracion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfiguracionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configuracion::class);
    }

    /*
     * Filtros de búsqueda de promociones
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('c');
        
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('c.enabled = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['promocion']) && $filtros['promocion'] != '') {
            $query->andWhere('c.promocion LIKE :copromocionlor ')
                    ->setParameter(':promocion', '%' . $filtros['promocion'] . '%');
        }

        $query->orderBy('c.id', 'DESC');
        return $query;
    }
}
