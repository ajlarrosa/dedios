<?php

namespace App\Repository;

use App\Entity\TipoCheque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TipoCheque|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoCheque|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoCheque[]    findAll()
 * @method TipoCheque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoChequeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoCheque::class);
    }

  /*
     * Filtros de búsqueda de tipos de cheque
     */
    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');
        if (isset($filtros['activo']) && $filtros['activo'] != '') {
            $query->andWhere('b.activo = :activo')
                    ->setParameter(':activo', $filtros['activo']);
        }       

        if (isset($filtros['descripcion']) && $filtros['descripcion'] != '') {
            $query->andWhere('b.descripcion LIKE :descripcion ')
                    ->setParameter(':descripcion', '%' . $filtros['descripcion'] . '%');
        }

        $query->orderBy('b.descripcion', 'ASC');
        return $query;
    }
}
