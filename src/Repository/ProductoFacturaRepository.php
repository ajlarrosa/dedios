<?php

namespace App\Repository;

use App\Entity\ProductoFactura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductoFactura|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductoFactura|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductoFactura[]    findAll()
 * @method ProductoFactura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoFacturaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductoFactura::class);
    }

    // /**
    //  * @return ProductoFactura[] Returns an array of ProductoFactura objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductoFactura
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
