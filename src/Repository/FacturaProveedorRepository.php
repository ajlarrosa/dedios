<?php

namespace App\Repository;

use App\Entity\FacturaProveedor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacturaProveedor|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacturaProveedor|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacturaProveedor[]    findAll()
 * @method FacturaProveedor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturaProveedorRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FacturaProveedor::class);
    }

    /*
     * Filtros de búsqueda de facturas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');

        if (isset($filtros['nroFactura']) && $filtros['nroFactura'] != '') {
            $query->andWhere('b.id = :nroFactura ')
                    ->setParameter(':nroFactura', $filtros['nroFactura']);
        }
        if (isset($filtros['proveedor']) && $filtros['proveedor'] != '') {
            $query->Join('b.movimientocc', 'mcc');
            $query->andWhere('mcc.clienteProveedor  = :proveedor ')
                    ->setParameter(':proveedor', $filtros['proveedor']);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaDesdeCreated']) && $filtros['fechaDesdeCreated'] != '') {
            $fechaDesdeCreated = new \DateTime($filtros['fechaDesdeCreated']);
            $query->andWhere('b.created >= :fechaDesdeCreated ')
                    ->setParameter(':fechaDesdeCreated', $fechaDesdeCreated);
        }

        if (isset($filtros['fechaHastaCreated']) && $filtros['fechaHastaCreated'] != '') {
            $fechaHastaCreated = new \DateTime($filtros['fechaHastaCreated'] . ' 23:59');
            $query->andWhere(' :fechaHastaCreated >= b.creted')
                    ->setParameter(':fechaHastaCreated', $fechaHastaCreated);
        }


        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        $query->orderBy('b.id', 'DESC');
        return $query;
    }

}
