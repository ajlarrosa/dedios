<?php

namespace App\Repository;

use App\Entity\OrdenPago;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrdenPago|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdenPago|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdenPago[]    findAll()
 * @method OrdenPago[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenPagoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdenPago::class);
    }
    
    /*
     * Filtros de búsqueda de facturas
     */

    public function filter($filtros) {
        $query = $this->createQueryBuilder('b');

        if (isset($filtros['nroOrden']) && $filtros['nroOrden'] != '') {
            $query->andWhere('b.id = :nroOrden ')
                    ->setParameter(':nroOrden', $filtros['nroOrden']);
        }
        if (isset($filtros['cliente']) && $filtros['cliente'] != '') {
            $query->Join('b.movimientocc', 'mcc');
            $query->andWhere('mcc.clienteProveedor  = :cliente ')
                    ->setParameter(':cliente', $filtros['cliente']);
        }
        if (isset($filtros['fechaDesde']) && $filtros['fechaDesde'] != '') {
            $fechaDesde = new \DateTime($filtros['fechaDesde']);
            $query->andWhere('b.fecha >= :fechaDesde ')
                    ->setParameter(':fechaDesde', $fechaDesde);
        }

        if (isset($filtros['fechaHasta']) && $filtros['fechaHasta'] != '') {
            $fechaHasta = new \DateTime($filtros['fechaHasta'] . ' 23:59');
            $query->andWhere(' :fechaHasta >= b.fecha')
                    ->setParameter(':fechaHasta', $fechaHasta);
        }

        if (isset($filtros['fechaDesdeCreated']) && $filtros['fechaDesdeCreated'] != '') {
            $fechaDesdeCreated = new \DateTime($filtros['fechaDesdeCreated']);
            $query->andWhere('b.created >= :fechaDesdeCreated ')
                    ->setParameter(':fechaDesdeCreated', $fechaDesdeCreated);
        }

        if (isset($filtros['fechaHastaCreated']) && $filtros['fechaHastaCreated'] != '') {
            $fechaHastaCreated = new \DateTime($filtros['fechaHastaCreated'] . ' 23:59');
            $query->andWhere(' :fechaHastaCreated >= b.creted')
                    ->setParameter(':fechaHastaCreated', $fechaHastaCreated);
        }


        $query->orderBy('b.id', 'DESC');
        return $query;
    }
}
