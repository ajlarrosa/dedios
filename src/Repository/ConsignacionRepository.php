<?php

namespace App\Repository;

use App\Entity\Consignacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Consignacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Consignacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Consignacion[]    findAll()
 * @method Consignacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsignacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Consignacion::class);
    }

    // /**
    //  * @return Consignacion[] Returns an array of Consignacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Consignacion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
