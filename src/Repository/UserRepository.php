<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByFilters($filters) {
        $qb = $this->createQueryBuilder('u');


        //Filter By Admin
        if (isset($filters['admin']) && $filters['admin'] != '') {
            
            if ($filters['admin']) {
                $qb->andWhere('u.roles LIKE :rol or u.roles LIKE :rol2')
                        ->setParameter(':rol', '%ROLE_OPERADOR%')
                        ->setParameter(':rol2', '%ROLE_ADMIN%');
            } else {
                $qb->andWhere('u.roles LIKE :rol or u.roles LIKE :rol2')
                        ->setParameter(':rol', '%ROLE_USER_MAYORISTA%')
                        ->setParameter(':rol2', '%ROLE_USER_MINORISTA%');
            }
        }

        //Filter By Username
        if (isset($filters['username']) && $filters['username'] != '') {
            $qb->andWhere('u.username LIKE :username')
                    ->setParameter(':username', '%' . $filters['username'] . '%');
        }
        //Filter By Name
        if (isset($filters['nombre']) && $filters['nombre'] != '') {
            $qb->andWhere('u.nombre LIKE :nombre')
                    ->setParameter(':nombre', '%' . $filters['nombre'] . '%');
        }
        //Filter By Mail
        if (isset($filters['mail']) && $filters['mail'] != '') {
            $qb->andWhere('u.mail LIKE :mail')
                    ->setParameter(':mail', '%' . $filters['mail'] . '%');
        }
        //Filter By Enabled
        if (isset($filters['activo']) && $filters['activo'] != '') {
            $qb->andWhere('u.enabled = :activo')
                    ->setParameter(':activo', $filters['activo']);
        }
        //Filter By isVerified
        if (isset($filters['verificado']) && $filters['verificado'] != '') {
            $qb->andWhere('u.isVerified = :verificado')
                    ->setParameter(':verificado', $filters['verificado']);
        }
        //Filter By Approved
        if (isset($filters['aprobado']) && $filters['aprobado'] != '') {
            $qb->andWhere('u.aprobado = :aprobado')
                    ->setParameter(':aprobado', $filters['aprobado']);
        }
        //Filter By Document
        if (isset($filters['dni']) && $filters['dni'] != '') {
            $qb->andWhere('u.dni LIKE :dni')
                    ->setParameter(':dni', '%' . $filters['dni'] . '%');
        }
        //Filter By Locality
        if (isset($filters['localidad']) && $filters['localidad'] != '') {
            $qb->Join('App\Entity\Tienda\DireccionEnvio', 'de', 'WITH', 'de.user = u.id');
            $qb->andWhere('de.localidad LIKE :localidad')
                    ->setParameter(':localidad', '%' . $filters['localidad'] . '%');
        }
        //Filter By User Role
        if (isset($filters['userRol']) && $filters['userRol'] != '') {
            $qb->andWhere('u.roles LIKE :role')
                    ->setParameter(':role', '%' . $filters['userRol'] . '%');
        }

        $qb->orderBy('u.username', 'ASC');
        
        return $qb->getQuery()->getResult();
    }

    /*
     * 
     */

    public function findOneByVerifyEmail($filters) {

        $date = new \DateTime;
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.signature = :signature')
                ->setParameter(':signature', urlencode($filters['signature']));

        $qb->andWhere('u.token = :token')
                ->setParameter(':token', urlencode($filters['token']));

        $qb->andWhere('u.expires > :expires')
                ->setParameter(':expires', $date)
                ->getQuery()
                ->getOneOrNullResult();
        return $qb->getQuery()->getOneOrNullResult();
    }

}
