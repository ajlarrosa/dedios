<?php

namespace App\Repository;

use App\Entity\NotaCredito;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NotaCredito|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotaCredito|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotaCredito[]    findAll()
 * @method NotaCredito[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotaCreditoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotaCredito::class);
    }

 
}
