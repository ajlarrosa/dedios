<?php

namespace App\Controller;

use App\Entity\Cotizacion;
use App\Form\CotizacionType;
use App\Repository\CotizacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/cotizacion")
 */
class CotizacionController extends AbstractController {

    /**
     * @Route("/operator/", name="cotizacion_index", methods={"GET"})
     */
    public function index(Request $request, CotizacionRepository $cotizacionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $cotizacions = $paginator->paginate(
                $cotizacionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('cotizacion/index.html.twig', [
                    'cotizacions' => $cotizacions,
        ]);
    }

    /**
     * @Route("/admin/new", name="cotizacion_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $cotizacion = new Cotizacion();
        $form = $this->createForm(CotizacionType::class, $cotizacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cotizacion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Quotation created successfully.'));
            return $this->redirectToRoute('cotizacion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the quotation.'));
        }
        return $this->render('cotizacion/new.html.twig', [
                    'cotizacion' => $cotizacion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="cotizacion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Cotizacion $cotizacion, TranslatorInterface $translator) {
        if (!$cotizacion) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected quotation.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $cotizacion->setActivo($activo);
            $entityManager->persist($cotizacion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Quotation status has been changed', ['%cotizacion%' => $cotizacion]));
        }
        return $this->redirect($this->generateUrl('cotizacion_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="cotizacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cotizacion $cotizacion, TranslatorInterface $translator): Response {
        $form = $this->createForm(CotizacionType::class, $cotizacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Quotation edited successfully.'));
            return $this->redirectToRoute('cotizacion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the quotation.'));
        }
        return $this->render('cotizacion/edit.html.twig', [
                    'cotizacion' => $cotizacion,
                    'form' => $form->createView(),
        ]);
    }

}
