<?php

namespace App\Controller;

use App\Entity\TipoCheque;
use App\Form\TipoChequeType;
use App\Repository\TipoChequeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/tipocheque")
 */
class TipoChequeController extends AbstractController {

    /**
     * @Route("/operator/", name="tipocheque_index", methods={"GET"})
     */
    public function index(Request $request, TipoChequeRepository $tipochequeRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $tipocheques = $paginator->paginate(
                $tipochequeRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tipocheque/index.html.twig', [
                    'tipocheques' => $tipocheques,
        ]);
    }

    /**
     * @Route("/admin/new", name="tipocheque_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $tipocheque = new TipoCheque();
        $form = $this->createForm(TipoChequeType::class, $tipocheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tipocheque);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Check type created successfully.'));
            return $this->redirectToRoute('tipocheque_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the check type.'));
        }
        return $this->render('tipocheque/new.html.twig', [
                    'tipocheque' => $tipocheque,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="tipocheque_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, TipoCheque $tipocheque, TranslatorInterface $translator) {
        if (!$tipocheque) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected check type.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $tipocheque->setActivo($activo);
            $entityManager->persist($tipocheque);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Check type status has been changed', ['%check type%' => $tipocheque->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tipocheque_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="tipocheque_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoCheque $tipocheque, TranslatorInterface $translator): Response {
        $form = $this->createForm(TipoChequeType::class, $tipocheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Check type edited successfully.'));
            return $this->redirectToRoute('tipocheque_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the check type.'));
        }
        return $this->render('tipocheque/edit.html.twig', [
                    'tipocheque' => $tipocheque,
                    'form' => $form->createView(),
        ]);
    }

}
