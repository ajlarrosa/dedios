<?php

namespace App\Controller;

use App\Entity\Letra;
use App\Form\LetraType;
use App\Repository\LetraRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;


class LetraController extends AbstractController
{
    
    /**
     * @Route("/operator/letra", name="letra_index", methods={"GET"})
     */
    public function index(Request $request, LetraRepository $letraRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $letras = $paginator->paginate(
                $letraRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('letra/index.html.twig', [
                    'letras' => $letras,
        ]);
    }
    
    /**
     * @Route("/admin/letra/new", name="letra_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $letra = new Letra();
        $form = $this->createForm(LetraType::class, $letra);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($letra);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Letter created successfully.'));
            return $this->redirectToRoute('letra_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the letter.'));
        }
        return $this->render('letra/new.html.twig', [
                    'letra' => $letra,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/letra/{id}/edit", name="letra_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Letra $letra, TranslatorInterface $translator): Response {
        $form = $this->createForm(LetraType::class, $letra);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Letter edited successfully.'));
            return $this->redirectToRoute('letra_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the letter.'));
        }
        return $this->render('letra/edit.html.twig', [
                    'letra' => $letra,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/letra/activate/{id}", name="letra_activate", methods={"GET"})
     *
     */
    public function activate(Request $request, Letra $letra, TranslatorInterface $translator) {
        if (!$letra) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected letter.'));
        } else {
            $enabled = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $letra->setEnabled($enabled);
            $entityManager->persist($letra);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Letter {{ letter }} status has been changed', ['{{ letter }}' => $letra->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('letra_index'));
    }
}
