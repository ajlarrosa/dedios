<?php

namespace App\Controller;

use App\Entity\ClienteProveedor;
use App\Form\ClienteType;
use App\Repository\ClienteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/cliente/operator")
 */
class ClienteController extends AbstractController {

    /**
     * @Route("/", name="cliente_index", methods={"GET"})
     */
    public function index(Request $request, ClienteRepository $clienteRepository, PaginatorInterface $paginator): Response {        
        $filtros = $request->query->all();
        $clientes = $paginator->paginate(
                $clienteRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('cliente/index.html.twig', [
                    'clientes' => $clientes,
        ]);
    }

    /**
     * @Route("/new", name="cliente_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $cliente = new ClienteProveedor();
        $cliente->setClienteProveedor(ClienteProveedor::CLIENTE_COD);
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cliente);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Client created successfully.'));
            return $this->redirectToRoute('cliente_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the client.'));
        }
        return $this->render('cliente/new.html.twig', [
                    'cliente' => $cliente,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activate/{id}", name="cliente_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, ClienteProveedor $cliente, TranslatorInterface $translator) {
        if (!$cliente) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected client.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $cliente->setEnabled($activo);
            $entityManager->persist($cliente);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Client status has been changed', ['%client%' => $cliente]));
        }
        return $this->redirect($this->generateUrl('cliente_index'));
    }

    /**
     * @Route("/{id}/edit", name="cliente_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ClienteProveedor $cliente, TranslatorInterface $translator): Response {
        $form = $this->createForm(ClienteType::class, $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Client edited successfully.'));
            return $this->redirectToRoute('cliente_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the client.'));
        }
        return $this->render('cliente/edit.html.twig', [
                    'cliente' => $cliente,
                    'form' => $form->createView(),
        ]);
    }

}
