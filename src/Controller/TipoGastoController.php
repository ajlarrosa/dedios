<?php

namespace App\Controller;

use App\Entity\TipoGasto;
use App\Form\TipoGastoType;
use App\Repository\TipoGastoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/tipogasto")
 */
class TipoGastoController extends AbstractController {

    /**
     * @Route("/operator/", name="tipogasto_index", methods={"GET"})
     */
    public function index(Request $request, TipoGastoRepository $tipogastoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $tipogastos = $paginator->paginate(
                $tipogastoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tipogasto/index.html.twig', [
                    'tipogastos' => $tipogastos,
        ]);
    }

    /**
     * @Route("/admin/new", name="tipogasto_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $tipogasto = new TipoGasto();
        $form = $this->createForm(TipoGastoType::class, $tipogasto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tipogasto);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Expense type created successfully.'));
            return $this->redirectToRoute('tipogasto_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the expense type.'));
        }
        return $this->render('tipogasto/new.html.twig', [
                    'tipogasto' => $tipogasto,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="tipogasto_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, TipoGasto $tipogasto, TranslatorInterface $translator) {
        if (!$tipogasto) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected expense type.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $tipogasto->setActivo($activo);
            $entityManager->persist($tipogasto);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Expense type status has been changed', ['%expense type%' => $tipogasto->getNombre()]));
        }
        return $this->redirect($this->generateUrl('tipogasto_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="tipogasto_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoGasto $tipogasto, TranslatorInterface $translator): Response {
        $form = $this->createForm(TipoGastoType::class, $tipogasto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Expense type edited successfully.'));
            return $this->redirectToRoute('tipogasto_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the expense type.'));
        }
        return $this->render('tipogasto/edit.html.twig', [
                    'tipogasto' => $tipogasto,
                    'form' => $form->createView(),
        ]);
    }

}
