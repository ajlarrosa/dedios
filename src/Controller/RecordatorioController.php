<?php

namespace App\Controller;

use App\Entity\Recordatorio;
use App\Form\RecordatorioType;
use App\Repository\RecordatorioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/")
 */
class RecordatorioController extends AbstractController
{   
    private $translator;
    private $recordatorioRepository;
    
    function __construct(RecordatorioRepository $recordatorioRepository, TranslatorInterface $translator) {
        $this->recordatorioRepository = $recordatorioRepository;
        $this->translator = $translator;        
    }

    /**
     * @Route("/operator/recordatorio", name="recordatorio_index", methods={"GET"})
     */
    public function index(Request $request,RecordatorioRepository $recordatorioRepository, PaginatorInterface $paginator): Response
    {
        
        $filtros = $request->query->all();
        
        $recordatorios = $paginator->paginate(
                $recordatorioRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );

        return $this->render('recordatorio/index.html.twig', [
                    'recordatorios' => $recordatorios,
        ]);
    }

    /**
     * @Route("/operator/recordatorio/new", name="recordatorio_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $recordatorio = new Recordatorio();
        $form = $this->createForm(RecordatorioType::class, $recordatorio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($recordatorio);
            $entityManager->flush();

            return $this->redirectToRoute('recordatorio_index');
        }

        return $this->render('recordatorio/new.html.twig', [
            'recordatorio' => $recordatorio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/operator/recordatorio/{id}", name="recordatorio_show", methods={"GET"})
     */
    public function show(Recordatorio $recordatorio): Response
    {
        return $this->render('recordatorio/show.html.twig', [
            'recordatorio' => $recordatorio,
        ]);
    }

    /**
     * @Route("/operator/recordatorio/{id}/edit", name="recordatorio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Recordatorio $recordatorio): Response
    {
        $form = $this->createForm(RecordatorioType::class, $recordatorio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recordatorio_index');
        }

        return $this->render('recordatorio/edit.html.twig', [
            'recordatorio' => $recordatorio,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/operator/recordatorio/{id}", name="recordatorio_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Recordatorio $recordatorio): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recordatorio->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($recordatorio);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recordatorio_index');
    }
}
