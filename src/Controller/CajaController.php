<?php

namespace App\Controller;

use App\Entity\MovimientoCC;
use App\Entity\ClienteProveedor;
use App\Form\MovimientoCCType;
use App\Repository\CajaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/caja")
 */
class CajaController extends AbstractController {

    /**
     * @Route("/gestion", name="caja_index", methods={"GET"})
     */
    public function index(Request $request, CajaRepository $cajaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $movimientoscc = $paginator->paginate(
                $cajaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
       
        
        return $this->render('caja/index.html.twig', [
                    'movimientoscc' =>$movimientoscc
        ]);
    }

    /**
     * @Route("/new", name="caja_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $movimientoCC = new MovimientoCC();
        $form = $this->createForm(MovimientoCCType::class, $movimientoCC);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movimientoCC);
            $entityManager->flush();

            return $this->redirectToRoute('caja_index');
        }

        return $this->render('caja/new.html.twig', [
                    'movimientocc' => $movimientoCC,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="caja_show", methods={"GET"})
     */
    public function show(MovimientoCC $movimientoCC): Response {
        return $this->render('caja/show.html.twig', [
                    'movimientocc' => $movimientoCC,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="caja_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MovimientoCC $movimientoCC): Response {
        $form = $this->createForm(MovimientoCCType::class, $movimientoCC);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('caja_index');
        }

        return $this->render('caja/edit.html.twig', [
                    'movimientocc' => $movimientoCC,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="caja_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MovimientoCC $movimientoCC): Response {
        if ($this->isCsrfTokenValid('delete' . $movimientoCC->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movimientoCC);
            $entityManager->flush();
        }

        return $this->redirectToRoute('caja_index');
    }

}
