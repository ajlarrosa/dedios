<?php

namespace App\Controller;

use App\Entity\Consignacion;
use App\Form\ConsignacionType;
use App\Repository\ConsignacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/consignacion")
 */
class ConsignacionController extends AbstractController
{
    /**
     * @Route("/", name="consignacion_index", methods={"GET"})
     */
    public function index(ConsignacionRepository $consignacionRepository): Response
    {
        return $this->render('consignacion/index.html.twig', [
            'consignacions' => $consignacionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="consignacion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $consignacion = new Consignacion();
        $form = $this->createForm(ConsignacionType::class, $consignacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($consignacion);
            $entityManager->flush();

            return $this->redirectToRoute('consignacion_index');
        }

        return $this->render('consignacion/new.html.twig', [
            'consignacion' => $consignacion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="consignacion_show", methods={"GET"})
     */
    public function show(Consignacion $consignacion): Response
    {
        return $this->render('consignacion/show.html.twig', [
            'consignacion' => $consignacion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="consignacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Consignacion $consignacion): Response
    {
        $form = $this->createForm(ConsignacionType::class, $consignacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('consignacion_index');
        }

        return $this->render('consignacion/edit.html.twig', [
            'consignacion' => $consignacion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="consignacion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Consignacion $consignacion): Response
    {
        if ($this->isCsrfTokenValid('delete'.$consignacion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($consignacion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('consignacion_index');
    }
}
