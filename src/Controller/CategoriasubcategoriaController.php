<?php

namespace App\Controller;

use App\Entity\Categoriasubcategoria;
use App\Entity\Categoria;
use App\Form\CategoriasubcategoriaType;
use App\Repository\CategoriasubcategoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;

/**
 * @Route("/categoria-subcategoria-metal")
 */
class CategoriasubcategoriaController extends AbstractController {

      private $generalService;
    
    function __construct(GeneralService $generalService) {
        $this->generalService= $generalService;        
    }

    
    /**
     * @Route("/operator/", name="categoriasubcategoria_index", methods={"GET"})
     */
    public function index(Request $request, CategoriasubcategoriaRepository $categoriasubcategoriaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();

        $categoriasubcategorias = $paginator->paginate(
                $categoriasubcategoriaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('categoriasubcategoria/index.html.twig', [
                    'categoriasubcategorias' => $categoriasubcategorias,
        ]);
    }

    /**
     * @Route("/admin/new", name="categoriasubcategoria_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $categoriasubcategoria = new Categoriasubcategoria();
        $form = $this->createForm(CategoriasubcategoriaType::class, $categoriasubcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $error = false;
            if ($_FILES['categoriasubcategoria']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['categoriasubcategoria']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $blobImage = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($_FILES['categoriasubcategoria']['tmp_name']['imagen']));
                    $categoriasubcategoria->setImagen($blobImage);
                }
            }
            if (!$error) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($categoriasubcategoria);
                $entityManager->flush();
                $this->addFlash('msgOk', $translator->trans('Category subcategory metal relation created successfully.'));
                return $this->redirectToRoute('categoriasubcategoria_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the category subcategory metal relation.'));
        }
        return $this->render('categoriasubcategoria/new.html.twig', [
                    'categoriasubcategoria' => $categoriasubcategoria,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="categoriasubcategoria_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Categoriasubcategoria $categoriasubcategoria, TranslatorInterface $translator) {
        if (!$categoriasubcategoria) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected category subcategory metal relation.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $categoriasubcategoria->setActivo($activo);
            $entityManager->persist($categoriasubcategoria);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Category subcategory metal relation status has been changed', ['%replace%' => $categoriasubcategoria]));
        }
        return $this->redirect($this->generateUrl('categoriasubcategoria_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="categoriasubcategoria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categoriasubcategoria $categoriasubcategoria, TranslatorInterface $translator): Response {
        $form = $this->createForm(CategoriasubcategoriaType::class, $categoriasubcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($_FILES['categoriasubcategoria']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['categoriasubcategoria']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    return $this->render('categoriasubcategoria/edit.html.twig', [
                                'categoriasubcategoria' => $categoriasubcategoria,
                                'form' => $form->createView(),
                    ]);
                }

                $blobImage = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($_FILES['categoriasubcategoria']['tmp_name']['imagen']));
                $categoriasubcategoria->setImagen($blobImage);
            }
            
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Category subcategory metal relation edited successfully.'));
            return $this->redirectToRoute('categoriasubcategoria_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the category subcategory metal relation.'));
        }
        return $this->render('categoriasubcategoria/edit.html.twig', [
                    'categoriasubcategoria' => $categoriasubcategoria,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}/getSubcategories", name="categoriasubcategoria_get_subcategories", methods={"GET","POST"})
     */
    public function getSubcategories(Categoria $categoria, TranslatorInterface $translator): Response {
        if($categoria){
            $entitityManager = $this->getDoctrine()->getManager();
            
        $categoriaSubcategoria = $entitityManager->getRepository(CategoriaSubcategoria::class)->findBy(['activo'=>true,'categoria'=>$categoria]);
            
            $html='<option value="">'.$translator->trans('Select subcategory').'</option>';
                    
            foreach($categoriaSubcategoria as $cs){
                $html.='<option value="'.$cs->getSubcategoria()->getId().'" >'.$cs->getSubcategoria()->getDescripcion().'</option>';
            }
            return new Response($html);
        }
        
        return new Response('Error');
    }
    

}
