<?php

namespace App\Controller;

use App\Entity\Factura;
use App\Service\FacturaService;
use App\Service\ProductoService;
use App\Service\ClienteService;
use App\Service\MovimientoCCService;
use App\Service\ProductoFacturaService;
use App\Form\FacturaType;
use App\Repository\FacturaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/operator/factura")
 */
class FacturaController extends AbstractController {

    /**
     * @Route("/", name="factura_index", methods={"GET"})
     */
    public function index(Request $request, FacturaRepository $facturaRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {

        $filtros = $request->query->all();
        
        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $facturas = $facturaRepository->filter($filtros)->getQuery()->getResult();
            
            $spreadSheet = $this->excelInforme($facturas, $filtros);

            $nombreInforme = 'Ventas';
            
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }
        
        
        $facturas = $paginator->paginate(
                $facturaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('factura/index.html.twig', [
                    'facturas' => $facturas,
        ]);
    }

     /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($facturas, $parametros) {

     $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $fila = 3;
        
        
        
        foreach ($facturas as $factura) {
            $subtotal = 0;
            $cont = 0;
            
            $sheet->setCellValue('A' . $fila, 'Factura Nro.'.$factura->getId());
            $sheet->setCellValue('B' . $fila, 'Cliente');
            $sheet->setCellValue('C' . $fila, 'Fecha');
            $sheet->setCellValue('D' . $fila, 'Observación');
            $sheet->setCellValue('E' . $fila, 'Descuento');
            $sheet->setCellValue('F' . $fila, 'Bonificación');
            
            $sheet->getStyle('A' . $fila . ':H' . $fila)->applyFromArray($styleArray2);

            $fila++;
            
            $sheet->setCellValue('B' . $fila, $factura->getMovimientocc()->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, $factura->getMovimientocc()->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('D' . $fila, $factura->getObservacion());
            $sheet->setCellValue('E' . $fila, $factura->getDescuento());
            $sheet->setCellValue('F' . $fila, $factura->getBonificacion());
            
            
            $fila++;
            
            $sheet->setCellValue('B' . $fila, 'Cantidad');
            $sheet->setCellValue('C' . $fila, 'Código');
            $sheet->setCellValue('D' . $fila, 'Descripción');
            $sheet->setCellValue('E' . $fila, 'Precio');
            $sheet->setCellValue('F' . $fila, 'Subtotal');
            $sheet->getStyle('B'.$fila.':F'.$fila)->applyFromArray($styleArray2);
            
            $fila++;
            
            foreach ($factura->getProductosFactura() as $productoFactura) {
                $cont+1;
                
                $sheet->setCellValue('B' . $fila, $productoFactura->getCantidad());
                $sheet->setCellValue('C' . $fila, $productoFactura->getProducto()->getCodigo());
                $sheet->setCellValue('D' . $fila, $productoFactura->getProducto()->getDescripcion());
                $sheet->setCellValue('E' . $fila, $factura->getMonedaSimbolo().$productoFactura->getPrecio());
                $subtotal=$subtotal+$productoFactura->getPrecio();
                $sheet->setCellValue('F' . $fila, $factura->getMonedaSimbolo().$subtotal);
                
                $fila++;
            }
            
            
            $fila++;
            $sheet->setCellValue('B' . $fila, 'Total Oro:');
            $sheet->setCellValue('C' . $fila, $factura->getOro().' gr');
            $sheet->setCellValue('E' . $fila, 'Subtotal:');
            $sheet->setCellValue('F' . $fila, $factura->getMonedaSimbolo().$subtotal);
            $fila++;
            $sheet->setCellValue('B' . $fila, 'Total Plata:');
            $sheet->setCellValue('C' . $fila, $factura->getPlata().' gr');
            $sheet->setCellValue('E' . $fila, 'Descuento('.$factura->getDescuento().'%):');
            $sheet->setCellValue('F' . $fila, $factura->getMonedaSimbolo().$factura->getDescuentoValue());
            $sheet->getStyle('B'.($fila-1).':B'.$fila)->applyFromArray($styleArray2);
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Bonificación:');
            $sheet->setCellValue('F' . $fila, $factura->getMonedaSimbolo().$factura->getBonificacion());
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Total:');
            $sheet->setCellValue('F' . $fila, $factura->getMonedaSimbolo().$factura->getTotal());
            $fila++;
            $sheet->getStyle('E'.($fila-4).':E'.$fila)->applyFromArray($styleArray2);
            $fila++;
        }        
        
        $fila++;
        
        //Totales
        $sheet->setCellValue('A1', 'Total Facturas Listadas: '.(sizeof($facturas)));
        
        
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }
    
    
    
    /**
     * @Route("/new", name="factura_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, ProductoService $productoService, ProductoFacturaService $productoFacturaService, ClienteService $clienteService, MovimientoCCService $movimientoCCService): Response {
        $factura = new Factura();
        $form = $this->createForm(FacturaType::class, $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $facturasIds = $request->get('id');
            $cantidades = $request->get('cantidad');
            $descripciones = $request->get('descripcion');
            $precios = $request->get('precio');
            $codigos = $request->get('codigo');
            
            $entityManager = $this->getDoctrine()->getManager();
            
            foreach ($facturasIds as $key => $productoId) {
                if ($codigos[$key] != '') {
                    $productoService->getProducto($productoId);
                    $productoFactura = $productoFacturaService->create($factura, $productoService->getProducto($productoId), $cantidades[$key], $descripciones[$key], $precios[$key]);
                    $entityManager->persist($productoFactura);
                }
            }

            $movimientocc = $movimientoCCService->create($clienteService->getClienteById($request->get('cliente')), $factura);

            $factura->setMovimientocc($movimientocc);            
            $entityManager->persist($movimientocc);
            $entityManager->persist($factura);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Invoice created successfully.'));
            return $this->redirectToRoute('factura_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the invoice.'));
        }
        return $this->render('factura/new.html.twig', [
                    'factura' => $factura,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/addRow", name="factura_add_row", methods={"GET","POST"})
     */
    public function addRow(Request $request, FacturaService $facturaService): Response {

        $line = $request->get('line');
        return new Response($facturaService->addFacturaRow($line));
    }

    /**
     * @Route("/{id}", name="factura_show", methods={"GET"})
     */
    public function show(Factura $factura): Response {

        return $this->render('factura/show.html.twig', [
                    'factura' => $factura
        ]);
    }

}
