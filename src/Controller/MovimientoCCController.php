<?php

namespace App\Controller;

use App\Entity\MovimientoCC;
use App\Entity\ClienteProveedor;
use App\Form\MovimientoCCType;
use App\Repository\MovimientoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/movimientocc")
 */
class MovimientoCCController extends AbstractController {

    /**
     * @Route("/{id}/cuentaCorriente", name="movimientocc_index", methods={"GET"})
     */
    public function index(ClienteProveedor $clienteProveedor, Request $request, MovimientoRepository $movimientoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $filtros['clienteProveedor'] = $clienteProveedor;
        $movimientoscc = $paginator->paginate(
                $movimientoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
       
        
        return $this->render('movimiento_cc/index.html.twig', [
                    'clienteProveedor' => $clienteProveedor,
                    'movimientoscc' =>$movimientoscc
        ]);
    }

    /**
     * @Route("/new", name="movimientocc_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $movimientoCC = new MovimientoCC();
        $form = $this->createForm(MovimientoCCType::class, $movimientoCC);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movimientoCC);
            $entityManager->flush();

            return $this->redirectToRoute('movimientocc_index');
        }

        return $this->render('movimiento_cc/new.html.twig', [
                    'movimientocc' => $movimientoCC,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movimientocc_show", methods={"GET"})
     */
    public function show(MovimientoCC $movimientoCC): Response {
        return $this->render('movimiento_cc/show.html.twig', [
                    'movimientocc' => $movimientoCC,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="movimientocc_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MovimientoCC $movimientoCC): Response {
        $form = $this->createForm(MovimientoCCType::class, $movimientoCC);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('movimientocc_index');
        }

        return $this->render('movimiento_cc/edit.html.twig', [
                    'movimientocc' => $movimientoCC,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movimientocc_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MovimientoCC $movimientoCC): Response {
        if ($this->isCsrfTokenValid('delete' . $movimientoCC->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movimientoCC);
            $entityManager->flush();
        }

        return $this->redirectToRoute('movimientocc_index');
    }

}
