<?php

namespace App\Controller;

use App\Entity\Metal;
use App\Form\MetalType;
use App\Repository\MetalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/metal")
 */
class MetalController extends AbstractController {

    /**
     * @Route("/operator/", name="metal_index", methods={"GET"})
     */
    public function index(Request $request, MetalRepository $metalRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $metals = $paginator->paginate(
                $metalRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('metal/index.html.twig', [
                    'metals' => $metals,
        ]);
    }

    /**
     * @Route("/admin/new", name="metal_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $metal = new Metal();
        $form = $this->createForm(MetalType::class, $metal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($metal);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Metal created successfully.'));
            return $this->redirectToRoute('metal_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the metal.'));
        }
        return $this->render('metal/new.html.twig', [
                    'metal' => $metal,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="metal_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Metal $metal, TranslatorInterface $translator) {
        if (!$metal) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected metal.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $metal->setActivo($activo);
            $entityManager->persist($metal);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Metal status has been changed', ['%metal%' => $metal->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('metal_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="metal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Metal $metal, TranslatorInterface $translator): Response {
        $form = $this->createForm(MetalType::class, $metal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Metal edited successfully.'));
            return $this->redirectToRoute('metal_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the metal.'));
        }
        return $this->render('metal/edit.html.twig', [
                    'metal' => $metal,
                    'form' => $form->createView(),
        ]);
    }

}
