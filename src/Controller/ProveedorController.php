<?php

namespace App\Controller;

use App\Entity\ClienteProveedor;
use App\Form\ProveedorType;
use App\Repository\ProveedorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/proveedor/operator")
 */
class ProveedorController extends AbstractController {

    /**
     * @Route("/", name="proveedor_index", methods={"GET"})
     */
    public function index(Request $request, ProveedorRepository $proveedorRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $proveedors = $paginator->paginate(
                $proveedorRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('proveedor/index.html.twig', [
                    'proveedors' => $proveedors,
        ]);
    }

    /**
     * @Route("/new", name="proveedor_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $proveedor = new ClienteProveedor();
        $proveedor->setClienteProveedor(ClienteProveedor::PROVEEDOR_COD);
        $form = $this->createForm(ProveedorType::class, $proveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($proveedor);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Provider created successfully.'));
            return $this->redirectToRoute('proveedor_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the provider.'));
        }
        return $this->render('proveedor/new.html.twig', [
                    'proveedor' => $proveedor,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activate/{id}", name="proveedor_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, ClienteProveedor $proveedor, TranslatorInterface $translator) {
        if (!$proveedor) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected provider.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $proveedor->setActivo($activo);
            $entityManager->persist($proveedor);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Provider status has been changed', ['%provider%' => $proveedor]));
        }
        return $this->redirect($this->generateUrl('proveedor_index'));
    }

    /**
     * @Route("/{id}/edit", name="proveedor_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,ClienteProveedor $proveedor, TranslatorInterface $translator): Response {
        $form = $this->createForm(ProveedorType::class, $proveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Provider edited successfully.'));
            return $this->redirectToRoute('proveedor_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the provider.'));
        }
        return $this->render('proveedor/edit.html.twig', [
                    'proveedor' => $proveedor,
                    'form' => $form->createView(),
        ]);
    }

}
