<?php

namespace App\Controller;

use App\Entity\Banco;
use App\Form\BancoType;
use App\Repository\BancoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/banco")
 */
class BancoController extends AbstractController {

    /**
     * @Route("/operator/", name="banco_index", methods={"GET"})
     */
    public function index(Request $request, BancoRepository $bancoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $bancos = $paginator->paginate(
                $bancoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('banco/index.html.twig', [
                    'bancos' => $bancos,
        ]);
    }

    /**
     * @Route("/admin/new", name="banco_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $banco = new Banco();
        $form = $this->createForm(BancoType::class, $banco);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($banco);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Bank created successfully.'));
            return $this->redirectToRoute('banco_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the bank.'));
        }
        return $this->render('banco/new.html.twig', [
                    'banco' => $banco,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="banco_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Banco $banco, TranslatorInterface $translator) {
        if (!$banco) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected bank.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $banco->setActivo($activo);
            $entityManager->persist($banco);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Bank status has been changed', ['%bank%' => $banco->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('banco_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="banco_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Banco $banco, TranslatorInterface $translator): Response {
        $form = $this->createForm(BancoType::class, $banco);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Bank edited successfully.'));
            return $this->redirectToRoute('banco_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the bank.'));
        }
        return $this->render('banco/edit.html.twig', [
                    'banco' => $banco,
                    'form' => $form->createView(),
        ]);
    }

}
