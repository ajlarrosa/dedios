<?php

namespace App\Controller;

use App\Entity\Subcategoria;
use App\Form\SubcategoriaType;
use App\Repository\SubcategoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/subcategoria")
 */
class SubcategoriaController extends AbstractController {

    /**
     * @Route("/operator/", name="subcategoria_index", methods={"GET"})
     */
    public function index(Request $request, SubcategoriaRepository $subcategoriaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $subcategorias = $paginator->paginate(
                $subcategoriaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('subcategoria/index.html.twig', [
                    'subcategorias' => $subcategorias,
        ]);
    }

    /**
     * @Route("/admin/new", name="subcategoria_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $subcategoria = new Subcategoria();
        $form = $this->createForm(SubcategoriaType::class, $subcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subcategoria);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Subcategory created successfully.'));
            return $this->redirectToRoute('subcategoria_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the subcategory.'));
        }
        return $this->render('subcategoria/new.html.twig', [
                    'subcategoria' => $subcategoria,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/enabled/{id}", name="subcategoria_enabled", methods={"GET"})
     *
     */
    public function enabled(Request $request, Subcategoria $subcategoria, TranslatorInterface $translator) {
        if (!$subcategoria) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected subcategory.'));
        } else {
            $enabled = $request->get('enabled');
            $entityManager = $this->getDoctrine()->getManager();
            $subcategoria->setEnabled($enabled);
            $entityManager->persist($subcategoria);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Subcategory status has been changed', ['%subcategory%' => $subcategoria->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('subcategoria_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="subcategoria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Subcategoria $subcategoria, TranslatorInterface $translator): Response {
        $form = $this->createForm(SubcategoriaType::class, $subcategoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Subcategory edited successfully.'));
            return $this->redirectToRoute('subcategoria_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the subcategory.'));
        }
        return $this->render('subcategoria/edit.html.twig', [
                    'subcategoria' => $subcategoria,
                    'form' => $form->createView(),
        ]);
    }

}
