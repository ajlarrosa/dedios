<?php

namespace App\Controller;

use App\Entity\ListaPrecio;
use App\Form\ListaPrecioType;
use App\Repository\ListaPrecioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/listaprecio")
 */
class ListaPrecioController extends AbstractController {

    /**
     * @Route("/operator/", name="listaprecio_index", methods={"GET"})
     */
    public function index(Request $request, ListaPrecioRepository $listaprecioRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $listaprecios = $paginator->paginate(
                $listaprecioRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('listaprecio/index.html.twig', [
                    'listaprecios' => $listaprecios,
        ]);
    }

    /**
     * @Route("/admin/new", name="listaprecio_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $listaprecio = new ListaPrecio();
        $form = $this->createForm(ListaPrecioType::class, $listaprecio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($listaprecio);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Price List created successfully.'));
            return $this->redirectToRoute('listaprecio_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the price list.'));
        }
        return $this->render('listaprecio/new.html.twig', [
                    'listaprecio' => $listaprecio,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="listaprecio_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, ListaPrecio $listaprecio, TranslatorInterface $translator) {
        if (!$listaprecio) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected price list.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $listaprecio->setActivo($activo);
            $entityManager->persist($listaprecio);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Price List status has been changed', ['%price list%' => $listaprecio->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('listaprecio_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="listaprecio_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ListaPrecio $listaprecio, TranslatorInterface $translator): Response {
        $form = $this->createForm(ListaPrecioType::class, $listaprecio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Price List edited successfully.'));
            return $this->redirectToRoute('listaprecio_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the price list.'));
        }
        return $this->render('listaprecio/edit.html.twig', [
                    'listaprecio' => $listaprecio,
                    'form' => $form->createView(),
        ]);
    }

}
