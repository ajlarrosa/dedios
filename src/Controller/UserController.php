<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Tienda\FormatoMail;
use App\Entity\Common\Email;
use App\Service\General\MailerService;
use App\Service\ClienteService;
use App\Service\Tienda\FormatoMailService;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\ClienteProveedorRepository;
use App\Service\Tienda\PedidoService;
use App\Service\Tienda\ConfiguracionService;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController {

    private $clienteProveedorRepository;
    private $pedidoService;
    private $formatoMailService;

    function __construct(ClienteProveedorRepository $clienteProveedorRepository, PedidoService $pedidoService, ConfiguracionService $configuracionService, FormatoMailService $formatoMailService) {
        $this->clienteProveedorRepository = $clienteProveedorRepository;
        $this->pedidoService = $pedidoService;
        $this->formatoMailService = $formatoMailService;
        $this->configuracion = $configuracionService->getLastConfiguration();
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(
            Request $request,
            UserRepository $userRepository,
            PaginatorInterface $paginator): Response {
        $filters = $request->query->all();

        $users = $paginator->paginate(
                $userRepository->findByFilters($filters),
                $request->query->getInt('page', 1),
                15
        );

        return $this->render('user/index.html.twig', [
                    'users' => $users,
        ]);
    }

    /**
     * @Route("/desaprobar", name="user_desaprobar", methods={"GET","POST"})
     */
    public function desaprobar(Request $request, TranslatorInterface $translator): Response {

        $users_id = $request->get('verificar');
        $cant = 0;
        foreach ($users_id as $key => $val) {
            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->find($key);
            $user->setIsVerified(true);
            $user->setEnabled(false);
            $user->setAprobado(false);
            $entityManager->persist($user);
            $entityManager->flush();
            $cant++;
        }

        $this->addFlash("msgOk", $translator->trans($cant . " users have been disapproved."));

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setPassword($passwordEncoder->encodePassword(
                            $user,
                            $user->getPassword()
            ));

            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash("msgOk", $translator->trans("A new User has been created."));
            return $this->redirectToRoute('user_index', [
                        'admin' => $request->get('admin')
                            ]
            );
        }
        if ($form->isSubmitted()) {
            $this->addFlash("msgError", $translator->trans("It was not possible to create the User."));
        }

        return $this->render('user/new.html.twig', [
                    'user' => $user,
                    'admin' => $request->get('admin'),
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response {
        return $this->render('user/show.html.twig', [
                    'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response {
        $form = $this->createForm(UserType::class, $user)->remove('password');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newpass = trim($request->get('password_first'));
            if ($newpass != '') {
                if (strcmp($newpass, trim($request->get('password_second'))) == 0) {
                    $password = $passwordEncoder->encodePassword(
                            $user,
                            $newpass
                    );
                    $user->setPassword($password);
                } else {
                    $this->addFlash("msgError", $translator->trans("Passwords do not match."));
                    return $this->render('user/edit.html.twig', [
                                'user' => $user,
                                'admin' => $request->get('admin'),
                                'form' => $form->createView(),
                    ]);
                }
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("msgOk", $translator->trans("User successfully edited."));
            return $this->redirectToRoute('user_index', [
                        'admin' => $request->get('admin'),
                        'username' => $user->getUsername()
                            ]
            );
        }
        if ($form->isSubmitted()) {
            $this->addFlash("msgError", $translator->trans("It was not possible to edit the User."));
        }
        return $this->render('user/edit.html.twig', [
                    'user' => $user,
                    'admin' => $request->get('admin'),
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/habilitarUserWeb/{id}", name="user_web_habilitar", methods={"GET","POST"})
     */
    public function habilitarUserWeb(Request $request, User $user, MailerService $mailerService, TranslatorInterface $translator, ClienteService $clienteService): Response {

        $entityManager = $this->getDoctrine()->getManager();
        $user->setIsVerified(true);
        $user->setAprobado(true);
        $user->setEnabled(true);

        $cliente = $user->getCliente();
        if (is_null($cliente)) {

            $entityManager->persist($this->pedidoService->create($user));
            $cliente = $clienteService->createClienteFromUser($user);
            if ($cliente !== false) {
                $user->setCliente($cliente);
                $this->addFlash('msgOk', $translator->trans('A customer associated with this user has been created.'));
            } else {
                $cliente = $this->clienteProveedorRepository->findBy(['cuit' => $user->getCuit()]);
                if ($cliente) {
                    $user->setCliente($cliente[0]);
                } else {
                    $cliente = $this->clienteProveedorRepository->findBy(['mail' => $user->getMail()]);
                    $user->setCliente($cliente[0]);
                }
                $this->addFlash('msgOk', $translator->trans('The user has been associated with the customer {{ cliente }}.', ['{{ cliente }}' => $cliente[0]->getRazonSocial()]));
            }
        }

        $entityManager->persist($user);
        $entityManager->flush();
        $this->addFlash("msgOk", $translator->trans("User has been approved to start operating"));

        $email = new Email();
        $email->setTitle($translator->trans("De Dios Joyas - User Verification"));
        $email->setSendTo($user->getMail());
        $email->setTemplate('email/confirmUser.html.twig');

        $formatosMails = $this->formatoMailService->getFormatoMail([
            'configuracion' => $this->configuracion->getId(),
            'type' => FormatoMail::COD_CONFIRM_MAYORISTA,
            'enabled' => true
        ]);

        if (sizeof($formatosMails) == 1) {
            $formatoEmail = $formatosMails[0];
            $email->setParameters(['nombre' => $user->getNombre(), 'title' => $formatoEmail->getTitle(), 'body' => $formatoEmail->getBody(), 'footer' => $formatoEmail->getFooter()]);
            $response = $mailerService->enviarMail($email);
            if ($response === true) {
                $this->addFlash("msgOk", $translator->trans("The message has been sent to the client"));
            } else {
                $this->addFlash("msgError", $response);
            }
        } else {
            $this->addFlash("msgError", 'The message could not be sent to the client. The email format is not set.');
        }

        return $this->redirect($this->generateUrl('user_index', [
                            'admin' => $request->get('admin')
                                ]
                        )
        );
    }

    /**
     * @Route("/admin/activate/{id}", name="user_activate", methods={"GET"})
     *
     */
    public function activate(Request $request, User $user, TranslatorInterface $translator) {
        if (!$user) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected user.'));
        } else {
            $entityManager = $this->getDoctrine()->getManager();

            $enabled = $request->get('enabled');


            $user->setEnabled($enabled);
            if (!$enabled) {
                $user->setMail($user->getMail() . $user->getId() . '-no_valido');
            }

            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('User status has been changed', ['%user%' => $user->getUsername()]));
        }
        return $this->redirect($this->generateUrl('user_index', [
                            'admin' => $request->get('admin')
        ]));
    }

}
