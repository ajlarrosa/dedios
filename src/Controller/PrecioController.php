<?php

namespace App\Controller;

use App\Entity\Precio;
use App\Entity\Producto;
use App\Form\PrecioType;
use App\Repository\PrecioRepository;
use App\Repository\ListaPrecioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/operator/precio")
 */
class PrecioController extends AbstractController {

    private $precioRepository;

    function __construct(PrecioRepository $precioRepository) {
        $this->precioRepository = $precioRepository;
    }

    /**
     * @Route("/", name="precio_index", methods={"GET"})
     */
    public function index(PrecioRepository $precioRepository): Response {
        return $this->render('precio/index.html.twig', [
                    'precios' => $precioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{producto}/new", name="precio_new", methods={"GET","POST"})
     * @ParamConverter("producto", class="App:Producto")   
     */
    public function new(Producto $producto, Request $request, TranslatorInterface $translator): Response {

        $precio = new Precio();
        $precio->setProducto($producto);
        $form = $this->createForm(PrecioType::class, $precio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($precio);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Price created successfully.'));

            return $this->redirectToRoute('producto_index', ['finder' => $producto->getCodigo()]);
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the price.'));
        }
        return $this->render('precio/new.html.twig', [
                    'producto' => $producto,
                    'precio' => $precio,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/predictiva", name="precio_predictiva", methods={"GET","POST"})
     */
    public function predictiva(Request $request, PrecioRepository $precioRepository,ListaPrecioRepository $listaPrecioRepository): Response {
        $search = $request->get('search');
        $idLista = $request->get('idLista');
        
        $html = '<ul>';
        $precios = $precioRepository->predictiva($search, $idLista);
        $listaPrecio = $listaPrecioRepository->find($idLista);
        foreach ($precios as $precio) {
            $html = $html . '<li class="suggest-element pointer " data-simbolo="' . $listaPrecio->getMonedaSimbolo() . '" data-codigo="' . $precio->getProducto()->getCodigo() . '" data-stock="' . $precio->getProducto()->getStock() . '" data-descripcion="' . $precio->getProducto()->getDescripcion() . '" data-precio="' . $precio->getValor() . '" data-id="' . $precio->getProducto()->getId() . '"><a>' . $precio->getProducto()->getDescripcion() . ' ( ' . $precio->getProducto()->getCodigo() . ' ) </a></li>';
        }

        $html = $html . '</ul>';
        return new Response($html);
    }

    /**
     * @Route("/{id}", name="precio_show", methods={"GET"})
     */
    public function show(Precio $precio): Response {
        return $this->render('precio/show.html.twig', [
                    'precio' => $precio,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="precio_edit", methods={"GET","POST"})
     */
    public function edit(Precio $precio, Request $request, TranslatorInterface $translator): Response {
        $form = $this->createForm(PrecioType::class, $precio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('msgOk', $translator->trans('Price edited successfully.'));

            return $this->redirectToRoute('producto_index', ['finder' => $precio->getProducto()->getCodigo()]);
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the price.'));
        }
        return $this->render('precio/edit.html.twig', [
                    'producto' => $precio->getProducto(),
                    'precio' => $precio,
                    'form' => $form->createView()
        ]);
    }

}
