<?php

namespace App\Controller;

use App\Entity\FacturaProveedor;
use App\Form\FacturaProveedorType;
use App\Service\ProveedorService;
use App\Service\MovimientoCCService;
use App\Repository\FacturaProveedorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/operator/compras")
 */
class FacturaProveedorController extends AbstractController {

    /**
     * @Route("/", name="facturaproveedor_index", methods={"GET"})
     */
    public function index(Request $request, FacturaProveedorRepository $facturaProveedorRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {

        $filtros = $request->query->all();

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $facturasProveedor = $facturaProveedorRepository->filter($filtros)->getQuery()->getResult();
            
            $spreadSheet = $this->excelInforme($facturasProveedor, $filtros);

            $nombreInforme = 'Compras';
            
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }
        
        
        $facturasproveedor = $paginator->paginate(
                $facturaProveedorRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('facturaproveedor/index.html.twig', [
                    'facturasproveedor' => $facturasproveedor,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($facturasProveedor, $parametros) {

     $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $fila = 3;
        
        foreach ($facturasProveedor as $facturaProveedor) {
            $sheet->setCellValue('A' . $fila, 'Factura Nro.'.$facturaProveedor->getId());
            $sheet->setCellValue('B' . $fila, 'Proveedor');
            $sheet->setCellValue('C' . $fila, 'Fecha');
            $sheet->setCellValue('D' . $fila, 'Fecha de Registración');
            $sheet->setCellValue('E' . $fila, 'Observación');
            $sheet->setCellValue('F' . $fila, 'Descuento');
            $sheet->setCellValue('G' . $fila, 'Bonificación');
            
            $sheet->getStyle('A' . $fila . ':G' . $fila)->applyFromArray($styleArray2);

            $fila++;
            
            $sheet->setCellValue('B' . $fila, $facturaProveedor->getMovimientocc()->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, $facturaProveedor->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('D' . $fila, $facturaProveedor->getCreated()->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $facturaProveedor->getObservacion());
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getDescuento());
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getBonificacion());
            
            
            $fila++;
            
            $sheet->setCellValue('B' . $fila, 'Total Oro:');
            $sheet->setCellValue('C' . $fila, $facturaProveedor->getOro().' gr');
            $sheet->setCellValue('E' . $fila, 'Subtotal:');
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getImporte());
            $fila++;
            $sheet->setCellValue('B' . $fila, 'Total Plata:');
            $sheet->setCellValue('C' . $fila, $facturaProveedor->getPlata().' gr');
            $sheet->setCellValue('E' . $fila, 'Descuento('.$facturaProveedor->getDescuento().'%):');
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getDescuentoValue());
            $sheet->getStyle('B'.($fila-1).':B'.$fila)->applyFromArray($styleArray2);
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Bonificación:');
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getBonificacion());
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Total:');
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getTotal());
            $fila++;
            $sheet->getStyle('E'.($fila-4).':E'.$fila)->applyFromArray($styleArray2);
            $fila++;
        }        
        
        $fila++;
        
        //Totales
        $sheet->setCellValue('A1', 'Total Compras Listadas: '.(sizeof($facturasProveedor)));
        
        
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);

        return $spreadsheet;
    }
    
    /**
     * @Route("/new", name="facturaproveedor_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, ProveedorService $proveedorService, MovimientoCCService $movimientoCCService): Response {
        $facturaProveedor = new FacturaProveedor();
        $form = $this->createForm(FacturaProveedorType::class, $facturaProveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movimientocc = $movimientoCCService->create($proveedorService->getProveedorById($request->get('proveedor')), null, null, null, null, null, $facturaProveedor);
            $entityManager = $this->getDoctrine()->getManager();
            $facturaProveedor->setMovimientocc($movimientocc);
            $entityManager->persist($movimientocc);
            $entityManager->persist($facturaProveedor);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Invoice Provider created successfully.'));
            return $this->redirectToRoute('facturaproveedor_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the invoice provider.'));
        }
        return $this->render('facturaproveedor/new.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facturaproveedor_show", methods={"GET"})
     */
    public function show(FacturaProveedor $facturaProveedor): Response {
        return $this->render('facturaproveedor/show.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="facturaproveedor_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FacturaProveedor $facturaProveedor): Response {
        $form = $this->createForm(FacturaProveedorType::class, $facturaProveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facturaproveedor_index');
        }

        return $this->render('facturaproveedor/edit.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facturaproveedor_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FacturaProveedor $facturaProveedor): Response {
        if ($this->isCsrfTokenValid('delete' . $facturaProveedor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($facturaProveedor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('facturaproveedor_index');
    }

}
