<?php

namespace App\Controller;

use App\Repository\PrecioMedidaProductoRepository;
use App\Repository\PrecioRepository;
use App\Entity\ListaPrecio;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/") 
 */
class PrecioMedidaProductoController extends AbstractController {

    private $precioMedidaProductoRepository;
    private $precioRepository;
    private $security;

    function __construct(PrecioRepository $precioRepository, PrecioMedidaProductoRepository $precioMedidaProductoRepository, Security $security) {
        $this->precioMedidaProductoRepository = $precioMedidaProductoRepository;
        $this->precioRepository = $precioRepository;
        $this->security = $security;
    }

    /**
     * @Route("/precio_medida_producto/precio", name="preciomedidaproducto_precio", methods={"GET"})
     */
    public function precio(Request $request): Response {
        $filtros['medida'] = $request->get('medida');

        $filtros['producto'] = $request->get('producto');

        $filtros['listaPrecio'] = $this->security->isGranted("ROLE_USER_MAYORISTA") ? ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB : ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;

        $precioMedidaProducto = $this->precioMedidaProductoRepository->precioFinal($filtros);

        $response = '';
        if ($precioMedidaProducto) {
            $response = $precioMedidaProducto[0]->getPrecio()->getValor();
        } else {
            $filtros['activo'] = true;

            $precio = $this->precioRepository->findPreciosBy($filtros);

            if ($precio) {
                $response = $precio[0]->getValor();
            }
        }

        if ($this->security->isGranted("ROLE_USER_MAYORISTA")) {
            return new Response(str_replace(".", ",", $response));
        }

        return new Response(str_replace(".", ",", round($response)));
    }

}
