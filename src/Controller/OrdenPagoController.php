<?php

namespace App\Controller;

use App\Entity\OrdenPago;
use App\Entity\Cheque;
use App\Form\OrdenPagoType;
use App\Repository\OrdenPagoRepository;
use App\Service\ChequeService;
use App\Service\MovimientoCCService;
use App\Service\ClienteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/ordenpago")
 */
class OrdenPagoController extends AbstractController {

    /**
     * @Route("/", name="ordenpago_index", methods={"GET"})
     */
    public function index(Request $request, OrdenPagoRepository $ordenPagoRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {
        $filtros = $request->query->all();
        
        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $ordenesPago = $ordenPagoRepository->filter($filtros)->getQuery()->getResult();
            
            $spreadSheet = $this->excelInforme($ordenesPago, $filtros);

            $nombreInforme = 'Pagos';
            
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }
        
        $ordenes_pago = $paginator->paginate(
                $ordenPagoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('orden_pago/index.html.twig', [
                    'ordenes_pago' => $ordenes_pago,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($ordenesPago, $parametros) {

     $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $fila = 3;
        
        foreach ($ordenesPago as $ordenPago) {
            $sheet->setCellValue('A' . $fila, 'Orden de Pago Nro.'.$ordenPago->getId());
            $sheet->setCellValue('B' . $fila, 'Cliente');
            $sheet->setCellValue('C' . $fila, 'Fecha');
            $sheet->setCellValue('D' . $fila, 'Fecha de Registración');
            $sheet->setCellValue('E' . $fila, 'Observación');
            
            $sheet->getStyle('A' . $fila . ':G' . $fila)->applyFromArray($styleArray2);

            $fila++;
            
            $sheet->setCellValue('B' . $fila, $ordenPago->getMovimientocc()->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, $ordenPago->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('D' . $fila, $ordenPago->getCreated()->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $ordenPago->getObservacion());
            
            
            $fila++;
            
            $sheet->setCellValue('B' . $fila, 'Total Oro:');
            $sheet->setCellValue('C' . $fila, $ordenPago->getOro().' gr');
            $sheet->setCellValue('E' . $fila, 'Subtotal:');
            $sheet->setCellValue('F' . $fila, $ordenPago->getMonedaSimbolo().$ordenPago->getImporte());
            $fila++;
            $sheet->setCellValue('B' . $fila, 'Total Plata:');
            $sheet->setCellValue('C' . $fila, $ordenPago->getPlata().' gr');
            $sheet->setCellValue('E' . $fila, 'Total Cheques:');
            $sheet->setCellValue('F' . $fila, $ordenPago->getMonedaSimbolo().$ordenPago->getTotalCheques());
            $sheet->getStyle('B'.($fila-1).':B'.$fila)->applyFromArray($styleArray2);
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Total:');
            $sheet->setCellValue('F' . $fila, $ordenPago->getMonedaSimbolo().$ordenPago->getTotal());
            $fila++;
            $sheet->getStyle('E'.($fila-3).':E'.$fila)->applyFromArray($styleArray2);
            $fila++;
        }        
        
        $fila++;
        
        //Totales
        $sheet->setCellValue('A1', 'Total Órdenes de Pago Listados: '.(sizeof($ordenesPago)));
        
        
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }
    
    
    /**
     * @Route("/new", name="ordenpago_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, ChequeService $chequeService, ClienteService $clienteService, MovimientoCCService $movimientoCCService ): Response {
        $ordenPago = new OrdenPago();
        $form = $this->createForm(OrdenPagoType::class, $ordenPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if(isset($request->get('orden_pago')['chequesOrdenPago'])){
                foreach($request->get('orden_pago')['chequesOrdenPago'] as $idCheque) {
                    $cheque = $entityManager->getRepository(Cheque::class)->find($idCheque);
                    $ordenPago->addChequesOrdenPago($cheque);
                }
            }
            $movimientocc = $movimientoCCService->create($clienteService->getClienteById($request->get('cliente')), null, null, null, null,$ordenPago );

            $ordenPago->setMovimientocc($movimientocc);
            $entityManager->persist($movimientocc);
            $entityManager->persist($ordenPago);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Payment order created successfully.'));

            return $this->redirectToRoute('ordenpago_index');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the collection.'));
        }
        return $this->render('orden_pago/new.html.twig', [
                    'ordenpago' => $ordenPago,
                    'form' => $form->createView(),
        ]);
    }    
    
    /**
     * @Route("/{id}", name="ordenpago_show", methods={"GET"})
     */
    public function show(OrdenPago $ordenPago): Response {
        return $this->render('orden_pago/show.html.twig', [
                    'orden_pago' => $ordenPago
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ordenpago_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OrdenPago $ordenPago): Response {
        $form = $this->createForm(OrdenPagoType::class, $ordenPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ordenpago_index');
        }

        return $this->render('orden_pago/edit.html.twig', [
                    'ordenpago' => $ordenPago,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ordenpago_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OrdenPago $ordenPago): Response {
        if ($this->isCsrfTokenValid('delete' . $ordenPago->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ordenPago);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ordenpago_index');
    }

}
