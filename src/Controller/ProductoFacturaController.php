<?php

namespace App\Controller;

use App\Entity\ProductoFactura;
use App\Form\ProductoFacturaType;
use App\Repository\ProductoFacturaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/producto/factura")
 */
class ProductoFacturaController extends AbstractController
{
    /**
     * @Route("/", name="producto_factura_index", methods={"GET"})
     */
    public function index(ProductoFacturaRepository $productoFacturaRepository): Response
    {
        return $this->render('producto_factura/index.html.twig', [
            'producto_facturas' => $productoFacturaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="producto_factura_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $productoFactura = new ProductoFactura();
        $form = $this->createForm(ProductoFacturaType::class, $productoFactura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($productoFactura);
            $entityManager->flush();

            return $this->redirectToRoute('producto_factura_index');
        }

        return $this->render('producto_factura/new.html.twig', [
            'producto_factura' => $productoFactura,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producto_factura_show", methods={"GET"})
     */
    public function show(ProductoFactura $productoFactura): Response
    {
        return $this->render('producto_factura/show.html.twig', [
            'producto_factura' => $productoFactura,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="producto_factura_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ProductoFactura $productoFactura): Response
    {
        $form = $this->createForm(ProductoFacturaType::class, $productoFactura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('producto_factura_index');
        }

        return $this->render('producto_factura/edit.html.twig', [
            'producto_factura' => $productoFactura,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producto_factura_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ProductoFactura $productoFactura): Response
    {
        if ($this->isCsrfTokenValid('delete'.$productoFactura->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($productoFactura);
            $entityManager->flush();
        }

        return $this->redirectToRoute('producto_factura_index');
    }
}
