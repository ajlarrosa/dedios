<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Tienda\DireccionEnvio;
use App\Form\RegistrationFormType;
use App\Form\RegistrationWebFormType;
use App\Form\RegistrationMayoristaFormType;
use App\Security\EmailVerifier;
use App\Service\General\MailerService;
use App\Entity\Common\Email;
use App\Entity\Tienda\Mailing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\UserRepository;
use App\Service\Tienda\PedidoService;
use App\Service\ClienteService;
use App\Service\General\GeoService;
use App\Repository\Tienda\ClienteWebRepository;
use App\Repository\Tienda\DireccionEnvioRepository;
use App\Repository\Tienda\PedidoRepository;
use App\Service\General\SessionService;

class RegistrationController extends AbstractController {

    private $emailVerifier;
    private $mailerService;
    private $userRepository;
    private $pedidoService;
    private $clienteService;
    private $geoService;
    private $clienteWebRepository;
    private $direccionEnvioRepository;
    private $pedidoRepository;
    private $sessionService;

    public function __construct(
            SessionService $sessionService,
            MailerService $mailerService,
            EmailVerifier $emailVerifier,
            UserRepository $userRespository,
            PedidoService $pedidoService,
            ClienteService $clienteService,
            GeoService $geoService,
            ClienteWebRepository $clienteWebRepository,
            DireccionEnvioRepository $direccionEnvioRepository,
            PedidoRepository $pedidoRepository
    ) {
        $this->emailVerifier = $emailVerifier;
        $this->userRepository = $userRespository;
        $this->pedidoService = $pedidoService;
        $this->clienteService = $clienteService;
        $this->geoService = $geoService;
        $this->clienteWebRepository = $clienteWebRepository;
        $this->direccionEnvioRepository = $direccionEnvioRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->mailerService = $mailerService;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response {

        $clienteWebId = $request->get('clienteWeb');
        $clienteWeb = null;
        $web = '';
        $createMailing = true;

        $user = new User();
        $user->addRoleMinorista();

        if ($clienteWebId != '') {
            $clienteWeb = $this->clienteWebRepository->find($clienteWebId);
            $pedidos = $this->pedidoRepository->findBy(['clienteWeb' => $clienteWeb]);
            $direccionesEnvioWeb = $this->direccionEnvioRepository->findBy(['clienteWeb' => $clienteWeb]);
            $form = $this->createForm(RegistrationWebFormType::class, $user);
            $web = 'Web';
        } else {
            $form = $this->createForm(RegistrationFormType::class, $user);
        }

        $form->handleRequest($request);

        if (!$form->isSubmitted() && $clienteWebId != '') {
            $hash = false;

            $btu = $request->get('btu');
            foreach ($pedidos as $pedido) {
                if ($pedido->getHash() == $btu) {
                    $hash = true;
                }
            }

            if (!$hash) {
                $this->addFlash("msgError", $translator->trans("Unauthorized"));
                return $this->redirectToRoute('home');
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            
            $data = $request->request->all();
                        
            $entityManager = $this->getDoctrine()->getManager();

            if ($clienteWeb) {
                foreach ($pedidos as $pedido) {
                    $pedido->setClienteWeb(null);
                    $pedido->setUser($user);
                }
                foreach ($direccionesEnvioWeb as $direccionEnvioWeb) {
                    $direccionEnvioWeb->setClienteWeb(null);
                    $direccionEnvioWeb->setUser($user);
                }
                $user->setNombre($clienteWeb->getNombre());
                $user->setApellido($clienteWeb->getApellido());
                $user->setTelefono($clienteWeb->getTelefono());
                $user->setDni($clienteWeb->getDni());
                
                if(array_key_exists ( 'mailing' , $data['registration_form'] )){
                    //Busco un registro de mailing y lo asocio al nuevo user
                    $mailing = $entityManager->getRepository(Mailing::class)->findBy(['clienteWeb'=>$clienteWeb]);

                    if(sizeof($mailing)>0){
                        if(!$mailing[0]->getEnabled()){
                            $mailing[0]->setEnabled(true);
                        }
                        $mailing[0]->setClienteWeb(null);
                        $mailing[0]->setUser($user);
                        $entityManager->persist($mailing[0]);
                        $createMailing = false;
                    }
                }
            } else {
                $provincia = $this->geoService->getprovincia($data['registration_form']['provincia']);
                $localidad = $this->geoService->getLocalidad($data['localidad']);
                $departamento = $this->geoService->getDepartamento($data['departamento']);

                $direccionEnvio = new DireccionEnvio();
                $direccionEnvio->setCalle($data['calle']);
                $direccionEnvio->setCalleNumero($data['registration_form']['calleNumero']);
                $direccionEnvio->setUser($user);
                $direccionEnvio->setDepartamento(is_object($departamento)?$departamento->nombre:'');
                $direccionEnvio->setLocalidad(is_null($localidad->nombre)? ' - ': $localidad->nombre);
                $direccionEnvio->setProvincia($provincia->nombre);
                $direccionEnvio->setUnidadFuncional($data['registration_form']['unidadFuncional']);
                $direccionEnvio->setPiso($data['registration_form']['piso']);
                $direccionEnvio->setZipCode($data['registration_form']['zipCode']);
                $direccionEnvio->setProvinciaId($data['registration_form']['provincia']);
                $direccionEnvio->setLocalidadId($data['localidad']);
                $direccionEnvio->setDepartamentoId($data['departamento']);
                $direccionEnvio->setCalleId($data['calle_id']);

                $entityManager->persist($direccionEnvio);
                $user->addDireccionesEnvio($direccionEnvio);
            }

            // encode the plain password
            $user->setPassword(
                    $passwordEncoder->encodePassword(
                            $user,
                            $form->get('plainPassword')->getData()
                    )
            );          
            
            if(array_key_exists ( 'mailing' , $data['registration_form'] ) && $createMailing){
                $mailingPrevio = $entityManager->getRepository(Mailing::class)->buscarMail($user->getMail());
                
                if(isset($mailingPrevio[0])){
                    if($mailingPrevio[0]->getUser()==null){
                        if(!$mailingPrevio[0]->getEnabled()){
                            $mailingPrevio[0]->setEnabled(true);
                        }
                        $mailingPrevio[0]->setClienteWeb(null);
                        $mailingPrevio[0]->setUser($user);
                        $entityManager->persist($mailingPrevio[0]);
                    }
                }else{    
                    $mailing = new Mailing();
                    $mailing->setUser($user);
                    $entityManager->persist($mailing);
                }
            }
            
            $entityManager->persist($user);
            $entityManager->flush();

            $response = $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user);

            if ($response === true) {
                $this->addFlash("msgOk", $translator->trans("An e-mail has been sent to continue with the registration. If you don't receive an email please check your spam folder.", ['%email%' => $user->getMail()]));
            } else {
                $this->addFlash("msgError", $translator->trans("It has not been possible to send the email to continue with the registration. Please contact De Dios for assistance"));
            }

            return $this->redirectToRoute('home', ['fb' => 5]);
        }

        return $this->render('registration/register' . $web . '.html.twig', [
                    'clienteWeb' => $clienteWeb,
                    'registrationForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request, TranslatorInterface $translator): Response {

        $user = $this->userRepository->findOneByVerifyEmail($request->query->all());
        if ($user) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setIsVerified(true);

            if ($user->hasRole('ROLE_USER_MINORISTA')) {
                $user->setAprobado(true);
                $cliente = $this->clienteService->createClienteFromUser($user);

                if ($cliente !== false) {
                    $user->setCliente($cliente);
                }
                $entityManager->persist($this->pedidoService->create($user));
            }
            $entityManager->persist($user);
            $entityManager->flush();
        } else {
            $this->addFlash('msgError', $translator->trans("Your email address could not been verified."));
            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        if ($user->hasRole('ROLE_USER_MAYORISTA')) {
            //Para DeDios
            $email = new Email();
            $email->setTitle("De Dios Joyas - Nuevo usuario mayorista para aprobar");

            $email->setSendTo($this->getParameter('email_receiver'));

            $email->setTemplate('email/newWholeSalerUser.html.twig');
            $email->setParameters(['user' => $user]);

            $response = $this->mailerService->enviarMail($email);

            $this->addFlash('msgOk', $translator->trans("Verified email address, you will soon receive an email approving you as a user."));
            return $this->redirectToRoute('home');
        } else {
            $this->addFlash('msgOk', $translator->trans("Your email address has been verified. Login to start buying."));
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/mayoristas", name="app_mayoristas")
     */
    public function mayoristas(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator) {

        $user = new User();
        $user->setEnabled(false);
        $form = $this->createForm(RegistrationMayoristaFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();

            // encode the plain password
            $user->setPassword(
                    $passwordEncoder->encodePassword(
                            $user,
                            $form->get('plainPassword')->getData()
                    )
            );
            $user->setRoles(['ROLE_USER_MAYORISTA', 'ROLE_USER']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);

            $provincia = $this->geoService->getprovincia($data['registration_mayorista_form']['provincia']);
            $localidad = $this->geoService->getLocalidad($data['localidad']);
            $departamento = $this->geoService->getDepartamento($data['departamento']);

            $direccionEnvio = new DireccionEnvio();
            $direccionEnvio->setCalle($data['calle']);
            $direccionEnvio->setCalleNumero($data['registration_mayorista_form']['calleNumero']);
            $direccionEnvio->setUser($user);
            $direccionEnvio->setDepartamento(is_object($departamento)?$departamento->nombre:'');
            $direccionEnvio->setLocalidad($localidad->nombre);
            $direccionEnvio->setProvincia($provincia->nombre);
            $direccionEnvio->setUnidadFuncional($data['registration_mayorista_form']['unidadFuncional']);
            $direccionEnvio->setPiso($data['registration_mayorista_form']['piso']);
            $direccionEnvio->setZipCode($data['registration_mayorista_form']['zipCode']);
            $direccionEnvio->setProvinciaId($data['registration_mayorista_form']['provincia']);
            $direccionEnvio->setLocalidadId($data['localidad']);
            $direccionEnvio->setDepartamentoId($data['departamento']);
            $direccionEnvio->setCalleId($data['calle_id']);

            $entityManager->persist($direccionEnvio);

            $entityManager->flush();

            $response = $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user);

            if ($response === true) {
                $this->addFlash("msgOk", $translator->trans("An e-mail has been sent to continue with the registration. If you don't receive an email please check your spam folder.", ['%email%' => $user->getMail()]));
            } else {
                $this->addFlash("msgError", $translator->trans("It has not been possible to send the email to continue with the registration. Please contact De Dios for assistance"));
            }

            return $this->redirectToRoute('home', ['fb' => 5]);
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash("msgError", $translator->trans("Registration has not been possible, please verify your details and try again."));
        }

        return $this->render('tienda/default/mayoristas.html.twig', [
                    'registrationForm' => $form->createView(),
        ]);
    }

}
