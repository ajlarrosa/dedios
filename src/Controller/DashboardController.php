<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\PedidoRepository;

/**
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController {

    /**
     * @Route("/operator", name="dashboard_index")
     */
    public function index(PedidoRepository $pedidoRepository) {

        $result = $pedidoRepository->countPedidos();

        return $this->render('dashboard/index.html.twig', [
                    'result' => $result
        ]);
    }

}
