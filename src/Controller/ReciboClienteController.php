<?php

namespace App\Controller;

use App\Entity\ReciboCliente;
use App\Entity\Tienda\FormaPago;
use App\Service\ChequeService;
use App\Service\MovimientoCCService;
use App\Service\ClienteService;
use App\Form\ReciboClienteType;
use App\Repository\ReciboClienteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\Tienda\FormaPagoRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/reciboCliente")
 */
class ReciboClienteController extends AbstractController {

    private $formaPagoRepository;
    private $phpOfficeService;
    
    function __construct(FormaPagoRepository $formaPagoRepository, PhpOfficeService $phpOffice) {
        $this->formaPagoRepository=$formaPagoRepository;
        $this->phpOfficeService=$phpOffice;
    }

    
    /**
     * @Route("/", name="recibocliente_index", methods={"GET"})
     */
    public function index(Request $request, ReciboClienteRepository $reciboClienteRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        
        $recibosCliente= $reciboClienteRepository->filter($filtros);
        
        /*
         * Verifico si se presiono el botón exportar excel
         */
                
        if ($request->get('action') == 'excel') {            
            
            $spreadSheet = $this->excelInforme($recibosCliente->getQuery()->getResult());
            return $this->phpOfficeService->exportarExcel($spreadSheet, 'Cobranzas');
        }
        
        $reciboscliente = $paginator->paginate(
                $reciboClienteRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('recibo_cliente/index.html.twig', [
                    'reciboscliente' => $reciboscliente,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($recibosCliente) {

     $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $fila = 3;
        
        foreach ($recibosCliente as $reciboCliente) {
            $sheet->setCellValue('A' . $fila, 'Recibo Cliente Nro.'.$reciboCliente->getId());
            $sheet->setCellValue('B' . $fila, 'Cliente');
            $sheet->setCellValue('C' . $fila, 'Fecha');
            $sheet->setCellValue('D' . $fila, 'Fecha de Registración');
            $sheet->setCellValue('E' . $fila, 'Observación');
            $sheet->setCellValue('F' . $fila, 'Forma de Pago');
            
            $sheet->getStyle('A' . $fila . ':G' . $fila)->applyFromArray($styleArray2);

            $fila++;
            
            $sheet->setCellValue('B' . $fila, $reciboCliente->getMovimientocc()->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, $reciboCliente->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('D' . $fila, $reciboCliente->getCreated()->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $reciboCliente->getObservacion());
            $sheet->setCellValue('F' . $fila, $reciboCliente->getFormaPago());
            
            
            $fila++;
            
            $sheet->setCellValue('B' . $fila, 'Total Oro:');
            $sheet->setCellValue('C' . $fila, $reciboCliente->getOro().' gr');
            $sheet->setCellValue('E' . $fila, 'Subtotal:');
            $sheet->setCellValue('F' . $fila, $reciboCliente->getMonedaSimbolo().$reciboCliente->getImporte());
            $fila++;
            $sheet->setCellValue('B' . $fila, 'Total Plata:');
            $sheet->setCellValue('C' . $fila, $reciboCliente->getPlata().' gr');
            $sheet->setCellValue('E' . $fila, 'Total Cheques:');
            $sheet->setCellValue('F' . $fila, $reciboCliente->getMonedaSimbolo().$reciboCliente->getTotalCheques());
            $sheet->getStyle('B'.($fila-1).':B'.$fila)->applyFromArray($styleArray2);
            $fila++;
            $sheet->setCellValue('E' . $fila, 'Total:');
            $sheet->setCellValue('F' . $fila, $reciboCliente->getMonedaSimbolo().$reciboCliente->getTotal());
            $fila++;
            $sheet->getStyle('E'.($fila-3).':E'.$fila)->applyFromArray($styleArray2);
            $fila++;
        }        
        
        $fila++;
        
        //Totales
        $sheet->setCellValue('A1', 'Total Cobranzas Listadas: '.(sizeof($recibosCliente)));
        
        
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        return $spreadsheet;
    }
    
    /**
     * @Route("/new", name="recibocliente_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, ChequeService $chequeService, ClienteService $clienteService, MovimientoCCService $movimientoCCService ): Response {
        $reciboCliente = new ReciboCliente();
        $form = $this->createForm(ReciboClienteType::class, $reciboCliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fechaEmision = $request->get('fechaEmision');
            $fechaCobro = $request->get('fechaCobro');
            $nroCheque = $request->get('nroCheque');
            $importe = $request->get('importe');
            $firmantes = $request->get('firmantes');
            $cuit = $request->get('cuit');
            $banco = $request->get('banco');
            $tipoCheque = $request->get('tipoCheque');

            $entityManager = $this->getDoctrine()->getManager();

            foreach ($nroCheque as $key => $nCheque) {
                if ($nroCheque[$key] != '' && $fechaEmision[$key] != '' && $importe[$key] > 0) {
                    $data = [
                        'fechaEmision' => $fechaEmision[$key],
                        'fechaCobro' => $fechaCobro[$key],
                        'nroCheque' => $nroCheque[$key],
                        'importe' => $importe[$key],
                        'firmantes' => $firmantes[$key],
                        'cuit' => $cuit[$key],
                        'banco' => $banco[$key],
                        'tipoCheque' => $tipoCheque[$key]
                    ];
                    $cheque = $chequeService->create((object) $data);
                    $cheque->setReciboCliente($reciboCliente);
                    $entityManager->persist($cheque);
                }
            }

            $movimientocc = $movimientoCCService->create($clienteService->getClienteById($request->get('cliente')), null, null, null,$reciboCliente );

            $reciboCliente->setMovimientocc($movimientocc);
            if ($reciboCliente->getImporte()>0){
                $formaPago = $this->formaPagoRepository->find(FormaPago::EFECTIVO_ID);
                $reciboCliente->setFormaPago($formaPago);
            }
            
            $entityManager->persist($movimientocc);
            $entityManager->persist($reciboCliente);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Collection created successfully.'));

            return $this->redirectToRoute('recibocliente_index');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the collection.'));
        }
        return $this->render('recibo_cliente/new.html.twig', [
                    'recibocliente' => $reciboCliente,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recibocliente_show", methods={"GET"})
     */
    public function show(ReciboCliente $reciboCliente): Response {
        return $this->render('recibo_cliente/show.html.twig', [
                    'reciboCliente' => $reciboCliente,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="recibocliente_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ReciboCliente $reciboCliente): Response {
        $form = $this->createForm(ReciboClienteType::class, $reciboCliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recibocliente_index');
        }

        return $this->render('recibo_cliente/edit.html.twig', [
                    'recibocliente' => $reciboCliente,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recibocliente_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ReciboCliente $reciboCliente): Response {
        if ($this->isCsrfTokenValid('delete' . $reciboCliente->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($reciboCliente);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recibocliente_index');
    }

}
