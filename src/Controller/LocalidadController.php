<?php

namespace App\Controller;

use App\Entity\Localidad;
use App\Form\LocalidadType;
use App\Service\LocalidadService;
use App\Repository\LocalidadRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/localidad")
 */
class LocalidadController extends AbstractController {

    /**
     * @Route("/operator/", name="localidad_index", methods={"GET"})
     */
    public function index(Request $request, LocalidadRepository $localidadRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $localidads = $paginator->paginate(
                $localidadRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('localidad/index.html.twig', [
                    'localidads' => $localidads,
        ]);
    }

    /**
     * @Route("/admin/new", name="localidad_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $localidad = new Localidad();
        $form = $this->createForm(LocalidadType::class, $localidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($localidad);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Locality created successfully.'));
            return $this->redirectToRoute('localidad_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the locality.'));
        }
        return $this->render('localidad/new.html.twig', [
                    'localidad' => $localidad,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="localidad_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Localidad $localidad, TranslatorInterface $translator) {
        if (!$localidad) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected locality.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $localidad->setActivo($activo);
            $entityManager->persist($localidad);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Locality status has been changed', ['%locality%' => $localidad->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('localidad_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="localidad_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Localidad $localidad, TranslatorInterface $translator): Response {
        $form = $this->createForm(LocalidadType::class, $localidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Locality edited successfully.'));
            return $this->redirectToRoute('localidad_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the locality.'));
        }
        return $this->render('localidad/edit.html.twig', [
                    'localidad' => $localidad,
                    'form' => $form->createView(),
        ]);
    }
     /**
     * @Route("/operator/byProvincia", name="localidad_getby_provincia", methods={"GET","POST"})
     */
    public function byProvincia(Request $request,LocalidadService $localidadService): Response {
        $id=$request->get('id');
        $activo=$request->get('activo');
        return new Response($localidadService->getSelectByProvincia($activo, $id));
    }

}
