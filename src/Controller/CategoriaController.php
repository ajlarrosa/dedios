<?php

namespace App\Controller;

use App\Entity\Categoria;
use App\Form\CategoriaType;
use App\Repository\CategoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;

/** 
 * @Route("/categoria")
 */
class CategoriaController extends AbstractController {
    
    private $generalService;
    
    function __construct(GeneralService $generalService) {
        $this->generalService= $generalService;        
    }

    /**
     * @Route("/operator/", name="categoria_index", methods={"GET"})
     */
    public function index(Request $request, CategoriaRepository $categoriaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $categorias = $paginator->paginate(
                $categoriaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('categoria/index.html.twig', [
                    'categorias' => $categorias,
        ]);
    }

    /**
     * @Route("/admin/new", name="categoria_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $categoria = new Categoria();
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $error = false;
            
            $entityManager = $this->getDoctrine()->getManager();
            if ($_FILES['categoria']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['categoria']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $entityManager->persist($categoria);
                    $entityManager->flush();
                    
                    $uploaddir = $this->getParameter('root_dir_categoria') . $categoria->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }
                    
                    $name = date('YmdHis').'_'.$_FILES['categoria']['name']['imagen'];
                
                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['categoria']["tmp_name"]['imagen'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagen($name);
                    }else{
                        $error = true;
                    }
                }
            }
            
            if (!$error) {
                $entityManager->persist($categoria);
                $entityManager->flush();
                $this->addFlash('msgOk', $translator->trans('Category created successfully.'));
                
                return $this->redirectToRoute('categoria_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the category.'));
        }
        return $this->render('categoria/new.html.twig', [
                    'categoria' => $categoria,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/enabled/{id}", name="categoria_enabled", methods={"GET"})
     *
     */
    public function enabled(Request $request, Categoria $categoria, TranslatorInterface $translator) {
        if (!$categoria) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected category.'));
        } else {
            $enabled = $request->get('enabled');
            $entityManager = $this->getDoctrine()->getManager();
            $categoria->setEnabled($enabled);
            $entityManager->persist($categoria);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Category status has been changed', ['%category%' => $categoria->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('categoria_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="categoria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Categoria $categoria, TranslatorInterface $translator): Response {
        $form = $this->createForm(CategoriaType::class, $categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             if ($_FILES['categoria']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['categoria']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    return $this->render('categoria/edit.html.twig', [
                                'categoria' => $categoria,
                                'form' => $form->createView(),
                    ]);
                }

                $uploaddir = $this->getParameter('root_dir_categoria') . $categoria->getId() . '/';
            
                if (!file_exists($uploaddir)) {
                    mkdir($uploaddir, 0777, true);
                }

                $name = date('YmdHis').'_'.$_FILES['categoria']['name']['imagen'];

                $uploadfile = $uploaddir . $name;

                $tmp_name = $_FILES['categoria']["tmp_name"]['imagen'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    if($categoria->getImagen()!='' && is_file($uploaddir.'../../../'.$categoria->getImagen())){
                        unlink($uploaddir.'../../../'.$categoria->getImagen());
                    }
                    $categoria->setImagen($name);
                } else{
                    
                   $this->addFlash('msgError', $translator->trans('It was not possible to upload the image.'));                   
                   return $this->redirectToRoute('categoria_index');
                }
            }
            
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Category edited successfully.'));
            
            return $this->redirectToRoute('categoria_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the category.'));
        }
        return $this->render('categoria/edit.html.twig', [
                    'categoria' => $categoria,
                    'form' => $form->createView(),
        ]);
    }

}
