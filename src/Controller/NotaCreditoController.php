<?php

namespace App\Controller;

use App\Entity\NotaCredito;
use App\Service\MovimientoCCService;
use App\Entity\ClienteProveedor;
use App\Form\NotaCreditoType;
use App\Repository\NotaCreditoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/notaCredito")
 */
class NotaCreditoController extends AbstractController
{
    /**
     * @Route("/", name="nota_credito_index", methods={"GET"})
     */
    public function index(NotaCreditoRepository $notaCreditoRepository): Response
    {
        return $this->render('nota_credito/index.html.twig', [
            'nota_creditos' => $notaCreditoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/new", name="nota_credito_new", methods={"GET","POST"})
     */
    public function new(Request $request, ClienteProveedor $clienteProveedor, MovimientoCCService $movimientoCCService): Response
    {
        $notaCredito = new NotaCredito();
        $form = $this->createForm(NotaCreditoType::class, $notaCredito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $movimientocc = $movimientoCCService->create($clienteProveedor, null, $notaCredito, null, null, null, null);
            $notaCredito->setMovimientocc($movimientocc);
            $entityManager->persist($notaCredito);
            $entityManager->flush();
            return $this->redirectToRoute('movimientocc_index',['id'=>$movimientocc->getClienteProveedor()->getId()]);
        }

        return $this->render('nota_credito/new.html.twig', [
            'nota_credito' => $notaCredito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nota_credito_show", methods={"GET"})
     */
    public function show(NotaCredito $notaCredito): Response
    {
        return $this->render('nota_credito/show.html.twig', [
            'nota_credito' => $notaCredito,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nota_credito_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NotaCredito $notaCredito): Response
    {
        $form = $this->createForm(NotaCreditoType::class, $notaCredito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nota_credito_index');
        }

        return $this->render('nota_credito/edit.html.twig', [
            'nota_credito' => $notaCredito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nota_credito_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NotaCredito $notaCredito): Response
    {
        if ($this->isCsrfTokenValid('delete'.$notaCredito->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($notaCredito);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nota_credito_index');
    }
}
