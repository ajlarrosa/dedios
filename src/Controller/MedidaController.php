<?php

namespace App\Controller;

use App\Entity\Medida;
use App\Form\MedidaType;
use App\Repository\MedidaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;


class MedidaController extends AbstractController
{
    
    /**
     * @Route("/operator/medida", name="medida_index", methods={"GET"})
     */
    public function index(Request $request, MedidaRepository $medidaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $medidas = $paginator->paginate(
                $medidaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('medida/index.html.twig', [
                    'medidas' => $medidas,
        ]);
    }
    
    /**
     * @Route("/admin/medida/new", name="medida_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $medida = new Medida();
        $form = $this->createForm(MedidaType::class, $medida);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($medida);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Measure created successfully.'));
            return $this->redirectToRoute('medida_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the measure.'));
        }
        return $this->render('medida/new.html.twig', [
                    'medida' => $medida,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/medida/{id}/edit", name="medida_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Medida $medida, TranslatorInterface $translator): Response {
        $form = $this->createForm(MedidaType::class, $medida);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Measure edited successfully.'));
            return $this->redirectToRoute('medida_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the measure.'));
        }
        return $this->render('medida/edit.html.twig', [
                    'medida' => $medida,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/medida/activate/{id}", name="medida_activate", methods={"GET"})
     *
     */
    public function activate(Request $request, Medida $medida, TranslatorInterface $translator) {
        if (!$medida) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected measure.'));
        } else {
            $enabled = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $medida->setEnabled($enabled);
            $entityManager->persist($medida);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Measure {{ measure }} status has been changed', ['{{ measure }}' => $medida->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('medida_index'));
    }
}
