<?php

namespace App\Controller;

use App\Entity\CondicionFiscal;
use App\Form\CondicionFiscalType;
use App\Repository\CondicionFiscalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/condicionfiscal")
 */
class CondicionFiscalController extends AbstractController
{
    /**
     * @Route("/", name="condicionfiscal_index", methods={"GET"})
     */
    public function index(Request $request, CondicionFiscalRepository $condicionFiscalRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $condicionfiscales = $paginator->paginate(
                $condicionFiscalRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('condicionfiscal/index.html.twig', [
                    'condicionfiscales' => $condicionfiscales,
        ]);
    }

    /**
     * @Route("/new", name="condicionfiscal_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $condicionfiscal = new CondicionFiscal();
        $form = $this->createForm(CondicionFiscalType::class, $condicionfiscal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($condicionfiscal);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Fiscal Condition created successfully.'));
            return $this->redirectToRoute('condicionfiscal_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the fiscal condition.'));
        }
        return $this->render('condicionfiscal/new.html.twig', [
                    'condicionfiscal' => $condicionfiscal,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/activate/{id}", name="condicionfiscal_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, CondicionFiscal $condicionfiscal, TranslatorInterface $translator) {
        if (!$condicionfiscal) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected fiscal condition.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $condicionfiscal->setEnabled($activo);
            $entityManager->persist($condicionfiscal);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Fiscal condition status has been changed', ['%fiscal_condition%' => $condicionfiscal->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('condicionfiscal_index'));
    }    

    /**
     * @Route("/{id}/edit", name="condicionfiscal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CondicionFiscal $condicionfiscal, TranslatorInterface $translator): Response {
        $form = $this->createForm(CondicionFiscalType::class, $condicionfiscal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Fiscal condition edited successfully.'));
            return $this->redirectToRoute('condicionfiscal_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the fiscal condition.'));
        }
        return $this->render('condicionfiscal/edit.html.twig', [
                    'condicionfiscal' => $condicionfiscal,
                    'form' => $form->createView(),
        ]);
    }
    
}
