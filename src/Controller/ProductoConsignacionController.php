<?php

namespace App\Controller;

use App\Entity\ProductoConsignacion;
use App\Form\ProductoConsignacionType;
use App\Repository\ProductoConsignacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/producto/consignacion")
 */
class ProductoConsignacionController extends AbstractController
{
    /**
     * @Route("/", name="producto_consignacion_index", methods={"GET"})
     */
    public function index(ProductoConsignacionRepository $productoConsignacionRepository): Response
    {
        return $this->render('producto_consignacion/index.html.twig', [
            'producto_consignacions' => $productoConsignacionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="producto_consignacion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $productoConsignacion = new ProductoConsignacion();
        $form = $this->createForm(ProductoConsignacionType::class, $productoConsignacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($productoConsignacion);
            $entityManager->flush();

            return $this->redirectToRoute('producto_consignacion_index');
        }

        return $this->render('producto_consignacion/new.html.twig', [
            'producto_consignacion' => $productoConsignacion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producto_consignacion_show", methods={"GET"})
     */
    public function show(ProductoConsignacion $productoConsignacion): Response
    {
        return $this->render('producto_consignacion/show.html.twig', [
            'producto_consignacion' => $productoConsignacion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="producto_consignacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ProductoConsignacion $productoConsignacion): Response
    {
        $form = $this->createForm(ProductoConsignacionType::class, $productoConsignacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('producto_consignacion_index');
        }

        return $this->render('producto_consignacion/edit.html.twig', [
            'producto_consignacion' => $productoConsignacion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="producto_consignacion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ProductoConsignacion $productoConsignacion): Response
    {
        if ($this->isCsrfTokenValid('delete'.$productoConsignacion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($productoConsignacion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('producto_consignacion_index');
    }
}
