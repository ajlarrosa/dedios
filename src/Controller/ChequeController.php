<?php

namespace App\Controller;

use App\Entity\Cheque;
use App\Entity\MovimientoCheque;
use App\Service\MovimientoChequeService;
use App\Form\ChequeType;
use App\Repository\ChequeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/operator/cheque")
 */
class ChequeController extends AbstractController {

    /**
     * @Route("/", name="cheque_index", methods={"GET"})
     */
    public function index(Request $request, ChequeRepository $chequeRepository, PaginatorInterface $paginator): Response {

        $filtros = $request->query->all();
        $cheques = $paginator->paginate(
                $chequeRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('cheque/index.html.twig', [
                    'cheques' => $cheques
        ]);
    }

    /**
     * @Route("/new", name="cheque_new", methods={"GET","POST"})
     */
    public function new(Request $request,MovimientoChequeService $movimientoChequeService ,TranslatorInterface $translator): Response {
        $cheque = new Cheque();
        $form = $this->createForm(ChequeType::class, $cheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movimientoCheque = $movimientoChequeService->create(MovimientoCheque::ALTA_MANUAL,$cheque);
            $entityManager = $this->getDoctrine()->getManager();           
            $entityManager->persist($movimientoCheque);
            $entityManager->persist($cheque);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Check created successfully.'));
            return $this->redirectToRoute('cheque_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the check.'));
        }
        return $this->render('cheque/new.html.twig', [
                    'cheque' => $cheque,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cheque_show", methods={"GET"})
     */
    public function show(Cheque $cheque): Response {
        return $this->render('cheque/show.html.twig', [
                    'cheque' => $cheque,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cheque_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cheque $cheque): Response {
        $form = $this->createForm(ChequeType::class, $cheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cheque_index');
        }

        return $this->render('cheque/edit.html.twig', [
                    'cheque' => $cheque,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/cambiarestado", name="cheque_cambiarestado", methods={"GET","POST"})
     */
    public function cambiarestadoAction(Cheque $cheque, Request $request, TranslatorInterface $translator) {

        $estado = $request->get('estado');

        if (isset($estado)) {

            if ($cheque) {
                $em = $this->getDoctrine()->getManager();

                $cheque->setEstado($estado);
                $em->flush();
                $movimientoCheque = new MovimientoCheque();
                $movimientoCheque->setCheque($cheque);                
                if ($cheque->getEstado() == Cheque::COD_RECHAZADO) {
                    $movimientoCheque->setTipoMovimiento(Cheque::RECHAZADO);
                } elseif ($cheque->getEstado() == Cheque::COD_ELIMINADO) {
                    $movimientoCheque->setTipoMovimiento(Cheque::ELIMINADO);
                } elseif ($cheque->getEstado() == Cheque::COD_ACTIVO) {
                    $movimientoCheque->setTipoMovimiento(Cheque::ACTIVO);
                }

                $em->persist($movimientoCheque);
                $em->flush();
                $this->addFlash('msgOk', 'The status of the check changed successfully.');
                return $this->redirect($this->generateUrl('cheque_index'));
            }
        }
        $this->addFlash($translator->trans('It was not possible to change the status of the check.'));

        return $this->redirect($this->generateUrl('cheque_index'));
    }

    /**
     * @Route("/{id}", name="cheque_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Cheque $cheque): Response {
        if ($this->isCsrfTokenValid('delete' . $cheque->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cheque);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cheque_index');
    }

}
