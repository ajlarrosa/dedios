<?php

namespace App\Controller;

use App\Entity\NotaDebito;
use App\Service\MovimientoCCService;
use App\Entity\ClienteProveedor;
use App\Form\NotaDebitoType;
use App\Repository\NotaDebitoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/notaDebito")
 */
class NotaDebitoController extends AbstractController
{
    /**
     * @Route("/", name="nota_debito_index", methods={"GET"})
     */
    public function index(NotaDebitoRepository $notaDebitoRepository): Response
    {
        return $this->render('nota_debito/index.html.twig', [
            'nota_debitos' => $notaDebitoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/new", name="nota_debito_new", methods={"GET","POST"})
     */
    public function new(Request $request, ClienteProveedor $clienteProveedor, MovimientoCCService $movimientoCCService): Response
    {
        $notaDebito = new NotaDebito();
        $form = $this->createForm(NotaDebitoType::class, $notaDebito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $movimientocc = $movimientoCCService->create($clienteProveedor, null, null, $notaDebito, null, null, null);
            $notaDebito->setMovimientocc($movimientocc);
            $entityManager->persist($notaDebito);
            $entityManager->flush();

            return $this->redirectToRoute('movimientocc_index',['id'=>$movimientocc->getClienteProveedor()->getId()]);
        }

        return $this->render('nota_debito/new.html.twig', [
            'nota_debito' => $notaDebito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nota_debito_show", methods={"GET"})
     */
    public function show(NotaDebito $notaDebito): Response
    {
        return $this->render('nota_debito/show.html.twig', [
            'nota_debito' => $notaDebito,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="nota_debito_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NotaDebito $notaDebito): Response
    {
        $form = $this->createForm(NotaDebitoType::class, $notaDebito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('nota_debito_index');
        }

        return $this->render('nota_debito/edit.html.twig', [
            'nota_debito' => $notaDebito,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="nota_debito_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NotaDebito $notaDebito): Response
    {
        if ($this->isCsrfTokenValid('delete'.$notaDebito->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($notaDebito);
            $entityManager->flush();
        }

        return $this->redirectToRoute('nota_debito_index');
    }
}
