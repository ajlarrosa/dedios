<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Repository\ImagenProductoRepository;
use App\Repository\MedidaRepository;
use App\Repository\ListaPrecioRepository;
use App\Repository\PrecioRepository;
use App\Repository\AtributosProductoRepository;
use App\Repository\Tienda\ProductoGrupoRepository;
use App\Repository\ProductoRepository;
use App\Repository\Tienda\ProducotsLineaProductoRepository;
use App\Entity\Categoriasubcategoria;
use App\Entity\Precio;
use App\Entity\ListaPrecio;
use App\Entity\ImagenProducto;
use App\Entity\Medida;
use App\Entity\Color;
use App\Entity\Letra;
use App\Entity\AtributosProducto;
use App\Entity\Tienda\ProductosLineaProducto;
use App\Entity\Tienda\LineaProducto;
use App\Entity\PrecioMedidaProducto;
use App\Form\ProductoType;
use App\Form\ProductoStockType;
use App\Service\CotizacionService;
use App\Service\ResizeService;
use App\Service\General\PhpOfficeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\General\GeneralService;
use App\Service\ImagenProductoService;
use App\Service\PrecioService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("")
 */
class ProductoController extends AbstractController {

    private $productoRepository;
    private $generalService;
    private $imagenProductoService;
    private $precioService;

    function __construct(ProductoRepository $productoRepository, GeneralService $generalService, ImagenProductoService $imagenProductoService, PrecioService $precioService) {
        $this->productoRepository = $productoRepository;
        $this->generalService = $generalService;
        $this->imagenProductoService = $imagenProductoService;
        $this->precioService = $precioService;
    }

    /**
     * @Route("/operator/producto", name="producto_index", methods={"GET","POST"})
     */
    public function index(Request $request, ProductoRepository $productoRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice, \Symfony\Component\Asset\Packages $assetsManager): Response {
        $filtros = $request->query->all();

        /*
         * Verifico si se presionó el botón actualizar precios
         */
        if ($request->get('action') == 'precios') {
            $productos = $productoRepository->filter($filtros)->getQuery()->getResult();
            $porcentaje = $request->get('porcentaje');
            $archivo = $request->files->get('archivoPrecios');

            if ($porcentaje != '' && is_numeric($porcentaje)) {
                if (floatval($porcentaje) != 0) {
                    foreach ($productos as $producto) {
                        $precios = $producto->getPrecios();
                        foreach ($precios as $precio) {
                            $precio->setValor($precio->getValor() + number_format(floatval($precio->getValor()) * floatval($porcentaje) / 100), 2);
                        }
                    }
                    $em->flush();
                    $this->addFlash("msgOk", "Se han actualizado correctamente los precios de " . count($productos) . " productos.");
                } else {
                    $this->addFlash("msgWarn", "No se realizaron modificaciones en los precios (0%).");
                }
            }
            if ($archivo !== null) {
                $this->uploadPrecios($request, $archivo, $productoRepository);
            }
        }

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $productos = $productoRepository->filter($filtros)->getQuery()->getResult();
            $spreadSheet = $this->excelInforme($productos, $filtros);

            $nombreInforme = '';
            if ($request->get('tipoReporte') == 'stock') {
                $nombreInforme = 'Stock de Productos';
            } elseif ($request->get('tipoReporte') == 'precios') {
                $nombreInforme = 'Productos';
            }
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }


        /*
         * Verifico si se presiono el botón exportar xml Google Shopping
         */
        if ($request->get('action') == 'google_shop') {
            $productos = $productoRepository->filter($filtros)->getQuery()->getResult();

            $filename = 'Catalogo_Google_Shopping_' . date('Ymd') . '.xml';

            // The dinamically created content of the file
            $fileContent = '<?xml version="1.0"?>
                <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
                    <channel>
                        <title>De Dios Store</title>
                        <link>https://dediosjoyas.com/</link>
                        <description>Actualización Catálogo De Dios ' . date("d/m/Y") . '</description>';

            foreach ($productos as $producto) {

                $categoria = $producto->getCategoriasubcategoria()->getCategoria()->getDescripcion();
                $subcategoria = $producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion();
                $url = $this->generateUrl('producto_tienda_show', [
                    'categoria' => $categoria,
                    'subcategoria' => $subcategoria,
                    'id' => $producto->getId(),
                    'descripcion' => $this->generalService->slugify($producto->getDescripcion())
                        ],
                        UrlGeneratorInterface::ABSOLUTE_URL);

                $firstImagen = $this->imagenProductoService->getFirstImage($producto->getId());
                $urlImagen = '';
                if ($firstImagen != '') {
                    $urlImagen = $request->getSchemeAndHttpHost() . $assetsManager->getUrl('uploads/productos/' . $producto->getId() . '/' . str_replace(' ', '%20', $firstImagen));
                }
                $listaPrecioId = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;

                $precio = $this->precioService->getPrecioPorListaPrecio($producto->getId(), $listaPrecioId);
                if ($precio != '') {
                    $fileContent .= '
                                <item>
                                    <g:id>' . $producto->getCodigo() . '_' . $producto->getId() . '</g:id>
                                    <g:title>' . str_replace('&', 'y', $producto->getDescripcion()) . '</g:title>
                                    <g:description>' . str_replace('&', 'y', $producto->getDescripcionAmpliada()) . '</g:description>
                                    <g:link>' . $url . '</g:link>
                                    <g:image_link>' . $urlImagen . '</g:image_link>
                                    <g:condition>new</g:condition>
                                    <g:availability>in stock</g:availability>
                                    <g:price>' . $precio . ' ARS</g:price>
                                    <g:shipping>
                                        <g:country>AR</g:country>
                                        <g:service>Envío</g:service>
                                    </g:shipping>
                                    <g:google_product_category>' . str_replace('&', 'y', $categoria) . ' > ' . str_replace('&', 'y', $subcategoria) . '</g:google_product_category>
                                    <g:product_type>Jewels</g:product_type>
                                </item>';
                }
            }

            $fileContent .= '
                    </channel>
                </rss>';

            $response = new Response($fileContent);

            $disposition = $response->headers->makeDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $filename
            );

            $response->headers->set('Content-Disposition', $disposition);

            return $response;
        }

        $productos = $paginator->paginate(
                $productoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('producto/index.html.twig', [
                    'productos' => $productos,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($productos, $parametros) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];

        $fila = 1;

        switch ($parametros['tipoReporte']) {
            case 'precios':

                $listasPrecio = $parametros['listaPrecio'];
                $entityManager = $this->getDoctrine()->getManager();

                $listaPrecio1 = $entityManager->getRepository(ListaPrecio::class)->find($listasPrecio[0]);

                if (isset($listasPrecio[1])) {
                    $listaPrecio2 = $entityManager->getRepository(ListaPrecio::class)->find($listasPrecio[1]);
                }

                $sheet->setCellValue('A' . $fila, 'Categoria Subcategoria Metal');
                $sheet->setCellValue('B' . $fila, 'Codigo');
                $sheet->setCellValue('C' . $fila, 'Cantidad');
                $sheet->setCellValue('D' . $fila, 'Costo (Hechura)');
                $sheet->setCellValue('E' . $fila, 'Moneda Hechura');
                $sheet->setCellValue('F' . $fila, 'Peso Unitario AG');
                $sheet->setCellValue('G' . $fila, 'Peso Unitario AU');
                $sheet->setCellValue('H' . $fila, 'Descripción');
                $sheet->setCellValue('I' . $fila, 'Descripción Ampliada');
                $sheet->setCellValue('J' . $fila, 'Tipo de Stock');
                $sheet->setCellValue('K' . $fila, 'Orden');
                $sheet->setCellValue('L' . $fila, $listaPrecio1->getDescripcion());
                if (isset($listaPrecio2)) {
                    $sheet->setCellValue('M' . $fila, $listaPrecio2->getDescripcion());
                }

                $sheet->setCellValue('N' . $fila, '¿Tiene imágenes?');
                $sheet->setCellValue('O' . $fila, 'Medida');
                $sheet->setCellValue('P' . $fila, 'Color');
                $sheet->setCellValue('Q' . $fila, 'Letra');

                $sheet->getStyle('A' . $fila . ':Q' . $fila)->applyFromArray($styleArray2);
                
                foreach ($productos as $producto) {     
                     $fila++;
                    if ($producto->getCategoriasubcategoria())
                        $sheet->setCellValue('A' . $fila, $producto->getCategoriasubcategoria()->getId());
                    $sheet->setCellValue('B' . $fila, $producto->getCodigo());
                    $sheet->setCellValue('C' . $fila, $producto->getStock());
                    $sheet->setCellValue('D' . $fila, $producto->getCosto());
                    $sheet->setCellValue('E' . $fila, $producto->getMonedaSimbolo());
                    $sheet->setCellValue('F' . $fila, $producto->getPlata());
                    $sheet->setCellValue('G' . $fila, $producto->getOro());
                    $sheet->setCellValue('H' . $fila, $producto->getDescripcion());
                    $sheet->setCellValue('I' . $fila, $producto->getDescripcionAmpliada());
                    $sheet->setCellValue('J' . $fila, $producto->getTipoStock());
                    $sheet->setCellValue('K' . $fila, $producto->getOrden());
                    $sheet->setCellValue('L' . $fila, $producto->getPrecio($listaPrecio1->getId()));
                    if (isset($listaPrecio2))
                        $sheet->setCellValue('M' . $fila, $producto->getPrecio($listaPrecio2->getId()));

                    $sheet->setCellValue('N' . $fila, sizeof($producto->getImagenesproducto()));
                    
                    $sheet->setCellValue('O' . $fila, '');
                    $sheet->setCellValue('P' . $fila, '');
                    $sheet->setCellValue('Q' . $fila, '');
                    $precio1 = $producto->getPrecio($listaPrecio1->getId());
                    if (!is_numeric($precio1)) {
                        $precio1 = 0;
                    }
                    $sheet->setCellValue('L' . $fila, $precio1);
                    if (isset($listaPrecio2)) {
                        $precio2 = $producto->getPrecio($listaPrecio2->getId());
                        if (!is_numeric($precio2)) {
                            $precio2 = 0;
                        }
                        $sheet->setCellValue('M' . $fila, $precio2);
                    }
                    
                    if(count($producto->getAtributosProducto())>0){
                        
                        foreach ($producto->getAtributosProducto() as $atributo) {
                            $fila++;
                            if ($producto->getCategoriasubcategoria())
                                $sheet->setCellValue('A' . $fila, $producto->getCategoriasubcategoria()->getId());
                            $sheet->setCellValue('B' . $fila, $producto->getCodigo());
                            $sheet->setCellValue('O' . $fila, $atributo->getMedida());
                            $sheet->setCellValue('P' . $fila, $atributo->getColor());
                            $sheet->setCellValue('Q' . $fila, $atributo->getLetra());
                            if ($atributo->getMedida() != null) {
                                $precio1 = $this->precioService->getPrecioMedida($producto->getId(), $atributo->getMedida()->getId(), $listaPrecio1->getId());
                                if (!is_numeric($precio1)) {
                                    $precio1 = $this->precioService->getPrecioPorListaPrecio($producto->getId(), $listaPrecio1->getId());
                                    if (!is_numeric($precio1)) {
                                        $precio1 = 0;
                                    }
                                }
                                $sheet->setCellValue('L' . $fila, $precio1);
                                if (isset($listaPrecio2)) {
                                    $precio2 = $this->precioService->getPrecioMedida($producto->getId(), $atributo->getMedida()->getId(), $listaPrecio2->getId());
                                    if (!is_numeric($precio2)) {
                                        $precio2 = $this->precioService->getPrecioPorListaPrecio($producto->getId(), $listaPrecio2->getId());
                                        if (!is_numeric($precio2)) {
                                            $precio2 = 0;
                                        }
                                    }
                                    $sheet->setCellValue('M' . $fila, $precio2);
                                }
                            } else {
                                $precio1 = $producto->getPrecio($listaPrecio1->getId());
                                if (!is_numeric($precio1)) {
                                    $precio1 = 0;
                                }
                                $sheet->setCellValue('L' . $fila, $precio1);
                                if (isset($listaPrecio2)) {
                                    $precio2 = $producto->getPrecio($listaPrecio2->getId());
                                    if (!is_numeric($precio2)) {
                                        $precio2 = 0;
                                    }
                                    $sheet->setCellValue('M' . $fila, $precio2);
                                }
                            }
                            
                        }
                    }                    
                }
               
                $fila++;
                break;
            case 'stock':

                $sheet->setCellValue('A' . $fila, 'Id');
                $sheet->setCellValue('B' . $fila, 'Código');
                $sheet->setCellValue('C' . $fila, 'Descripción');
                $sheet->setCellValue('D' . $fila, 'Stock');
                $sheet->getStyle('A' . $fila . ':D' . $fila)->applyFromArray($styleArray2);

                $fila++;

                foreach ($productos as $producto) {
                    $sheet->setCellValue('A' . $fila, $producto->getId());
                    $sheet->setCellValue('B' . $fila, $producto->getCodigo());
                    $sheet->setCellValue('C' . $fila, $producto->getDescripcion());
                    $sheet->setCellValue('D' . $fila, $producto->getStock());
                    $fila++;
                }

                $fila++;
                break;
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);

        return $spreadsheet;
    }

    /**
     * Carga masiva de precios por archivos
     *
     */
    public function uploadPrecios(Request $request, $archivo, $productoRepository) {
        $em = $this->getDoctrine()->getManager();
        if ($archivo !== null) {

            if ($archivo->getMimeType() != 'application/vnd.ms-excel' &&
                    $archivo->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
                    $archivo->getMimeType() != 'application/vnd.oasis.opendocument.spreadsheet') {

                $this->addFlash("msgError", "Error: Tipo de archivo incorrecto.");
                return;
            }

            $spreadsheet = IOFactory::load($request->files->get('archivoPrecios'));

            $worksheet = $spreadsheet->getActiveSheet();  // get active worksheet
            $rows = []; //empty array of rows
            $r = 0;
            $c = 0;
            $error = false;
            foreach ($worksheet->getRowIterator() AS $row) {
                $r++;
                $cells = $row->getCellIterator();
                $cells->setIterateOnlyExistingCells(FALSE); // iterates through all cells, including empty ones
                $cellData = [];
                foreach ($cells as $cell) {
                    $c++;
                    $cellData[] = $cell->getValue();
                    if ($r > 1 && $cell->getValue()) {
                        if ($c == 1) {
                            $id = $cell->getValue();
                        }
                        if ($c == 4 || $c == 5) {
                            if (is_int(intval($id))) {
                                $valor = $cell->getValue();
                                if (is_float($valor)) {
                                    $producto = $productoRepository->find($id);
                                    if ($producto) {
                                        $precios = $producto->getPrecios();
                                        foreach ($precios as $precio) {
                                            if (($precio->getListaPrecio()->getMoneda() == 'ARG' && $c == 4) || ($precio->getListaPrecio()->getMoneda() == 'US' && $c == 5)) {
                                                $precio->setValor(floatval($valor));
                                            }
                                        }
                                    } else {
                                        $error = true;
                                        $this->addFlash("msgError", "Error de formato de archivo: En la línea " . $r . " no se encontró el PRODUCTO.");
                                    }
                                } else {
                                    $error = true;
                                    $this->addFlash("msgError", "Error de formato de archivo: En la línea " . $r . " columna " . $c . " no se encontró un PRECIO de producto válido.");
                                }
                            } else {
                                $error = true;
                                $this->addFlash("msgError", "Error de formato de archivo: En la línea " . $r . " columna 1 no se encontró un ID de producto válido.");
                            }
                        }
                    }
                }
                $rows[] = $cells;
                $c = 0;
            }
        } else {
            $error = true;
            $this->addFlash("msgError", "Error al intentar subir el archivo.");
        }

        if (!$error) {
            $em->flush();
            $this->addFlash("msgOk", "Se han actualizado correctamente los precios de " . ($r - 1) . " productos.");
        }
    }

    /**
     * @Route("/operator/producto/vistarapida", name="producto_vistarapida", methods={"GET","POST"})
     */
    public function vistaRapida(Request $request, ProductoRepository $productoRepository): Response {
        $entity = null;

        if ($request->get('codigo')) {
            $entity = $productoRepository->findOneBy(["codigo" => $request->get('codigo')]);
        }

        return $this->render('producto/vistaRapida.html.twig', [
                    'entity' => $entity,
        ]);
    }

    /**
     * @Route("/operator/producto/new", name="producto_new", methods={"GET","POST"})
     */
    public function new(Request $request, ResizeService $resize, MedidaRepository $medidaRepository, ProductoRepository $productoRepository): Response {
        $producto = new Producto();
        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $codigo = $producto->getCodigo();
            if (trim($codigo) == '') {
                $codigo = $this->getCodigo($producto, $productoRepository->getMaxId() + 1);
                $producto->setCodigo($codigo);
            }

            $entityManager = $this->getDoctrine()->getManager();

            try {
                $entityManager->persist($producto);
                $entityManager->flush();

                $error = false;
                if (!$this->actualizarAtributos($request, $producto)) {
                    $this->addFlash("msgError", "No se han podido crear los atributos del producto.");
                    $error = true;
                }
                if (!$this->actualizarLineasProductos($request, $producto)) {
                    $this->addFlash("msgError", "No se ha podido asociar el producto a las líneas de productos seleccionadas.");
                    $error = true;
                }

                if (!$this->altaImagenes($request, $producto, $resize)) {
                    $this->addFlash("msgError", "No se han podido cargar las imágenes.");
                    $error = true;
                }
                if ($error) {
                    $this->addFlash("msgError", "Han habido errores durante la carga del producto. Algunos atributos no se han cargado.");
                } else {
                    $this->addFlash("msgOk", "Se ha creado un nuevo producto.");
                }
                return $this->redirectToRoute('producto_index', ['finder' => $producto->getCodigo()]);
            } catch (\Exception $e) {
                $this->addFlash("msgError", "No ha sido posible crear el producto con el código. " . $producto->getCodigo());
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash("msgError", "No se han podido cargar las imágenes.");
        }

        return $this->render('producto/new.html.twig', [
                    'medidas' => $medidaRepository->findBy(['enabled' => true], ['descripcion' => 'ASC']),
                    'producto' => $producto,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/operator/producto/predictiva",name="producto_predictiva", methods={"GET","POST"})
     */
    public function productoPredictivaAction(Request $request) {
        $data = $request->get('input');
        $em = $this->getDoctrine()->getManager();

        $productoList = '';
        if (strlen($data) >= 3) {
            $productos = $em->getRepository(Producto::class)->getProductoPredictiva($data);

            $productoList = '<ul id="matchList">';
            foreach ($productos as $producto) {
                $match = $producto->__toString();
                $matchStringBold = preg_replace('/(' . $data . ')/i', '<strong>$1</strong>', $match);
                $productoList .= '<li id="' . $producto->getId() . '">' . $matchStringBold . '</li>';
            }
            $productoList .= '</ul>';
        }
        $response = new JsonResponse();
        $response->setData(array('productoList' => $productoList));
        return $response;
    }

    /**
     * @Route("/operator/producto/importacionMasiva", name="producto_form_importacion_masiva", methods={"GET"})
     */
    public function formImportacionMasiva(): Response {

        return $this->render('producto/importacionmasiva.html.twig');
    }

    /**
     * @Route("/operator/producto/importarProductos", name="producto_importacion_masiva", methods={"POST"})
     */
    public function importarProductos(Request $request, ListaPrecioRepository $listaPrecioRepository, ProductoRepository $productoRepository, TranslatorInterface $translator): Response {
        $creadosActualizados = 0;
        $conErrores = 0;
        $archivo = $request->files->get('archivo');
        $error = false;
        if ($archivo === null) {
            $error = true;
            $this->addFlash("msgError", $translator->trans("Error trying to upload file."));
        }
        if (!$error) {
            $em = $this->getDoctrine()->getManager();
            if ($archivo->getMimeType() != 'application/vnd.ms-excel' &&
                    $archivo->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
                    $archivo->getMimeType() != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheetapplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
                    $archivo->getMimeType() != 'application/vnd.oasis.opendocument.spreadsheet') {

                $this->addFlash("msgError", $translator->trans('Incorrect file type.'));
                $error = true;
            }
        }
        if (!$error) {
            $spreadsheet = IOFactory::load($archivo);
            $worksheet = $spreadsheet->getActiveSheet();  // get active worksheet
            $listaPrecioName1 = $worksheet->getCell('L1')->getValue();
            $listaPrecio1 = $listaPrecioRepository->findOneBy(array('descripcion' => $listaPrecioName1));
            if (!$listaPrecio1) {
                $this->addFlash('msgError', $translator->trans('Price list not found.', ['%list%' => $listaPrecioName1]));
                $error = true;
            }
        }

        if (!$error) {

            $codigoAnterior = '';
            foreach ($worksheet->getRowIterator() AS $row) {
                $r = $row->getRowIndex();
                if ($r > 1) {
                    //Código, puede venir vacío en ese caso hay que autogenerarlo
                    $codigo = $worksheet->getCell('B' . $r)->getValue();

                    if ($codigoAnterior != $codigo) {

                        $codigoAnterior = $codigo;

                        if ($worksheet->getCell('A' . $r)->getValue() == '') {
                            break;
                        }

                        //Categoría Subcategoría Metal
                        $categoriaSubcategoriaId = $worksheet->getCell('A' . $r)->getValue();
                        if ($categoriaSubcategoriaId > 0) {
                            $categoriaSubcategoria = $em->getRepository(Categoriasubcategoria::class)->findOneBy(array('id' => $categoriaSubcategoriaId));
                            if (!isset($categoriaSubcategoria)) {
                                $this->addFlash('msgError', $translator->trans('The line does not have category-subcategory-metal id', ['%row%' => $r]));
                                $conErrores++;
                                break;
                            }
                        } else {
                            $this->addFlash('msgError', $translator->trans('The line does not have category-subcategory-metal id', ['%row%' => $r]));
                            $conErrores++;
                            break;
                        }

                        $producto = $em->getRepository(Producto::class)->findOneBy(array('codigo' => $codigo));
                        if ($codigo == '') { //Crear Productos
                            $producto = new Producto();
                            $producto->setCategoriasubcategoria($categoriaSubcategoria);
                            $producto->setCodigo($this->getCodigo($producto, $productoRepository->getMaxId() + 1));
                        } else if (!$producto) { //No existe el producto en la base de datos y no se permite generar códigos
                            $this->addFlash('msgError', $translator->trans('The code entered in the line does not exist in the product database.', ['%row%' => $r, '%code%' => $codigo]));
                            $conErrores++;
                            break;
                        } else { //Actualizar producto
                            $producto->setCategoriasubcategoria($categoriaSubcategoria);
                        }

                        //Cantidad
                        $cantidad = floatval($worksheet->getCell('C' . $r)->getCalculatedValue());
                        if (is_float($cantidad)) {
                            $producto->setStock($cantidad);
                        } else {
                            $this->addFlash('msgError', $translator->trans('The amount entered on the line must be a positive real.', ['%row%' => $r, '%amount%' => $cantidad]));
                            $conErrores++;
                            break;
                        }

                        //Hechura
                        $hechura = floatval($worksheet->getCell('D' . $r)->getValue());
                        if (is_float($hechura)) {
                            $producto->setCosto($hechura);
                        } else {
                            $this->addFlash('msgError', $translator->trans('The cost entered on the line must be a positive real number.', ['%row%' => $r, '%cost%' => $hechura]));
                            $conErrores++;
                            break;
                        }

                        //Moneda Hechura USD o ARG
                        $moneda = $worksheet->getCell('E' . $r)->getValue();

                        if (in_array($moneda, [Producto::PESOS, Producto::DOLAR])) {
                            if ($moneda == Producto::PESOS) {
                                $producto->setMoneda(Producto::COD_PESOS);
                            } else {
                                $producto->setMoneda(Producto::COD_DOLAR);
                            }
                        } else {
                            $producto->setMoneda(Producto::COD_PESOS);
                        }

                        //Peso Ag
                        $pesoAg = $worksheet->getCell('F' . $r)->getValue();
                        if ($pesoAg != '') {
                            $pesoAg = floatval($worksheet->getCell('F' . $r)->getCalculatedValue());
                            if (is_float($pesoAg)) {
                                $producto->setPlata($pesoAg);
                            } else {
                                $this->addFlash('msgError', $translator->trans('The weight entered on the line must be a positive real number.', ['%row%' => $r, '%weight%' => $pesoAg]));
                                $conErrores++;
                                break;
                            }
                        }

                        //Peso Au
                        $pesoAu = $worksheet->getCell('G' . $r)->getValue();
                        if ($pesoAu != '') {
                            $pesoAu = floatval($worksheet->getCell('G' . $r)->getCalculatedValue());
                            if (is_float($pesoAu)) {
                                $producto->setOro($hechura);
                            } else {
                                $this->addFlash('msgError', $translator->trans('The weight entered on the line must be a positive real number.', ['%row%' => $r, '%weight%' => $pesoAu]));
                                $conErrores++;
                                break;
                            }
                        }

                        //Descripción               
                        if ($worksheet->getCell('H' . $r)->getValue() == null) {
                            $this->addFlash('msgError', $translator->trans("The product's description entered on the line %row% can not be empty.", ['%row%' => $r]));
                            $conErrores++;
                            break;
                        }
                        $producto->setDescripcion($worksheet->getCell('H' . $r)->getValue());

                        //Descripción Ampliada              
                        $producto->setDescripcionAmpliada($worksheet->getCell('I' . $r)->getValue());

                        //Tipo de Stock U, O, P
                        $tipoStock = $worksheet->getCell('J' . $r)->getValue();
                        if (in_array($tipoStock, [Producto::COD_UNIDAD, Producto::COD_GRAMO_PLATA, Producto::COD_GRAMO_ORO])) {
                            $producto->setTipoStock($tipoStock);
                        } else {
                            $producto->setTipoStock(Producto::COD_UNIDAD);
                        }

                        //Order               
                        $producto->setOrden(intval($worksheet->getCell('K' . $r)->getValue()));


                        //Lista de Precios Nro. 1 Lista 1 o Lista Web               
                        $precioValue = $worksheet->getCell('L' . $r)->getValue();

                        if ($precioValue != '') {
                            if ((is_float($precioValue) || is_integer($precioValue)) && $precioValue >= 0 ) {
                                $precio = $em->getRepository(Precio::class)->findOneBy(array('producto' => $producto, 'listaPrecio' => $listaPrecio1));
                                if (!$precio) {
                                    $precio = new Precio();
                                    $precio->setListaPrecio($listaPrecio1);
                                    $precio->setProducto($producto);
                                }
                                $precio->setValor(round($precioValue, 2));
                                $em->persist($precio);
                                $em->persist($producto);
                                $em->flush();
                                $producto->setCodigo($this->getCodigo($producto));
                                $em->flush();
                                $creadosActualizados++;
                            } else {
                                $this->addFlash('msgError', $translator->trans('The price entered in the row must be a positive real number.', ['%row%' => $r, '%price%' => $precioValue]));
                                $conErrores++;
                                break;
                            }
                        } else {
                            $this->addFlash('msgError', $translator->trans('Se debe colocar un precio en la línea {{ row }}. El precio es obligatorio.', ['{{ row }}' => $r]));
                            $conErrores++;
                            break;
                        }
                        
                        $this->procesarAtributos($worksheet, $r, $producto, $listaPrecio1, $conErrores,$translator);
                        
                    } else {
                        $producto = $em->getRepository(Producto::class)->findOneBy(array('codigo' => $codigo));

                        if (!$producto) { //No existe el producto en la base de datos y no se permite generar códigos
                            $this->addFlash('msgError', $translator->trans('The code entered in the line does not exist in the product database.', ['%row%' => $r, '%code%' => $codigo]));
                            $conErrores++;
                            break;
                        }

                        $this->procesarAtributos($worksheet, $r, $producto, $listaPrecio1, $conErrores, $translator);

                        $em->flush();
                        $creadosActualizados++;
                    }
                }
            }
            if ($creadosActualizados > 0) {
                $this->addFlash("msgOk", $translator->trans('Products have been updated and / or created.', ['%row%' => $creadosActualizados]));
            }
            if ($conErrores > 0) {
                $this->addFlash("msgErrores", $translator->trans('lines with errors.', ['%row%' => $conErrores]));
            }
        }
        if ($archivo) {
            unlink($archivo);
        }

        return $this->render('producto/importacionmasiva.html.twig');
    }

    private function procesarAtributos($worksheet, $r, $producto, $listaPrecio1, &$conErrores, $translator) {
        $errorMedida=false;
        $errorColor=false;
        $errorLetra=false;
        $em = $this->getDoctrine()->getManager();
        $r++;
        $medidaValue = trim($worksheet->getCell('O' . $r)->getValue());

        //Buscamos el atributo medida por descripcion
        if ($medidaValue != '') {
            $medida = $em->getRepository(Medida::class)->findBy(['descripcion' => $medidaValue]);

            //Si existe verifico que esté activo, si no lo creo
            /*if (sizeof($medida) > 0) {
                if (!$medida[0]->getEnabled()) {
                    $medida[0]->setEnabled(true);
                }
                $medida = $medida[0];
            } else {
                $medida = new Medida();
                $medida->setDescripcion($medidaValue);
                $em->persist($medida);
            }*/
            
            if (sizeof($medida) > 0) {
                $medida = $medida[0];
            }else{
                $this->addFlash('msgError', $translator->trans('The meassure entered in the row does not exists.', ['%row%' => $r, '%meassure%' => $medidaValue]));
                $conErrores++;
                $errorMedida=true;
            }

            if(!$errorMedida){
                $atributoProducto = null;

                foreach ($producto->getAtributosProducto() as $atributo) {
                    if ($atributo->getMedida() != null && $atributo->getMedida()->getId() == $medida->getId()) {
                        $atributoProducto = $atributo;
                    }
                }

                if ($atributoProducto == null) {
                    $atributoProducto = new AtributosProducto();
                    $atributoProducto->setMedida($medida);
                    $em->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);
                }

                //Lista de Precios Nro. 1 Lista 1 o Lista Web               
                $precioValue = $worksheet->getCell('L' . $r)->getValue();

                if (!((is_float($precioValue) || is_integer($precioValue)) && $precioValue >= 0 )) {
                    $this->addFlash('msgError', $translator->trans('The price entered in the row must be a positive real number.', ['%row%' => $r, '%price%' => $precioValue]));
                    $conErrores++;
                } else {
                    $precioReferencia = $em->getRepository(Precio::class)->findPreciosBy(['producto' => $producto, 'listaPrecio' => $listaPrecio1, 'activo' => true]);

                    if (sizeof($precioReferencia) > 0) {

                        $idPrecioMedida = $this->precioService->getIdPrecioMedida($producto->getId(), $medida->getId(), $listaPrecio1->getId());

                        if (is_int($idPrecioMedida)) {
                            $precio = $em->getRepository(Precio::class)->find($idPrecioMedida);
                            $precio->setValor(round($precioValue, 2));
                        } else {
                            $precio = new Precio();
                            $precio->setListaPrecio($listaPrecio1);
                            $precio->setProducto($producto);
                            $precio->setValor(round($precioValue, 2));
                            $precio->setPrecioReferencia($precioReferencia[0]);
                            $em->persist($precio);
                            $precioMedidaProducto = new PrecioMedidaProducto();
                            $precioMedidaProducto->setPrecio($precio);
                            $precioMedidaProducto->setAtributoProducto($atributoProducto);
                            $em->persist($precioMedidaProducto);
                        }
                    }
                }
            }    
        }

        $colorValue = trim($worksheet->getCell('P' . $r)->getValue());

        if ($colorValue != '') {
            $color = $em->getRepository(Color::class)->findBy(['descripcion' => $colorValue]);

            //Si existe verifico que esté activo, si no lo creo
            /*if (sizeof($color) > 0) {
                if (!$color[0]->getEnabled()) {
                    $color[0]->setEnabled(true);
                }
                $color = $color[0];
            } else {
                $color = new Color();
                $color->setDescripcion($colorValue);
                $em->persist($color);
            }*/

            if (sizeof($color) > 0) {
                $color = $color[0];
            } else {
                $this->addFlash('msgError', $translator->trans('The color entered in the row does not exists.', ['%row%' => $r, '%color%' => $colorValue]));
                $conErrores++;
                $errorColor=true;
            }
            
            if(!$errorColor){
            
                $atributoProducto = null;

                foreach ($producto->getAtributosProducto() as $atributo) {
                    if ($atributo->getColor() != null && $atributo->getColor()->getId() == $color->getId()) {
                        $atributoProducto = $atributo;
                    }
                }

                if ($atributoProducto == null) {
                    $atributoProducto = new AtributosProducto();
                    $atributoProducto->setColor($color);
                    $em->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);
                }
            }    
        }

        $letraValue = $worksheet->getCell('Q' . $r)->getValue();

        if ($letraValue != '') {
            $letra = $em->getRepository(Letra::class)->findBy(['descripcion' => $letraValue]);

            //Si existe verifico que esté activo, si no lo creo
            /*if (sizeof($letra) > 0) {
                if (!$letra[0]->getEnabled()) {
                    $letra[0]->setEnabled(true);
                }
                $letra = $letra[0];
            } else {
                $letra = new Letra();
                $letra->setDescripcion($letraValue);
                $em->persist($letra);
            }*/

            if (sizeof($letra) > 0) {
                $letra = $letra[0];
            } else {
                $this->addFlash('msgError', $translator->trans('The letter entered in the row does not exists.', ['%row%' => $r, '%letter%' => $letraValue]));
                $conErrores++;
                $errorLetra=true;
            }
            
            if(!$errorLetra){
                
                $atributoProducto = null;

                foreach ($producto->getAtributosProducto() as $atributo) {
                    if ($atributo->getLetra() != null && $atributo->getLetra()->getId() == $letra->getId()) {
                        $atributoProducto = $atributo;
                    }
                }

                if ($atributoProducto == null) {
                    $atributoProducto = new AtributosProducto();
                    $atributoProducto->setLetra($letra);
                    $em->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);
                }
            }   
        }
    }

    /*
     * Get Codigo
     */

    private function getCodigo($producto, $id = null) {
        if (!$id) {
            $id = $producto->getId();
        }
        if (is_null($producto->getCategoriasubcategoria())) {
            if (!$id) {
                $codigo = 'OTR' . $id;
            }
        } else {
            $sigla = $producto->getCategoriasubcategoria()->getCategoria()->getSigla();
            $codigo = $sigla . $id;
        }
        return $codigo;
    }

    /**
     * @Route("/operator/producto/{id}", name="producto_show", methods={"GET"})
     */
    public function show(Producto $producto, PrecioRepository $precioRepository): Response {

        return $this->render('producto/show.html.twig', [
                    'precios' => $precioRepository->findPreciosBy(['activo' => 1, 'producto' => $producto]),
                    'producto' => $producto,
        ]);
    }

    /**
     * @Route("/operator/producto/{id}/edit", name="producto_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Producto $producto, ResizeService $resize, MedidaRepository $medidaRepository, PrecioRepository $precioRepository): Response {


        $form = $this->createForm(ProductoType::class, $producto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $producto->setCodigo($this->getCodigo($producto, $producto->getid()));

            $entityManager->flush();
            $error = false;

            if (!$this->actualizarAtributos($request, $producto)) {
                $this->addFlash("msgError", "No se han podido editar los atributos del producto.");
                $error = true;
            }
            if (!$this->actualizarLineasProductos($request, $producto)) {
                $this->addFlash("msgError", "No se ha podido asociar el producto a las líneas de productos seleccionadas.");
                $error = true;
            }

            if (!$this->actualizarPosiciones($request)) {
                $this->addFlash("msgError", "No se han podido editar las posiciones de las imágenes.");
                $error = true;
            }
            if (!$this->altaImagenes($request, $producto, $resize)) {
                $this->addFlash("msgError", "No se han podido cargar las imágenes.");
                $error = true;
            }
            if ($error) {
                $this->addFlash("msgError", "Han habido errores durante la carga del producto. Algunos atributos no se han cargado.");
            } else {
                $this->addFlash("msgOk", "Se ha editado el producto.");
            }
            return $this->redirectToRoute('producto_index', ['finder' => $producto->getCodigo()]);
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash("msgError", "No se ha podido editar el producto.");
        }

        return $this->render('producto/edit.html.twig', [
                    'medidas' => $medidaRepository->findBy(['enabled' => 1], ['descripcion' => 'ASC']),
                    'precios' => $precioRepository->findPreciosBy(['activo' => 1, 'producto' => $producto]),
                    'producto' => $producto,
                    'form' => $form->createView(),
        ]);
    }

    /*
     * Actualizar atributos
     */

    private function actualizarAtributos($request, $producto) {

        try {
            $p = $request->request->get('producto');
            $entityManager = $this->getDoctrine()->getManager();

            //primero elimino todos los atributos 
            foreach ($producto->getAtributosProducto() as $atributo) {
                $producto->removeAtributosProducto($atributo);
                $entityManager->remove($atributo);
            }

            if (isset($p['medidas'])) {
                $listasPrecios = $entityManager->getRepository(ListaPrecio::class)->findBy(['activo' => true]);
                $preciosMedida = $request->request->get('producto');
                foreach ($p['medidas'] as $medida_id) {

                    $atributoProducto = new AtributosProducto();
                    $medida = $entityManager->getRepository(Medida::class)->find($medida_id);
                    $atributoProducto->setMedida($medida);
                    $entityManager->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);

                    foreach ($listasPrecios as $listaPrecio) {
                        $valor = $request->request->get('_' . $listaPrecio->getId() . '_medida_precio_' . $medida_id);

                        if (is_numeric($valor)) {
                            $precioReferencia = $entityManager->getRepository(Precio::class)->findPreciosBy(['producto' => $producto, 'listaPrecio' => $listaPrecio, 'activo' => true]);

                            if (sizeof($precioReferencia) > 0) {
                                $precios = $entityManager->getRepository(Precio::class)->findPreciosBy(['producto' => $producto, 'listaPrecio' => $listaPrecio, 'activo' => true, 'precioReferencia' => $precioReferencia[0]->getId()]);

                                if (sizeof($precios) > 0) {
                                    $precio = $precios[0];
                                } else {
                                    $precio = new Precio();
                                }
                                $precio->setListaPrecio($listaPrecio);
                                $precio->setProducto($producto);
                                $precio->setValor($valor);
                                $precio->setPrecioReferencia($precioReferencia[0]);
                                $entityManager->persist($precio);
                                $precioMedidaProducto = new PrecioMedidaProducto();
                                $precioMedidaProducto->setPrecio($precio);
                                $precioMedidaProducto->setAtributoProducto($atributoProducto);
                                $entityManager->persist($precioMedidaProducto);
                            }
                        }
                    }
                }
            }


            if (isset($p['colores'])) {
                foreach ($p['colores'] as $color_id) {
                    $atributoProducto = new AtributosProducto();
                    $color = $entityManager->getRepository(Color::class)->find($color_id);
                    $atributoProducto->setColor($color);
                    $entityManager->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);
                }
            }

            if (isset($p['letras'])) {
                foreach ($p['letras'] as $letra_id) {
                    $atributoProducto = new AtributosProducto();
                    $letra = $entityManager->getRepository(Letra::class)->find($letra_id);
                    $atributoProducto->setLetra($letra);
                    $entityManager->persist($atributoProducto);
                    $producto->addAtributosProducto($atributoProducto);
                }
            }
            $entityManager->persist($producto);
            $entityManager->flush();
        } catch (\Exception $e) {
            //var_dump($e->getMessage());
            //die;
            return false;
        }
        return true;
    }

    /*
     * Actualizar lineas de producto
     */

    private function actualizarLineasProductos($request, Producto $producto) {

        try {
            $p = $request->request->get('producto');
            $entityManager = $this->getDoctrine()->getManager();

            //primero elimino todos los atributos 
            foreach ($producto->getProductosLineaProducto() as $productoLineaProducto) {
                $producto->removeProductosLineaProducto($productoLineaProducto);
                $entityManager->remove($productoLineaProducto);
            }

            if (isset($p['lineasProducto'])) {
                foreach ($p['lineasProducto'] as $linea_producto_id) {
                    $productoLineaProducto = new ProductosLineaProducto();
                    $lineaProducto = $entityManager->getRepository(LineaProducto::class)->find($linea_producto_id);
                    $productoLineaProducto->setLineaProducto($lineaProducto);
                    $entityManager->persist($productoLineaProducto);
                    $producto->addProductosLineaProducto($productoLineaProducto);
                }
            }

            $entityManager->persist($producto);
            $entityManager->flush();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /*
     * Actualizar posicionesde imágenes
     */

    private function actualizarPosiciones(Request $request) {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $imagenesId = $request->request->get('imagen');
            $imagenPosicion = $request->request->get('imagenPosicion');
            if (isset($imagenesId)) {
                foreach ($imagenesId as $key => $imagenId) {
                    $imagenProducto = $entityManager->getRepository(ImagenProducto::class)->find($imagenId);
                    $imagenProducto->setPos($imagenPosicion[$key]);
                    $entityManager->flush();
                }
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @Route("/operator/producto/enabled/{id}", name="producto_enabled", methods={"GET"})
     *
     */
    public function enabled(Request $request, Producto $producto) {
        if (!$producto) {
            $this->addFlash('msgOk', 'No ha sido posible cambair el estado del producto seleccionado');
        } else {
            $enabled = $request->get('enabled');
            $entityManager = $this->getDoctrine()->getManager();
            $producto->setEnabled($enabled);
            $entityManager->persist($producto);
            $entityManager->flush();
            $this->addFlash('msgOk', 'Se ha modificado el estado del producto con el código ' . $producto->getCodigo());
        }
        return $this->redirect($this->generateUrl('producto_index'));
    }

    /**
     * @Route("/operator/producto/imprimirCodigo/{id}", name="producto_imprimir_codigo", methods={"GET"})
     *
     */
    public function imprimirCodigoAction(Producto $producto, CotizacionService $cotizacionService) {

        if (!$producto) {
            $this->addFlash('msgOk', 'No se ha encontrado el producto seleccionado.');
            return $this->redirect($this->generateUrl('producto_index'));
        }
        return $this->render('producto/imprimirCodigo.html.twig', [
                    'producto' => $producto,
                    'cotizacion' => $cotizacionService->getLastCotizacion()
        ]);
    }

    /**
     * @Route("/operator/producto/imprimir/etiquetas", name="producto_etiquetas", methods={"GET"})
     *
     */
    public function etiquetas() {      
        return $this->render('producto/etiqueta.html.twig');
    }

    /*
     * Alta de Imagenes
     */

    private function altaImagenes($request, $producto, ResizeService $resize) {
        try {
            $uploaddir = $this->getParameter('root_dir_images') . $producto->getId() . '/';
            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0777, true);
            }

            $em = $this->getDoctrine()->getManager();
            $ordenes = $request->request->get('pos');

            foreach ($ordenes as $key => $orden) {

                $filesize = $_FILES['archivo']['size'][$key];
                if ($filesize > 0) {
                    $filename = trim($_FILES['archivo']['name'][$key]);

                    $uploadfile = $uploaddir . $filename;

                    $tmp_name = $_FILES['archivo']["tmp_name"][$key];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $resize->image_resize(600, 600, $producto->getId() . '/' . $filename);

                        $imagenproducto = new ImagenProducto();
                        if ($orden != '' && $orden >= 0 && $orden <= 1000) {
                            $imagenproducto->setPos($orden);
                        }
                        $imagenproducto->setPath($filename);
                        $imagenproducto->setProducto($producto);
                        $em->persist($imagenproducto);
                        $em->flush();
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @Route("/operator/producto/{id}/editStock", name="producto_stock_edit", methods={"GET","POST"})
     */
    public function editStock(Producto $producto, Request $request): Response {
        if ($producto) {
            $form = $this->createForm(ProductoStockType::class, $producto);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->flush();

                $this->addFlash("msgOk", "Se editó correctamente el stock del producto.");

                return $this->redirectToRoute('producto_index');
            }
            if ($form->isSubmitted() && !$form->isValid()) {
                $this->addFlash("msgError", "No se ha podido editar el stock del producto.");
            }

            return $this->render('producto/editStock.html.twig', [
                        'producto' => $producto,
                        'form' => $form->createView(),
            ]);
        }

        return $this->redirectToRoute('producto_index');
    }

    /**
     * @Route("/admin/producto/{id}/delete", name="producto_delete", methods={"GET","POST"})
     */
    public function delete(Producto $producto, ImagenProductoRepository $imagenProductoRepository, AtributosProductoRepository $atributoProductoRepository, ProducotsLineaProductoRepository $ProductosLineaProductoRepository, ProductoGrupoRepository $productoGrupoRepository): Response {
        if ($producto) {
            $entityManager = $this->getDoctrine()->getManager();
            $imagenes = $imagenProductoRepository->findBy(['producto' => $producto]);

            $codigo = $producto->getCodigo();

            if (sizeof($imagenes) == 0) {

                $atributosProducto = $atributoProductoRepository->findBy(['producto' => $producto]);
                foreach ($atributosProducto as $atributoProducto) {
                    $entityManager->remove($atributoProducto);
                }

                $productoLineasProducto = $ProductosLineaProductoRepository->findBy(['producto' => $producto]);
                foreach ($productoLineasProducto as $productoLineaProducto) {
                    $entityManager->remove($productoLineaProducto);
                }

                $productsoGrupoProducto = $productoGrupoRepository->findBy(['producto' => $producto]);
                foreach ($productsoGrupoProducto as $productoGrupo) {
                    $entityManager->remove($productoGrupo);
                }

                $entityManager->remove($producto);
                $entityManager->flush();
                $this->addFlash("msgOk", "Se ha eliminado el producto " . $codigo);
            } else {
                $this->addFlash("msgError", "No ha sido posible eliminar el producto " . $codigo . " posiblemente tenga imágenes cargadas.");
            }
            return $this->redirectToRoute('producto_index', ['finder' => $producto->getCodigo()]);
        }
    }

    /**
     * @Route("/producto/countFilter", name="producto_count_filter", methods={"GET","POST"})
     */
    public function countFilter(Request $request): Response {

        $periodo = $request->get('period');
        $rango = $request->get('rango');
        if ($rango != '') {
            if (strpos($rango, '1') !== false) {
                $filtros['fromPrecio'] = 0;
                $filtros['toPrecio'] = intval($periodo);
            }
            if (strpos($rango, '2') !== false) {

                $filtros['fromPrecio'] = intval($periodo);
                $filtros['toPrecio'] = intval($periodo) * 2;
            }
            if (strpos($rango, '3') !== false) {
                $filtros['fromPrecio'] = intval($periodo) * 2;
                $filtros['toPrecio'] = intval($periodo) * 3;
                ;
            }
            if (strpos($rango, '4') !== false) {
                $filtros['fromPrecio'] = intval($periodo) * 3;
                $filtros['toPrecio'] = null;
            }
        }

        $type = $request->get('type');
        if ($type == 'categoria') {
            $filtros['categoria'] = $request->get('element');
            $filtros['subcategoria'] = $request->get('idSubcategoria');
        }

        if ($type == 'lineaProducto') {
            $filtros['lineaProducto'] = $request->get('element');
        }

        if ($this->isGranted('ROLE_USER_MAYORISTA')) {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB;
        } else {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;
        }

        $cuenta = $this->productoRepository->getCountProductos($filtros);

        return new Response($cuenta);
    }

}
