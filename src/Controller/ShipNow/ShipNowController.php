<?php

namespace App\Controller\ShipNow;

use App\Service\ShipNow\ShipNowService;
use App\Repository\ShipNow\ShipNowRepository;
use App\Entity\Tienda\Pedido;
use App\Entity\ShipNow\ShipNow;
use App\Repository\PedidoRepository;
use App\Service\General\GeoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\Tienda\DetallePedidoRepository;

/**
 * @Route("/shipNow")
 */
class ShipNowController extends AbstractController {

    private $shipNowService;
    private $shipNowRepository;
    private $pedidoRepository;
    private $geoService;

    function __construct(
            ShipNowService $shipNowService,
            GeoService $geoService,
            ShipNowRepository $shipNowRepository,
            PedidoRepository $pedidoRepository) {
        $this->shipNowRepository = $shipNowRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->shipNowService = $shipNowService;
        $this->geoService = $geoService;
    }

    /**
     * @Route("/{zipCode}/simplify_options_price", name="shipnow_simplify_options_price", methods={"GET"})
     */
    public function simplifyOptionsPrice(Request $request, $zipCode): Response {
        $data['zipCode'] = $zipCode;
        $grabado = $request->get('grabado');
        return new Response(json_encode($this->shipNowService->simplifyOptionsPrice($data, $grabado)));
    }
    
    /**
     * @Route("/{zipCode}/{id}/findShipNowOptionById", name="shipnow_find_ship_now_option_by_id", methods={"GET"})
     */
    public function findShipNowOptionById(Request $request, $zipCode,$id): Response {
        $data['zipCode'] = $zipCode;
        $grabado = $request->get('grabado');
        return JsonResponse::create($this->shipNowService->findShipNowOptionById($data, $grabado,$id));        
    }
    

    /**
     * @Route("/verificarEnvios", name="shipnow_verificar_envios")
     * 
     */
    public function verificarEnvios(Request $request) {
        $token = $request->get('token');
        if ($token != $this->getParameter('secret')) {
            return false;
        }
        $em = $this->getDoctrine()->getManager();

        $pedidos = $this->pedidoRepository->findByPedidosPendientesEntrega();

        $respuesta['cantidadPendientes'] = sizeof($pedidos);
        $respuesta['cantidadProcesados'] = 0;
        $respuesta['cantidadEntregados'] = 0;
        $respuesta['cantidadNoEntregados'] = 0;
        $respuesta['cantidadError'] = 0;

        foreach ($pedidos as $pedido) {

            if (sizeof($pedido->getShipsNow()) > 0) {

                $shipNow = $pedido->getShipsNow()[0]; //Obtengo la última respuesta
                $respuestaAnterior = json_decode($shipNow->getRespuesta());
                $respuestaActual = $this->shipNowService->getOrder($respuestaAnterior->id);
                $this->shipNowService->new($pedido, json_encode($respuestaActual));

                if (!property_exists($respuestaActual, 'errors')) {
                    $respuesta['cantidadProcesados']++;
                    if ($respuestaActual->status == ShipNow::STATUS_DELIVERED) {
                        $respuesta['cantidadEntregados']++;
                        $pedido->setEstado(Pedido::ENTREGADO);
                        $em->persist($pedido);
                    }
                    if (in_array($respuestaActual->status,[ShipNow::STATUS_CANCELLED,ShipNow::STATUS_NOT_DELIVERED,ShipNow::STATUS_RETURN])) {
                        $respuesta['cantidadNoEntregados']++;
                        $pedido->setEstado(Pedido::NO_ENTREGADO);
                        $em->persist($pedido);
                    }
                    
                } else {
                    $respuesta['cantidadError']++;
                }
            }
        }

        $em->flush();
        return new JsonResponse($respuesta);
    }

    
    /**
     * @Route("/test", name="shipnow_test")
     * 
     */
    public function test(Request $request, DetallePedidoRepository $detallePedidoRepository) {
        
        $detallePedido= $detallePedidoRepository->find(1);
      
        //$this->shipNowService->createVariant($detallePedido);
        $this->shipNowService->updateVariant(2454922,$detallePedido);
        
        $this->shipNowService->getVariants('BR755_');
        die;
        
    }
    
    
    /**
     * @Route("/getShippingNowServiceById/{id}", name="shipnow_get_shipping_now_service_by_id")
     */
    public function getShippingNowServiceById($id){
        return JsonResponse::create($this->shipNowService->getShippingService($id));
    }
        

    
}
