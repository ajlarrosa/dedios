<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Mailing;
use App\Form\Tienda\MailingType;
use App\Repository\Tienda\MailingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/")
 */
class MailingController extends AbstractController
{

    /**
     * @Route("/admin/mailing", name="tienda_mailing_index", methods={"GET"})
     */
    public function index(Request $request, MailingRepository $mailingRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response
    {
        
        $filtros = $request->query->all();
        
        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $mailings = $mailingRepository->filter($filtros)->getQuery()->getResult();

            $spreadSheet = $this->excelInforme($mailings, $filtros);

            $nombreInforme = 'Listado de subscriptos al mailing';
            
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }
        
        $mailings = $paginator->paginate(
                $mailingRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        
        return $this->render('tienda/mailing/index.html.twig', [
                    'mailings' => $mailings
        ]);
    }
    
    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($mailings, $parametros) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];

        $fila = 1;

        $sheet->setCellValue('A' . $fila, 'Id');
        $sheet->setCellValue('B' . $fila, 'Usuario');
        $sheet->setCellValue('C' . $fila, 'Cliente Web');
        $sheet->setCellValue('D' . $fila, 'Nombre');
        $sheet->setCellValue('E' . $fila, 'Mail');
        $sheet->getStyle('A' . $fila . ':E' . $fila)->applyFromArray($styleArray2);

        $fila++;

        foreach ($mailings as $mailing) {
            if($mailing->getUser() != null){
                $entity =  $mailing->getUser();
                $mail = $entity->getMail();
            }else{
                $entity = $mailing->getClienteWeb();
                $mail = $entity->getEmail();
            }
            
            $sheet->setCellValue('A' . $fila, $mailing->getId());
            $sheet->setCellValue('B' . $fila, $mailing->getUser());
            $sheet->setCellValue('C' . $fila, $mailing->getClienteWeb());
            $sheet->setCellValue('D' . $fila, $entity->getNombre().' '.$entity->getApellido());
            $sheet->setCellValue('E' . $fila, $mail);
            $fila++;
        }

        $fila++;
                

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

        return $spreadsheet;
    }
    
    
    /**
     * @Route("/admin/mailing/new", name="tienda_mailing_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $mailing = new Mailing();
        $form = $this->createForm(MailingType::class, $mailing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mailing);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_mailing_index');
        }

        return $this->render('tienda/mailing/new.html.twig', [
            'mailing' => $mailing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/mailing/{id}", name="tienda_mailing_show", methods={"GET"})
     */
    public function show(Mailing $mailing): Response
    {
        return $this->render('tienda/mailing/show.html.twig', [
            'mailing' => $mailing,
        ]);
    }

    /**
     * @Route("/admin/mailing/{id}/edit", name="tienda_mailing_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mailing $mailing): Response
    {
        $form = $this->createForm(MailingType::class, $mailing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_mailing_index');
        }

        return $this->render('tienda/mailing/edit.html.twig', [
            'mailing' => $mailing,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/mailing/{id}", name="tienda_mailing_delete", methods={"POST"})
     */
    public function delete(Request $request, Mailing $mailing): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mailing->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mailing);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_mailing_index');
    }
    
    
    /**
     * @Route("admin/mailing/activate/{id}", name="tienda_mailing_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Mailing $mailing, TranslatorInterface $translator) {
        if (!$mailing) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the  mailing.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $mailing->setEnabled($activo);
            $entityManager->persist($mailing);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Mailing status has been changed.'));
        }
        return $this->redirect($this->generateUrl('tienda_mailing_index'));
    }
    
    /**
     * @Route("/subscribe", name="tienda_mailing_subscribe", methods={"GET","POST"})
     *
     */
    public function subscribeAction(Request $request, TranslatorInterface $translator) {
        $user = $this->getUser();
        
        if (!$user) {
            $this->addFlash('msgError', $translator->trans('It was not possible to subscribe to mailing list.'));
        } else {
            $entityManager = $this->getDoctrine()->getManager();
            
            $mailingPrevio = $entityManager->getRepository(Mailing::class)->buscarMail($user->getMail());
             
            if(isset($mailingPrevio[0])){
                if($mailingPrevio[0]->getUser()==null){
                    if(!$mailingPrevio[0]->getEnabled()){
                        $mailingPrevio[0]->setEnabled(true);
                    }
                    $mailingPrevio[0]->setClienteWeb(null);
                    $mailingPrevio[0]->setUser($user);
                    $entityManager->persist($mailingPrevio[0]);
                }
            }else{    
                $mailing = new Mailing();
                $mailing->setUser($user);
                $entityManager->persist($mailing);
            }
            
            $entityManager->flush();
            
            $this->addFlash('msgOk', $translator->trans('Successful subscription'));
        }
        
        return $this->redirectToRoute('home');
    }
    
}
