<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\DireccionEnvio;
use App\Entity\Tienda\Pedido;
use App\Service\Tienda\DireccionEnvioService;
use App\Form\Tienda\DireccionEnvioType;
use App\Service\General\GeoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/direccionEnvio/")
 */
class DireccionEnvioController extends AbstractController {

    private $translator;
    private $geoService;
    private $direccionEnvioService;

    function __construct(TranslatorInterface $translator, GeoService $geoService, DireccionEnvioService $direccionEnvioService) {
        $this->translator = $translator;
        $this->geoService = $geoService;
        $this->direccionEnvioService = $direccionEnvioService;
    }

    /**
     * @Route("{id}/edit", name="direccion_envio_edit", methods={"POST"})
     */
    public function edit(Request $request, DireccionEnvio $direccionEnvio): Response {
        if ($direccionEnvio) {

            $form = $this->createForm(DireccionEnvioType::class, $direccionEnvio);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $direccionEnvio = $this->direccionEnvioService->setData($request,'edit', $direccionEnvio);                

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($direccionEnvio);
                $entityManager->flush();

                $this->addFlash('msgOk', $this->translator->trans('Address edited successfully.'));
            }

            if ($form->isSubmitted() && !$form->isValid()) {
                $this->addFlash('msgError', $this->translator->trans('It was not possible to edit the address.'));
            }

            return $this->redirectToRoute('tienda_pedido_checkout');
        }

        $this->addFlash('msgError', $this->translator->trans('There is no address'));
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("new", name="direccion_envio_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $direccionEnvio = new DireccionEnvio();
        $form = $this->createForm(DireccionEnvioType::class, $direccionEnvio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {            
            $direccionEnvio = $this->direccionEnvioService->setData($request,'create', $direccionEnvio);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($direccionEnvio);
            $entityManager->flush();
            $this->addFlash("msgOk", $this->translator->trans('A new address has been added.'));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash("msgError", $this->translator->trans('Address could not be added.'));
        }

        return $this->redirectToRoute('tienda_pedido_checkout');
    }

    /**
     * @Route("zipCode", name="direccion_envio_cp", methods={"GET"})
     */
    public function zipCode(DireccionEnvioService $direccionEnvioService): JsonResponse {
        return new JsonResponse($direccionEnvioService->getZipCode());
    }

    /**
     * @Route("{id}/getForm", name="direccion_envio_get_form", methods={"GET"})
     */
    public function getForm(Request $request, Pedido $pedido): Response {
        $tipoDireccionEnvio = $request->query->get('type', null);

        if (is_null($tipoDireccionEnvio)) {
            $direccionEnvio = new DireccionEnvio();

            is_object($pedido->getUser()) ? $direccionEnvio->setUser($pedido->getUser()) : $direccionEnvio->setClienteWeb($pedido->getClienteWeb());

            return new Response($this->direccionEnvioService->renderFormAlta($direccionEnvio));
        }

        $direccionEnvio = $pedido->getDireccionEnvio();

        if ($tipoDireccionEnvio === DireccionEnvio::DIRECCION_FACTURACION && $pedido->getDireccionFacturacionActiva()->getTipoDireccion() !== DireccionEnvio::DIRECCION_FACTURACION) {
            $direccionFacturacion = new DireccionEnvio();

            is_null($direccionEnvio->getClienteWeb()) ? $direccionFacturacion->setUser($direccionEnvio->getUser()) : $direccionFacturacion->setClienteWeb($direccionEnvio->getClienteWeb());

            $direccionFacturacion->setCalle($direccionEnvio->getCalle());
            $direccionFacturacion->setCalleId($direccionEnvio->getCalleId());
            $direccionFacturacion->setCalleNumero($direccionEnvio->getCalleNumero());
            $direccionFacturacion->setDepartamento($direccionEnvio->getDepartamento());

            if (is_int($direccionEnvio->getDepartamentoId())) {
                $direccionFacturacion->setDepartamentoId($direccionEnvio->getDepartamentoId());
            }

            $direccionFacturacion->setLocalidad($direccionEnvio->getLocalidad());
            $direccionFacturacion->setLocalidadId($direccionEnvio->getLocalidadId());
            $direccionFacturacion->setProvincia($direccionEnvio->getProvincia());
            $direccionFacturacion->setProvinciaId($direccionEnvio->getProvinciaId());
            $direccionFacturacion->setUnidadFuncional($direccionEnvio->getUnidadFuncional());
            $direccionFacturacion->setPiso($direccionEnvio->getPiso());
            $direccionFacturacion->setZipCode($direccionEnvio->getZipCode());
            $direccionFacturacion->setTipoDireccion(DireccionEnvio::DIRECCION_FACTURACION);

            $pedido->setDireccionFacturacion($direccionFacturacion);

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($direccionFacturacion);
            $entityManager->persist($pedido);

            $entityManager->flush();
        }

        $entity = ($tipoDireccionEnvio === DireccionEnvio::DIRECCION_FACTURACION) ? $pedido->getDireccionFacturacionActiva() : $direccionEnvio;

        return new Response($this->direccionEnvioService->renderFormEdit($entity));
    }

}
