<?php

namespace App\Controller\Tienda;

use App\Entity\Producto;
use App\Entity\Categoria;
use App\Entity\Subcategoria;
use App\Entity\Categoriasubcategoria;
use App\Entity\ListaPrecio;
use App\Entity\Tienda\LineaProducto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\ProductoService;
use App\Service\General\GeneralService;
use App\Service\PrecioService;
use App\Service\Tienda\MercadoPagoService;
use App\Service\General\SessionService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\PrecioRepository;
use App\Repository\ProductoRepository;

/**
 * @Route("/producto/")
 */
class ProductoController extends AbstractController {

    private $precioService;
    private $mercadoPagoService;
    private $productoService;
    private $precioRepository;
    private $productoRepository;
    private $generalService;
    private $router;
    private $sessionService;

    function __construct(
            SessionService $sessionService,
            GeneralService $generalService,
            ProductoService $productoService,
            MercadoPagoService $mercadoPagoService,
            UrlGeneratorInterface $router,
            PrecioRepository $precioRepository,
            ProductoRepository $productoRepository,
            PrecioService $precioService
    ) {
        $this->router = $router;
        $this->precioRepository = $precioRepository;
        $this->productoRepository = $productoRepository;
        $this->precioService = $precioService;
        $this->productoService = $productoService;
        $this->generalService = $generalService;
        $this->sessionService = $sessionService;
        $this->mercadoPagoService = $mercadoPagoService;
    }

    /**
     * @Route("{categoria}-{subcategoria}/{id}/{descripcion}", name="producto_tienda_show")
     */
    public function show(Producto $producto, PaginatorInterface $paginator): Response {

        $filtros['subcategoria'] = $producto->getCategoriasubcategoria()->getSubcategoria();
        $filtros['categoria'] = $producto->getCategoriasubcategoria()->getCategoria();

        $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;
        if ($this->isGranted('ROLE_USER_MAYORISTA')) {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB;
        }
        
        $relacionados = $paginator->paginate(
                $this->productoRepository->getProductos($filtros),
                1,
                20
        );
        
        $paymentMethods =$this->mercadoPagoService->getPaymentMethods();        
        return $this->render('tienda/producto/producto.html.twig', [
                    'fb' => 4,
                    'producto' => $producto,
                    'relacionados' => $relacionados,
                    'paymentMethods'=> $paymentMethods
        ]);
    }

    /**
     * @Route("search", name="producto_tienda_search")
     */
    public function search(Request $request): Response {
      
        
        $search = $request->query->get('search');

        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository(Producto::class)->findOneBy(['enabled' => true, 'codigo' => $search]);

        if ($producto) {
            return $this->redirectToRoute('producto_tienda_show', [
                        'id' => $producto->getId(),
                        'descripcion' => $this->generalService->slugify($producto->getDescripcion()),
                        'categoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getCategoria()->getDescripcion()),
                        'subcategoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion())
            ]);
        }

        //Se debe obtener según el tipo de usuario 
        $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;
        if ($this->isGranted('ROLE_USER_MAYORISTA')) {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB;
        }

        $lineaproducto = $em->getRepository(LineaProducto::class)->search($search);

        if (isset($lineaproducto[0]) && !is_null($lineaproducto[0])) {
            $filtros['lineaProducto'] = $lineaproducto[0]->getId();

            $cuenta = $this->productoRepository->getCountProductos($filtros);

            if ($cuenta > 0) {
                return $this->redirectToRoute('linea_producto_tienda_index', [
                            'id' => $lineaproducto[0]->getId(),
                            'descripcion' => $this->generalService->slugify($lineaproducto[0]->getDescripcion())
                ]);
            }
        }

        $categoria = $em->getRepository(Categoria::class)->search($search);

        if (isset($categoria[0]) && !is_null($categoria[0])) {
            $filtros['categoria'] = $categoria[0]->getId();

            $cuenta = $this->productoRepository->getCountProductos($filtros);

            if ($cuenta > 0) {
                return $this->redirectToRoute('categoria_tienda_index', [
                            'id' => $categoria[0]->getId(),
                            'category' => $categoria[0]->getDescripcion()
                ]);
            }
        }

        $subcategoria = $em->getRepository(Subcategoria::class)->search($search);

        if ($subcategoria) {
            $categoriasSubcategoria = $em->getRepository(Categoriasubcategoria::class)->findBy(['activo' => true, 'subcategoria' => $subcategoria]);

            if ($categoriasSubcategoria) {
                if (sizeof($categoriasSubcategoria) == 1) {

                    $categoria = $categoriasSubcategoria[0]->getCategoria();
                    $subcategoria = $categoriasSubcategoria[0]->getSubcategoria();

                    $filtros['categoria'] = $categoria->getId();
                    $filtros['subcategoria'] = $subcategoria->getId();

                    $cuenta = $this->productoRepository->getCountProductos($filtros);

                    if ($cuenta > 0) {
                        return $this->redirectToRoute('subcategoria_tienda_index', [
                                    'id' => $subcategoria->getId(),
                                    'category_id' => $categoria->getId(),
                                    'category' => $this->generalService->slugify($categoria->getDescripcion()),
                                    'subcategory' => $this->generalService->slugify($subcategoria->getDescripcion())
                        ]);
                    }
                } else {
                    return $this->redirectToRoute('producto_tienda_index', [
                                'search' => $search,
                                'mobile' => $request->get('mobile'),
                                'subcategoria' => $subcategoria[0]->getId()
                    ]);
                }
            }
        }
        return $this->redirectToRoute('producto_tienda_index', [
                    'search' => $search,  
                    'fb' => 2,
                    'mobile' => $request->get('mobile')
        ]);
    }

    /**
     * @Route("busqueda", name="producto_tienda_index", methods={"GET","POST"})
     */
    public function indexTienda(Request $request): Response {
        $filtros =$request->query->all();
                
        $search = $request->get('search');      
        
        if ($request->get('subcategoria') != '' && is_numeric($request->get('subcategoria'))) {
            $filtros['subcategoria'] = $request->get('subcategoria');
        } else {
            $filtros['palabrasClave'] = explode(' ', trim($search));
        }

        /* Se debe obtener según el tipo de usuario */
        if ($this->isGranted('ROLE_USER_MAYORISTA')) {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB;
        } else {
            $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;
        }

        //Obtengo Periodo
        $cuenta = $this->productoRepository->getCountProductos($filtros);

        if ($cuenta == 0) {
            $this->addFlash("msgError", "No se encontró ningún producto, categoría, subcategoría ni línea de productos con el criterio de búsqueda ingresado.");
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
      
        return $this->render('tienda/categoria/categoria.html.twig', [
                    'element' => null,
                    'cuenta' => $cuenta,
                    'cantShow' => 16,
                    'productos' => $this->productoRepository->getProductos($filtros),
                    'search' => $search,
                    'period' => $this->precioService->getPeriod($filtros)
        ]);
    }

}
