<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Pedido;
use App\Entity\Tienda\Cupon;
use App\Entity\Tienda\DireccionEnvio;
use App\Entity\Tienda\ClienteWeb;
use App\Entity\Tienda\Mailing;
use App\Entity\User;
use App\Entity\ListaPrecio;
use App\Entity\Tienda\FormaPago;
use App\Entity\Tienda\DetallePedido;
use App\Entity\Producto;
use App\Repository\ProductoRepository;
use App\Repository\Tienda\FormaPagoRepository;
use App\Repository\ClienteRepository;
use App\Repository\ColorRepository;
use App\Repository\Tienda\DireccionEnvioRepository;
use App\Repository\LetraRepository;
use App\Repository\MedidaRepository;
use App\Repository\Tienda\DetallePedidoRepository;
use App\Form\Tienda\PedidoType;
use App\Form\Tienda\ClienteWebType;
use App\Service\Tienda\PedidoService;
use App\Service\Tienda\ClienteWebService;
use App\Service\Tienda\CuponService;
use App\Service\ShipNow\ShipNowService;
use App\Service\Tienda\DireccionEnvioService;
use App\Service\General\GeoService;
use App\Service\PrecioService;
use App\Service\ListaPrecioService;
use App\Service\Tienda\PromotionService;
use App\Repository\Tienda\PedidoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\Tienda\MobileDetectService;
use Symfony\Component\Security\Core\Security;
use App\Service\ImagenProductoService;
use Symfony\Component\HttpFoundation\Cookie;
use App\Service\General\GeneralService;
use App\Service\General\SessionService;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 */
class PedidoController extends AbstractController {

    private $clienteRepository;
    private $productoRepository;
    private $pedidoRepository;
    private $colorRepository;
    private $letraRepository;
    private $formaPagoRepository;
    private $detallePedidoRepository;
    private $translator;
    private $pedidoService;
    private $shipNowService;
    private $direccionEnvioService;
    private $precioService;
    private $listaPrecioService;
    private $geoService;
    private $security;
    private $clienteWebService;
    private $imagenProductoService;
    private $generalService;
    private $mobileDetectService;
    private $sessionService;
    private $cuponService;

    function __construct(
            GeneralService $generalService,
            ClienteRepository $clienteRepository,
            ProductoRepository $productoRepository,
            DetallePedidoRepository $detallePedidoRepository,
            PedidoRepository $pedidoRepository,
            FormaPagoRepository $formaPagoRepository,
            ColorRepository $colorRepository,
            LetraRepository $letraRepository,
            MedidaRepository $medidaRepository,
            TranslatorInterface $translator,
            PedidoService $pedidoService,
            PrecioService $precioService,
            ListaPrecioService $listaPrecioService,
            ClienteWebService $clienteWebService,
            GeoService $geoService,
            ShipNowService $shipNowService,
            Security $security,
            ImagenProductoService $imagenProductoService,
            DireccionEnvioService $direccionEnvioService,
            MobileDetectService $mobileDetectService,
            SessionService $sessionService,
            CuponService $cuponService
    ) {
        $this->clienteRepository = $clienteRepository;
        $this->productoRepository = $productoRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->formaPagoRepository = $formaPagoRepository;
        $this->colorRepository = $colorRepository;
        $this->letraRepository = $letraRepository;
        $this->medidaRepository = $medidaRepository;
        $this->detallePedidoRepository = $detallePedidoRepository;
        $this->translator = $translator;
        $this->pedidoService = $pedidoService;
        $this->geoService = $geoService;
        $this->shipNowService = $shipNowService;
        $this->direccionEnvioService = $direccionEnvioService;
        $this->precioService = $precioService;
        $this->clienteWebService = $clienteWebService;
        $this->listaPrecioService = $listaPrecioService;
        $this->security = $security;
        $this->imagenProductoService = $imagenProductoService;
        $this->generalService = $generalService;
        $this->mobileDetectService = $mobileDetectService;
        $this->sessionService = $sessionService;
        $this->cuponService = $cuponService;
    }

    /**
     * @Route("pedido/{id}/add", name="tienda_pedido_add", methods={"GET","POST"})
     */
    public function addPedido(Request $request, Producto $producto): Response {

        if (!$producto) {
            $this->addFlash('msgError', $this->translator->trans('The selected product is not in our store'));
            return $this->redirectToRoute('home');
        }

        //Color
        $colorId = $request->get('color');
        $color = null;

        if (isset($colorId) && $colorId != '') {
            $color = $this->colorRepository->find($colorId);
            if (!$color) {
                $this->addFlash('msgError', $this->translator->trans('The selected color is not available'));
                return $this->redirectToRoute('home');
            }
        }

        //Letra
        $letraId = $request->get('letra');
        $letra = null;
        if (isset($letraId) && $letraId != '') {
            $letra = $this->letraRepository->find($letraId);
            if (!$letra) {
                $this->addFlash('msgError', $this->translator->trans('The selected letter is not available'));
                return $this->redirectToRoute('home');
            }
        }

        //Medida
        $medidaId = $request->get('medida');
        $medida = null;
        if (isset($medidaId) && $medidaId != '') {
            $medida = $this->medidaRepository->find($medidaId);
            if (!$medida) {
                $this->addFlash('msgError', $this->translator->trans('The selected measure is not available'));
                return $this->redirectToRoute('home');
            }
        }

        //Cantidad
        $cantidad = intval($request->get('cantidad'));
        if ($cantidad <= 0) {
            $this->addFlash('error', $this->translator->trans('The quantity is not valid'));
            return $this->redirectToRoute('home');
        }

        //Grabado
        $product['grabado'] = $request->get('grabado');
        $product['producto'] = $producto;
        $product['cantidad'] = $cantidad;
        $product['color'] = $color;
        $product['letra'] = $letra;
        $product['medida'] = $medida;

        $response = $this->redirectToRoute('producto_tienda_show', [
            'categoria' => $producto->getCategoriasubcategoria()->getCategoria()->getDescripcion(),
            'subcategoria' => $producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion(),
            'id' => $producto->getId(),
            'descripcion' => $this->generalService->slugify($producto->getDescripcion())
        ]);

        //Para clientes Web no logueados
        if (!$this->security->isGranted("ROLE_USER_TIENDA")) {
            $oldCookies = $request->cookies;
            $cookies = $this->clienteWebService->addProduct($request, (object) $product);
            $error = false;

            if ($oldCookies->get('idPedido') !== null && $oldCookies->get('idPedido') != '') {
                $pedido = $this->pedidoRepository->find($oldCookies->get('idPedido'));
                $detallePedido = $this->pedidoService->addProductPedido((object) $product, $pedido);

                if ($detallePedido === false) {
                    $error = true;
                    $this->addFlash('msgError', $this->translator->trans('It was not possible to add the product to your bag'));
                } else {
                    $entityManager = $this->getDoctrine()->getManager();
                    $pedido->addDetallespedido($detallePedido);
                    $entityManager->persist($detallePedido);
                    $entityManager->persist($pedido);
                    $entityManager->flush();
                }
            }

            if (!$error) {
                $response->headers->setCookie($cookies['detallePedido']);
                $response->headers->setCookie($cookies['cantidadProductos']);
                $this->addFlash('msgOk', $this->translator->trans('The product has been added to your bag'));
            }
        } else {
            $detallePedido = $this->pedidoService->addProduct((object) $product);

            if ($detallePedido === false) {
                $this->addFlash('msgError', $this->translator->trans('It was not possible to add the product to your bag'));
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $pedido = $this->pedidoService->getPedidoPendiente();
                $pedido->addDetallespedido($detallePedido);
                $entityManager->persist($pedido);
                $entityManager->flush();
                $this->addFlash('msgOk', $this->translator->trans('The product has been added to your bag'));
            }
        }

        return $response;
    }

    /**
     * @Route("pedido/{id}/remove", name="tienda_pedido_remove", methods={"GET","POST"})
     */
    public function removePedido(Request $request, DetallePedido $detallePedido): Response {

        $btu = $request->get('btu');
        $pedido = $detallePedido->getPedido();
        if ($btu != null && $btu == $pedido->getHash()) {
            if (!$detallePedido) {
                $this->addFlash('msgError', $this->translator->trans('The selected item is not in yout cart'));
            }

            $producto = $detallePedido->getProductodesc();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($detallePedido);
            $entityManager->flush();

            $response = $this->redirectToRoute('tienda_pedido_checkout');

            if (!$this->security->isGranted("ROLE_USER_TIENDA")) {
                $cookies['detallePedido'] = $this->clienteWebService->getDetallePedido($request);
                $idx = 0;
                foreach ($cookies['detallePedido'] as $detalle) {
                    if ($detalle->producto == $detallePedido->getProducto()->getId() &&
                            $detalle->cantidad == $detallePedido->getCantidad()) {

                        unset($cookies['detallePedido'][$idx]);
                    }
                    $idx++;
                }

                $response->headers->setCookie(new Cookie("detallePedido", json_encode($cookies['detallePedido']), time() + 86400, '/', null, false, false));
            }

            if (sizeof($pedido->getDetallespedido()) == 0) {
                return $this->redirectToRoute('tienda_no_producto_in cart');
            }

            $this->addFlash('msgOk', $this->translator->trans('The product {{ product }} has been removed from your cart', ['{{ product }}' => $producto]));

            return $response;
        } else {
            $this->addFlash("msgError", "You are not authorized to view this order");
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("pedido/{id}/modify", name="tienda_pedido_modify", methods={"GET","POST"})
     */
    public function modifyPedido(Request $request, DetallePedido $detallePedido): Response {

        if (!$detallePedido) {
            $this->addFlash('msgError', $this->translator->trans('The selected item is not in yout cart'));
        }

        $cantidad = intval($request->get('cantidad'));

        if ($cantidad <= 0) {
            $this->addFlash('msgError', $this->translator->trans('The quantity must be greater then 0'));
        }

        $detallePedido->setCantidad($cantidad);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($detallePedido);
        $entityManager->flush();

        $this->addFlash('msgOk', $this->translator->trans('Product {{ product }} quantity has been updated ', ['{{ product }}' => $detallePedido->getProductodesc()]));

        return $this->redirectToRoute('tienda_pedido_checkout');
    }

    /**
     * @Route("pedido/confirm", name="tienda_pedido_confirm", methods={"GET","POST"})
     */
    public function confirmPedido(
            Request $request,
            PromotionService $promotionService
    ): Response {

        $formaPago = $this->formaPagoRepository->find($request->get('formaPago')[0]);
        $entityManager = $this->getDoctrine()->getManager();

        $pedido = $this->pedidoRepository->find($request->get('idPedido'));

        $promotions = $promotionService->getActivePedidoPromotion([], $pedido);

        $descuentoPromociones = 0;
        $descripcionPromocion = '';

        $listaPrecioId = ($this->security->isGranted("ROLE_USER_MAYORISTA")) ? ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB : ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;

        foreach ($pedido->getDetallespedido() as $detallePedido) {
            $detallePedido->setPrecio($this->precioService->getPrecioMedidaFinal($detallePedido->getProducto(), $detallePedido->getMedida(), $listaPrecioId));

            $grabado = $detallePedido->getGrabado();

            if (isset($grabado) && trim($grabado) != '') {
                $detallePedido->setPrecioGrabado($this->listaPrecioService->getPrecioGrabado());
            }

            $entityManager->persist($detallePedido);
        }

        //Promo verification                        
        if (array_key_exists('productos', $promotions)) {

            foreach ($promotions['productos']['producto'] as $producto) {

                if ($producto['tipoDescuento'] === 'Descuento por Cantidad') {

                    $detallePedido = $this->detallePedidoRepository->find($producto['detallePedidoId']);

                    $calculoPorcentaje = round($detallePedido->getTotal() * $producto['porcentaje'] / 100);

                    if ($producto['cantidadMinima'] > 1) {
                        $flag = false;

                        if (count($producto['types']) > 0) {
                            
                            foreach ($producto['types'] as $value) {

                                if (array_key_exists($value['type'], $promotions['totales']) && array_key_exists($value['id'], $promotions['totales'][$value['type']])) {
                                    if ($promotions['totales'][$value['type']][$value['id']]['cantidad'] >= $producto['cantidadMinima'] 
                                            || $promotions['totales']['GRUPO'][$value['type']][$value['id']]['cantidad'] >= $producto['cantidadMinima']) {
                                        
                                        $descuentoPromociones += $calculoPorcentaje;

                                        if (!$flag && strpos($descripcionPromocion, "Descuento (" . $producto['porcentaje'] . "%)") === false) {
                                            $descripcionPromocion = "Descuento (" . $producto['porcentaje'] . "%)";
                                            $flag = true;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (!$flag && $promotions['totales']['TODOS']['cantidad'] >= $producto['cantidadMinima']) {
                            $descuentoPromociones += $calculoPorcentaje;
                            if (strpos($descripcionPromocion, "Descuento (" . $producto['porcentaje'] . "%)") === false) {
                                $descripcionPromocion = "Descuento (" . $producto['porcentaje'] . "%)";
                            }

                            $flag = true;
                        }
                    }
                } else {
                    if ($producto['tipoDescuento'] !== 'Envio Cero') {
                        $descuentoPromociones += $calculoPorcentaje;
                        $descripcionPromocion = "Descuento (" . $producto['porcentaje'] . "%)";
                    }
                }
            }
        }

        $descuentoPromociones += $promotions['montos']['2x1'] + $promotions['montos']['3x2'];

        //Descripcion promocion
        if ($promotions['montos']['2x1'] > 0) {
            if (strpos($descripcionPromocion, "2x1") === false) {
                if ($descripcionPromocion != '') {
                    $descripcionPromocion .= ' - ';
                }
                $descripcionPromocion .= '2x1';
            }
        }

        if ($promotions['montos']['3x2'] > 0) {
            if (strpos($descripcionPromocion, "3x2") === false) {
                if ($descripcionPromocion != '') {
                    $descripcionPromocion .= ' - ';
                }
                $descripcionPromocion .= '3x2';
            }
        }

        $pedido->setFormapago($formaPago);

        if ($formaPago) {
            $pedido->setDescuento($formaPago->getPorcentajeDescuento());
            $pedido->setRecargo($formaPago->getPorcentajeRecargo());
        }

        $pedido->setObservacion($request->get('observaciones'));

        $pedido->setFecha(new \DateTime());

        if (is_object($pedido->getCupon())) {
            switch ($pedido->getCupon()->getTipoDescuento()) {
                case Cupon::PORCENTAJE:
                    $descuentoPromociones += $pedido->getTotal() * $pedido->getCupon()->getDescuento() / 100;
                    break;
                case Cupon::MONTO:
                    $descuentoPromociones += $pedido->getCupon()->getMontoFijo();
                    break;
                case Cupon::ENVIO:
                    $descuentoPromociones += $pedido->getCostoEnvio() * $pedido->getCupon()->getDescuento() / 100 - $pedido->getCupon()->getMontoFijo();
                    break;
            }
            $descripcionPromocion .= ' CUPON ';
        }

        $pedido->setMontoPromocion($descuentoPromociones);

        $pedido->setDescripcionPromocion($descripcionPromocion);

        $pedido->setEstado(Pedido::ACTIVO);

        $pedido->setHash(hash("sha256", serialize($pedido)));

        if ($this->security->isGranted("ROLE_USER_TIENDA")) {
            $newPedido = $this->pedidoService->create($this->getUser());
            $entityManager->persist($newPedido);
        }

        $pedido->setCostoEnvio(floatval($request->get('shippingPrice', 0)));

        $this->mobileDetectService->setDevicePedido($pedido);

        $entityManager->flush();

        if ($pedido->getFormapago()->getId() == FormaPago::MERCADOPAGO_ID) {
            return $this->redirectToRoute('tienda_formapago_mercadopago', [
                        'id' => $pedido->getId()
            ]);
        }

        if ($this->pedidoService->sendEmailOrderConfirmation($pedido) === false) {
            $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
        }

        $message = ($pedido->getFormapago()->getId() == FormaPago::TRANSFERENCIA_ID) ? 'You confirmed your order. Please send us proof of payment to shop@dediosjoyas.com to continue with it.' : 'You confirmed your order. We will be communicating with you to complete the purchase.';
        $this->addFlash('msgOk', $this->translator->trans($message));

        $response = $this->redirectToRoute('gracias', [
            'currency' => $this->security->isGranted("ROLE_USER_MAYORISTA") ? 'USD' : 'ARG',
            'totalvalue' => $pedido->getTotal()
        ]);

        $cookies = $request->cookies;
        if ($cookies->has('idPedido')) {
            $response->headers->clearCookie('idPedido', '/', null);
            $response->headers->clearCookie('detallePedido', '/', null);
            $response->headers->clearCookie('cantidadProductos', '/', null);
        }

        return $response;
    }

    /**
     * @Route("tienda/pedido/mispedidos", name="tienda_pedido_mispedidos", methods={"GET","POST"})
     */
    public function misPedidos(): Response {

        return $this->render('tienda/pedido/mispedidos.html.twig', [
                    'pedidos' => $this->pedidoService->getMisPedidos()
        ]);
    }

    /**
     * @Route("pedido/{id}/show", name="tienda_pedido_show", methods={"GET"})
     */
    public function show(Request $request, Pedido $pedido): Response {
        $btu = $request->get('btu');
        if ($btu === $pedido->getHash()) {
            return $this->render('tienda/pedido/show.html.twig', [
                        'pedido' => $pedido,
                        'showpedido' => true
            ]);
        } else {
            $this->addFlash("msgError", "You are not authorized to view this order");
            return $this->redirectToRoute('home');
        }
    }

    /**
     * @Route("pedido/checkout", name="tienda_pedido_checkout", methods={"GET","POST"})
     */
    public function checkout(Request $request): Response {

        $errorMessage = $request->get('errorMessage', null);
        if (!is_null($errorMessage)) {
            $this->addFlash('msgError', $errorMessage);
        }

        $cookie = null;
        $formView = null;
        $shipsNow = [];
        $cookiesRecuperacion = [];

        $pedidoId = $request->get('id');
        $btu = $request->get('btu');

        if ($this->security->isGranted("ROLE_USER_TIENDA")) {
            $pedido = $this->pedidoService->getPedidoPendiente();

            if ($this->security->isGranted("ROLE_USER_MAYORISTA")) {
                $responseParameters = [
                    'form' => null,
                    'pedido' => $pedido
                ];
            } else {
                $entityManager = $this->getDoctrine()->getManager();

                $direccionEnvioActual = $pedido->getDireccionEnvio();

                if (!isset($direccionEnvioActual)) {
                    $pedido->setDireccionEnvio($this->direccionEnvioService->getBillingAddressByPedido($pedido->getId()));
                    $entityManager->persist($pedido);
                    $entityManager->flush();
                }

                $data['zipCode'] = $pedido->getDireccionEnvio()->getZipCode();
                $cookie = new Cookie("zipCode", $data['zipCode'], time() + 86400, '/', null, false, false);

                $grabado = $this->pedidoService->tieneGrabado($pedido->getId());

                $shipsNow = $this->shipNowService->simplifyOptionsPrice($data, $grabado);

                $responseParameters = [
                    'fb' => 3,
                    'form' => null,
                    'pedido' => $pedido,
                    'shipsNow' => $shipsNow
                ];
            }
        } else {
            $cookies = $request->cookies;

            if ($pedidoId != '' && $btu != '') {

                $pedido = $this->pedidoRepository->findOneBy(['id' => $pedidoId, 'hash' => $btu]);

                if ($pedido) {
                    $cookies->set('idPedido', $pedidoId);
                    $cookiesRecuperacion[] = new Cookie("idPedido", $pedidoId, time() + 86400, '/', null, false, false);
                    $cookies->set('zipCode', $pedido->getDireccionEnvio()->getZipCode());
                    $cookiesRecuperacion[] = new Cookie("zipCode", $pedido->getDireccionEnvio()->getZipCode(), time() + 86400, '/', null, false, false);
                    $idx = 0;

                    foreach ($pedido->getDetallesPedido() as $detPedido) {
                        $detallePedido[$idx]['id'] = $idx + 1;
                        $detallePedido[$idx]['producto'] = $detPedido->getProducto()->getId();
                        $detallePedido[$idx]['productodesc'] = $detPedido->getProducto()->getDescripcion();
                        $detallePedido[$idx]['grabado'] = $detPedido->getGrabado();
                        $detallePedido[$idx]['cantidad'] = $detPedido->getCantidad();
                        $detallePedido[$idx]['color'] = $detPedido->getColor() !== null ? $detPedido->getColor()->getId() : null;
                        $detallePedido[$idx]['colorDescripcion'] = $detPedido->getColor() !== null ? $detPedido->getColor()->getDescripcion() : null;
                        $detallePedido[$idx]['letra'] = $detPedido->getLetra() !== null ? $detPedido->getLetra()->getId() : null;
                        $detallePedido[$idx]['letraDescripcion'] = $detPedido->getLetra() !== null ? $detPedido->getLetra()->getDescripcion() : null;
                        $detallePedido[$idx]['medida'] = $detPedido->getMedida() !== null ? $detPedido->getMedida()->getId() : null;
                        $detallePedido[$idx]['medidaDescripcion'] = $detPedido->getMedida() !== null ? $detPedido->getMedida()->getDescripcion() : null;
                        $idx++;
                    }

                    $cookies->set('detallePedido', json_encode($detallePedido));
                    $cookiesRecuperacion[] = new Cookie("detallePedido", json_encode($detallePedido), time() + 86400, '/', null, false, false);
                    $cookies->set('cantidadProductos', sizeof($pedido->getDetallesPedido()));
                    $cookiesRecuperacion[] = new Cookie("cantidadProductos", sizeof($pedido->getDetallesPedido()), time() + 86400, '/', null, false, false);
                }
            }

            if ($cookies->has('idPedido')) {
                $pedido = $this->pedidoRepository->find($cookies->get('idPedido'));

                $data['zipCode'] = $pedido->getDireccionEnvio()->getZipCode();
                $grabado = $this->pedidoService->tieneGrabado($cookies->get('idPedido'));
                $shipsNow = $this->shipNowService->simplifyOptionsPrice($data, $grabado);

                $responseParameters['fb'] = 3;
            } else {

                $pedido = $this->clienteWebService->getDetallePedido($request);

                if (empty($pedido)) {
                    return $this->redirectToRoute('home');
                }

                foreach ($pedido as $detPedido) {
                    $detPedido->producto = $this->productoRepository->find($detPedido->producto);
                }

                $clienteWeb = new ClienteWeb();
                $form = $this->createForm(ClienteWebType::class, $clienteWeb, [
                    'action' => $this->generateUrl('tienda_pedido_checkout'),
                    'method' => 'POST',
                ]);

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();

                    $pedidoWeb = $this->pedidoService->create(null, $clienteWeb);


                    $pedidoWeb->setEstado(Pedido::INICIADO);

                    $shipNowId = intval($request->request->all()['shipNowId']);

                    $pedidoWeb->setTipoEnvio(Pedido::OFFICE);
                    if (isset($shipNowId) && $shipNowId > 0) {
                        $pedidoWeb->setShipNowServiceId($shipNowId);
                        $pedidoWeb->setTipoEnvio(Pedido::SHIP_NOW);
                    }

                    foreach ($pedido as $detalle) {
                        $detallePedido = new DetallePedido();
                        $detallePedido->setPrecio($this->precioService->getPrecioMedidaFinal($detalle->producto, $detalle->medida, ListaPrecio::LISTA_PRECIO_MINORISTA_WEB));

                        $producto = $this->productoRepository->find($detalle->producto);
                        $detallePedido->setProducto($producto);

                        if (isset($detalle->color)) {
                            $color = $this->colorRepository->find($detalle->color);
                            $detallePedido->setColor($color);
                        }
                        if (isset($detalle->medida)) {
                            $medida = $this->medidaRepository->find($detalle->medida);
                            $detallePedido->setMedida($medida);
                        }
                        if (isset($detalle->letra)) {
                            $letra = $this->letraRepository->find($detalle->letra);
                            $detallePedido->setLetra($letra);
                        }
                        if (isset($detalle->grabado) && trim($detalle->grabado) != '') {
                            $detallePedido->setGrabado($detalle->grabado);
                            $detallePedido->setPrecioGrabado($this->listaPrecioService->getPrecioGrabado());
                        }
                        $detallePedido->setCantidad($detalle->cantidad);
                        $detallePedido->setProductodesc($detalle->productodesc);

                        $detallePedido->setPedido($pedidoWeb);
                        $pedidoWeb->addDetallespedido($detallePedido);
                        $entityManager->persist($detallePedido);
                    }

                    $searchCupon = $this->cuponService->getActiveCupon($request->request->get('cuponCode'));

                    if (count($searchCupon) > 0) {
                        $cupon = $searchCupon[0];
                        if (is_null($cupon->getMontoMinimo()) || $cupon->getMontoMinimo() <= $pedidoWeb->getTotal()) {
                            $pedidoWeb->setCupon($cupon);
                        }
                    }

                    $direccionEnvio = $this->direccionEnvioService->create($request);
                    $direccionFacturacion = $this->direccionEnvioService->create($request, DireccionEnvio::DIRECCION_FACTURACION);

                    $entityManager->persist($direccionEnvio);
                    $entityManager->persist($direccionFacturacion);

                    $pedidoWeb->setDireccionEnvio($direccionEnvio);
                    $pedidoWeb->setDireccionFacturacion($direccionFacturacion);
                    $clienteWeb->addDireccionesEnvio($direccionEnvio);

                    $entityManager->persist($pedidoWeb);
                    $entityManager->persist($clienteWeb);

                    if (isset($request->request->all()['cliente_web']['mailing'])) {

                        $mailUser = $entityManager->getRepository(User::class)->findBy(['mail' => $clienteWeb->getEmail(), 'enabled' => true]);
                        //No se puede agregar un mailing de clienteWeb si el mail pertenece a un User    
                        if (count($mailUser) == 0) {
                            $mailingPrevio = $entityManager->getRepository(Mailing::class)->buscarMail($clienteWeb->getEmail());
                            if (isset($mailingPrevio[0])) {
                                if (!$mailingPrevio[0]->getEnabled()) {
                                    $mailingPrevio[0]->setEnabled(true);
                                }
                            } else {
                                $mailing = new Mailing();
                                $mailing->setClienteWeb($clienteWeb);
                                $entityManager->persist($mailing);
                            }
                        }
                    }

                    $entityManager->flush();
                    $cookie = $this->clienteWebService->pedidoConfirmado($pedidoWeb->getId());

                    $pedido = $pedidoWeb;

                    $responseParameters['fb'] = 1;
                } else {
                    $formView = $form->createView();
                }
            }

            $responseParameters = [
                'form' => $formView,
                'pedido' => $pedido,
                'shipsNow' => NULL
            ];
        }

        $response = $this->render('tienda/pedido/checkout.html.twig', $responseParameters);

        if (isset($cookie)) {
            $response->headers->setCookie($cookie);
        }

        if (sizeof($cookiesRecuperacion) > 0) {
            foreach ($cookiesRecuperacion as $cookieRec) {
                $response->headers->setCookie($cookieRec);
            }
        }

        return $response;
    }

    /**
     * @Route("operator/pedido", name="tienda_pedido_index", methods={"GET"})
     */
    public function index(PedidoRepository $pedidoRepository): Response {
        return $this->render('tienda/pedido/index.html.twig', [
                    'pedidos' => $pedidoRepository->findAll(),
        ]);
    }

    /**
     * @Route("operator/pedido/new", name="tienda_pedido_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $pedido = new Pedido();
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $pedido->setHash(hash("sha256", serialize($pedido)));
            $entityManager->persist($pedido);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_pedido_index');
        }

        return $this->render('tienda/pedido/new.html.twig', [
                    'pedido' => $pedido,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("operator/pedido/{id}/delete", name="tienda_pedido_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pedido $pedido): Response {
        if ($this->isCsrfTokenValid('delete' . $pedido->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pedido);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_pedido_index');
    }

    /**
     * @Route("operator/pedido/{id}/edit", name="tienda_pedido_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pedido $pedido): Response {
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_pedido_index');
        }

        return $this->render('tienda/pedido/edit.html.twig', [
                    'pedido' => $pedido,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pedido/subtotal",name="pedido_subtotal", methods={"GET","POST"})
     */
    public function subtotal(Request $request) {
        $idPedido = $request->get('idPedido');
        return new Response($this->pedidoService->getSubTotal($idPedido));
    }

    /**
     * @Route("/pedido/total",name="pedido_total", methods={"GET","POST"})
     */
    public function total(Request $request) {
        $idPedido = $request->get('idPedido');
        return new Response($this->pedidoService->getTotal($idPedido));
    }

    /**
     * @Route("/pedido/unificar",name="tienda_pedido_unificar", methods={"GET","POST"})
     */
    public function unificar(Request $request) {
        $cookies = $request->cookies;
        $detallesPedido = json_decode($cookies->get('detallePedido'));
        $cantErrores = 0;
        $cantOk = 0;

        foreach ($detallesPedido as $product) {
            //Color
            $colorId = $product->color;
            $product->color = null;

            if (isset($colorId) && $colorId != '') {
                $product->color = $this->colorRepository->find($colorId);
                if (!$product->color) {
                    $this->addFlash('msgError', $this->translator->trans('The selected color is not available'));
                    return $this->redirectToRoute('home');
                }
            }

            //Letra
            $letraId = $product->letra;
            $product->letra = null;
            if (isset($letraId) && $letraId != '') {
                $product->letra = $this->letraRepository->find($letraId);
                if (!$product->letra) {
                    $this->addFlash('msgError', $this->translator->trans('The selected letter is not available'));
                    return $this->redirectToRoute('home');
                }
            }

            //Medida
            $medidaId = $product->medida;

            $product->medida = null;
            if (isset($medidaId) && $medidaId != '') {
                $product->medida = $this->medidaRepository->find($medidaId);
                if (!$product->medida) {
                    $this->addFlash('msgError', $this->translator->trans('The selected measure is not available'));
                    return $this->redirectToRoute('home');
                }
            }

            //Cantidad
            $cantidad = intval($product->cantidad);
            if ($cantidad <= 0) {
                $this->addFlash('error', $this->translator->trans('The quantity is not valid'));
                return $this->redirectToRoute('home');
            }

            //Producto
            $productoId = $product->producto;

            $product->producto = null;
            if (isset($productoId) && $productoId != '') {
                $product->producto = $this->productoRepository->find($productoId);
                if (!$product->producto) {
                    $this->addFlash('msgError', $this->translator->trans('The selected product is not available'));
                    return $this->redirectToRoute('home');
                }
            }

            $detallePedido = $this->pedidoService->addProduct((object) $product);

            if ($detallePedido === false) {
                $cantErrores++;
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $pedido = $this->pedidoService->getPedidoPendiente();
                $pedido->addDetallespedido($detallePedido);
                $entityManager->persist($pedido);
                $entityManager->flush();
                $cantOk++;
            }
        }

        return new Response($this->pedidoService->countProductosPedidoPendiente());
    }

    /**
     * @Route("/pedido/borrarweb",name="tienda_pedido_borrar_web", methods={"GET","POST"})
     */
    public function borrarWeb(Request $request) {
        $cookies = $request->cookies;
        $response = new Response();

        if ($cookies->has('idPedido')) {
            $response->headers->clearCookie('idPedido', '/', null);
            $response->headers->clearCookie('detallePedido', '/', null);
            $response->headers->clearCookie('cantidadProductos', '/', null);
        }

        return $response;
    }

    /**
     * @Route("/pedido/noProductInCart",name="tienda_no_producto_in cart", methods={"GET","POST"})
     */
    public function noproductInCart() {
        $this->addFlash('msgError', $this->translator->trans('There are no products in the bag.'));
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("pedido/{id}/modifyEnvio", name="tienda_pedido_modify_envio", methods={"GET"})
     */
    public function modifyEnvio(
            Request $request,
            Pedido $pedido,
            DireccionEnvioRepository $direccionEnvioRepository): Response {

        if ($pedido) {
            $shipNowServiceId = $request->get('shipNowServiceId');
            $direccionId = $request->get('direccionId');

            $direccionEnvio = $direccionEnvioRepository->find($direccionId);

            if ($direccionEnvio) {
                $pedido->setDireccionEnvio($direccionEnvio);

                if ($shipNowServiceId !== Pedido::OFFICE) {
                    $pedido->setTipoEnvio(Pedido::SHIP_NOW);
                    $pedido->setShipNowServiceId(intval($shipNowServiceId));
                    $pedido->setCostoEnvio(floatval($request->get('costoEnvio', '0')));
                } else {
                    $pedido->setTipoEnvio(Pedido::OFFICE);
                    $pedido->setShipNowServiceId(null);
                    $pedido->setCostoEnvio(0);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($pedido);
                $entityManager->flush();

                $this->addFlash('msgOk', $this->translator->trans('Shipping method succesfully modified'));

                return JsonResponse::create([
                            'code' => 200,
                            'resultado' => $this->translator->trans('Shipping method succesfully modified')
                ]);
            }
        }

        return JsonResponse::create([
                    'code' => 400,
                    'resultado' => $this->translator->trans('It was not possible to modify the Shipping method')
        ]);
    }

}
