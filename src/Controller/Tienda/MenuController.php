<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Menu;
use App\Entity\Tienda\Grupo;
use App\Form\Tienda\MenuType;
use App\Repository\Tienda\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;

/**
 * @Route("/admin/menu")
 */
class MenuController extends AbstractController {

    private $generalService;

    function __construct(GeneralService $generalService) {
        $this->generalService = $generalService;
    }

    /**
     * @Route("/", name="tienda_menu_index", methods={"GET"})
     */
    public function index(Request $request, MenuRepository $menuRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $menus = $paginator->paginate(
                $menuRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/menu/index.html.twig', [
                    'menus' => $menus,
        ]);
    }

    /**
     * @Route("/new", name="tienda_menu_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $menu = new Menu();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $menuValidation = $request->get('menu');

            $count1 = 0;
            $count2 = 0;

            if ($menuValidation['categoria1'] != '') {
                $count1++;
            }
            if ($menuValidation['subcategoria1'] != '') {
                $count1++;
            }
            if ($menuValidation['producto1'] != '') {
                $count1++;
            }
            if ($menuValidation['lineaProducto1'] != '') {
                $count1++;
            }
            if ($menuValidation['categoria2'] != '') {
                $count2++;
            }
            if ($menuValidation['subcategoria2'] != '') {
                $count2++;
            }
            if ($menuValidation['producto2'] != '') {
                $count2++;
            }
            if ($menuValidation['lineaProducto2'] != '') {
                $count2++;
            }

            $error = false;

            if ($count1 > 1) {
                $this->addFlash('msgError', $translator->trans('Only one option can be set to text 1'));
                $error = true;
            }
            if ($count2 > 1) {
                $this->addFlash('msgError', $translator->trans('Only one option can be set to text 2'));
                $error = true;
            }

            /*$cant = 0;
            foreach ($menuValidation['grupos'] as $grupo_id) {
                $grupo = $entityManager->getRepository(Grupo::class)->find($grupo_id);
                $size = sizeof($grupo->getCategoriasGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getCategoriasubcategoriasGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getProductosGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getLineaProductosGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
            }
            if ($cant != 8) {
                $this->addFlash('msgError', $translator->trans('One group must contain at least 8 elements'));
                $error = true;
            }*/

            if ($error) {
                return $this->render('tienda/menu/new.html.twig', [
                            'menu' => $menu,
                            'form' => $form->createView(),
                ]);
            }

            $error = false;
            if ($_FILES['menu']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['menu']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $blobImage = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($_FILES['menu']['tmp_name']['imagen']));
                    $menu->setImagen($blobImage);
                }
            }
            if (!$error) {
                $entityManager->persist($menu);

                $entityManager->flush();

                $this->addFlash('msgOk', $translator->trans('Menu created successfully.'));
                return $this->redirectToRoute('tienda_menu_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the menu.'));
        }
        return $this->render('tienda/menu/new.html.twig', [
                    'menu' => $menu,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_menu_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Menu $menu, TranslatorInterface $translator) {
        if (!$menu) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected menu.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $menu->setEnabled($activo);
            $entityManager->persist($menu);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Menu status has been changed', ['%menu%' => $menu->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_menu_index'));
    }

    /**
     * @Route("/{id}/edit", name="tienda_menu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Menu $menu, TranslatorInterface $translator): Response {
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $menuValidation = $request->request->get('menu');

            $count1 = 0;
            $count2 = 0;

            if ($menuValidation['categoria1'] != '') {
                $count1++;
            }
            if ($menuValidation['subcategoria1'] != '') {
                $count1++;
            }
            if ($menuValidation['producto1'] != '') {
                $count1++;
            }
            if ($menuValidation['lineaProducto1'] != '') {
                $count1++;
            }
            if ($menuValidation['categoria2'] != '') {
                $count2++;
            }
            if ($menuValidation['subcategoria2'] != '') {
                $count2++;
            }
            if ($menuValidation['producto2'] != '') {
                $count2++;
            }
            if ($menuValidation['lineaProducto2'] != '') {
                $count2++;
            }

            $error = false;

            if ($count1 > 1) {
                if ($menuValidation['subcategoria1'] == '') {
                    $this->addFlash('msgError', $translator->trans('Only one option can be set to text 1'));
                    $error = true;
                } else {
                    if ($menuValidation['categoria1'] == '') {
                        $this->addFlash('msgError', 'Al seleccionar una subcategoría es necesario seleccionar un categoría a su vez.');
                        $error = true;
                    } else {
                        if ($count1 > 1) {
                            $this->addFlash('msgError', 'Se ha seleccionado demasiadas opciones para el texto 1');
                            $error = true;
                        }
                    }
                }
            }
            if ($count2 > 1) {
                
                if ($menuValidation['subcategoria2'] == '') {
                    $this->addFlash('msgError', $translator->trans('Only one option can be set to text 2'));
                    $error = true;
                } else {
                    if ($menuValidation['categoria2'] == '') {
                        $this->addFlash('msgError', 'Al seleccionar una subcategoría es necesario seleccionar un categoría a su vez.');
                        $error = true;
                    } else {
                        if ($count1 > 1) {
                            $this->addFlash('msgError', 'Se ha seleccionado demasiadas opciones para el texto 2');
                            $error = true;
                        }
                    }
                }                
            }

           /* $cant = 0;
            foreach ($menuValidation['grupos'] as $grupo_id) {
                $grupo = $entityManager->getRepository(Grupo::class)->find($grupo_id);
                $size = sizeof($grupo->getCategoriasGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getCategoriasubcategoriasGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getProductosGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
                $size = sizeof($grupo->getLineaProductosGrupo());
                if ($size > $cant) {
                    $cant = $size;
                }
            }
            if ($cant != 8) {
                $this->addFlash('msgError', $translator->trans('One group must contain at least 8 elements'));
                $error = true;
            }*/

            if ($error) {
                return $this->render('tienda/menu/new.html.twig', [
                            'menu' => $menu,
                            'form' => $form->createView(),
                ]);
            }

            if ($_FILES['menu']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['menu']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    return $this->render('tienda/menu/edit.html.twig', [
                                'menu' => $menu,
                                'form' => $form->createView(),
                    ]);
                }

                $blobImage = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($_FILES['menu']['tmp_name']['imagen']));
                $menu->setImagen($blobImage);
            }
            $entityManager->persist($menu);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Menu edited successfully.'));
            return $this->redirectToRoute('tienda_menu_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the menu.'));
        }
        return $this->render('tienda/menu/edit.html.twig', [
                    'menu' => $menu,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_menu_show", methods={"GET"})
     */
    public function show(Menu $menu): Response {
        return $this->render('tienda/menu/show.html.twig', [
                    'menu' => $menu,
        ]);
    }

}
