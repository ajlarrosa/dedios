<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Promocion;
use App\Form\Tienda\PromocionType;
use App\Repository\Tienda\PromocionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/promocion")
 */
class PromocionController extends AbstractController
{
    /**
     * @Route("/", name="tienda_promocion_index", methods={"GET"})
     */
    public function index(Request $request, PromocionRepository $promocionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $promociones = $paginator->paginate(
                $promocionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/promocion/index.html.twig', [
                    'promociones' => $promociones,
        ]);
    }

    /**
     * @Route("/new", name="tienda_promocion_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $promocion = new Promocion();
        $form = $this->createForm(PromocionType::class, $promocion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($promocion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Special offer created successfully.'));
            return $this->redirectToRoute('tienda_promocion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the special offer.'));
        }
        return $this->render('tienda/promocion/new.html.twig', [
                    'promocion' => $promocion,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/activate/{id}", name="tienda_promocion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Promocion $promocion, TranslatorInterface $translator) {
        if (!$promocion) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected special offer.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $promocion->setEnabled($activo);
            $entityManager->persist($promocion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Special offer status has been changed', ['%special_offer%' => $promocion->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_promocion_index'));
    }    

    /**
     * @Route("/{id}/edit", name="tienda_promocion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Promocion $promocion, TranslatorInterface $translator): Response {
        $form = $this->createForm(PromocionType::class, $promocion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Special offer edited successfully.'));
            return $this->redirectToRoute('tienda_promocion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the special offer.'));
        }
        return $this->render('tienda/promocion/edit.html.twig', [
                    'promocion' => $promocion,
                    'form' => $form->createView(),
        ]);
    }
    
}
