<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Configuracion;
use App\Form\Tienda\ConfiguracionType;
use App\Repository\Tienda\ConfiguracionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * @Route("/admin/configuracion")
 */
class ConfiguracionController extends AbstractController {

    private $generalService;
    private $translator;

    function __construct(GeneralService $generalService, TranslatorInterface $translator) {
        $this->generalService = $generalService;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="tienda_configuracion_index", methods={"GET"})
     */
    public function index(Request $request, ConfiguracionRepository $configuracionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $configuraciones = $paginator->paginate(
                $configuracionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/configuracion/index.html.twig', [
                    'configuraciones' => $configuraciones,
        ]);
    }

    /**
     * @Route("/new", name="tienda_configuracion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $configuracion = new Configuracion();
        $form = $this->createForm(ConfiguracionType::class, $configuracion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $error = false;
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($configuracion);
            $entityManager->flush();

            if ($_FILES['configuracion']['size']['logo'] > 0) {
                //Validates if file is image
                $logo = $_FILES['configuracion']['name']['logo'];
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['logo'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    return $this->render('tienda/configuracion/new.html.twig', [
                                'configuracion' => $configuracion,
                                'form' => $form->createView(),
                    ]);
                }

                $uploaddir = $this->getParameter('root_dir_logo') . '/';

                $uploadfile = $uploaddir . $logo;

                $tmp_name = $_FILES['configuracion']["tmp_name"]['logo'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    $configuracion->setLogo($logo);
                }
            }

            $uploaddir = $this->getParameter('root_dir_config') . $configuracion->getId() . '/';

            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0777, true);
            }

            if ($_FILES['configuracion']['size']['imagenResultado'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenResultado'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    return $this->render('tienda/configuracion/new.html.twig', [
                                'configuracion' => $configuracion,
                                'form' => $form->createView(),
                    ]);
                }

                $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenResultado'];

                $uploadfile = $uploaddir . $name;

                $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenResultado'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    $configuracion->setImagenResultado($name);
                } else {
                    $error = true;
                }
            }

            if ($_FILES['configuracion']['size']['imagenMainSlider1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider1'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider1'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSlider1($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSlider2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider2'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider2'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSlider2($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSlider3'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider3'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider3'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider3'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSlider3($name);
                    } else {
                        $error = true;
                    }
                }
            }

            if ($_FILES['configuracion']['size']['imagenMainSliderMobile1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile1'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile1'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSliderMobile1($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSliderMobile2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile2'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile2'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSliderMobile2($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSliderMobile3'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile3'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile3'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile3'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $configuracion->setImagenMainSliderMobile3($name);
                    } else {
                        $error = true;
                    }
                }
            }

            if (!$error) {
                $entityManager->persist($configuracion);
                $entityManager->flush();

                $cache = new FilesystemAdapter();

                if ($cache->hasItem('configuracion')) {
                    $cache->deleteItem('configuracion');
                }
                $this->addFlash('msgOk', $this->translator->trans('Configuration created successfully.'));
                return $this->redirectToRoute('tienda_configuracion_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to create the configuration.'));
        }
        return $this->render('tienda/configuracion/new.html.twig', [
                    'configuracion' => $configuracion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_configuracion_show", methods={"GET"})
     */
    public function show(Configuracion $configuracion): Response {
        return $this->render('tienda/configuracion/show.html.twig', [
                    'configuracion' => $configuracion,
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_configuracion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Configuracion $configuracion) {
        if (!$configuracion) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to change the status of the selected configuration.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $configuracion->setEnabled($activo);
            $entityManager->persist($configuracion);
            $entityManager->flush();

            $cache = new FilesystemAdapter();

            if ($cache->hasItem('configuracion')) {
                $cache->deleteItem('configuracion');
            }

            $this->addFlash('msgOk', $this->translator->trans('Configuration status has been changed'));
        }
        return $this->redirect($this->generateUrl('tienda_configuracion_index'));
    }

    /**
     * @Route("/{id}/edit", name="tienda_configuracion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Configuracion $configuracion): Response {
        $form = $this->createForm(ConfiguracionType::class, $configuracion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($_FILES['configuracion']['size']['logo'] > 0) {
                //Validates if file is image
                $logo = $_FILES['configuracion']['name']['logo'];

                if (!$this->generalService->isImage($_FILES['configuracion']['type']['logo'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    return $this->render('tienda/configuracion/new.html.twig', [
                                'configuracion' => $configuracion,
                                'form' => $form->createView(),
                    ]);
                }

                $uploaddir = $this->getParameter('root_dir_logo');

                $uploadfile = $uploaddir . $logo;

                $tmp_name = $_FILES['configuracion']['tmp_name']['logo'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    $configuracion->setLogo($logo);
                }
            }


            $uploaddir = $this->getParameter('root_dir_config') . $configuracion->getId() . '/';

            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0777, true);
            }

            if ($_FILES['configuracion']['size']['imagenResultado'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenResultado'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    return $this->render('tienda/configuracion/new.html.twig', [
                                'configuracion' => $configuracion,
                                'form' => $form->createView(),
                    ]);
                }

                $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenResultado'];

                $uploadfile = $uploaddir . $name;

                $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenResultado'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    if ($configuracion->getImagenResultadoSrc() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenResultadoSrc())) {
                        unlink($uploaddir . '../../../' . $configuracion->getImagenResultadoSrc());
                    }
                    $configuracion->setImagenResultado($name);
                } else {
                    $error = true;
                }
            }

            if ($_FILES['configuracion']['size']['imagenMainSlider1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider1'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider1'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSlider1() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSlider1())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSlider1());
                        }
                        $configuracion->setImagenMainSlider1($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSlider2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider2'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider2'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSlider2() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSlider2())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSlider2());
                        }
                        $configuracion->setImagenMainSlider2($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSlider3'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSlider3'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSlider3'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSlider3'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSlider3() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSlider3())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSlider3());
                        }
                        $configuracion->setImagenMainSlider3($name);
                    } else {
                        $error = true;
                    }
                }
            }

            if ($_FILES['configuracion']['size']['imagenMainSliderMobile1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile1'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile1'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSliderMobile1() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile1())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile1());
                        }
                        $configuracion->setImagenMainSliderMobile1($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSliderMobile2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile2'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {

                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile2'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSliderMobile2() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile2())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile2());
                        }
                        $configuracion->setImagenMainSliderMobile2($name);
                    } else {
                        $error = true;
                    }
                }
            }
            if ($_FILES['configuracion']['size']['imagenMainSliderMobile3'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['configuracion']['type']['imagenMainSliderMobile3'])) {
                    $this->addFlash('msgError', $this->translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $name = date('YmdHis') . '_' . $_FILES['configuracion']['name']['imagenMainSliderMobile3'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['configuracion']["tmp_name"]['imagenMainSliderMobile3'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if ($configuracion->getImagenMainSliderMobile3() != '' && is_file($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile3())) {
                            unlink($uploaddir . '../../../' . $configuracion->getImagenMainSliderMobile3());
                        }
                        $configuracion->setImagenMainSliderMobile3($name);
                    } else {
                        $error = true;
                    }
                }
            }

            $this->getDoctrine()->getManager()->flush();

            $cache = new FilesystemAdapter();

            if ($cache->hasItem('configuracion')) {
                $cache->deleteItem('configuracion');
            }

            $this->addFlash('msgOk', $this->translator->trans('Configuration edited successfully.'));
            return $this->redirectToRoute('tienda_configuracion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to edit the configuration.'));
        }
        return $this->render('tienda/configuracion/edit.html.twig', [
                    'configuracion' => $configuracion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="tienda_configuracion_image_delete", methods={"GET"})
     */
    public function delete(Configuracion $configuracion, Request $request): Response {

        try {
            $entityManager = $this->getDoctrine()->getManager();
            $tipo = $request->get('tipo');

            $uploaddir = $this->getParameter('root_dir_config');
            $method = 'get' . ucfirst($tipo);

            if (is_file($uploaddir . '../../' . $configuracion->{$method}())) {
                unlink($uploaddir . '../../' . $configuracion->{$method}());
            }

            $method = 'set' . ucfirst($tipo);

            $configuracion->{$method}(null);

            $configuracion->getLogo();
            $entityManager->persist($configuracion);
            $entityManager->flush();

            $cache = new FilesystemAdapter();

            if ($cache->hasItem('configuracion')) {
                $cache->deleteItem('configuracion');
            }

            $this->addFlash("msgOk", $this->translator->trans("The image has been successfully removed."));
        } catch (\Exception $e) {
            $this->addFlash("msgError", $this->translator->trans("It was not possible to delete the image."));
        }

        return $this->redirectToRoute('tienda_configuracion_edit', ['id' => $configuracion->getId()]);
    }

}
