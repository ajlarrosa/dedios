<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\FormatoMail;
use App\Form\Tienda\FormatoMailType;
use App\Repository\Tienda\FormatoMailRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\Tienda\ConfiguracionService;

/**
 * @Route("/admin/formato/mail")
 */
class FormatoMailController extends AbstractController {

    private $configuracion;

    function __construct(ConfiguracionService $configuracionService) {
        $this->configuracion = $configuracionService->getLastConfiguration();
    }

    /**
     * @Route("/", name="tienda_formato_mail_index", methods={"GET"})
     */
    public function index(Request $request, FormatoMailRepository $formatoMailRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $formatoMails = $paginator->paginate(
                $formatoMailRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/formato_mail/index.html.twig', [
                    'formato_mails' => $formatoMails,
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_formato_mail_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, FormatoMail $formatoMail, TranslatorInterface $translator) {
        if (!$formatoMail) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected mail format.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $formatoMail->setEnabled($activo);
            $entityManager->persist($formatoMail);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Mail format status has been changed'));
        }
        return $this->redirect($this->generateUrl('tienda_formato_mail_index'));
    }

    /**
     * @Route("/new", name="tienda_formato_mail_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $formatoMail = new FormatoMail();
        $form = $this->createForm(FormatoMailType::class, $formatoMail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formatoMail);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_formato_mail_index');
        }

        return $this->render('tienda/formato_mail/new.html.twig', [
                    'formato_mail' => $formatoMail,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_formato_mail_show", methods={"GET"})
     */
    public function show(FormatoMail $formatoMail): Response {
        $imagen_src = $this->configuracion->getLogo();

        return $this->render('tienda/formato_mail/show.html.twig', [
                    'formato_mail' => $formatoMail,
                    'imagen_src' => $imagen_src
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_formato_mail_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FormatoMail $formatoMail): Response {
        $form = $this->createForm(FormatoMailType::class, $formatoMail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_formato_mail_index');
        }        
        return $this->render('tienda/formato_mail/edit.html.twig', [
                    'formato_mail' => $formatoMail,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_formato_mail_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FormatoMail $formatoMail): Response {
        if ($this->isCsrfTokenValid('delete' . $formatoMail->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($formatoMail);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_formato_mail_index');
    }

}
