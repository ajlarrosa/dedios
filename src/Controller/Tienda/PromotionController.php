<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Promotion;
use App\Service\Tienda\PromotionService;
use App\Service\Tienda\PedidoService;
use App\Form\Tienda\PromotionType;
use App\Repository\Tienda\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/")
 */
class PromotionController extends AbstractController {

    private $translator;
    private $pedidoService;
    private $security;

    public function __construct(
            TranslatorInterface $translator,
            PedidoService $pedidoService,
            Security $security
    ) {
        $this->translator = $translator;
        $this->pedidoService = $pedidoService;
        $this->security = $security;
    }

    /**
     * @Route("admin/promotion/", name="tienda_promotion_index", methods={"GET"})
     */
    public function index(Request $request, PromotionRepository $promotionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $promotions = $paginator->paginate(
                $promotionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );

        return $this->render('tienda/promotion/index.html.twig', [
                    'promotions' => $promotions
        ]);
    }

    /**
     * @Route("admin/promotion/activate/{id}", name="tienda_promotion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Promotion $promotion) {
        if (!$promotion) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to change the status of the selected promotion.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $promotion->setEnabled($activo);
            $entityManager->persist($promotion);
            $entityManager->flush();
            $this->addFlash('msgOk', $this->translator->trans('Promotion status has been changed'));
        }
        return $this->redirect($this->generateUrl('tienda_promotion_index'));
    }

    /**
     * @Route("admin/promotion/new", name="tienda_promotion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $promotion = new Promotion();
        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($promotion->getFechaHasta() != '') {
                $promotion->setFechaHasta(new \DateTime($promotion->getFechaHasta()->format('Y-m-d') . '23:59:59'));
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($promotion);
            $entityManager->flush();
            $this->addFlash('msgOk', $this->translator->trans('Promotion created successfully.'));
            return $this->redirectToRoute('tienda_promotion_index');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to create the promotion.'));
        }

        return $this->render('tienda/promotion/new.html.twig', [
                    'promotion' => $promotion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/promotion/{id}", name="tienda_promotion_show", methods={"GET"})
     */
    public function show(Promotion $promotion): Response {
        return $this->render('tienda/promotion/show.html.twig', [
                    'promotion' => $promotion,
        ]);
    }

    /**
     * @Route("admin/promotion/{id}/edit", name="tienda_promotion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Promotion $promotion): Response {
        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($promotion->getFechaHasta() != '') {
                $promotion->setFechaHasta(new \DateTime($promotion->getFechaHasta()->format('Y-m-d') . '23:59:59'));
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $this->translator->trans('Promotion edited successfully.'));
            return $this->redirectToRoute('tienda_promotion_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to edit the promotion.'));
        }
        return $this->render('tienda/promotion/edit.html.twig', [
                    'promotion' => $promotion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/promotion/{id}", name="tienda_promotion_delete", methods={"POST"})
     */
    public function delete(Request $request, Promotion $promotion): Response {
        if ($this->isCsrfTokenValid('delete' . $promotion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($promotion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_promotion_index');
    }

    /**
     * @Route("/promociones/{productoId}", name="tienda_promotion_promociones", methods={"GET"})
     */
    public function promociones($productoId, PromotionService $promotionService): JsonResponse {        
        return new JsonResponse($promotionService->getActivePromotion($productoId));
    }

    /**
     * @Route("/promociones/pedido/totales", name="tienda_promotion_promociones_pedido", methods={"GET"})
     */
    public function promocionesPedido(Request $request, PromotionService $promotionService): JsonResponse {
        $pedido = null;
        $pedidoId = $request->get('pedidoId');

         if ($this->security->isGranted("ROLE_USER_MINORISTA")) {
              $pedido = $this->pedidoService->getPedidoPendiente();
         } elseif ($pedidoId > 0) {
            $pedido = $this->pedidoService->getPedidoPendienteById($pedidoId);
        }
        
        return new JsonResponse($promotionService->getActivePedidoPromotion($request, $pedido));
    }

}
