<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\FormaPago;
use App\Entity\Tienda\Pedido;
use App\Entity\Tienda\DireccionEnvio;
use App\Form\Tienda\FormaPagoType;
use App\Repository\Tienda\FormaPagoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\Tienda\MercadoPagoService;
use App\Repository\Tienda\DireccionEnvioRepository;
use App\Repository\Tienda\MercadoPagoRepository;
use Symfony\Component\Security\Core\Security;
use App\Service\General\SessionService;

/**
 * @Route("/")
 */
class FormaPagoController extends AbstractController {

    private $mercadoPagoService;
    private $security;
    private $sessionService;

    function __construct(
            SessionService $sessionService,
            MercadoPagoRepository $mercadoPagoRepository,
            MercadoPagoService $mercadoPagoService,
            Security $security
    ) {

        $this->mercadoPagoService = $mercadoPagoService;
        $this->mercadoPagoRepository = $mercadoPagoRepository;
        $this->security = $security;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("forma_pago/{id}/mercadoPago", name="tienda_formapago_mercadopago", methods={"GET","POST"})
     */
    public function mercadoPago(Request $request, Pedido $pedido, DireccionEnvioRepository $direccionEnvioRepository, TranslatorInterface $translator): Response {

        if ($pedido->getPagado()) {
            $this->addFlash('msgWarn', $translator->trans('You have already made the payment for this order, if you want to make any additional payment contact Dedios (joyas@dediosjoyas.com).'));
            return $this->redirectToRoute('home');
        }

        $mercadoPago = $this->mercadoPagoRepository->findOneBy(['pedido' => $pedido, 'enabled' => true]);

        if ($mercadoPago) {
            if (is_null($mercadoPago->getPaymentId())) {
                $em = $this->getDoctrine()->getManager();
                $mercadoPago->setEnabled(false);
                $em->persist($mercadoPago);
                $em->flush();
            } else {

                $payment = $this->mercadoPagoService->getPaymentStatus($mercadoPago->getPaymentId());
                
                if ($payment !== null) {
                    if ($payment->status === 'authorized') {
                        $this->addFlash('msgOk', $translator->trans('Previous payment is authorized, please wait.'));
                        return $this->redirectToRoute('home');
                    }
                    if ($payment->status === 'in_process') {
                        $this->addFlash('msgOk', $translator->trans('Previous payment is in process, please wait.'));
                        return $this->redirectToRoute('home');
                    }
                    if ($payment->status === 'approved') {
                        $this->addFlash('msgOk', $translator->trans('Previous payment was approved.'));
                        return $this->redirectToRoute('home');
                    }
                } else {
                    $em = $this->getDoctrine()->getManager();
                    $mercadoPago->setEnabled(false);
                    $em->persist($mercadoPago);
                    $em->flush();
                }
            }
        }

        $productos = [];

        $descuento = $pedido->getFormaPago()->getPorcentajeDescuento();
        
        $recargo = $pedido->getFormaPago()->getPorcentajeRecargo();
        
        $promos = $pedido->getMontoPromocion();
        
        foreach ($pedido->getDetallespedido() as $detallePedido) {
            
            $precio = round($detallePedido->getPrecio() * (1 - $descuento / 100) * (1 + $recargo / 100));
            
            if ($promos > 0 ) {
                if ($promos >= $precio) {
                    $promos = $promos - ($precio - 1); 
                    $precio = 1;
                } else {
                    $precio = $precio - $promos;
                    $promos = 0; 
                }                
            }
            
            $productos[] = (object) [
                        'id' => $detallePedido->getProducto()->getCodigo(),
                        'title' => $detallePedido->getProductodesc(),
                        'picture_url' => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif", //Reemplazar con la direccion del producto
                        'description' => $detallePedido->getProductodesc(),
                        'category_id' => $detallePedido->getProducto()->getCategoriasubcategoria()->getSubcategoria() . ' ' . $detallePedido->getProducto()->getCategoriasubcategoria()->getCategoria(),
                        'quantity' => $detallePedido->getCantidad(),
                        'currency_id' => 'ARS',
                        'unit_price' => $precio
            ];
        }
        
        $envio = $pedido->getShipNowServiceId();
        
        if (isset($envio) && $pedido->getCostoEnvio() > 0) {
            $productos[] = (object) [
                        'id' => 'ShipNow',
                        'title' => 'Costo envio Ship Now',
                        'picture_url' => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif", //Reemplazar con la direccion de imagen DE DIOS
                        'description' => 'Envio Ship Now',
                        'category_id' => '',
                        'quantity' => 1,
                        'currency_id' => 'ARS',
                        'unit_price' => $pedido->getCostoEnvio()
            ];
        }

        $this->mercadoPagoService->setPedidoId($pedido->getId());
        $this->mercadoPagoService->setProductos($productos);

        $dataFinderDireccion = ['enabled' => true, 'tipo_direccion' => DireccionEnvio::CASA];
        
        if (!is_null($pedido->getUser())) {
            $dataFinderDireccion['user'] = $pedido->getUser()->getId();
        } else {
            $dataFinderDireccion['clienteWeb'] = $pedido->getClienteWeb()->getId();
        }

        $direccion = $direccionEnvioRepository->findOneBy($dataFinderDireccion);
        
        $date_created = new \DateTime();
        
        $pagador = (object) ['name' => $pedido->getEntity()->getNombre(),
                    'surname' => $pedido->getEntity()->getApellido(),
                    'email' => $pedido->getEmail(),
                    'date_created' => $date_created->format('c'),
                    'phone' => [
                        "area_code" => "",
                        "number" => $pedido->getEntity()->getTelefono()],
                    'identification' => [
                        "type" => "DNI",
                        "number" => $pedido->getEntity()->getDni(),
                    ],
                    'address' => [
                        "street_name" => $direccion->getCalle(),
                        "street_number" => $direccion->getCalleNumero(),
                        "zip_code" => $direccion->getZipCode()
                    ]
        ];

        $this->mercadoPagoService->setPayer($pagador);

        $preference = $this->mercadoPagoService->getPreference();

        if (is_null($preference->id)) {
            $this->addFlash('msgWarn', $translator->trans('No ha sido posible conectarse con MercadoPago. Por favor para completar el pago envienos un whatsapp comentando el inconveniente. Gracias.'));
            
            return $this->redirectToRoute('home');
        }

        $this->mercadoPagoService->create($preference, $pedido);

        return $this->render('tienda/forma_pago/mercadoPago.html.twig', [
                    'pedidoId' => $pedido->getId(),
                    'preference' => $preference
        ]);
    }

    /**
     * @Route("admin/formapago/", name="tienda_forma_pago_index", methods={"GET"})
     */
    public function index(Request $request, FormaPagoRepository $formaPagoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $formasPago = $paginator->paginate(
                $formaPagoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/forma_pago/index.html.twig', [
                    'formasPago' => $formasPago,
        ]);
    }

    /**
     * @Route("admin/formapago/new", name="tienda_forma_pago_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $formaPago = new FormaPago();
        $form = $this->createForm(FormaPagoType::class, $formaPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formaPago);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Payment method created successfully.'));
            return $this->redirectToRoute('tienda_forma_pago_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the payment method.'));
        }
        return $this->render('tienda/forma_pago/new.html.twig', [
                    'formaPago' => $formaPago,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/formapago/{id}/edit", name="tienda_forma_pago_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FormaPago $formaPago, TranslatorInterface $translator): Response {
        $form = $this->createForm(FormaPagoType::class, $formaPago);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Payment method edited successfully.'));
            return $this->redirectToRoute('tienda_forma_pago_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the payment method.'));
        }
        return $this->render('tienda/forma_pago/edit.html.twig', [
                    'formaPago' => $formaPago,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/formapago/activate/{id}", name="tienda_forma_pago_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, FormaPago $formaPago, TranslatorInterface $translator) {
        if (!$formaPago) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected payment method.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $formaPago->setEnabled($activo);
            $entityManager->persist($formaPago);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Payment method status has been changed', ['%payment_method%' => $formaPago->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_forma_pago_index'));
    }

}
