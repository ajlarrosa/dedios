<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Grupo;
use App\Entity\Producto;
use App\Entity\Categoria;
use App\Entity\Tienda\ProductoGrupo;
use App\Entity\Tienda\CategoriaGrupo;
use App\Entity\Tienda\CategoriasubcategoriaGrupo;
use App\Entity\Tienda\LineaProductoGrupo;
use App\Form\Tienda\GrupoType;
use App\Repository\Tienda\GrupoRepository;
use App\Repository\Tienda\ProductoGrupoRepository;
use App\Repository\Tienda\LineaProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/admin/grupo")
 */
class GrupoController extends AbstractController {

    private $productoGrupoRepository;

    function __construct(ProductoGrupoRepository $productoGrupoRepository) {
        $this->productoGrupoRepository = $productoGrupoRepository;
    }

    /**
     * @Route("/", name="tienda_grupo_index", methods={"GET"})
     */
    public function index(Request $request, GrupoRepository $grupoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $grupos = $paginator->paginate(
                $grupoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/grupo/index.html.twig', [
                    'grupos' => $grupos,
        ]);
    }

    /**
     * @Route("/new", name="tienda_grupo_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, LineaProductoRepository $lineaProductoRepository): Response {
        $grupo = new Grupo();
        $form = $this->createForm(GrupoType::class, $grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $data = $request->request->all()['grupo'];
            if (isset($data['categorias'])) {
                foreach ($data['categorias'] as $categoria_id) {
                    $categoria = $entityManager->getRepository(Categoria::class)->find($categoria_id);

                    $categoriaGrupo = new CategoriaGrupo();
                    $categoriaGrupo->setGrupo($grupo);

                    $categoriaGrupo->setCategoria($categoria);
                    $entityManager->persist($categoriaGrupo);
                }
            }
            if (isset($data['categoriasubcategorias'])) {
                foreach ($data['categoriasubcategorias'] as $categoriasubcategoria_id) {
                    $categoriasubcategoria = $entityManager->getRepository(CategoriaSubcategoria::class)->find($categoriasubcategoria_id);
                    $categoriasubcategoriaGrupo = new CategoriasubcategoriaGrupo();
                    $categoriasubcategoriaGrupo->setGrupo($grupo);
                    $categoriasubcategoriaGrupo->setCategoriasubcategoria($categoriasubcategoria);
                    $entityManager->persist($categoriasubcategoriaGrupo);
                }
            }
            if (isset($data['lineasProducto'])) {
                foreach ($data['lineasProducto'] as $lineaProducto_id) {
                    $lineaProducto = $lineaProductoRepository->find($lineaProducto_id);
                    $lineaProductoGrupo = new LineaProductoGrupo();
                    $lineaProductoGrupo->setGrupo($grupo);
                    $lineaProductoGrupo->setLineaProducto($lineaProducto);
                    $entityManager->persist($lineaProductoGrupo);
                }
            }

            $productos = $request->get('productos');

            if ($productos) {
                foreach ($productos as $producto_id) {
                    $producto = $entityManager->getRepository(Producto::class)->find($producto_id);

                    $productoGrupo = new ProductoGrupo();
                    $productoGrupo->setGrupo($grupo);
                    $productoGrupo->setProducto($producto);
                    $entityManager->persist($productoGrupo);
                }
            }

            $entityManager->persist($grupo);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Group created successfully.'));
            return $this->redirectToRoute('tienda_grupo_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the group.'));
        }
        return $this->render('tienda/grupo/new.html.twig', [
                    'grupo' => $grupo,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_grupo_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Grupo $grupo, TranslatorInterface $translator) {
        if (!$grupo) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected group.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $grupo->setEnabled($activo);
            $entityManager->persist($grupo);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Group status has been changed', ['%group%' => $grupo->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_grupo_index'));
    }

    /**
     * @Route("/{id}/edit", name="tienda_grupo_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Grupo $grupo, TranslatorInterface $translator, LineaProductoRepository $lineaProductoRepository): Response {
        $form = $this->createForm(GrupoType::class, $grupo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $data = $request->request->all()['grupo'];
            if (isset($data['categorias'])) {
                foreach ($data['categorias'] as $categoria_id) {
                    $categoria = $entityManager->getRepository(Categoria::class)->find($categoria_id);
                    $categoriaGrupo = $entityManager->getRepository(CategoriaGrupo::class)->findBy(['categoria' => $categoria, 'grupo' => $grupo]);

                    if (!$categoriaGrupo) {
                        $categoriaGrupo = new CategoriaGrupo();
                        $categoriaGrupo->setGrupo($grupo);

                        $categoriaGrupo->setCategoria($categoria);
                        $entityManager->persist($categoriaGrupo);
                    }
                }
            }
            if (isset($data['categoriasubcategorias'])) {
                foreach ($data['categoriasubcategorias'] as $categoriasubcategoria_id) {
                    $categoriasubcategoria = $entityManager->getRepository(CategoriaSubcategoria::class)->find($categoriasubcategoria_id);
                    $categoriasubcategoriaGrupo = $entityManager->getRepository(CategoriasubcategoriaGrupo::class)->findBy(['categoriasubcategoria' => $categoriasubcategoria, 'grupo' => $grupo]);
                    if (!$categoriasubcategoriaGrupo) {
                        $categoriasubcategoriaGrupo = new CategoriasubcategoriaGrupo();
                        $categoriasubcategoriaGrupo->setGrupo($grupo);
                        $categoriasubcategoriaGrupo->setCategoriasubcategoria($categoriasubcategoria);
                        $entityManager->persist($categoriasubcategoriaGrupo);
                    }
                }
            }
            if (isset($data['lineasProducto'])) {

                foreach ($grupo->getLineaProductosGrupo() as $lineaProducto) {
                    $entityManager->remove($lineaProducto);
                }
                foreach ($data['lineasProducto'] as $lineaProducto_id) {
                    $lineaProducto = $lineaProductoRepository->find($lineaProducto_id);
                    $lineaProductoGrupo = new LineaProductoGrupo();
                    $lineaProductoGrupo->setGrupo($grupo);
                    $lineaProductoGrupo->setLineaProducto($lineaProducto);
                    $entityManager->persist($lineaProductoGrupo);
                }
            }

            $productos = $request->get('productos');

            if ($productos) {
                foreach ($productos as $producto_id) {
                    $producto = $entityManager->getRepository(Producto::class)->find($producto_id);
                    $productoGrupo = $entityManager->getRepository(ProductoGrupo::class)->findBy(['producto' => $producto, 'grupo' => $grupo]);
                    if (!$productoGrupo) {

                        $productoGrupo = new ProductoGrupo();
                        $productoGrupo->setGrupo($grupo);

                        $productoGrupo->setProducto($producto);
                        $entityManager->persist($productoGrupo);
                    }
                }
            }

            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Group edited successfully.'));
            return $this->redirectToRoute('tienda_grupo_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the group.'));
        }
        return $this->render('tienda/grupo/edit.html.twig', [
                    'grupo' => $grupo,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/desasociar", name="tienda_grupo_desasociar", methods={"GET","POST"})
     */
    public function desasociar(Grupo $grupo, Request $request) {
        $idProducto = $request->query->get('idProducto');

        $productoGrupo = $this->productoGrupoRepository->findOneBy(['producto' => $idProducto, 'grupo' => $grupo]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($productoGrupo);
        $entityManager->flush();

        return new Response('Ok');
    }

}
