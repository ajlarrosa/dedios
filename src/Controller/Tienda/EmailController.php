<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Email;
use App\Form\Tienda\EmailType;
use App\Repository\Tienda\EmailRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tienda/email")
 */
class EmailController extends AbstractController
{
    /**
     * @Route("/", name="tienda_email_index", methods={"GET"})
     */
    public function index(EmailRepository $emailRepository): Response
    {
        return $this->render('tienda/email/index.html.twig', [
            'emails' => $emailRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tienda_email_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $email = new Email();
        $form = $this->createForm(EmailType::class, $email);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($email);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_email_index');
        }

        return $this->render('tienda/email/new.html.twig', [
            'email' => $email,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_email_show", methods={"GET"})
     */
    public function show(Email $email): Response
    {
        return $this->render('tienda/email/show.html.twig', [
            'email' => $email,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_email_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Email $email): Response
    {
        $form = $this->createForm(EmailType::class, $email);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_email_index');
        }

        return $this->render('tienda/email/edit.html.twig', [
            'email' => $email,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_email_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Email $email): Response
    {
        if ($this->isCsrfTokenValid('delete'.$email->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($email);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_email_index');
    }
}
