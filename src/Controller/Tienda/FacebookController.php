<?php

namespace App\Controller\Tienda;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\General\MailerService;
use App\Service\ProductoService;
use App\Service\ImagenProductoService;
use App\Service\PrecioService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;
use App\Repository\ProductoRepository;
use App\Entity\ListaPrecio;

//use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/")
 */
class FacebookController extends AbstractController { 

    private $phpOfficeService;
    private $productoRepository;
    private $precioService;
    private $productoService;
    private $imagenProductoService;

    function __construct(PhpOfficeService $phpOfficeService, ProductoRepository $productoRepository, PrecioService $precioService, ProductoService $productoService, ImagenProductoService $imagenProductoService) {
        $this->phpOfficeService = $phpOfficeService;
        $this->productoRepository = $productoRepository;
        $this->precioService = $precioService;
        $this->productoService = $productoService;
        $this->imagenProductoService = $imagenProductoService;
    }

    /**
     * @Route("/admin/facebookCatalogue", name="facebook_catalogue")
     */
    public function facebookCatalogue(Request $request) {
        $filtros['precios'] = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;

        $productos = $this->productoRepository->getProductos($filtros);

        $spreadSheet = $this->exportarCsvCatalogueFacebook($productos, $request);

        $titulo = 'catalogoFacebook';

        return $this->phpOfficeService->exportCsv($spreadSheet, $titulo);
    }

    /*
     * 
     */

    private function exportarCsvCatalogueFacebook($productos, Request $request) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'id');
        $sheet->setCellValue('B1', 'title');
        $sheet->setCellValue('C1', 'description');
        $sheet->setCellValue('D1', 'availability');
        $sheet->setCellValue('E1', 'condition');
        $sheet->setCellValue('F1', 'price');
        $sheet->setCellValue('G1', 'link');
        $sheet->setCellValue('H1', 'image_link');
        $sheet->setCellValue('I1', 'brand');
        $sheet->setCellValue('J1', 'sale_price');
        $sheet->setCellValue('K1', 'item_group_id');
        $sheet->setCellValue('L1', 'color');
        $sheet->setCellValue('M1', 'material');
        $sheet->setCellValue('N1', 'product_type');
        $sheet->setCellValue('O1', 'size');
        $sheet->setCellValue('P1', 'rich_text_description');
        $sheet->setCellValue('Q1', 'additional_variant_attribute');

        $fila = 2;

        foreach ($productos as $producto) {

            //Caso que tenga colores
            if (sizeof($producto->getColores()) > 0) {
                foreach ($producto->getColores() as $color) {
                    if (sizeof($producto->getMedidas()) > 0) {
                        if (sizeof($producto->getLetras()) > 0) {
                            foreach ($producto->getMedidas() as $medida) {
                                foreach ($producto->getLetras() as $letra) {
                                    $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                                    $fila++;
                                }
                            }
                        } else {
                            $letra = null;
                            foreach ($producto->getMedidas() as $medida) {
                                $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                                $fila++;
                            }
                        }
                    } else {
                        $medida = null;
                        if (sizeof($producto->getLetras()) > 0) {
                            foreach ($producto->getLetras() as $letra) {
                                $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                                $fila++;
                            }
                        } else {
                            $letra = null;
                            $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                            $fila++;
                        }
                    }
                }
            } else {
                $color = null;
                if (sizeof($producto->getMedidas()) > 0) {
                    if (sizeof($producto->getLetras()) > 0) {
                        foreach ($producto->getMedidas() as $medida) {
                            foreach ($producto->getLetras() as $letra) {
                                $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                                $fila++;
                            }
                        }
                    } else {
                        $letra = null;
                        foreach ($producto->getMedidas() as $medida) {
                            $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                            $fila++;
                        }
                    }
                } else {
                    $medida = null;
                    if (sizeof($producto->getLetras()) > 0) {
                        foreach ($producto->getLetras() as $letra) {
                            $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                            $fila++;
                        }
                    } else {
                        $letra = null;
                        $this->addProduct($producto, $color, $medida, $letra, $fila, $sheet, $request);
                        $fila++;
                    }
                }
            }
        }

        return $spreadsheet;
    }

    /*
     * Add Product
     */

    private function addProduct($producto, $color, $medida, $letra, $fila, $sheet, Request $request) {
        $uniqueId = $this->getUniqueId($producto, $color, $medida, $letra);
        $sheet->setCellValue('A' . $fila, $uniqueId);

        $sheet->setCellValue('B' . $fila, str_replace('"', '""', $producto->getDescripcion()));

        $description = $this->getDescription($producto, $color, $medida, $letra);
        $sheet->setCellValue('C' . $fila, str_replace('"', '""', $description));

        $sheet->setCellValue('D' . $fila, 'available for order');

        $sheet->setCellValue('E' . $fila, 'new');

        if ($medida) {
            $medida = $medida->getId();
        }
        $precio = strval($this->precioService->getPrecioMedidaFinal($producto->getId(), $medida, ListaPrecio::LISTA_PRECIO_MINORISTA_WEB));
        $sheet->setCellValue('F' . $fila, str_replace(',', '.', $precio) . ' ARS');

        $link = $this->productoService->getRoute($producto->getId());

        $link = str_replace('/dedios/public/dedios/public', '/dedios/public', $request->getUriForPath($link));

        $sheet->setCellValue('G' . $fila, $link);

        $image_link = $request->getUriForPath('/uploads/productos/' . $producto->getId() . '/' . $this->imagenProductoService->getFirstImage($producto->getId()));
        $sheet->setCellValue('H' . $fila, $image_link);

        $sheet->setCellValue('I' . $fila, 'De Dios Joyas');

        //Sale price (precio de oferta)
        $sheet->setCellValue('J' . $fila, '');

        //Código de grupo
        $sheet->setCellValue('K' . $fila, $producto->getCodigo());

        //Color
        if ($color) {
            $sheet->setCellValue('L' . $fila, $color);
        } else {
            $sheet->setCellValue('L' . $fila, '');
        }
        //Material
        $sheet->setCellValue('M' . $fila, 'Plata 925');

        //Product type (Categoria > Subcategoria)
        $sheet->setCellValue('N' . $fila, $producto->getCategoriaSubcategoria()->getCategoria()->getDescripcion() . ' > ' . $producto->getCategoriaSubcategoria()->getSubcategoria()->getDescripcion());

        //Medida
        if ($medida) {
            $sheet->setCellValue('O' . $fila, $medida);
        } else {
            $sheet->setCellValue('O' . $fila, '');
        }

        //rich_text_description
        $sheet->setCellValue('P' . $fila, $producto->getDescripcionampliada());

        //additional_variant_attribute          
        if ($letra) {
            $sheet->setCellValue('Q' . $fila, 'Letra: ' . $letra);
        } else {
            $sheet->setCellValue('Q' . $fila, '');
        }
    }

    /*
     * Get Unique Id
     */

    private function getUniqueId($producto, $color, $medida, $letra) {
        $uniqueId = trim($producto->getCodigo());

        if ($color) {
            $uniqueId = $uniqueId . '_' . str_replace(' ', '', $color);
        }
        if ($medida) {
            $uniqueId = $uniqueId . '_' . str_replace(' ', '', $medida);
        }
        if ($letra) {
            $uniqueId = $uniqueId . '_' . str_replace(' ', '', $letra);
        }
        return $uniqueId;
    }

    /*
     * Get Description
     */

    private function getDescription($producto, $color, $medida, $letra) {
        $description = trim($producto->getDescripcion());

        if ($color) {
            $description = $description . ' Color: ' . $color;
        }
        if ($medida) {
            $description = $description . ' Medida: ' . $medida;
        }
        if ($letra) {
            $description = $description . ' Letra:' . $letra;
        }
        return $description;
    }

}
