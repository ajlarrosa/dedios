<?php

namespace App\Controller\Tienda;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ClienteProveedorRepository;
use App\Service\General\SessionService;

/**
 * @Route("/store/")
 */
class StoreController extends AbstractController {

    private $clienteProveedorRepository;
    private $sessionService;

    function __construct(
            SessionService $sessionService,
            ClienteProveedorRepository $clienteProveedorRepository
    ) {
        $this->clienteProveedorRepository = $clienteProveedorRepository;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("location", name="store_tienda_location")
     */
    public function location(): Response {

        $stores = $this->clienteProveedorRepository->findBy(['enabled' => true, 'visible' => true], ['razonSocial' => 'ASC']);

        return $this->render('tienda/store/stores.html.twig', [
                    'stores' => $stores
        ]);
    }

}
