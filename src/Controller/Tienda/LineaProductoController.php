<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\LineaProducto;
use App\Entity\ListaPrecio;
use App\Form\Tienda\LineaProductoType;
use App\Repository\Tienda\LineaProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\SubcategoriaService;
use App\Service\PrecioService;
use App\Repository\ProductoRepository;
use App\Repository\PrecioRepository;
use App\Service\General\GeneralService;
use App\Service\General\SessionService;

/**
 * @Route("/")
 */
class LineaProductoController extends AbstractController {

    private $productoRepository;
    private $precioRepository;
    private $precioService;
    private $subcategoriaService;
    private $generalService;
    private $sessionService;

    function __construct(
            SessionService $sessionService,
            ProductoRepository $productoRepository,
            PrecioRepository $precioRepository,
            SubcategoriaService $subcategoriaService,
            GeneralService $generalService,
            PrecioService $precioService
    ) {
        $this->productoRepository = $productoRepository;
        $this->subcategoriaService = $subcategoriaService;
        $this->precioRepository = $precioRepository;
        $this->generalService = $generalService;
        $this->precioService = $precioService;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("coleccion/{id}/{descripcion}", name="linea_producto_tienda_index", methods={"GET","POST"})
     */
    public function indexTienda(Request $request, LineaProducto $lineaProducto): Response {
        $filtros = $request->query->all();

        $filtros['lineaProducto'] = $lineaProducto->getId();
        
         $filtros['precios'] = $this->isGranted('ROLE_USER_MAYORISTA') ? ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB : ListaPrecio::LISTA_PRECIO_MINORISTA_WEB; 

        return $this->render('tienda/categoria/categoria.html.twig', [
                    'element' => $lineaProducto,
                    'cuenta' => $this->productoRepository->getCountProductos($filtros),
                    'cantShow' => 16,
                    'productos' => $this->productoRepository->getProductos($filtros),
                    'period' => $this->precioService->getPeriod($filtros)
        ]);
    }

    /**
     * @Route("admin/lineaproducto/", name="tienda_linea_producto_index", methods={"GET"})
     */
    public function index(Request $request, LineaProductoRepository $lineaProductoRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $lineasProductos = $paginator->paginate(
                $lineaProductoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/linea_producto/index.html.twig', [
                    'lineasProductos' => $lineasProductos,
        ]);
    }

    /**
     * @Route("admin/lineaproducto/new", name="tienda_linea_producto_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $lineaProducto = new LineaProducto();
        $form = $this->createForm(LineaProductoType::class, $lineaProducto);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $error = false;
            if ($_FILES['linea_producto']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['linea_producto']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $entityManager->persist($lineaProducto);
                    $entityManager->flush();
                    
                    $uploaddir = $this->getParameter('root_dir_linea_producto') . $lineaProducto->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }
                    
                    $name = date('YmdHis').'_'.$_FILES['linea_producto']['name']['imagen'];
                
                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['linea_producto']["tmp_name"]['imagen'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $lineaProducto->setImagen($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if (!$error) {
                $entityManager->persist($lineaProducto);
                $entityManager->flush();
                $this->addFlash('msgOk', $translator->trans('Product line created successfully.'));
                return $this->redirectToRoute('tienda_linea_producto_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the product line.'));
        }
        return $this->render('tienda/linea_producto/new.html.twig', [
                    'lineaProducto' => $lineaProducto,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/lineaproducto/enabled/{id}", name="tienda_linea_producto_enabled", methods={"GET"})
     *
     */
    public function enabled(Request $request, LineaProducto $lineaProducto, TranslatorInterface $translator) {
        if (!$lineaProducto) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected product line.'));
        } else {
            $enabled = $request->get('enabled');
            $entityManager = $this->getDoctrine()->getManager();
            $lineaProducto->setEnabled($enabled);
            $entityManager->persist($lineaProducto);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Product line status has been changed', ['%product_line%' => $lineaProducto->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_linea_producto_index'));
    }

    /**
     * @Route("admin/lineaproducto/{id}/edit", name="tienda_linea_producto_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, LineaProducto $lineaProducto, TranslatorInterface $translator): Response {
        $form = $this->createForm(LineaProductoType::class, $lineaProducto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($_FILES['linea_producto']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['linea_producto']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    return $this->render('tienda/linea_producto/edit.html.twig', [
                                'lineaProducto' => $lineaProducto,
                                'form' => $form->createView(),
                    ]);
                }

                $uploaddir = $this->getParameter('root_dir_linea_producto') . $lineaProducto->getId() . '/';
            
                if (!file_exists($uploaddir)) {
                    mkdir($uploaddir, 0777, true);
                }

                $name = date('YmdHis').'_'.$_FILES['linea_producto']['name']['imagen'];

                $uploadfile = $uploaddir . $name;

                $tmp_name = $_FILES['linea_producto']["tmp_name"]['imagen'];

                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    if($lineaProducto->getImagen()!='' && is_file($uploaddir.'../../../'.$lineaProducto->getImagen())){
                        unlink($uploaddir.'../../../'.$lineaProducto->getImagen());
                    }
                    $lineaProducto->setImagen($name);
                }else{
                    $error = true;
                }
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Product line edited successfully.'));
            return $this->redirectToRoute('tienda_linea_producto_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the product line.'));
        }
        return $this->render('tienda/linea_producto/edit.html.twig', [
                    'lineaProducto' => $lineaProducto,
                    'form' => $form->createView(),
        ]);
    }

}
