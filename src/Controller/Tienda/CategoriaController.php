<?php

namespace App\Controller\Tienda;

use App\Entity\Categoria;
use App\Entity\ListaPrecio;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SubcategoriaService;
use App\Service\PrecioService;
use App\Repository\ProductoRepository;
use App\Repository\PrecioRepository;
use App\Service\General\SessionService;
use App\Service\General\GeneralService;
use App\Repository\CategoriasubcategoriaRepository;

/**
 * @Route("/categoria/")
 */
class CategoriaController extends AbstractController {

    private $productoRepository;
    private $precioService;
    private $precioRepository;
    private $subcategoriaService;
    private $sessionService;
    private $generalService;
    private $categoriaSubcategoriaRepository;

    function __construct(
            SessionService $sessionService,
            ProductoRepository $productoRepository,
            PrecioRepository $precioRepository,
            SubcategoriaService $subcategoriaService,
            CategoriasubcategoriaRepository $categoriaSubcategoriaRepository,
            PrecioService $precioService,
            GeneralService $generalService
    ) {
        $this->productoRepository = $productoRepository;
        $this->subcategoriaService = $subcategoriaService;
        $this->precioService = $precioService;
        $this->precioRepository = $precioRepository;
        $this->sessionService = $sessionService;
        $this->generalService = $generalService;
        $this->categoriaSubcategoriaRepository  =$categoriaSubcategoriaRepository;
    }

    /**
     * @Route("{id}/{category}", name="categoria_tienda_index")
     */
    public function index(Request $request, Categoria $categoria): Response {
        $filtros = $request->query->all();        
      
        $filtros['categoria'] = $categoria->getId();
        
        $filtros['precios']  = $this->isGranted('ROLE_USER_MAYORISTA')  ? ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB : ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;      
        
        return $this->render('tienda/categoria/categoria.html.twig', [
                    'element' => $categoria,
                    'categoria'=> $categoria,
                    'subcategorias' => $this->subcategoriaService->getSubcategories(['categoria' => $categoria->getId()]),
                    'cuenta' => $this->productoRepository->getCountProductos($filtros),                    
                    'cantShow' => 16,
                    'productos' => $this->productoRepository->getProductos($filtros),
                    'period' => $this->precioService->getPeriod($filtros),
        ]);
    }
}
