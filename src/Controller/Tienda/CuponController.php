<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Cupon;
use App\Entity\Tienda\Pedido;
use App\Form\Tienda\CuponType;
use App\Repository\Tienda\CuponRepository;
use App\Repository\Tienda\PedidoRepository;
use App\Service\Tienda\CuponService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/")
 */
class CuponController extends AbstractController {

    private $translator;
    private $cuponRepository;
    private $pedidoRepository;
    private $cuponService;

    public function __construct(
            TranslatorInterface $translator,
            CuponRepository $cuponRepository,
            PedidoRepository $pedidoRepository,
            CuponService $cuponService
    ) {
        $this->translator = $translator;
        $this->cuponRepository = $cuponRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->cuponService = $cuponService;
    }

    /**
     * @Route("admin/cupon/", name="tienda_cupon_index", methods={"GET"})
     */
    public function index(Request $request, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $cupones = $paginator->paginate(
                $this->cuponRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/cupon/index.html.twig', [
                    'cupones' => $cupones
        ]);
    }

    /**
     * @Route("admin/cupon/new", name="tienda_cupon_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $cupon = new Cupon();
        $form = $this->createForm(CuponType::class, $cupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cupon);
            $entityManager->flush();
            $this->addFlash('msgOk', $this->translator->trans('Coupon created successfully.'));
            return $this->redirectToRoute('tienda_cupon_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to create the coupon.'));
        }
        return $this->render('tienda/cupon/new.html.twig', [
                    'cupon' => $cupon,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/cupon/activate/{id}", name="tienda_cupon_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Cupon $cupon) {
        if (!$cupon) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to change the status of the selected coupon.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $cupon->setEnabled($activo);
            $entityManager->persist($cupon);
            $entityManager->flush();
            $this->addFlash('msgOk', $this->translator->trans('Coupon status has been changed'));
        }
        return $this->redirect($this->generateUrl('tienda_cupon_index'));
    }

    /**
     * @Route("admin/cupon/{id}", name="tienda_cupon_show", methods={"GET"})
     */
    public function show(Cupon $cupon): Response {
        return $this->render('tienda/cupon/show.html.twig', [
                    'cupon' => $cupon,
        ]);
    }

    /**
     * @Route("admin/cupon/{id}/edit", name="tienda_cupon_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cupon $cupon): Response {
        $form = $this->createForm(CuponType::class, $cupon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $this->translator->trans('Coupon edited successfully.'));
            return $this->redirectToRoute('tienda_cupon_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $this->translator->trans('It was not possible to edit the coupon.'));
        }
        return $this->render('tienda/cupon/edit.html.twig', [
                    'cupon' => $cupon,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("admin/cupon/{id}", name="tienda_cupon_delete", methods={"POST"})
     */
    public function delete(Request $request, Cupon $cupon): Response {
        if ($this->isCsrfTokenValid('delete' . $cupon->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cupon);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_cupon_index');
    }

    /**
     * @Route("cupon/{pedidoId}/addPedido", name="cupon_add_pedido", methods={"GET"})     
     */
    public function addPedido(Request $request, $pedidoId): JsonResponse {
        $searchCupon = $this->cuponService->getActiveCupon($request->query->get('cupon'));

        $pedido = $this->pedidoRepository->find($pedidoId);

        if (count($searchCupon) === 0) {
            if ($pedido) {
                $this->editCuponInPedido($pedido, null);
            }

            return new JsonResponse([
                'message' => $this->translator->trans('There is no active promotion for that coupon code. If you have another please enter it.'),
                'status' => false
            ]);
        }

        $cupon = $searchCupon[0];

        if ($cupon->getUsos() !== -1 && $cupon->restarUso() === false) {
            return new JsonResponse([
                'message' => $this->translator->trans('The maximum number of uses for this coupon has been reached.'),
                'status' => false
            ]);
        }
        
        if ($cupon->getUsos() !== -1) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cupon);
            $entityManager->flush();
        }
        
        if ($pedido) {
            if (is_null($cupon->getMontoMinimo()) || $cupon->getMontoMinimo() <= $pedido->getTotal()) {
                $this->editCuponInPedido($pedido, $cupon);

                $this->addFlash("msgOk", $this->translator->trans('The coupon was added to your order.'));

                return new JsonResponse([
                    'status' => true
                ]);
            }

            return new JsonResponse([
                'message' => $this->translator->trans('You still do not reach the minimum amount to be able to use this coupon.'),
                'status' => false
            ]);
        }

        return new JsonResponse([
            'cupon' => [
                'codigo' => $cupon->getCodigo(),
                'tipoDescuento' => $cupon->getTipoDescuento(),
                'descuento' => $cupon->getDescuento(),
                'montoMinimo' => $cupon->getMontoMinimo(),
                'montoFijo' => $cupon->getMontoFijo(),
            ],
            'status' => true
        ]);
    }

    private function editCuponInPedido(Pedido $pedido, $cuponCode) {
        $em = $this->getDoctrine()->getManager();
        $pedido->setCupon($cuponCode);
        $em->persist($pedido);
        $em->flush();
    }

}
