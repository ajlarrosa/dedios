<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Pedido;
use App\Entity\Tienda\FormaPago;
use App\Entity\ListaPrecio;
use App\Entity\Tienda\FormatoMail;
use App\Service\ReciboClienteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\Tienda\MercadoPagoRepository;
use App\Repository\Tienda\FormaPagoRepository;
use App\Entity\Common\Email;
use App\Service\General\MailerService;
use App\Service\ShipNow\ShipNowService;
use App\Service\Tienda\ConfiguracionService;
use App\Service\Tienda\PedidoService;
use App\Service\Tienda\FormatoMailService;
use App\Service\Tienda\MercadoPagoService;

/**
 * MercadoPago controller.
 *
 * @Route("/")
 */
class MercadoPagoController extends AbstractController {

    private $mercadoPagoRepository;
    private $mercadoPagoService;
    private $formaPagoRepository;
    private $reciboClienteService;
    private $pedidoService;
    private $configuracion;
    private $mailerService;
    private $formatoMailService;
    private $shipNowService;
    private $translator;

    function __construct(MercadoPagoService $mercadoPagoService, MercadoPagoRepository $mercadoPagoRepository, FormatoMailService $formatoMailService, PedidoService $pedidoService, FormaPagoRepository $formaPagoRepository, ReciboClienteService $reciboClienteService, MailerService $mailerService, ConfiguracionService $configuracionService, ShipNowService $shipNowService, TranslatorInterface $translator) {
        $this->mercadoPagoRepository = $mercadoPagoRepository;
        $this->mercadoPagoService = $mercadoPagoService;
        $this->formaPagoRepository = $formaPagoRepository;
        $this->reciboClienteService = $reciboClienteService;
        $this->mailerService = $mailerService;
        $this->shipNowService = $shipNowService;
        $this->formatoMailService = $formatoMailService;
        $this->pedidoService = $pedidoService;
        $this->translator = $translator;
        $this->configuracion = $configuracionService->getLastConfiguration();
    }

    /**
     * Lists all cliente entities.
     *
     * @Route("operador/mercadoPago/", name="mercadopago_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, PaginatorInterface $paginator) {

        $filtros = $request->query->all();

        $operacionesMercadoPago = $paginator->paginate(
                $this->mercadoPagoRepository->filtro($filtros),
                $request->query->getInt('page', 1),
                15
        );
        
        return $this->render('tienda/mercadopago/index.html.twig', [
                    'operacionesMercadoPago' => $operacionesMercadoPago
        ]);
    }

    /**
     * @Route("mercadoPago/{id}/procesarPago", name="mercadopago_procesar_pago")    
     */
    public function procesarPagoAction(Pedido $pedido, Request $request) {

        $em = $this->getDoctrine()->getManager();

        $mercadoPago = $this->mercadoPagoRepository->findOneBy(['pedido' => $pedido, 'enabled' => true]);
        if (!$mercadoPago) {
            $preference['preference_id'] = $request->get('preference_id');
            $preference['external_reference'] = $request->get('external_reference');
            $preference['site_id'] = $request->get('site_id');
            $mercadoPago = $this->mercadoPagoService->create($preference, $pedido);
        }

        $mercadoPago->setCollectionId($request->get('collection_id'));
        $mercadoPago->setCollectionStatus($request->get('collection_status'));
        $mercadoPago->setPaymentType($request->get('payment_type'));
        $mercadoPago->setMerchantOrderId($request->get('merchant_order_id'));
        $mercadoPago->setProcessingMode($request->get('processing_mode'));
        $mercadoPago->setMerchantAccountId($request->get('merchant_account_id'));
        $mercadoPago->setPaymentId($request->get('payment_id'));
        $mercadoPago->setStatus($request->get('status'));

        if ($request->get('status') === 'approved') {
            $pedido->setPagado(true);
            $pedido->setFechaPagado(new \DateTime());
            $data = [];
            $data['fecha'] = new \DateTime();
            $data['importe'] = $pedido->getTotal();

            $data['moneda'] = ListaPrecio::COD_PESOS;
            $data['formaPago'] = $this->formaPagoRepository->find(FormaPago::MERCADOPAGO_ID);
            if ($this->isGranted("ROLE_USER_TIENDA")) {
                $data['cliente'] = $pedido->getUser()->getCliente();
                $reciboCliente = $this->reciboClienteService->create((object) $data);
                if ($reciboCliente !== FALSE) {
                    $mercadoPago->setPagoImputadoCC(TRUE);
                }
            } else {
                $mercadoPago->setPagoImputadoCC(TRUE);
                /*
                 * Ver si hay que registrar la cobranza de alguna manera en una cuenta corriente general
                 * aunque no le veo uso
                 */
            }

            if ($this->pedidoService->sendEmailOrderConfirmation($pedido) === false) {
                $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
            }

            $this->generarEnvioShipNow($pedido);
        }

        $em->persist($mercadoPago);
        $em->flush();

        if ($pedido->getPagado()) {

            $formatoEmail = $this->formatoMailService->getFormatoMail([
                'type' => FormatoMail::COD_CONFIRM_PAYMENT_CLIENT,
                'enabled' => true,
                'configuracion' => $this->configuracion
            ]);

            if (sizeof($formatoEmail) > 0) {
                //Envío de mail al cliente

                if ($pedido->getUser() != null) {
                    $sendMail = $pedido->getUser()->getMail();
                } else {
                    $sendMail = $pedido->getClienteWeb()->getEmail();
                }

                $emailCliente = new Email();

                $emailCliente->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
                $emailCliente->setSendTo($sendMail);
                $emailCliente->setTemplate('email/confirmPagoCliente.html.twig');

                $emailCliente->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'configuracion' => $this->configuracion]);
                $response = $this->mailerService->enviarMail($emailCliente);
            } else {
                $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
            }
            $this->addFlash("msgOk", "Pago procesado.");
        } else {
            if ($request->get('status') === 'rejected') {
                $this->addFlash("msgError", "El pago ha sido rechazado. Si quieres puedes vovelr a intentarlo con otro medio de pago.");
                return $this->redirectToRoute('tienda_formapago_mercadopago', [
                            'id' =>
                            $pedido->getId()
                ]);
            }
            if ($request->get('status') === 'cancelled') {
                $this->addFlash("msgError", "El pago ha sido cancelado. Si quieres puedes vovelr a intentarlo con otro medio de pago.");
                return $this->redirectToRoute('tienda_formapago_mercadopago', [
                            'id' =>
                            $pedido->getId()
                ]);
            }
        }

        $responsePage = $this->redirectToRoute('gracias', [
            'currency' => 'ARS',
            'totalvalue' => $pedido->getTotal()
        ]);
      
        $cookies = $request->cookies;
        if ($cookies->has('idPedido')) {
            $responsePage->headers->clearCookie('idPedido', '/', null);
            $responsePage->headers->clearCookie('detallePedido', '/', null);
            $responsePage->headers->clearCookie('cantidadProductos', '/', null);
        }
        return $responsePage;
    }

    /**
     * @Route("mercadoPago/verificarPagosPendientes", name="mercadopago_verificar_pagos_pendientes")
     * 
     */
    public function verificarPagosPendientesAction(Request $request) {
        
        if ($request->get('token') != $this->getParameter('secret')) {
            return false;
        }
        
        $em = $this->getDoctrine()->getManager();
        $mercadoPagos = $this->mercadoPagoRepository->findPendientes();

        $respuesta['cantidadPendientes'] = sizeof($mercadoPagos);
        $respuesta['cantidadProcesados'] = 0;
        $respuesta['cantidadAprobados'] = 0;
        $respuesta['cantidadActualizados'] = 0;
        $respuesta['cantidadErrorCurl'] = 0;
        $respuesta['movimientos'] = [];
        
        foreach ($mercadoPagos as $mercadoPago) {
            $arrayMovimientos = [];
            $pedido = $mercadoPago->getPedido();
            //Armar un método en el servicio mercadoPago para lo siguiente
            if (!is_null($mercadoPago->getPaymentId())) {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.mercadopago.com/v1/payments/" . $mercadoPago->getPaymentId() . "?access_token=" . $this->getParameter('access_token_mercadopago'),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if (!$err) {
                    $response = json_decode($response);
                    $respuesta['cantidadProcesados']++;


                    if ($response->status === 'approved') {
                        $respuesta['cantidadAprobados']++;
                        $arrayMovimientos = [
                            'idPedido' => $pedido->getId(),
                            'EstadoAnterior' => $mercadoPago->getStatus(),
                            'EstadoActual' => 'approved'
                        ];
                        $mercadoPago->setStatus($response->status);
                        if (!$pedido->getPagado()) {

                            $pedido->setPagado(true);
                            $pedido->setFechaPagado(new \DateTime());
                            $em->persist($pedido);

                            if ($pedido->getPagado()) {
                                //Envío de mail a Dedios
                                $emailDedios = new Email();

                                $emailDedios->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
                                $emailDedios->setSendTo($this->getParameter('email_receiver'));
                                $emailDedios->setTemplate('email/confirmPagoDedios.html.twig');

                                $emailDedios->setParameters(['pedido' => $pedido, 'configuracion' => $this->configuracion]);
                                $response = $this->mailerService->enviarMail($emailDedios);

                                //Envio de mail a Cliente
                                $formatoEmail = $this->formatoMailService->getFormatoMail([
                                    'type' => FormatoMail::COD_CONFIRM_PAYMENT_CLIENT,
                                    'enabled' => true,
                                    'configuracion' => $this->configuracion
                                ]);

                                if (sizeof($formatoEmail) > 0) {
                                    //Envío de mail al cliente

                                    if ($pedido->getUser() != null) {
                                        $sendMail = $pedido->getUser()->getMail();
                                    } else {
                                        $sendMail = $pedido->getClienteWeb()->getEmail();
                                    }

                                    $emailCliente = new Email();

                                    $emailCliente->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
                                    $emailCliente->setSendTo($sendMail);
                                    $emailCliente->setTemplate('email/confirmPagoCliente.html.twig');

                                    $emailCliente->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'configuracion' => $this->configuracion]);
                                    $response = $this->mailerService->enviarMail($emailCliente);
                                }

                                $this->generarEnvioShipNow($pedido);
                            }
                        }
                    } else if ($response->status !== $mercadoPago->getStatus()) {
                        $respuesta['cantidadActualizados']++;
                        $mercadoPago->setStatus($response->status);

                        $arrayMovimientos = [
                            'idPedido' => $pedido->getId(),
                            'EstadoAnterior' => $mercadoPago->getStatus(),
                            'EstadoActual' => $response->status
                        ];

                        $em->persist($mercadoPago);
                    } else {

                        $someDate = $pedido->getFecha();
                        $now = new \DateTime();

                        if ($someDate->diff($now)->days > 10) {
                            $pedido->setEstado(Pedido::ABANDONADO);
                            $em->persist($pedido);
                        }

                        $respuesta['cantidadActualizados']++;
                        $arrayMovimientos = [
                            'idPedido' => $pedido->getId(),
                            'EstadoAnterior' => $mercadoPago->getStatus(),
                            'EstadoActual' => $response->status
                        ];
                    }
                } else {
                    $arrayMovimientos = [
                        'idPedido' => $pedido->getId(),
                        'EstadoAnterior' => $mercadoPago->getStatus(),
                        'EstadoActual' => ''
                    ];
                    $respuesta['cantidadErrorCurl']++;
                }
                $respuesta['movimientos'][] = $arrayMovimientos;
            } else {

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.mercadopago.com/v1/payments/search?external_reference=" . urlencode($mercadoPago->getPedido()->getExternalReference()) . "&access_token=" . $this->getParameter('access_token_mercadopago'),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if (!$err) {
                    $response = json_decode($response);


                    if (sizeof($response->results) > 0) {

                        $response = $response->results[$response->paging->total - 1];

                        $respuesta['cantidadProcesados']++;
                        $mercadoPago->setPaymentType($response->payment_type_id);
                        $mercadoPago->setMerchantAccountId($response->merchant_account_id);
                        $mercadoPago->setPaymentId($response->id);
                        $mercadoPago->setStatus($response->status);

                        if ($response->status === 'approved') {
                            $respuesta['cantidadAprobados']++;
                            $arrayMovimientos = [
                                'idPedido' => $pedido->getId(),
                                'EstadoAnterior' => $mercadoPago->getStatus(),
                                'EstadoActual' => 'approved'
                            ];
                            $mercadoPago->setStatus($response->status);
                            if (!$pedido->getPagado(true)) {
                                $pedido->setPagado(true);
                                $pedido->setFechaPagado(new \DateTime());
                                $em->persist($pedido);

                                if ($pedido->getPagado()) {
                                    //Envío de mail a Dedios
                                    $emailDedios = new Email();

                                    $emailDedios->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
                                    $emailDedios->setSendTo($this->getParameter('email_receiver'));
                                    $emailDedios->setTemplate('email/confirmPagoDedios.html.twig');

                                    $emailDedios->setParameters(['pedido' => $pedido, 'configuracion' => $this->configuracion]);
                                    $response = $this->mailerService->enviarMail($emailDedios);
                                    $this->generarEnvioShipNow($pedido);

                                    //Envio de mail a Cliente
                                    $formatoEmail = $this->formatoMailService->getFormatoMail([
                                        'type' => FormatoMail::COD_CONFIRM_PAYMENT_CLIENT,
                                        'enabled' => true,
                                        'configuracion' => $this->configuracion
                                    ]);

                                    if (sizeof($formatoEmail) > 0) {
                                        //Envío de mail al cliente

                                        if ($pedido->getUser() != null) {
                                            $sendMail = $pedido->getUser()->getMail();
                                        } else {
                                            $sendMail = $pedido->getClienteWeb()->getEmail();
                                        }

                                        $emailCliente = new Email();

                                        $emailCliente->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
                                        $emailCliente->setSendTo($sendMail);
                                        $emailCliente->setTemplate('email/confirmPagoCliente.html.twig');

                                        $emailCliente->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'configuracion' => $this->configuracion]);
                                        $response = $this->mailerService->enviarMail($emailCliente);
                                    }
                                }
                            }
                        } else if ($response->status !== $mercadoPago->getStatus()) {
                            $respuesta['cantidadActualizados']++;

                            $arrayMovimientos = [
                                'idPedido' => $pedido->getId(),
                                'EstadoAnterior' => $mercadoPago->getStatus(),
                                'EstadoActual' => $response->status
                            ];

                            $mercadoPago->setStatus($response->status);
                            $em->persist($mercadoPago);
                        } else {
                            $someDate = $pedido->getFecha();
                            $now = new \DateTime();

                            if ($someDate->diff($now)->days > 10) {
                                $pedido->setEstado(Pedido::ABANDONADO);
                                $em->persist($pedido);
                            }
                            if ($pedido->getFecha())
                                $arrayMovimientos = [
                                    'idPedido' => $pedido->getId(),
                                    'EstadoAnterior' => $mercadoPago->getStatus(),
                                    'EstadoActual' => $response->status
                                ];
                        }
                    } else {
                        $someDate = $pedido->getFecha();
                        $now = new \DateTime();

                        if ($someDate->diff($now)->days > 10) {
                            $pedido->setEstado(Pedido::ABANDONADO);
                            $em->persist($pedido);
                        }
                        if ($pedido->getFecha())
                            $arrayMovimientos = [
                                'idPedido' => $pedido->getId(),
                                'EstadoAnterior' => $mercadoPago->getStatus(),
                                'EstadoActual' => $response->status
                            ];
                    }
                } else {
                    $arrayMovimientos = [
                        'idPedido' => $pedido->getId(),
                        'EstadoAnterior' => $mercadoPago->getStatus(),
                        'EstadoActual' => '',
                        'Error' => $err
                    ];
                    $respuesta['cantidadErrorCurl']++;
                }
                $respuesta['movimientos'][] = $arrayMovimientos;
            }
        }
        $em->flush();

        return new JsonResponse($respuesta);
    }

    /*
     * Generar envio ship now
     */

    private function generarEnvioShipNow(Pedido $pedido) {
        if ($pedido->getTipoEnvio() == Pedido::SHIP_NOW) {
            //Generar envio
            $this->shipNowService->createOrder($pedido);
        }
    }
}
