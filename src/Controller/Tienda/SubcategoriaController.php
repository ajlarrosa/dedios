<?php

namespace App\Controller\Tienda;

use App\Entity\Subcategoria;
use App\Entity\Categoria;
use App\Entity\ListaPrecio;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SubcategoriaService;
use App\Service\PrecioService;
use App\Repository\ProductoRepository;
use App\Repository\PrecioRepository;
use App\Repository\CategoriasubcategoriaRepository;
use App\Service\General\SessionService;

/**
 * @Route("/subcategoria/")
 */
class SubcategoriaController extends AbstractController {

    private $productoRepository;
    private $precioRepository;
    private $precioService;
    private $subcategoriaService;
    private $categoriaSubcategoriaRepository;
    private $sessionService;

    function __construct(
            SessionService $sessionService,
            ProductoRepository $productoRepository,
            PrecioRepository $precioRepository,
            SubcategoriaService $subcategoriaService,
            CategoriasubcategoriaRepository $categoriaSubcategoriaRepository,
            PrecioService $precioService
    ) {
        $this->productoRepository = $productoRepository;
        $this->subcategoriaService = $subcategoriaService;
        $this->precioRepository = $precioRepository;
        $this->categoriaSubcategoriaRepository = $categoriaSubcategoriaRepository;
        $this->precioService = $precioService;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("{id}-{category_id}/{category}/{subcategory}", name="subcategoria_tienda_index")
     */
    public function index(Request $request, Subcategoria $id, Categoria $category_id): Response {
        $subcategoria = $id;
        $filtros = $request->query->all();
        $filtros['categoria'] = $category_id;
        $filtros['subcategoria'] = $subcategoria->getId();

        $categoriaSubcategoria = $this->categoriaSubcategoriaRepository->findOneBy([
            'categoria' => $category_id,
            'subcategoria' => $subcategoria
        ]);

        $filtros['precios'] = ($this->isGranted('ROLE_USER_MAYORISTA')) ? ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB : ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;

        return $this->render('tienda/categoria/categoria.html.twig', [
                    'element' => $categoriaSubcategoria,
                    'categoria' => $category_id,
                    'cuenta' => $this->productoRepository->getCountProductos($filtros),
                    'cantShow' => 16,
                    'productos' => $this->productoRepository->getProductos($filtros),
                    'period' => $this->precioService->getPeriod($filtros)
        ]);
    }

}
