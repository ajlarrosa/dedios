<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\TipoImagen;
use App\Form\Tienda\TipoImagenType;
use App\Repository\Tienda\TipoImagenRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tienda/tipo/imagen")
 */
class TipoImagenController extends AbstractController
{
    /**
     * @Route("/", name="tienda_tipo_imagen_index", methods={"GET"})
     */
    public function index(TipoImagenRepository $tipoImagenRepository): Response
    {
        return $this->render('tienda/tipo_imagen/index.html.twig', [
            'tipo_imagens' => $tipoImagenRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tienda_tipo_imagen_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tipoImagen = new TipoImagen();
        $form = $this->createForm(TipoImagenType::class, $tipoImagen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tipoImagen);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_tipo_imagen_index');
        }

        return $this->render('tienda/tipo_imagen/new.html.twig', [
            'tipo_imagen' => $tipoImagen,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_tipo_imagen_show", methods={"GET"})
     */
    public function show(TipoImagen $tipoImagen): Response
    {
        return $this->render('tienda/tipo_imagen/show.html.twig', [
            'tipo_imagen' => $tipoImagen,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_tipo_imagen_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TipoImagen $tipoImagen): Response
    {
        $form = $this->createForm(TipoImagenType::class, $tipoImagen);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_tipo_imagen_index');
        }

        return $this->render('tienda/tipo_imagen/edit.html.twig', [
            'tipo_imagen' => $tipoImagen,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_tipo_imagen_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TipoImagen $tipoImagen): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tipoImagen->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tipoImagen);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_tipo_imagen_index');
    }
}
