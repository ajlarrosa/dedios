<?php

namespace App\Controller\Tienda;

use App\Repository\ImagenProductoRepository;
use App\Entity\Tienda\SeccionConfiguracion;
use App\Form\Tienda\SeccionConfiguracionType;
use App\Repository\Tienda\SeccionConfiguracionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;

/**
 * @Route("/admin/seccion")
 */
class SeccionConfiguracionController extends AbstractController {

    private $imagenProductoRepository;
    private $generalService;

    function __construct(ImagenProductoRepository $imagenProductoRepository, GeneralService $generalService) {
        $this->imagenProductoRepository = $imagenProductoRepository;
        $this->generalService = $generalService;
    }

    /**
     * @Route("/", name="tienda_seccion_configuracion_index", methods={"GET"})
     */
    public function index(Request $request, SeccionConfiguracionRepository $seccionConfiguracionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $seccionesConfiguracion = $paginator->paginate(
                $seccionConfiguracionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/seccion_configuracion/index.html.twig', [
                    'seccionesConfiguracion' => $seccionesConfiguracion,
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_seccion_configuracion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, SeccionConfiguracion $seccionConfiguracion, TranslatorInterface $translator) {
        if (!$seccionConfiguracion) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected section.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $seccionConfiguracion->setEnabled($activo);
            $entityManager->persist($seccionConfiguracion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Section status has been changed', ['%section%' => $seccionConfiguracion->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_seccion_configuracion_index'));
    }

    /**
     * @Route("/new", name="tienda_seccion_configuracion_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $seccionConfiguracion = new SeccionConfiguracion();
        $form = $this->createForm(SeccionConfiguracionType::class, $seccionConfiguracion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $error = false;
            if ($_FILES['seccion_configuracion']['size']['imagen1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion_configuracion']['type']['imagen1'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    
                    $entityManager->persist($seccionConfiguracion);
                    $entityManager->flush();
                    
                    $uploaddir = $this->getParameter('root_dir_seccion_config') . $seccionConfiguracion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }
                    
                    $name = date('YmdHis').'_'.$_FILES['seccion_configuracion']['name']['imagen1'];
                
                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion_configuracion']["tmp_name"]['imagen1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $seccionConfiguracion->setImagen1($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if ($_FILES['seccion_configuracion']['size']['imagen2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion_configuracion']['type']['imagen2'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    
                    $entityManager->persist($seccionConfiguracion);
                    $entityManager->flush();
                    
                    $uploaddir = $this->getParameter('root_dir_seccion_config') . $seccionConfiguracion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }
                    
                    $name = date('YmdHis').'_'.$_FILES['seccion_configuracion']['name']['imagen2'];
                
                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion_configuracion']["tmp_name"]['imagen2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $seccionConfiguracion->setImagen2($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if ($seccionConfiguracion->getProducto1()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto1()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto2()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto2()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto3()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto1()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto4()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto4()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto5()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto5()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto6()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto6()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }

            if (!$error) {
                $entityManager->persist($seccionConfiguracion);
                $entityManager->flush();
                $this->addFlash('msgOk', $translator->trans('Section created successfully.'));
                return $this->redirectToRoute('tienda_seccion_configuracion_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the section.'));
        }
        return $this->render('tienda/seccion_configuracion/new.html.twig', [
                    'seccionConfiguracion' => $seccionConfiguracion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_seccion_configuracion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SeccionConfiguracion $seccionConfiguracion, TranslatorInterface $translator): Response {
        $form = $this->createForm(SeccionConfiguracionType::class, $seccionConfiguracion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $error = false;
            if ($_FILES['seccion_configuracion']['size']['imagen1'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion_configuracion']['type']['imagen1'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $uploaddir = $this->getParameter('root_dir_seccion_config') . $seccionConfiguracion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }

                    $name = date('YmdHis').'_'.$_FILES['seccion_configuracion']['name']['imagen1'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion_configuracion']["tmp_name"]['imagen1'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if($seccionConfiguracion->getImagen1()!='' && is_file($uploaddir.'../../../'.$seccionConfiguracion->getImagen1())){
                            unlink($uploaddir.'../../../'.$seccionConfiguracion->getImagen1());
                        }
                        $seccionConfiguracion->setImagen1($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if ($_FILES['seccion_configuracion']['size']['imagen2'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion_configuracion']['type']['imagen2'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    
                    $uploaddir = $this->getParameter('root_dir_seccion_config') . $seccionConfiguracion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }

                    $name = date('YmdHis').'_'.$_FILES['seccion_configuracion']['name']['imagen2'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion_configuracion']["tmp_name"]['imagen2'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if($seccionConfiguracion->getImagen2()!='' && is_file($uploaddir.'../../../'.$seccionConfiguracion->getImagen2())){
                            unlink($uploaddir.'../../../'.$seccionConfiguracion->getImagen2());
                        }
                        $seccionConfiguracion->setImagen2($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if ($seccionConfiguracion->getProducto1()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto1()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto2()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto2()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto3()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto1()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto4()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto4()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto5()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto5()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }
            if ($seccionConfiguracion->getProducto6()) {

                if (!$this->imagenProductoRepository->findFirstImage($seccionConfiguracion->getProducto6()->getId())) {
                    $this->addFlash('msgError', $translator->trans('The product {{ codigo }} hasnt got any image', ['{{ codigo }}' => $seccionConfiguracion->getProducto1()->getCodigo()]));
                    $error = true;
                }
            }

            if (!$error) {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('msgOk', $translator->trans('Section edited successfully.'));
                return $this->redirectToRoute('tienda_seccion_configuracion_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the sections.'));
        }
        return $this->render('tienda/seccion_configuracion/edit.html.twig', [
                    'seccionConfiguracion' => $seccionConfiguracion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_seccion_configuracion_show", methods={"GET"})
     */
    public function show(SeccionConfiguracion $seccionConfiguracion): Response {
        return $this->render('tienda/seccion_configuracion/show.html.twig', [
                    'seccionConfiguracion' => $seccionConfiguracion,
        ]);
    }

}
