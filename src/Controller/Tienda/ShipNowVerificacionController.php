<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\ShipNowVerificacion;
use App\Form\Tienda\ShipNowVerificacionType;
use App\Repository\Tienda\ShipNowVerificacionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/")
 */
class ShipNowVerificacionController extends AbstractController {

    private $translator;
    private $shipNowVerificacionRepository;

    function __construct(ShipNowVerificacionRepository $shipNowVerificacionRepository, TranslatorInterface $translator) {
        $this->shipNowVerificacionRepository = $shipNowVerificacionRepository;
        $this->translator = $translator;
    }

    /**
     * @Route("/operator/shipNowVerificacion", name="shipNowVerificacion_index", methods={"GET"})
     */
    public function index(Request $request, ShipNowVerificacionRepository $shipNowVerificacionRepository, PaginatorInterface $paginator): Response {

        $filtros = $request->query->all();

        $shipNowVerificacions = $paginator->paginate(
                $shipNowVerificacionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );

        return $this->render('tienda/shipNowVerificacion/index.html.twig', [
                    'shipNowVerificacions' => $shipNowVerificacions,
        ]);
    }

    /**
     * @Route("/operator/shipNowVerificacion/new", name="shipNowVerificacion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $shipNowVerificacion = new ShipNowVerificacion();
        $form = $this->createForm(ShipNowVerificacionType::class, $shipNowVerificacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($shipNowVerificacion);
            $entityManager->flush();

            return $this->redirectToRoute('shipNowVerificacion_index');
        }

        return $this->render('tienda/shipNowVerificacion/new.html.twig', [
                    'shipNowVerificacion' => $shipNowVerificacion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/operator/shipNowVerificacion/{id}", name="shipNowVerificacion_show", methods={"GET"})
     */
    public function show(ShipNowVerificacion $shipNowVerificacion): Response {
        return $this->render('tienda/shipNowVerificacion/show.html.twig', [
                    'shipNowVerificacion' => $shipNowVerificacion,
        ]);
    }

    /**
     * @Route("/operator/shipNowVerificacion/{id}/edit", name="shipNowVerificacion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ShipNowVerificacion $shipNowVerificacion): Response {
        $form = $this->createForm(ShipNowVerificacionType::class, $shipNowVerificacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('shipNowVerificacion_index');
        }

        return $this->render('tienda/shipNowVerificacion/edit.html.twig', [
                    'shipNowVerificacion' => $shipNowVerificacion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/operator/shipNowVerificacion/{id}", name="shipNowVerificacion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ShipNowVerificacion $shipNowVerificacion): Response {
        if ($this->isCsrfTokenValid('delete' . $shipNowVerificacion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($shipNowVerificacion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('shipNowVerificacion_index');
    }

}
