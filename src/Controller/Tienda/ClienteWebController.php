<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\ClienteWeb;
use App\Form\Tienda\ClienteWebGestionType;
use App\Repository\Tienda\ClienteWebRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/admin/cliente/web")
 */
class ClienteWebController extends AbstractController {

    /**
     * @Route("/", name="tienda_cliente_web_index", methods={"GET"})
     */
    public function index(Request $request, ClienteWebRepository $clienteWebRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {
        $filtros = $request->query->all();

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $clientesWeb = $clienteWebRepository->filter($filtros)->getQuery()->getResult();

            $spreadSheet = $this->excelInforme($clientesWeb, $filtros);
            $nombreInforme = 'Clientes Web';

            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }

        $cliente_webs = $paginator->paginate(
                $clienteWebRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/cliente_web/index.html.twig', [
                    'cliente_webs' => $cliente_webs,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($clientesWeb, $parametros) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];

        $fila = 1;
        $sheet->setCellValue('A' . $fila, 'Fecha');
        $sheet->setCellValue('B' . $fila, 'Nombre');
        $sheet->setCellValue('C' . $fila, 'Email');
        $sheet->setCellValue('D' . $fila, 'Teléfono');
        $sheet->setCellValue('E' . $fila, 'Dni');
        $sheet->setCellValue('F' . $fila, 'Fecha Pedido');
        $sheet->setCellValue('G' . $fila, 'Pedido Nro.');
        $sheet->setCellValue('H' . $fila, 'Dirección Envio');
        $sheet->getStyle('A' . $fila . ':H' . $fila)->applyFromArray($styleArray2);
        $fila++;

        foreach ($clientesWeb as $clienteWeb) {
            if (!is_null($clienteWeb->getPedidos()[0])) {
                $pedido = $clienteWeb->getPedidos()[0];
                $sheet->setCellValue('A' . $fila, $clienteWeb->getCreated()->format('d/m/Y'));
                $sheet->setCellValue('B' . $fila, $clienteWeb->getNombre());
                $sheet->setCellValue('C' . $fila, $clienteWeb->getEmail());
                $sheet->setCellValue('D' . $fila, $clienteWeb->getTelefono());
                $sheet->setCellValue('E' . $fila, $clienteWeb->getDni());
                if (!is_null($pedido->getFecha())) {
                    $sheet->setCellValue('F' . $fila, $pedido->getFecha()->format('d/m/Y'));
                }
                $sheet->setCellValue('G' . $fila, $pedido->getId());
                $sheet->setCellValue('H' . $fila, $pedido->getDireccionEnvio());

                $fila++;
            }
        }

        $fila++;

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        return $spreadsheet;
    }

    /**
     * @Route("/{id}", name="tienda_cliente_web_show", methods={"GET"})
     */
    public function show(ClienteWeb $clienteWeb): Response {
        return $this->render('tienda/cliente_web/show.html.twig', [
                    'cliente_web' => $clienteWeb,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_cliente_web_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ClienteWeb $clienteWeb): Response {
        $form = $this->createForm(ClienteWebGestionType::class, $clienteWeb);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_cliente_web_index');
        }

        return $this->render('tienda/cliente_web/edit.html.twig', [
                    'cliente_web' => $clienteWeb,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_cliente_web_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, ClienteWeb $clienteWeb, TranslatorInterface $translator) {
        if (!$clienteWeb) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected web client.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $clienteWeb->setEnabled($activo);
            $entityManager->persist($clienteWeb);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Web client status has been changed', ['%web client%' => $clienteWeb]));
        }
        return $this->redirect($this->generateUrl('tienda_cliente_web_index'));
    }

    /**
     * @Route("/{id}", name="tienda_cliente_web_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ClienteWeb $clienteWeb): Response {
        if ($this->isCsrfTokenValid('delete' . $clienteWeb->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($clienteWeb);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tienda_cliente_web_index');
    }

}

