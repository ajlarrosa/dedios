<?php

namespace App\Controller\Tienda;

use App\Entity\Tienda\Seccion;
use App\Form\Tienda\SeccionType;
use App\Repository\Tienda\SeccionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use App\Service\General\GeneralService;

/**
 * @Route("/admin/seccion_type")
 */
class SeccionController extends AbstractController {

     private $generalService;
    
    function __construct(GeneralService $generalService) {
        $this->generalService= $generalService;        
    }
    
    /**
     * @Route("/", name="tienda_seccion_index", methods={"GET"})
     */
    public function index(Request $request, SeccionRepository $seccionRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $secciones = $paginator->paginate(
                $seccionRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('tienda/seccion/index.html.twig', [
                    'secciones' => $secciones,
        ]);
    }

    /**
     * @Route("/activate/{id}", name="tienda_seccion_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Seccion $seccion, TranslatorInterface $translator) {
        if (!$seccion) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected section.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $seccion->setEnabled($activo);
            $entityManager->persist($seccion);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Section status has been changed', ['%section%' => $seccion->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('tienda_seccion_index'));
    }

    /**
     * @Route("/new", name="tienda_seccion_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $seccion = new Seccion();
        $form = $this->createForm(SeccionType::class, $seccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $error = false;
            if ($_FILES['seccion']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $entityManager->persist($seccion);
                    $entityManager->flush();
                    
                    $uploaddir = $this->getParameter('root_dir_seccion') . $seccion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }
                    
                    $name = date('YmdHis').'_'.$_FILES['seccion']['name']['imagen'];
                
                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion']["tmp_name"]['imagen'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        $seccion->setImagen($name);
                    }else{
                        $error = true;
                    }
                }
            }
            if (!$error) {
                $entityManager->persist($seccion);
                $entityManager->flush();
                $this->addFlash('msgOk', $translator->trans('Section created successfully.'));
                return $this->redirectToRoute('tienda_seccion_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the section.'));
        }
        return $this->render('tienda/seccion/new.html.twig', [
                    'seccion' => $seccion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tienda_seccion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Seccion $seccion, TranslatorInterface $translator): Response {
        $form = $this->createForm(SeccionType::class, $seccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $error = false;
            if ($_FILES['seccion']['size']['imagen'] > 0) {
                //Validates if file is image
                if (!$this->generalService->isImage($_FILES['seccion']['type']['imagen'])) {
                    $this->addFlash('msgError', $translator->trans('file.error.noimage'));
                    $error = true;
                } else {
                    $uploaddir = $this->getParameter('root_dir_seccion') . $seccion->getId() . '/';
            
                    if (!file_exists($uploaddir)) {
                        mkdir($uploaddir, 0777, true);
                    }

                    $name = date('YmdHis').'_'.$_FILES['seccion']['name']['imagen'];

                    $uploadfile = $uploaddir . $name;

                    $tmp_name = $_FILES['seccion']["tmp_name"]['imagen'];

                    if (move_uploaded_file($tmp_name, $uploadfile)) {
                        if($seccion->getImagen()!='' && is_file($uploaddir.'../../../'.$seccion->getImagen())){
                            unlink($uploaddir.'../../../'.$seccion->getImagen());
                        }
                        $seccion->setImagen($name);
                    }else{
                        $error = true;
                    }
                    
                }
            }
            if (!$error) {
               $this->getDoctrine()->getManager()->flush();
                $this->addFlash('msgOk', $translator->trans('Section edited successfully.'));
                return $this->redirectToRoute('tienda_seccion_index');
            }
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the sections.'));
        }
        return $this->render('tienda/seccion/edit.html.twig', [
                    'seccion' => $seccion,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tienda_seccion_show", methods={"GET"})
     */
    public function show(Seccion $seccion): Response {
        return $this->render('tienda/seccion/show.html.twig', [
                    'seccion' => $seccion,
        ]);
    }

}
