<?php

namespace App\Controller\Tienda;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\General\SessionService;
use App\Service\General\MailerService;
use App\Entity\Common\Email;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController {

    private $sessionService;
    private $translator;

    function __construct(
            TranslatorInterface $translator,
            SessionService $sessionService
    ) {
        $this->translator = $translator;
        $this->sessionService = $sessionService;
    }

    /**
     * @Route("/", name="home")
     */
    public function home(Request $request) {
       
        if ($this->getParameter('web_is_up') == '1') {
            $parameters = [];

            $fb = $request->get('fb');

            if (isset($fb)) {
                $parameters['fb'] = $fb;
            }

            return $this->render('tienda/default/index.html.twig', $parameters);
        }
        return $this->render('underConstruction/index.html.twig');
    }

    /**
     * @Route("/gracias", name="gracias")
     */
    public function gracias(Request $request) {

        $currency=$request->get('currency');
        $totalValue=$request->get('totalvalue');
        
        return $this->render('tienda/default/gracias.html.twig', [
                    'fb'=>7,
                    'currency' => $currency,
                    'totalValue' => $totalValue
        ]);
    }

    /**
     * @Route("/terminos-y-condiciones", name="terminosCondiciones")
     */
    public function terminosCondiciones() {
        return $this->render('tienda/default/terminos-y-condiciones.html.twig');
    }

    /**
     * @Route("/medidor-pulseras", name="medidorPulseras")
     */
    public function medidorPulseras() {

        return $this->render('tienda/default/medidor-pulseras.html.twig');
    }

    /**
     * @Route("/medidor-anillos", name="medidorAnillos")
     */
    public function medidorAnillos() {

        return $this->render('tienda/default/medidor-anillos.html.twig');
    }

    /**
     * @Route("/medidor-collares", name="medidorCollares")
     */
    public function medidorCollares() {

        return $this->render('tienda/default/medidor-collares.html.twig');
    }

    /**
     * @Route("/cuidados-joya", name="cuidadosJoya")
     */
    public function cuidadoJoya() {

        return $this->render('tienda/default/cuidados-de-la-joya.html.twig');
    }

    /**
     * @Route("/nosotros", name="about")
     */
    public function about() {

        return $this->render('tienda/default/about.html.twig');
    }

    /**
     * @Route("/contacto", name="contact")
     */
    public function contact(Request $request, MailerService $mailerService) {
        $parameters = [];
        $action = $request->get('action');

        if (isset($action)) {

            $token = $request->get('token');
            $response = $this->recaptchav3($token, $action);

            if ($response === true) {

                $email = new Email();
                $email->setTitle($this->translator->trans("De Dios Jewels - New contact"));

                $email->setSendTo($this->getParameter('email_receiver'));

                $email->setTemplate('email/contact.html.twig');
                $email->setParameters(['contact' => $request->request->all()]);

                $response = $mailerService->enviarMail($email);
                if ($response === true) {
                    $parameters['fb'] = 6;
                    $this->addFlash("msgOk", $this->translator->trans("The message has been sent to DE DIOS"));
                } else {
                    $this->addFlash("msgError", $response);
                }
            }
        }
        return $this->render('tienda/default/contacto.html.twig', $parameters);
    }

    /*
     * recaptchav3
     */

    private function recaptchav3($token, $action) {

        // call curl to POST request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => '6Lf68tIUAAAAAJKTfB6knOEdNywgBi9FIT1DAL4h', 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $response = curl_exec($ch);

        curl_close($ch);
        $arrResponse = json_decode($response, true);

        // verify the response

        if ($arrResponse["success"] == true && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
            return true;
        } else {
            return $this->translator->trans("It has not been possible to specify the action. You don't appear to be a human. If it is, please try again.");
        }
    }

}
