<?php

namespace App\Controller;

use App\Entity\MovimientoCheque;
use App\Form\MovimientoChequeType;
use App\Repository\MovimientoChequeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/movimiento/cheque")
 */
class MovimientoChequeController extends AbstractController
{
    /**
     * @Route("/", name="movimiento_cheque_index", methods={"GET"})
     */
    public function index(MovimientoChequeRepository $movimientoChequeRepository): Response
    {
        return $this->render('movimiento_cheque/index.html.twig', [
            'movimiento_cheques' => $movimientoChequeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="movimiento_cheque_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $movimientoCheque = new MovimientoCheque();
        $form = $this->createForm(MovimientoChequeType::class, $movimientoCheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movimientoCheque);
            $entityManager->flush();

            return $this->redirectToRoute('movimiento_cheque_index');
        }

        return $this->render('movimiento_cheque/new.html.twig', [
            'movimiento_cheque' => $movimientoCheque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movimiento_cheque_show", methods={"GET"})
     */
    public function show(MovimientoCheque $movimientoCheque): Response
    {
        return $this->render('movimiento_cheque/show.html.twig', [
            'movimiento_cheque' => $movimientoCheque,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="movimiento_cheque_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MovimientoCheque $movimientoCheque): Response
    {
        $form = $this->createForm(MovimientoChequeType::class, $movimientoCheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('movimiento_cheque_index');
        }

        return $this->render('movimiento_cheque/edit.html.twig', [
            'movimiento_cheque' => $movimientoCheque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movimiento_cheque_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MovimientoCheque $movimientoCheque): Response
    {
        if ($this->isCsrfTokenValid('delete'.$movimientoCheque->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movimientoCheque);
            $entityManager->flush();
        }

        return $this->redirectToRoute('movimiento_cheque_index');
    }
}
