<?php

namespace App\Controller;

use App\Entity\Provincia;
use App\Form\ProvinciaType;
use App\Repository\ProvinciaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/provincia")
 */
class ProvinciaController extends AbstractController {

    /**
     * @Route("/operator/", name="provincia_index", methods={"GET"})
     */
    public function index(Request $request, ProvinciaRepository $provinciaRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $provincias = $paginator->paginate(
                $provinciaRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('provincia/index.html.twig', [
                    'provincias' => $provincias,
        ]);
    }

    /**
     * @Route("/admin/new", name="provincia_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $provincia = new Provincia();
        $form = $this->createForm(ProvinciaType::class, $provincia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($provincia);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Province created successfully.'));
            return $this->redirectToRoute('provincia_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the province.'));
        }
        return $this->render('provincia/new.html.twig', [
                    'provincia' => $provincia,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/activate/{id}", name="provincia_activate", methods={"GET"})
     *
     */
    public function activateAction(Request $request, Provincia $provincia, TranslatorInterface $translator) {
        if (!$provincia) {
            $this->addFlash('msgOk', $translator->trans('It was not possible to change the status of the selected province.'));
        } else {
            $activo = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $provincia->setActivo($activo);
            $entityManager->persist($provincia);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Province status has been changed', ['%province%' => $provincia->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('provincia_index'));
    }

    /**
     * @Route("/admin/{id}/edit", name="provincia_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Provincia $provincia, TranslatorInterface $translator): Response {
        $form = $this->createForm(ProvinciaType::class, $provincia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Province edited successfully.'));
            return $this->redirectToRoute('provincia_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the province.'));
        }
        return $this->render('provincia/edit.html.twig', [
                    'provincia' => $provincia,
                    'form' => $form->createView(),
        ]);
    }

}
