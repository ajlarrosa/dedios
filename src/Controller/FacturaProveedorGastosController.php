<?php

namespace App\Controller;

use App\Entity\FacturaProveedor;
use App\Form\FacturaProveedorType;
use App\Service\ProveedorService;
use App\Service\MovimientoCCService;
use App\Repository\FacturaProveedorGastosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\General\PhpOfficeService;

/**
 * @Route("/operator/gastos")
 */
class FacturaProveedorGastosController extends AbstractController {

    /**
     * @Route("/", name="facturaproveedor_gastos_index", methods={"GET"})
     */
    public function index(Request $request, FacturaProveedorGastosRepository $facturaProveedorGastosRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {

        $filtros = $request->query->all();

        /*
         * Verifico si se presiono el botón exportar excel
         */
        if ($request->get('action') == 'excel') {
            $facturasProveedorGastos = $facturaProveedorGastosRepository->filter($filtros)->getQuery()->getResult();
            
            $spreadSheet = $this->excelInforme($facturasProveedorGastos, $filtros);

            $nombreInforme = 'Gastos';
            
            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }
        
        
        $facturasproveedorgastos = $paginator->paginate(
                $facturaProveedorGastosRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('facturaproveedorgastos/index.html.twig', [
                    'facturasproveedor' => $facturasproveedorgastos,
        ]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme($facturasProveedor, $parametros) {

     $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];
        
        $fila = 3;
        
        foreach ($facturasProveedor as $facturaProveedor) {
            $sheet->setCellValue('A' . $fila, 'Factura Nro.'.$facturaProveedor->getId());
            $sheet->setCellValue('B' . $fila, 'Proveedor');
            $sheet->setCellValue('C' . $fila, 'Tipo de Gasto');
            $sheet->setCellValue('D' . $fila, 'Fecha');
            $sheet->setCellValue('E' . $fila, 'Fecha de Registración');
            $sheet->setCellValue('F' . $fila, 'Observación');
            $sheet->setCellValue('G' . $fila, 'Descuento');
            $sheet->setCellValue('H' . $fila, 'Bonificación');
            
            $sheet->getStyle('A' . $fila . ':H' . $fila)->applyFromArray($styleArray2);

            $fila++;
            
            $sheet->setCellValue('B' . $fila, $facturaProveedor->getMovimientocc()->getClienteProveedor());
            $sheet->setCellValue('C' . $fila, $facturaProveedor->getTipoGasto());
            $sheet->setCellValue('D' . $fila, $facturaProveedor->getFecha()->format('d-m-Y'));
            $sheet->setCellValue('E' . $fila, $facturaProveedor->getCreated()->format('d-m-Y'));
            $sheet->setCellValue('F' . $fila, $facturaProveedor->getObservacion());
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getDescuento());
            $sheet->setCellValue('H' . $fila, $facturaProveedor->getBonificacion());
            
            
            $fila++;
            
            $sheet->setCellValue('C' . $fila, 'Total Oro:');
            $sheet->setCellValue('D' . $fila, $facturaProveedor->getOro().' gr');
            $sheet->setCellValue('F' . $fila, 'Subtotal:');
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getImporte());
            $fila++;
            $sheet->setCellValue('C' . $fila, 'Total Plata:');
            $sheet->setCellValue('D' . $fila, $facturaProveedor->getPlata().' gr');
            $sheet->setCellValue('F' . $fila, 'Descuento('.$facturaProveedor->getDescuento().'%):');
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getDescuentoValue());
            $sheet->getStyle('C'.($fila-1).':C'.$fila)->applyFromArray($styleArray2);
            $fila++;
            $sheet->setCellValue('F' . $fila, 'Bonificación:');
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getBonificacion());
            $fila++;
            $sheet->setCellValue('F' . $fila, 'Total:');
            $sheet->setCellValue('G' . $fila, $facturaProveedor->getMonedaSimbolo().$facturaProveedor->getTotal());
            $fila++;
            $sheet->getStyle('F'.($fila-4).':F'.$fila)->applyFromArray($styleArray2);
            $fila++;
        }        
        
        $fila++;
        
        //Totales
        $sheet->setCellValue('A1', 'Total Gastos Listados: '.(sizeof($facturasProveedor)));
        
        
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        
        return $spreadsheet;
    }
    
    /**
     * @Route("/new", name="facturaproveedor_gastos_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator, ProveedorService $proveedorService, MovimientoCCService $movimientoCCService): Response {
        $facturaProveedor = new FacturaProveedor();
        $form = $this->createForm(FacturaProveedorType::class, $facturaProveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movimientocc = $movimientoCCService->create($proveedorService->getProveedorById($request->get('proveedor')), null, null, null, null, null, $facturaProveedor);
            $entityManager = $this->getDoctrine()->getManager();
            $facturaProveedor->setMovimientocc($movimientocc);
            $entityManager->persist($movimientocc);
            $entityManager->persist($facturaProveedor);

            $entityManager->flush();

            $this->addFlash('msgOk', $translator->trans('Invoice Provider Expense created successfully.'));
            return $this->redirectToRoute('facturaproveedor_gastos_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the invoice provider expense.'));
        }
        return $this->render('facturaproveedorgastos/new.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="facturaproveedor_gastos_show", methods={"GET"})
     */
    public function show(FacturaProveedor $facturaProveedor): Response {
        return $this->render('facturaproveedorgastos/show.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="facturaproveedor_gastos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FacturaProveedor $facturaProveedor): Response {
        $form = $this->createForm(FacturaProveedorType::class, $facturaProveedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facturaproveedor_gastos_index');
        }

        return $this->render('facturaproveedorgastos/edit.html.twig', [
                    'facturaproveedor' => $facturaProveedor,
                    'form' => $form->createView(),
        ]);
    }


}
