<?php

namespace App\Controller;

use App\Entity\Color;
use App\Form\ColorType;
use App\Repository\ColorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;


class ColorController extends AbstractController
{
    
    /**
     * @Route("/operator/color", name="color_index", methods={"GET"})
     */
    public function index(Request $request, ColorRepository $colorRepository, PaginatorInterface $paginator): Response {
        $filtros = $request->query->all();
        $colores = $paginator->paginate(
                $colorRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );
        return $this->render('color/index.html.twig', [
                    'colores' => $colores,
        ]);
    }
    
    /**
     * @Route("/admin/color/new", name="color_new", methods={"GET","POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response {
        $color = new Color();
        $form = $this->createForm(ColorType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($color);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('color created successfully.'));
            return $this->redirectToRoute('color_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to create the color.'));
        }
        return $this->render('color/new.html.twig', [
                    'color' => $color,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/color/{id}/edit", name="color_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Color $color, TranslatorInterface $translator): Response {
        $form = $this->createForm(ColorType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('msgOk', $translator->trans('Color edited successfully.'));
            return $this->redirectToRoute('color_index');
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('msgError', $translator->trans('It was not possible to edit the color.'));
        }
        return $this->render('color/edit.html.twig', [
                    'color' => $color,
                    'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/admin/color/activate/{id}", name="color_activate", methods={"GET"})
     *
     */
    public function activate(Request $request, Color $color, TranslatorInterface $translator) {
        if (!$color) {
            $this->addFlash('msgError', $translator->trans('It was not possible to change the status of the selected color.'));
        } else {
            $enabled = $request->get('activo');
            $entityManager = $this->getDoctrine()->getManager();
            $color->setEnabled($enabled);
            $entityManager->persist($color);
            $entityManager->flush();
            $this->addFlash('msgOk', $translator->trans('Color {{ color }} status has been changed', ['{{ color }}' => $color->getDescripcion()]));
        }
        return $this->redirect($this->generateUrl('color_index'));
    }
}
