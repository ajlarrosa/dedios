<?php

namespace App\Controller;

use App\Entity\ImagenProducto;
use App\Form\ImagenProductoType;
use App\Repository\ImagenProductoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/imagen/producto")
 */
class ImagenProductoController extends AbstractController {

    /**
     * @Route("/", name="imagen_producto_index", methods={"GET"})
     */
    public function index(ImagenProductoRepository $imagenProductoRepository): Response {
        return $this->render('imagen_producto/index.html.twig', [
                    'imagen_productos' => $imagenProductoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="imagen_producto_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $imagenProducto = new ImagenProducto();
        $form = $this->createForm(ImagenProductoType::class, $imagenProducto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($imagenProducto);
            $entityManager->flush();

            return $this->redirectToRoute('imagen_producto_index');
        }

        return $this->render('imagen_producto/new.html.twig', [
                    'imagen_producto' => $imagenProducto,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="imagen_producto_show", methods={"GET"})
     */
    public function show(ImagenProducto $imagenProducto): Response {
        return $this->render('imagen_producto/show.html.twig', [
                    'imagen_producto' => $imagenProducto,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="imagen_producto_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ImagenProducto $imagenProducto): Response {
        $form = $this->createForm(ImagenProductoType::class, $imagenProducto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('imagen_producto_index');
        }

        return $this->render('imagen_producto/edit.html.twig', [
                    'imagen_producto' => $imagenProducto,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="imagen_producto_delete", methods={"GET"})
     */
    public function delete(ImagenProducto $imagenProducto): Response {
        try {
            $idProducto = $imagenProducto->getProducto()->getId();
            $path = $this->getParameter('root_dir_images') . $idProducto . '/' . $imagenProducto->getPath();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($imagenProducto);
            $entityManager->flush();
            unlink($path);
            $this->addFlash("msgOk", "The image has been successfully removed.");
        } catch (\Exception $e) {
            $this->addFlash("msgError", "It was not possible to delete the image.");
        }

        return $this->redirectToRoute('producto_edit', ['id' => $idProducto]);
    }

}
