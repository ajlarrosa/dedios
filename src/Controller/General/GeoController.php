<?php

namespace App\Controller\General;

use App\Service\General\GeoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/geo")
 */
class GeoController extends AbstractController {

    private $geoService;

    function __construct(GeoService $geoService) {
        $this->geoService = $geoService;
    }

    /**
     * @Route("/optionSelectDepartamentos",name="geo_option_select_departamentos", methods={"GET"})
     */
    public function optionsSelectDepartamentos(Request $request) {
        $html = '<option value="">Seleccione un departamento</option>';
        $idProvincia = $request->get('idProvincia');
        $page = 1;
        $more = true;
        
        while ($more) {
            $arrayDepartamentos = $this->geoService->getDepartamentos($idProvincia, $page);

            if (sizeof($arrayDepartamentos) > 0) {
                foreach ($arrayDepartamentos as $departamento) {
                    $html = $html . '<option value="' . $departamento->id . '">' . $departamento->nombre . '</option>';
                }               
            } else {
                $more = false;
            }
            
             $page++;
        }

        return new Response($html);
    }

    /**
     * @Route("/optionSelectLocalidades",name="geo_option_select_localidades", methods={"GET"})
     */
    public function optionsSelectLocalidades(Request $request) {
        $html = '<option value="">Seleccione una localidad</option>';
        $idDepartamento = $request->get('idDepartamento');
        $idProvincia = $request->get('idProvincia');
        $page = 1;
        $more = true;

        while ($more) {

            $arrayLocalidades = $this->geoService->getLocalidades($idProvincia, $idDepartamento, $page);

            if (sizeof($arrayLocalidades) > 0) {
                foreach ($arrayLocalidades as $localidad) {
                    $html = $html . '<option data-localidadcensalid="' . $localidad->localidadCensalId . '" value="' . $localidad->id . '">' . $localidad->nombre . '</option>';
                }               
            } else {
                $more = false;
            }
            
             $page++;
        }

        return new Response($html);
    }

    /**
     * @Route("/optionSelectCalles",name="geo_option_select_calles", methods={"GET"})
     */
    public function optionsSelectCalles(Request $request) {
        $html = '<ul>';

        $page = 1;
        
        $more = true;

        while ($more) {            
            $arrayCalles = $this->geoService->getCalles($more, $request->get('idLocalidadCensal'), $page, $request->get('nombre'));          
            foreach ($arrayCalles as $calle) {
                $html = $html . '<li class="suggest-element" data-nombre="' . $calle->nombre . '" data-id="' . $calle->id . '">' . $calle->nombre . '</option>';
            }
            
            $page++;
        }

        return new Response($html . '</ul>');
    }

    /**
     * @Route("/{id}/calle",name="geo_calle", methods={"GET"})
     */
    public function calle($id) {
        return new JsonResponse($this->geoService->getCalle($id));
    }

}
