<?php

namespace App\Controller;

use App\Entity\Tienda\Pedido;
use App\Form\Tienda\PedidoType;
use App\Service\Tienda\PedidoService;
use App\Repository\PedidoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\General\PhpOfficeService;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Entity\Common\Moneda;
use App\Entity\Tienda\FormaPago;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\General\MailerService;
use App\Service\ShipNow\ShipNowService;
use App\Service\Tienda\DireccionEnvioService;
use App\Service\General\MonedaService;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Recordatorio;
use App\Entity\DetalleRecordatorio;

/**
 * @Route("/")
 */
class PedidoController extends AbstractController {

    private $pedidoRepository;
    private $translator;
    private $pedidoService;
    private $parameterBagInterface;
    private $shipNowService;
    private $direccionEnvioService;
    private $monedaService;

    function __construct(
            PedidoRepository $pedidoRepository,
            TranslatorInterface $translator,
            PedidoService $pedidoService,
            ParameterBagInterface $parameterBagInterface,
            MailerService $mailerService,
            ShipNowService $shipNowService,
            DireccionEnvioService $direccionEnvioService,
            MonedaService $monedaService
    ) {
        $this->pedidoRepository = $pedidoRepository;
        $this->translator = $translator;
        $this->pedidoService = $pedidoService;
        $this->parameterBagInterface = $parameterBagInterface;
        $this->shipNowService = $shipNowService;
        $this->mailerService = $mailerService;
        $this->direccionEnvioService = $direccionEnvioService;
        $this->monedaService = $monedaService;
    }

    /**
     * @Route("operator/pedido", name="pedido_index", methods={"GET"})
     */
    public function index(Request $request, PedidoRepository $pedidoRepository, PaginatorInterface $paginator, PhpOfficeService $phpOffice): Response {
        $filtros = $request->query->all();

        if ($request->get('action') == 'excel') {
            $pedidos = $pedidoRepository->filter($filtros)->getQuery()->getResult();

            $spreadSheet = $this->excelListado($pedidos, $filtros);

            $nombreInforme = 'ventas';

            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }

        $pedidos = $paginator->paginate(
                $pedidoRepository->filter($filtros),
                $request->query->getInt('page', 1),
                15
        );

        return $this->render('pedido/index.html.twig', [
                    'pedidos' => $pedidos,
        ]);
    }

    /**
     * @Route("operator/pedido/{id}", name="pedido_show", methods={"GET"})
     */
    public function show(Pedido $pedido): Response {

        return $this->render('pedido/show.html.twig', [
                    'pedido' => $pedido
        ]);
    }

    /**
     * @Route("operator/pedido/{id}/excel", name="pedido_exportarexcel", methods={"GET"})
     */
    public function exportarExcel(Pedido $pedido, PhpOfficeService $phpOffice): Response {
        if ($pedido) {
            $spreadSheet = $this->excelInforme($pedido);

            $nombreInforme = 'Presupuesto ' . $pedido->getId();

            return $phpOffice->exportarExcel($spreadSheet, $nombreInforme);
        }

        $this->addFlash('msgError', 'No ha sido posible exportar el informe.');

        return $this->redirectToRoute('pedido_show', ['id' => $pedido->getID()]);
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelInforme(Pedido $pedido) {

        $spreadsheet = new Spreadsheet();

        $moneda = (is_null($pedido->getUser())) ? Moneda::SIMBOLO_PESOS : $this->monedaService->getMonedaUsuario($pedido->getUser()->getId());        

        $spreadsheet->getProperties()->setCreator($this->getUser()->getNombre())
                ->setLastModifiedBy($this->getUser()->getNombre())
                ->setTitle("Presupuesto NRO. " . $pedido->getId())
                ->setSubject($this->getUser()->getNombre());

        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->mergeCells('B2:C2');
        $spreadsheet->getActiveSheet()->mergeCells('B3:C3');
        $spreadsheet->getActiveSheet()->mergeCells('D5:D6');
        $spreadsheet->getActiveSheet()->mergeCells('C5:C6');

        $spreadsheet->getActiveSheet()
                ->setCellValue('B2', 'DE DIOS');
        $spreadsheet->getActiveSheet()
                ->getStyle('B2')->getFont()->setSize(22);
        $spreadsheet->getActiveSheet()
                ->setCellValue('B3', 'joyas@dediosjoyas.com');
        $spreadsheet->getActiveSheet()
                ->getStyle('B3')->getFont()->setSize(13);
        $spreadsheet->getActiveSheet()
                ->setCellValue('D2', 'PRESUPUESTO');
        $spreadsheet->getActiveSheet()->getStyle('D2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()
                ->getStyle('D2')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->getStyle('D2')->getFont()->setSize(17);
        $spreadsheet->getActiveSheet()
                ->setCellValue('E2', 'N°');
        $spreadsheet->getActiveSheet()
                ->getStyle('E2')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->setCellValue('F2', $pedido->getId());
        $spreadsheet->getActiveSheet()
                ->getStyle('F2')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->getStyle('E2:F2')->getFont()->setSize(14);
        $spreadsheet->getActiveSheet()->getStyle('F2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()
                ->setCellValue('F3', 'Vigencia: Nov. 2019');
        $spreadsheet->getActiveSheet()
                ->getStyle('F3')->getFont()->setSize(12);
        $spreadsheet->getActiveSheet()
                ->setCellValue('C5', 'CLIENTE');
        $spreadsheet->getActiveSheet()
                ->getStyle('C5')->getFont()->setSize(17);
        $spreadsheet->getActiveSheet()
                ->getStyle('C5')->getFont()->setBold(true);
        $user = $pedido->getUser();
        if ($user) {
            $spreadsheet->getActiveSheet()
                    ->setCellValue('D5', strtoupper($pedido->getUser()->getNombre() . ' ' . $pedido->getUser()->getApellido()));

            $telefono = $pedido->getUser()->getTelefono();
        } else {
            $spreadsheet->getActiveSheet()
                    ->setCellValue('D5', strtoupper($pedido->getClienteWeb()->getNombre() . ' ' . $pedido->getClienteWeb()->getApellido()));
            $telefono = $pedido->getClienteWeb()->getTelefono();
        }

        $spreadsheet->getActiveSheet()
                ->getStyle('D5')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('D5')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()
                ->getStyle('D5')->getFont()->setSize(21);
        $spreadsheet->getActiveSheet()
                ->getStyle('B5')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->setCellValue('C7', 'E-mail / Teléfono');
        $spreadsheet->getActiveSheet()
                ->setCellValue('D7', $pedido->getEmail() . ' / ' . $telefono);
        $spreadsheet->getActiveSheet()
                ->getStyle('C7:D7')->getFont()->setSize(14);
        $spreadsheet->getActiveSheet()
                ->setCellValue('E5', 'FECHA');
        $spreadsheet->getActiveSheet()
                ->setCellValue('F5', $pedido->getFecha()->format('d-m-Y'))
                ->setCellValue('E6', 'ENTREGA');
        $spreadsheet->getActiveSheet()
                ->getStyle('E5:E6')->getFont()->setSize(15);
        $spreadsheet->getActiveSheet()
                ->getStyle('F5')->getFont()->setSize(15);
        $spreadsheet->getActiveSheet()
                ->getStyle('E6')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->setCellValue('B8', 'Reviso');
        $spreadsheet->getActiveSheet()
                ->getStyle('B8')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->getStyle('B8')->getFont()->setSize(21);
        $spreadsheet->getActiveSheet()
                ->setCellValue('E8', 'RETIRA');
        $spreadsheet->getActiveSheet()
                ->getStyle('E8:F8')->getFont()->setSize(21);
        $spreadsheet->getActiveSheet()
                ->getStyle('E8')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->setCellValue('F8', 'ENVIO');
        $spreadsheet->getActiveSheet()
                ->getStyle('F8')->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()
                ->setCellValue('B10', 'CANTIDAD');
        $spreadsheet->getActiveSheet()
                ->setCellValue('C10', 'CÓDIGO');
        $spreadsheet->getActiveSheet()
                ->setCellValue('D10', 'DESCRIPCIÓN');
        $spreadsheet->getActiveSheet()
                ->setCellValue('E10', 'Precio Unitario');
        $spreadsheet->getActiveSheet()
                ->setCellValue('F10', 'TOTAL');
        $spreadsheet->getActiveSheet()
                ->setCellValue('B2', 'DE DIOS');
        $styleArray = array(
            'font' => array(
                'color' => array('rgb' => 'FFFFFF'),
        ));

        $spreadsheet->getActiveSheet()->getStyle('B10:F10')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('B2:C2')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()
                ->getStyle('B2:C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('000000');
        $spreadsheet->getActiveSheet()
                ->getStyle('B10:F10')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('000000');
        $fila = 11;
        $totalPiezas = 0;
        foreach ($pedido->getDetallespedido() as $detallePedido) {

            $spreadsheet->getActiveSheet()
                    ->setCellValue('B' . $fila, $detallePedido->getCantidad())
                    ->setCellValue('C' . $fila, $detallePedido->getProducto()->getCodigo())
                    ->setCellValue('D' . $fila, $detallePedido)
                    ->setCellValue('E' . $fila, $detallePedido->getPrecio())
                    ->setCellValue('F' . $fila, round($detallePedido->getPrecio() * $detallePedido->getCantidad(), 2));
            $fila++;
            $spreadsheet->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setWrapText(true);
            $totalPiezas = $totalPiezas + $detallePedido->getCantidad();
        }
        $spreadsheet->getActiveSheet()->getStyle('E2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('E5:E6')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('B8')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('C5')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('C7')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $spreadsheet->getActiveSheet()->getStyle('B2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B3')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D5')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E8:F8')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F2')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('F5')
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B10:F' . ($fila + 5))
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('D11:D' . ($fila - 1))
                ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        $spreadsheet->getActiveSheet()->getStyle('B10:F' . ($fila + 5))
                ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('C5:F6')
                ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('E8:F8')
                ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $spreadsheet->getActiveSheet()->getStyle('B8')
                ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


        $spreadsheet->getActiveSheet()->getStyle('F2')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('F6')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('F5')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN))));
        $spreadsheet->getActiveSheet()->getStyle('E8:F8')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('F2')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('B' . $fila)->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('E' . $fila . ':F' . ($fila + 5))->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('D5:D6')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN))));
        $spreadsheet->getActiveSheet()->getStyle('C8')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('B10:F10')->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('B11:F' . ($fila - 1))->applyFromArray(
                array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN))));
        $spreadsheet->getActiveSheet()->getStyle('B11:F' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('B10:B' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('C10:C' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('D10:D' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('E10:E' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
        $spreadsheet->getActiveSheet()->getStyle('F10:F' . ($fila - 1))->applyFromArray(
                array('borders' => array('right' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'left' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'top' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK), 'bottom' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));


        $spreadsheet->getActiveSheet()
                ->setCellValue('B' . $fila, $totalPiezas)
                ->setCellValue('E' . $fila, 'SUBTOTAL')
                ->setCellValue('F' . $fila, $moneda . ' ' . ($this->pedidoService->getSubTotal($pedido->getId())))
                ->setCellValue('D' . ($fila + 5), 'OBSERVACIONES')
                ->setCellValue('B' . ($fila + 6), $pedido->getObservacion())
                ->setCellValue('E' . ($fila + 1), 'ENVIO')
                ->setCellValue('F' . ($fila + 1), $pedido->getCostoEnvio() != '' ? $moneda . ' ' . $pedido->getCostoEnvio() : '')
                ->setCellValue('E' . ($fila + 2), 'DESC. (' . $pedido->getDescuento() . '%)')
                ->setCellValue('F' . ($fila + 2), $pedido->getValorDescuento() != '' ? $moneda . ' ' . $pedido->getValorDescuento() : '')
                ->setCellValue('E' . ($fila + 3), 'PROMO.')
                ->setCellValue('F' . ($fila + 3), $pedido->getMontoPromocion() != '' ? $moneda . ' ' . $pedido->getMontoPromocion() : '')
                ->setCellValue('E' . ($fila + 4), 'REC. (' . $pedido->getRecargo() . '%)')
                ->setCellValue('F' . ($fila + 4), $pedido->getValorRecargo() != '' ? $moneda . ' ' . $pedido->getValorRecargo() : '')
                ->setCellValue('E' . ($fila + 5), 'TOTAL')
                ->setCellValue('F' . ($fila + 5), $moneda . ' ' . ($this->pedidoService->getTotal($pedido->getId())));

        $spreadsheet->getActiveSheet()
                ->getStyle('B10:F' . ($fila + 6))->getFont()->setSize(17);

        $spreadsheet->getActiveSheet()->mergeCells('B' . ($fila + 6) . ':f' . ($fila + 6));

        if ($pedido->getObservacion() != '') {
            $spreadsheet->getActiveSheet()->getStyle('B' . ($fila + 6))->getAlignment()->setWrapText(true);
            $spreadsheet->getActiveSheet()->getRowDimension(($fila + 6))->setRowHeight(120);
        }
                    
        for ($i = 8; $i <= 10; $i++) {
            $spreadsheet->getActiveSheet()->mergeCells('B' . ($fila + $i) . ':C' . ($fila + $i));
            $spreadsheet->getActiveSheet()->mergeCells('D' . ($fila + $i) . ':F' . ($fila + $i));
            $spreadsheet->getActiveSheet()->getStyle('B' . ($fila + $i) . ':F' . ($fila + $i))->applyFromArray(
                    array('borders' => array('allBorders' => array('borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK))));
            $spreadsheet->getActiveSheet()
                    ->getStyle('B' . ($fila + $i))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('D5D3D3');
            $spreadsheet->getActiveSheet()->getStyle('B' . ($fila + $i))->getFont()->setSize(17);
            $spreadsheet->getActiveSheet()->getStyle('D' . ($fila + $i))->getFont()->setSize(15);
            $spreadsheet->getActiveSheet()->getStyle('B' . ($fila + $i) . ':D' . ($fila + $i))
                    ->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        }

        $spreadsheet->getActiveSheet()
                ->setCellValue('B' . ($fila + 8), 'FORMA DE ENVÍO')
                ->setCellValue('B' . ($fila + 9), 'INFORMACIÓN DEL CLIENTE')
                ->setCellValue('B' . ($fila + 10), 'FORMA DE PAGO');

        switch ($pedido->getTipoEnvio()) {
            case $pedido::STORE:
                $spreadsheet->getActiveSheet()
                        ->setCellValue('D' . ($fila + 8), ' Retira de ' . $pedido->getStore());
                break;
            case $pedido::SHIP_NOW:
                $spreadsheet->getActiveSheet()
                        ->setCellValue('D' . ($fila + 8), ' Envío a través de Ship Now — ' . $moneda . ' ' . $pedido->getCostoEnvio());
                $spreadsheet->getActiveSheet()
                        ->setCellValue('D' . ($fila + 9), ' ' . $this->translator->trans('Shipping address') . ': ' . $pedido->getDireccionEnvio());
                break;
            case $pedido::ARREGLAR_ENVIO:
                $spreadsheet->getActiveSheet()
                        ->setCellValue('D' . ($fila + 8), ' Falta arreglar envío');
                break;
            case $pedido::OFFICE:
                $spreadsheet->getActiveSheet()
                        ->setCellValue('D' . ($fila + 8), ' Retira por oficina');
                break;
        }

        $direccionEnvio = $pedido->getDireccionFacturacionActiva();        

        $spreadsheet->getActiveSheet()
                ->setCellValue('D' . ($fila + 9), $spreadsheet->getActiveSheet()->getCell('D' . ($fila + 9)) . "\r " .
                        $this->translator->trans('Billing address') . ': ' . $direccionEnvio);

        $spreadsheet->getActiveSheet()->getStyle('D' . ($fila + 9))->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getRowDimension(($fila + 9))->setRowHeight(80);

        if ($pedido->getFormapago() !== null) {
            $formaPago = $pedido->getFormapago() . ' — ' . $moneda . ' ' . $this->pedidoService->getTotal($pedido->getId());
            if ($pedido->getFormaPago()->getId() == FormaPago::MERCADOPAGO_ID && !$pedido->getPagado()) {
                $formaPago .= ' ' . $this->translator->trans('Incomplete payment process');
            }
        } else {
            $formaPago = $this->translator->trans('Without agreeing');
        }

        $spreadsheet->getActiveSheet()
                ->setCellValue('D' . ($fila + 10), $formaPago);

        $spreadsheet->getActiveSheet()->getStyle('D' . ($fila + 2))
                ->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()
                ->getStyle('B' . $fila)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('D5D3D3');
        $spreadsheet->getActiveSheet()
                ->getStyle('E' . $fila . ':F' . ($fila + 4))->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('D5D3D3');

        for ($x = 10; $x <= ($fila + 2); $x++) {
            $spreadsheet->getActiveSheet()->getRowDimension($x)->setRowHeight(35);
        }
        $spreadsheet->getActiveSheet()->getRowDimension('2')->setRowHeight(25);
        $spreadsheet->getActiveSheet()->getRowDimension('5')->setRowHeight(30);
        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(30);
        $spreadsheet->getActiveSheet()->getRowDimension('8')->setRowHeight(45);
        /* Tamaño de las columnas */
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(100);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);

        //Con envio via ship now o tienda
        if ($pedido->getTipoEnvio() == Pedido::SHIP_NOW || $pedido->getTipoEnvio() == Pedido::STORE) {
            $spreadsheet->getActiveSheet()
                    ->getStyle('F8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('D5D3D3');
        } else {
            $spreadsheet->getActiveSheet()
                    ->getStyle('E8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('D5D3D3');
        }

        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToWidth(1);
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(0);
        $spreadsheet->getActiveSheet()->getPageSetup()->clearPrintArea();
        $spreadsheet->getActiveSheet()->getPageSetup()->setPrintArea('B1:F' . ($fila + 10));
        $spreadsheet->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 10);
        $spreadsheet->getActiveSheet()
                ->getPageMargins()->setTop(1);
        $spreadsheet->getActiveSheet()
                ->getPageMargins()->setRight(0.1);
        $spreadsheet->getActiveSheet()
                ->getPageMargins()->setLeft(0.3);
        $spreadsheet->getActiveSheet()
                ->getPageMargins()->setBottom(1);

        $spreadsheet->getActiveSheet()
                ->getHeaderFooter()->setDifferentOddEven(false);
        $spreadsheet->getActiveSheet()
                ->getHeaderFooter()->setOddFooter('Página &P / &N');

        return $spreadsheet;
    }

    /*
     * Generación de contenido archivo excel para Informe de Horas de Proyectos
     */

    private function excelListado($pedidos, $parametros) {

        $spreadsheet = new Spreadsheet();
        // Get active sheet - it is also possible to retrieve a specific sheet
        $sheet = $spreadsheet->getActiveSheet();

        //Estilos
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        //Estilos
        $styleArray2 = [
            'font' => [
                'bold' => true,
            ]
        ];

        $fila = 1;

        $sheet->setCellValue('A' . $fila, 'Número de orden');
        $sheet->setCellValue('B' . $fila, 'Email');
        $sheet->setCellValue('C' . $fila, 'Fecha');
        $sheet->setCellValue('D' . $fila, 'Estado de la orden');
        $sheet->setCellValue('E' . $fila, 'Estado del pago');
        $sheet->setCellValue('F' . $fila, 'Estado del envío');
        $sheet->setCellValue('G' . $fila, 'Moneda');
        $sheet->setCellValue('H' . $fila, 'Subtotal de productos');
        $sheet->setCellValue('I' . $fila, 'Descuento');
        $sheet->setCellValue('J' . $fila, 'Costo de envío');
        $sheet->setCellValue('K' . $fila, 'Total');
        $sheet->setCellValue('L' . $fila, 'Nombre del comprador');
        $sheet->setCellValue('M' . $fila, 'DNI / CUIT');
        $sheet->setCellValue('N' . $fila, 'Teléfono');
        $sheet->setCellValue('O' . $fila, 'Nombre para el envío');
        $sheet->setCellValue('P' . $fila, 'Teléfono para el envío');
        $sheet->setCellValue('Q' . $fila, 'Dirección');
        $sheet->setCellValue('R' . $fila, 'Número');
        $sheet->setCellValue('S' . $fila, 'Piso');
        $sheet->setCellValue('T' . $fila, 'Localidad');
        $sheet->setCellValue('U' . $fila, 'Departamento');
        $sheet->setCellValue('V' . $fila, 'Código postal');
        $sheet->setCellValue('W' . $fila, 'Provincia o estado');
        $sheet->setCellValue('X' . $fila, 'País');
        $sheet->setCellValue('Y' . $fila, 'Medio de envío');
        $sheet->setCellValue('Z' . $fila, 'Medio de pago');
        $sheet->setCellValue('AA' . $fila, 'Cupón de descuento');
        $sheet->setCellValue('AB' . $fila, 'Notas del comprador');
        $sheet->setCellValue('AC' . $fila, 'Notas del vendedor');
        $sheet->setCellValue('AD' . $fila, 'Fecha de pago');
        $sheet->setCellValue('AE' . $fila, 'Fecha de envío');
        $sheet->setCellValue('AF' . $fila, 'Nombre del producto');
        $sheet->setCellValue('AG' . $fila, 'Precio del producto');
        $sheet->setCellValue('AH' . $fila, 'Cantidad del producto');
        $sheet->setCellValue('AI' . $fila, 'SKU');
        $sheet->setCellValue('AJ' . $fila, 'Canal');
        $sheet->setCellValue('AK' . $fila, 'Código de tracking del envío');
        $sheet->setCellValue('AL' . $fila, 'Identificador de la transacción en el medio de pago');
        $sheet->setCellValue('AM' . $fila, 'Identificador de la orden');
        $sheet->setCellValue('AN' . $fila, 'Producto Físico');

        $sheet->getStyle('A' . $fila . ':AN' . $fila)->applyFromArray($styleArray2);

        $fila++;

        foreach ($pedidos as $pedido) {

            if ($pedido->getUser() !== null) {
                $entity = $pedido->getUser();
            } else {
                $entity = $pedido->getClienteWeb();
            }

            $sheet->setCellValue('C' . $fila, $pedido->getFecha() !== null ? $pedido->getFecha()->format('d/m/Y') : '');
            if ($pedido->getEstado() == $pedido::INICIADO || $pedido->getEstado() == $pedido::ACTIVO) {
                $sheet->setCellValue('D' . $fila, 'Abierta');
            } else {
                $sheet->setCellValue('D' . $fila, 'Cerrada');
            }
            if ($pedido->getPagado()) {
                $sheet->setCellValue('E' . $fila, 'Recibido');
            } else {
                $sheet->setCellValue('E' . $fila, 'Pendiente');
            }
            if ($pedido->getEstado() == $pedido::ENTREGADO) {
                $sheet->setCellValue('F' . $fila, 'Entregado');
            } else {
                $sheet->setCellValue('F' . $fila, 'Pendiente de Envío');
            }
            if ($pedido->getUser() != null) {
                $moneda = $this->monedaService->getMonedaIso($this->monedaService->getCodMonedaUsuario($pedido->getUser()->getId()));
            } else {
                $moneda = $this->monedaService->getMonedaIso(Moneda::COD_PESOS);
            }
            $sheet->setCellValue('G' . $fila, $moneda);

            $sheet->setCellValue('I' . $fila, $pedido->getValorDescuento());
            $sheet->setCellValue('J' . $fila, $pedido->getCostoEnvio());
            $sheet->setCellValue('K' . $fila, $pedido->getTotal());
            $sheet->setCellValue('L' . $fila, $entity->getNombre());
            if ($pedido->getUser() !== null && $entity->getCuit() !== null) {
                $sheet->setCellValue('M' . $fila, $entity->getCuit());
            } else {
                $sheet->setCellValue('M' . $fila, $entity->getDni());
            }
            $sheet->setCellValue('N' . $fila, $entity->getTelefono());
            $sheet->setCellValue('O' . $fila, $entity->getNombre());
            $sheet->setCellValue('P' . $fila, $entity->getTelefono());
            $sheet->setCellValue('Q' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getCalle() : '');
            $sheet->setCellValue('R' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getCalleNumero() : '');
            $sheet->setCellValue('S' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getPiso() . ' ' . $pedido->getDireccionEnvio()->getUnidadFuncional() : '');
            $sheet->setCellValue('T' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getLocalidad() : '');
            $sheet->setCellValue('U' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getDepartamento() : '');
            $sheet->setCellValue('V' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getZipCode() : '');
            $sheet->setCellValue('W' . $fila, $pedido->getDireccionEnvio() !== null ? $pedido->getDireccionEnvio()->getProvincia() : '');
            $sheet->setCellValue('X' . $fila, 'Argentina');
            $sheet->setCellValue('Y' . $fila, $this->translator->trans($pedido->getTipoEnvio()));
            $sheet->setCellValue('Z' . $fila, $this->translator->trans($pedido->getFormaPago()));
            $sheet->setCellValue('AA' . $fila, '');
            $sheet->setCellValue('AB' . $fila, $pedido->getObservacion());
            $sheet->setCellValue('AC' . $fila, '');
            $fechaPago = null;
            $idMp = '';
            $orderMp = '';
            if ($pedido->getFormaPago() !== null && $pedido->getFormaPago()->getId() == FormaPago::MERCADOPAGO_ID) {
                foreach ($pedido->getMercadosPagos() as $mp) {
                    if ($mp->getStatus() == 'approved' && ($fechaPago == null || $mp->getCreated() > $fechaPago)) {
                        $fechaPago = $mp->getCreated();
                        $idMp = $mp->getPaymentId();
                        $orderMp = $mp->getMerchantOrderId();
                    }
                }
            } else {
                $fechaPago = $pedido->getUpdated();
            }
            $sheet->setCellValue('AD' . $fila, $fechaPago !== null ? $fechaPago->format('d/m/Y') : '');
            $sheet->setCellValue('AE' . $fila, $pedido->getUpdated() !== null ? $pedido->getUpdated()->format('d/m/Y') : '');
            $sheet->setCellValue('AI' . $fila, '');
            $sheet->setCellValue('AJ' . $fila, $pedido->getDeviceCheckout());
            $sheet->setCellValue('AK' . $fila, '');
            $sheet->setCellValue('AL' . $fila, $idMp);
            $sheet->setCellValue('AM' . $fila, $orderMp);


            foreach ($pedido->getDetallespedido() as $detallePedido) {
                $sheet->setCellValue('A' . $fila, $pedido->getId());
                $sheet->setCellValue('B' . $fila, $pedido->getEmail());
                $sheet->setCellValue('AF' . $fila, $detallePedido->getProducto()->getDescripcion());
                $sheet->setCellValue('AG' . $fila, $detallePedido->getPrecio());
                $sheet->setCellValue('AH' . $fila, $detallePedido->getCantidad());
                $sheet->setCellValue('H' . $fila, $detallePedido->getPrecio() * $detallePedido->getCantidad());
                $sheet->setCellValue('AN' . $fila, 'Sí');
                $fila++;
            }
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);
        $sheet->getColumnDimension('O')->setAutoSize(true);
        $sheet->getColumnDimension('P')->setAutoSize(true);
        $sheet->getColumnDimension('Q')->setAutoSize(true);
        $sheet->getColumnDimension('R')->setAutoSize(true);
        $sheet->getColumnDimension('S')->setAutoSize(true);
        $sheet->getColumnDimension('T')->setAutoSize(true);
        $sheet->getColumnDimension('U')->setAutoSize(true);
        $sheet->getColumnDimension('V')->setAutoSize(true);
        $sheet->getColumnDimension('W')->setAutoSize(true);
        $sheet->getColumnDimension('X')->setAutoSize(true);
        $sheet->getColumnDimension('Y')->setAutoSize(true);
        $sheet->getColumnDimension('Z')->setAutoSize(true);
        $sheet->getColumnDimension('AA')->setAutoSize(true);
        $sheet->getColumnDimension('AB')->setAutoSize(true);
        $sheet->getColumnDimension('AC')->setAutoSize(true);
        $sheet->getColumnDimension('AD')->setAutoSize(true);
        $sheet->getColumnDimension('AE')->setAutoSize(true);
        $sheet->getColumnDimension('AF')->setAutoSize(true);
        $sheet->getColumnDimension('AG')->setAutoSize(true);
        $sheet->getColumnDimension('AH')->setAutoSize(true);
        $sheet->getColumnDimension('AI')->setAutoSize(true);
        $sheet->getColumnDimension('AJ')->setAutoSize(true);
        $sheet->getColumnDimension('AK')->setAutoSize(true);
        $sheet->getColumnDimension('AL')->setAutoSize(true);
        $sheet->getColumnDimension('AM')->setAutoSize(true);
        $sheet->getColumnDimension('AN')->setAutoSize(true);

        return $spreadsheet;
    }

    /**
     * @Route("operator/pedido/{id}/edit", name="pedido_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pedido $pedido): Response {
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tienda_pedido_index');
        }

        return $this->render('tienda/pedido/edit.html.twig', [
                    'pedido' => $pedido,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("operator/pedido/new", name="pedido_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response {
        $pedido = new Pedido();
        $form = $this->createForm(PedidoType::class, $pedido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $pedido->setHash(hash("sha256", serialize($pedido)));
            $entityManager->persist($pedido);
            $entityManager->flush();

            return $this->redirectToRoute('tienda_pedido_index');
        }

        return $this->render('tienda/pedido/new.html.twig', [
                    'pedido' => $pedido,
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("operator/enabled/{id}", name="pedido_enabled", methods={"GET"})
     *
     */
    public function enabled(Request $request, Pedido $pedido) {
        if (!$pedido) {
            $this->addFlash('msgOk', 'No ha sido posible cambiar el estado del pedido seleccionado');
        } else {
            $enabled = $request->get('enabled');
            $entityManager = $this->getDoctrine()->getManager();
            $pedido->setEnabled($enabled);
            $entityManager->persist($pedido);
            $entityManager->flush();
            $this->addFlash('msgOk', 'Se ha modificado el estado del pedido Nro. ' . $pedido->getId());
        }
        return $this->redirect($this->generateUrl('pedido_index'));
    }

    /**
     * @Route("operator/pagado/{id}", name="pedido_pagado", methods={"GET"})
     *
     */
    public function pagado(Request $request, Pedido $pedido) {
        if (!$pedido) {
            $this->addFlash('msgOk', 'No ha sido posible cambiar el estado de pago del pedido seleccionado');
        } else {
            $pagado = $request->get('pagado');
            $entityManager = $this->getDoctrine()->getManager();
            $pedido->setPagado($pagado);
            $pedido->setFechaPagado(new \DateTime());
            $entityManager->persist($pedido);
            $entityManager->flush();
            $this->addFlash('msgOk', 'Se ha modificado el estado de pago del pedido Nro. ' . $pedido->getId());

            if ($pedido->getPagado()) {

                //Envío de mail al Cliente
                if ($this->pedidoService->sendClientPaymentConfirmation($pedido) === false) {
                    $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
                } else {
                    $this->addFlash('msgOk', 'Se ha enviado el e-mail de confirmación de cobro al cliente');
                }

                if ($pedido->getTipoEnvio() == Pedido::SHIP_NOW) {
                    //Generar envio
                    if ($this->shipNowService->createOrder($pedido) === true) {
                        $this->addFlash('msgOk', 'Se ha generado el envio por ship now ');
                    } else {
                        $this->addFlash('msgError', '. No se ha podido generar el envio. Inténte realizar el envio nuevamente o contáctese con TITANIO para verificar el motivo por el cual no se ha generado el envio por ship now ');
                    }
                }
            }
        }
        return $this->redirect($this->generateUrl('pedido_index'));
    }

    /**
     * Cambia el estado del pedido
     *
     * @Route("operator/{id}/cambiarEstadoPedido", name="pedido_cambiar_estado", methods={"GET","POST"})
     */
    public function cambiarEstadoAction(Request $request, Pedido $pedido) {
        $estado = $request->get('estado');

        $em = $this->getDoctrine()->getManager();

        if ($pedido) {
            $pedido->setEstado($estado);

            $em->flush();
            $this->addFlash("msgOk", "Cambio de estado del pedido exitoso.");
        } else {
            $this->addFlash("msgError", "No se ha podido concretar el cambio de estado del pedido.");
        }

        return $this->redirectToRoute('pedido_index');
    }

    /**
     * Cambia el estado del pedido
     *
     * @Route("operator/{id}/pedidoListoEntregar", name="pedido_listo_entregar", methods={"GET","POST"})
     */
    public function pedidoListoEntregar(Pedido $pedido) {

        if ($this->pedidoService->sendEmailConfirmTakeAway($pedido) === false) {
            $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
        } else {
            $this->addFlash("msgOk", $this->translator->trans('The e-mail has been sent to the customer to withdraw the order.'));
        }
        return $this->redirectToRoute('pedido_index');
    }

    /**
     * @Route("operator/generar_envio/{id}", name="pedido_generar_envio", methods={"GET"})
     *
     */
    public function generarEnvio(Request $request, Pedido $pedido) {
        if (!$pedido) {
            $this->addFlash('msgOk', 'No ha sido posible generar un nuevo envio para el pedido seleccionado');
        } else {
            //Generar envio
            if ($this->shipNowService->createOrder($pedido) === true) {
                $this->addFlash('msgOk', 'Se ha generado un nuevo envio por ship now ');
            } else {
                $this->addFlash('msgError', '. No se ha podido generar el envio. Inténte realizar el envio nuevamente o contáctese con TITANIO para verificar el motivo por el cual no se ha generado el envio por ship now ');
            }
        }
        return $this->redirect($this->generateUrl('pedido_index', ['id' => $pedido->getId()]));
    }

    /**
     * @Route("reminder", name="pedido_recordatorio", methods={"GET","POST"})
     */
    public function reminderUnconfirmedOrders(Request $request): Response {

        $token = $request->get('token');
        if ($request->get('id') == '' && $token != $this->getParameter('secret')) {
            return new JsonResponse(['Error' => 'Invalid Token']);
        }

        $em = $this->getDoctrine()->getManager();
        $recordatorio = new Recordatorio();
        $recordatorio->setFecha(new \DateTime());
        $em->persist($recordatorio);

        $resultados = [];

        if ($request->get('id') != '') {
            $pedidos = $this->pedidoRepository->findBy(['id' => $request->get('id')]);
        } else {
            $pedidos = $this->pedidoRepository->traerPendientesPago();
        }

        $resultados['cantidadPendientesPago'] = sizeof($pedidos);
        $resultados['errorEnvioMail'] = 0;
        $resultados['envioCorrecto'] = 0;

        foreach ($pedidos as $pedido) {

            $detalleRecordatorio = new DetalleRecordatorio();
            $detalleRecordatorio->setPedido($pedido);
            $detalleRecordatorio->setRecordatorio($recordatorio);

            if ($this->pedidoService->sendReminderUnconfirmedOrder($pedido) === false) {
                $resultados['errorEnvioMail']++;
                $resultados['idPedidosEmailNoEnviados'][] = $pedido->getId();
                $detalleRecordatorio->setResultado(DetalleRecordatorio::ERROR);
                if ($request->get('id') != '') {
                    $this->addFlash('msgError', 'Error al intentar enviar un recordatorio al mail ' . $pedido->getEmail());
                }
            } else {
                $resultados['envioCorrecto']++;
                $pedido->setUltimoRecordatorio((new \DateTime())->setTime(0, 0, 0, 0));
                $pedido->setRecordatoriosEnviados($pedido->getRecordatoriosEnviados() + 1);
                $this->getDoctrine()->getManager()->persist($pedido);
                $detalleRecordatorio->setResultado(DetalleRecordatorio::OK);
                if ($request->get('id') != '') {
                    $this->addFlash('msgOk', 'Se ha enviado un recordatorio al mail ' . $pedido->getEmail());
                }
            }
            $em->persist($detalleRecordatorio);
        }

        $em->flush();

        if ($request->get('id') != '') {
            return $this->redirectToRoute('pedido_index');
        }
        return new JsonResponse($resultados);
    }

}
