<?php

namespace App\DataFixtures;

use App\Entity\Cotizacion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppCotizacion extends Fixture {

    public function load(ObjectManager $manager) {
        $cotizacion = new Cotizacion();
        $cotizacion->setDolar(160);
        $cotizacion->setOro(9920);
        $cotizacion->setPlata(127);
        $manager->persist($cotizacion);
       
        $manager->flush();
    }

}
