<?php

namespace App\DataFixtures;

use App\Entity\CondicionFiscal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppCondicionFiscal extends Fixture {
   
    public function load(ObjectManager $manager) {
        $condicionFiscal1 = new CondicionFiscal();
        $condicionFiscal1->setDescripcion('Monotributista');
        $manager->persist($condicionFiscal1);
        
        $condicionFiscal2 = new CondicionFiscal();
        $condicionFiscal2->setDescripcion('Responsable Inscripto');
        $manager->persist($condicionFiscal2);

        $manager->flush();
    }

}
