<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppUser extends Fixture {

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager) {
        $user = new User();
        $user->setUsername('admin');
        $user->setNombre('admin');
        $user->setTelefono('-');        
        $user->setMail('-');
        $user->setIsVerified(true);
        $roles[] = 'ROLE_ADMIN';
        $user->setRoles($roles);
        $user->setPassword($this->passwordEncoder->encodePassword(
                        $user,
                        'admin'
        ));

        $manager->persist($user);

        $manager->flush();
    }

}
