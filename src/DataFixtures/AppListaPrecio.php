<?php

namespace App\DataFixtures;

use App\Entity\ListaPrecio;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppListaPrecio extends Fixture {

    public function load(ObjectManager $manager) {
        $listaPrecio = new ListaPrecio();
        $listaPrecio->setDescripcion('Lista Web');
        $listaPrecio->setMoneda(1);
        $manager->persist($listaPrecio);

        $listaPrecio = new ListaPrecio();
        $listaPrecio->setDescripcion('Lista web USD');
        $listaPrecio->setMoneda(1);
        $manager->persist($listaPrecio);

        $listaPrecio = new ListaPrecio();
        $listaPrecio->setDescripcion('Lista Web Mayorista');
        $listaPrecio->setMoneda(2);
        $manager->persist($listaPrecio);

        $manager->flush();
    }

}
