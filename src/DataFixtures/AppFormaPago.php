<?php

namespace App\DataFixtures;

use App\Entity\Tienda\FormaPago;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFormaPago extends Fixture {
   
    public function load(ObjectManager $manager) {
        $formaPago = new FormaPago();
        $formaPago->setDescripcion('Mercado Pago');
        $manager->persist($formaPago);
        
        $formaPago = new FormaPago();
        $formaPago->setDescripcion('Arregla con De Dios');
        $manager->persist($formaPago);
        
        $formaPago = new FormaPago();
        $formaPago->setDescripcion('Efectivo');
        $manager->persist($formaPago);
        
        $formaPago = new FormaPago();
        $formaPago->setDescripcion('Transferencia Bancaria');
        $manager->persist($formaPago);
        
        $manager->flush();
    }

}
