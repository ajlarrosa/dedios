<?php

namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Common\Email;
use App\Service\General\MailerService;
use App\Entity\Tienda\FormatoMail;
use App\Service\Tienda\ConfiguracionService;
use App\Service\Tienda\FormatoMailService;

class EmailVerifier extends AbstractController {

    private $verifyEmailHelper;
    private $mailer;
    private $entityManager;
    private $translator;
    private $mailerService;
    private $configuracion;
    private $formatoMailService;

    public function __construct(VerifyEmailHelperInterface $helper, MailerInterface $mailer, EntityManagerInterface $manager, TranslatorInterface $translator, MailerService $mailerService, ConfiguracionService $configuracionService, FormatoMailService $formatoMailService) {
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        $this->entityManager = $manager;
        $this->translator = $translator;
        $this->mailerService = $mailerService;
        $this->formatoMailService = $formatoMailService;
        $this->configuracion = $configuracionService->getLastConfiguration();
        $this->formatoMailService = $formatoMailService;
    }

    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface $user) {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
                $verifyEmailRouteName,
                $user->getId(),
                $user->getMail()
        );
        $parametros = explode('&', parse_url($signatureComponents->getSignedUrl(), PHP_URL_QUERY));

        $expires = explode('=', $parametros[0]);
        $signature = explode('=', $parametros[1]);
        $token = explode('=', $parametros[2]);

        $date = new \DateTime();
        $date->setTimestamp(intval($expires[1]));

        $user->setExpires($date);
        $user->setSignature($signature[1]);
        $user->setToken($token[1]);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if ($user->hasRole('ROLE_USER_MAYORISTA')) {
            $type = FormatoMail::COD_CONFIRM_MAIL_WHOLESALER;
        } else {
            $type = FormatoMail::COD_CONFIRM_MAIL_RETAIL;
        }
                
        $formatosMails = $this->formatoMailService->getFormatoMail([
            'configuracion' => $this->configuracion->getId(),
            'type' => $type,
            'enabled' => true
        ]);        

        if (sizeof($formatosMails) == 1) {
            $formatoEmail = $formatosMails[0];
            $email = new Email();
            $email->setTitle($this->translator->trans("De Dios Jewels - Please Confirm your Email"));
            $email->setSendTo($user->getMail());
            $email->setTemplate('registration/confirmation_email.html.twig');
            $email->setParameters(['signedUrl' => $signatureComponents->getSignedUrl(), 'expiresAt' => $signatureComponents->getExpiresAt(), 'title' => $formatoEmail->getTitle(), 'body' => $formatoEmail->getBody(), 'footer' => $formatoEmail->getFooter()]);

            $response = $this->mailerService->enviarMail($email);
        } else {
            $this->addFlash("msgError", $this->translator->trans('The message could not be sent to the client. The email format is not set.'));
            $response = $this->redirectToRoute('home');
        }

        return $response;
    }

    /**
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, UserInterface $user): void {

        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getMail());

        $user->setIsVerified(true);
       
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}
