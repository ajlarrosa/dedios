<?php

namespace App\Form;

use App\Entity\MovimientoCC;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovimientoCCType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoDocumento')
            ->add('moneda')
            ->add('tarjeta')
            ->add('estado')
            ->add('factura')
            ->add('documento')
            ->add('clienteProveedor')
            ->add('unidadNegocio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MovimientoCC::class,
        ]);
    }
}
