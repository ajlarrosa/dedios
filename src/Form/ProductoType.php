<?php

namespace App\Form;

use App\Entity\Producto;
use App\Entity\Medida;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProductoType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('categoriasubcategoria', EntityType::class, [
                    'class' => 'App:Categoriasubcategoria',
                    'query_builder' => function (\App\Repository\CategoriasubcategoriaRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->Join('c.categoria', 'p')
                                ->Join('c.metal', 'm')
                                ->Join('c.subcategoria', 's')
                                ->addOrderBy('p.descripcion', 'ASC')
                                ->addOrderBy('s.descripcion', 'ASC')
                                ->addOrderBy('m.descripcion', 'ASC');
                    },
                    'label' => 'Categoría/Subcategoría/Metal',
                    'required' => true
                ])
                ->add('codigo', TextType::class, array(
                    'label' => 'Código',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('descripcion', TextType::class, array('label' => 'Nombre', 'attr' => array('class' => 'form-control')))
                ->add('descripcionMenu', TextType::class, [
                    'label' => 'Name for menu',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 25
                    ]
                ])
                ->add('tipoStock', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Units' => 'U',
                        'Silver Grams' => 'P',
                        'Gold Grams' => 'O'
            )))
                ->add('stock', TextType::class, array('label' => 'Stock', 'attr' => array('class' => 'form-control')))
                ->add('moneda', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Pesos ($)' => 1,
                        'Dolars (u$s)' => 2
            )))
                ->add('costo', TextType::class, array('label' => 'Hechura', 'attr' => array('class' => 'form-control')))
                ->add('plata', TextType::class, array(
                    'label' => 'Plata',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('oro', TextType::class, array(
                    'label' => 'Oro',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('descripcionampliada', TextareaType::class, array('label' => 'Descripción Ampliada',
                    'attr' => array('class' => 'form-control', 'style' => 'height:200px'),
                    'required' => false
                        )
                )
                ->add('orden', IntegerType::class, array('label' => 'Orden', 'attr' => array('class' => 'form-control', 'min' => '0', 'max' => '100000')))
                ->add('visible', CheckboxType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                        )
                )
                ->add('novedad', CheckboxType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('destacado', CheckboxType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('descuento', IntegerType::class, array('label' => 'Descuento',
                    'attr' => array('class' => 'form-control', 'max' => '100'),
                    'required' => false
                        )
                )
                ->add('esGrabable', CheckboxType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('maxGrabado', IntegerType::class, [
                    'label' => 'Max. characters',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('medidas', EntityType::class, [
                    'class' => 'App:Medida',
                    'query_builder' => function (\App\Repository\MedidaRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled = true')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'mapped' => false,
                    'label' => 'Medidas',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'by_reference' => false
                ])
                ->add('colores', EntityType::class, [
                    'class' => 'App:Color',
                    'query_builder' => function (\App\Repository\ColorRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled = true')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'mapped' => false,
                    'label' => 'Colores',
                    'required' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'attr' => ['class' => 'select2 form-control']
                ])
                ->add('lineasProducto', EntityType::class, [
                    'class' => 'App\Entity\Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $er) {
                        return $er->createQueryBuilder('lp')
                                ->where('lp.enabled = true')
                                ->addOrderBy('lp.descripcion', 'ASC');
                    },
                    'mapped' => false,
                    'label' => 'Líneas de producto',
                    'required' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'attr' => ['class' => 'select2 form-control']
                ])
                ->add('letras', EntityType::class, [
                    'class' => 'App:Letra',
                    'query_builder' => function (\App\Repository\LetraRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled = true')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'mapped' => false,
                    'label' => 'Letras',
                    'required' => false,
                    'multiple' => true,
                    'by_reference' => false,
                    'attr' => ['class' => 'select2 form-control']
                ])
                ->add('tipoMedida', ChoiceType::class, [
                    'label' => 'Tipo de medida',
                    'required' => false,
                    'choices' => [Medida::ANILLO => Medida::ANILLO, Medida::CADENA => Medida::CADENA, Medida::PULSERA => Medida::PULSERA],
                    'attr' => [
                        'class' => 'form-control select2'
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Producto::class,
        ]);
    }

}
