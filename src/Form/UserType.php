<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Length;

class UserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $user = $options['data'];
        if (!($user->hasRole(User::ROLE_USER_MAYORISTA) || $user->hasRole(User::ROLE_USER_MINORISTA))) {
            $builder->add('roles', ChoiceType::class, [
                'label' => 'Roles',
                'required' => true,
                'multiple' => true,
                'choices' => ['ADMINISTRATOR' => 'ROLE_ADMIN',
                    'MONITOR' => 'ROLE_OPERADOR'
                ],
                'attr' => [
                    'class' => 'form-control select2'
                ]
            ]);
        }

        $builder
                ->add('nombre', TextType::class, [
                    'required' => true,
                    'label' => 'Name',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('apellido', TextType::class, [
                    'required' => true,
                    'label' => 'Surname',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('username', TextType::class, [
                    'required' => true,
                    'label' => 'User Name',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('mail', EmailType::class, [
                    'required' => true,
                    'label' => 'E-mail',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('telefono', TextType::class, [
                    'required' => true,
                    'label' => 'Telephone',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('dni', TextType::class, [
                    'required' => false,
                    'label' => 'Document',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('cuit', TextType::class, [
                    'required' => false,
                    'label' => 'Cuit',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('web', TextType::class, [
                    'required' => false,
                    'label' => 'Web',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'options' => [
                        'attr' => ['class' => 'password-field form-control']],
                    'required' => false,
                    'first_options' => [
                        'label' => 'Password',
                        'attr' => [
                            'class' => 'form-control'
                        ]
                    ],
                    'second_options' => [
                        'label' => 'Repeat Password',
                        'attr' => [
                            'class' => 'form-control'
                        ]
                    ],
                ])        
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}
