<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Service\General\GeoService;

class RegistrationFormType extends AbstractType {

    private $geoService;

    function __construct(GeoService $geoService) {
        $this->geoService = $geoService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('username', TextType::Class, [
                    'label' => 'Username',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Please enter a username'
                    ]
                ])
                ->add('nombre', TextType::Class, [
                    'label' => 'Name',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your name'
                    ]
                ])
                ->add('apellido', TextType::Class, [
                    'label' => 'Surname',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your surname'
                    ]
                ])
                ->add('telefono', TextType::Class, [
                    'label' => 'Telephone contact',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your phone'
                    ]
                ])
                ->add('dni', IntegerType::Class, [
                    'label' => 'DNI',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'min' => 1000000,
                        'placeholder' => 'Enter your dni'
                    ]
                ])
                ->add('provincia', ChoiceType::Class, [
                    'label' => 'Province',
                    'mapped' => false,
                    'choices' => $this->getArrayProvincias(),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('calleNumero', TextType::Class, [
                    'label' => 'Número de la calle',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Enter your street number'
                    ]
                ])
                ->add('zipCode', IntegerType::Class, [
                    'label' => 'Código Postal',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your zip code'
                    ]
                ])
                ->add('piso', TextType::Class, [
                    'label' => 'Piso',
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your floot'
                    ]
                ])
                ->add('unidadFuncional', TextType::Class, [
                    'label' => 'Unidad Funcional',
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your functional unit'
                    ]
                ])
                ->add('mail', EmailType::Class, [
                    'label' => 'Contact email',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your e-mail'
                    ]
                ])
                ->add('agreeTerms', CheckboxType::class, [
                    'mapped' => false,
                    'constraints' => [
                        new IsTrue([
                            'message' => 'You should agree to our terms.',
                                ]),
                    ],
                ])
                ->add('plainPassword', PasswordType::class, [
                    // instead of being set onto the object directly,
                    // this is read and encoded in the controller
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Please enter a password'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                                ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                                ]),
                    ],
                ])
                ->add('mailing', CheckboxType::Class, [
                    'label' => 'Deseo recibir novedades y descuentos',
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'align-middle',
                        'checked' => true
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getArrayProvincias() {
        $provincias = [];
        $provinciaArray = $this->geoService->getProvincias();
        foreach ($provinciaArray as $provincia) {
            $provincias[$provincia->nombre] = $provincia->id;
        }
        return $provincias;
    }

}
