<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Service\General\GeoService;

class RegistrationMayoristaFormType extends AbstractType {

    private $geoService;

    function __construct(GeoService $geoService) {
        $this->geoService = $geoService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', TextType::Class, [
                    'label' => 'Name or company name',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your first and last name here'
                    ]
                ])
                ->add('apellido', TextType::Class, [
                    'label' => 'Surname',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your first and last name here'
                    ]
                ])
                ->add('dni', TextType::Class, [
                    'label' => 'DNI',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'No dots or spaces'
                    ]
                ])
                ->add('cuit', TextType::Class, [
                    'label' => 'CUIT',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'No dots or spaces'
                    ]
                ])
                ->add('condicionFiscal', EntityType::class, [
                    'class' => 'App:CondicionFiscal',
                    'query_builder' => function (\App\Repository\CondicionFiscalRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled =:enabled')
                                ->setParameter('enabled', 1)
                                ->orderBy('c.descripcion', 'ASC');
                    },
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Fiscal condition',
                    'required' => true
                ])
                ->add('marcaComercial', TextType::Class, [
                    'label' => 'Trademark',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Your trademark'
                    ]
                ])
                ->add('joyeriaRevendedora', ChoiceType::class,
                        [
                            'label' => 'Type of activity',
                            'attr' => ['class' => 'form-control'],
                            'choices' => [
                                User::JEWELRY => User::COD_JEWELRY,
                                User::RESELLER => User::COD_RESELLER
                            ]
                        ]
                )
                ->add('username', TextType::Class, [
                    'label' => 'User name',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Please enter a username'
                    ]
                ])
                ->add('telefono', TextType::Class, [
                    'label' => 'Telephone contact',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Input yout telephone number'
                    ]
                ])
                ->add('provincia', ChoiceType::Class, [
                    'label' => 'Province',
                    'mapped' => false,
                    'choices' => $this->getArrayProvincias(),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('calleNumero', TextType::Class, [
                    'label' => 'Número de la calle',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Enter your street number'
                    ]
                ])
                ->add('zipCode', IntegerType::Class, [
                    'label' => 'Código Postal',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your zip code'
                    ]
                ])
                ->add('piso', TextType::Class, [
                    'label' => 'Piso',
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your floot'
                    ]
                ])
                ->add('unidadFuncional', TextType::Class, [
                    'label' => 'Unidad Funcional',
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your functional unit'
                    ]
                ])
                ->add('mail', EmailType::Class, [
                    'label' => 'Contact email',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your email here'
                    ]
                ])
                ->add('plainPassword', PasswordType::class, [
                    // instead of being set onto the object directly,
                    // this is read and encoded in the controller
                    'mapped' => false,
                    'label' => 'Password',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Please enter a username'
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                                ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                                ]),
                    ],
                ])
                ->add('web', TextType::Class, [
                    'label' => 'Web / Social Network',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your web or social network url'
                    ],
                    'constraints' => [
                        new Length([
                            'max' => 255,
                            'maxMessage' => 'Your web/social network name can\'t exceed {{ limit }} characters'
                                ]),
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    private function getArrayProvincias() {
        $provincias = [];
        $provinciaArray = $this->geoService->getProvincias();
        foreach ($provinciaArray as $provincia) {
            $provincias[$provincia->nombre] = $provincia->id;
        }
        return $provincias;
    }

}
