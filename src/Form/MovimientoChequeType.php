<?php

namespace App\Form;

use App\Entity\MovimientoCheque;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovimientoChequeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoMovimiento')
            ->add('fechaMovimiento')
            ->add('cheque')
            ->add('unidadNegocio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MovimientoCheque::class,
        ]);
    }
}
