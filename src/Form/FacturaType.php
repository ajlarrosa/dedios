<?php

namespace App\Form;

use App\Entity\Factura;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FacturaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descuento', NumberType::class, ['label' => 'Descuento',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('bonificacion', NumberType::class, ['label' => 'Bonificación',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('oro', NumberType::class, ['label' => 'Oro',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('plata', NumberType::class, ['label' => 'Plata',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('observacion', TextareaType::class, ['label' => 'Observación',
                    'attr' => [
                        'maxlength' => 999,
                        'rows'=>6,
                        'placeholder'=>'Add some details to the invoice',
                        'class' => 'form-control'],
                    'required' => false])
                ->add('listaPrecio', EntityType::class, [
                    'class' => 'App:ListaPrecio',
                    'query_builder' => function (\App\Repository\ListaPrecioRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Lista de Precios',                               
                    'required' => true,                            
                    'attr' => ['class' => 'form-control select2']
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Factura::class,
        ]);
    }

}
