<?php

namespace App\Form;

use App\Entity\ClienteProveedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ProveedorType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('razonSocial', TextType::class, array(
                    'label' => 'Razón Social',
                    'attr' => array('class' => 'form-control'),
                    'required' => true
                ))
                ->add('direccion', TextType::class, array(
                    'label' => 'Dirección',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('telefono', TextType::class, array(
                    'label' => 'Teléfono',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('celular', TextType::class, array(
                    'label' => 'Celular',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('mail', EmailType::class, array(
                    'label' => 'E-mail',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('cuit', TextType::class, array(
                    'label' => 'CUIT',
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
                ->add('localidad')
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => ClienteProveedor::class,
        ]);
    }

}
