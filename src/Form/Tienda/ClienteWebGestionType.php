<?php

namespace App\Form\Tienda;

use App\Form\Tienda\ClienteWebType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ClienteWebGestionType extends ClienteWebType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder->remove('provincia');

        $builder
                ->add('provincia', TextType::Class, [
                    'label' => 'Province',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Please enter a province'
                    ]
                ])
                ->add('departamento', TextType::Class, [
                    'label' => 'Department',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Please enter a department'
                    ]
                ])
                ->add('localidad', TextType::Class, [
                    'label' => 'Locality',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Please enter a locality'
                    ]
                ])
                ->add('calle', TextType::Class, [
                    'label' => 'Street',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Please enter a street'
                    ]
                ])
        ;
    }

}
