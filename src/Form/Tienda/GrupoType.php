<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Grupo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class GrupoType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', TextType::class, ['label' => 'Descripción',
                    'attr' => [
                        'class' => 'form-control',
                        'minlength' => '2',
                        'maxlength' => '15']])
                ->add('categorias', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c) {
                        return $c->createQueryBuilder('c')
                                ->where('c.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Categoría',
                    'multiple' => true,
                    'mapped' => false,
                    'by_reference' => false,
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 opciones']
                ])
                ->add('categoriasubcategorias', EntityType::class, [
                    'class' => 'App:Categoriasubcategoria',
                    'query_builder' => function (\App\Repository\CategoriasubcategoriaRepository $s) {
                        return $s->createQueryBuilder('s')
                                ->where('s.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('s.titleWeb', 'ASC');
                    },
                    'label' => 'Subcategoría',
                    'multiple' => true,
                    'mapped' => false,
                    'by_reference' => false,
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 opciones']
                ])
                ->add('lineasProducto', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l) {
                        return $l->createQueryBuilder('l')
                                ->where('l.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l.descripcion', 'ASC');
                    },
                    'label' => 'Líneas de Producto',
                    'multiple' => true,
                    'mapped' => false,
                    'by_reference' => false,
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 opciones']
                ])
               /* ->add('menu', EntityType::class, [
                    'class' => 'App:Tienda\Menu',
                    'query_builder' => function (\App\Repository\Tienda\MenuRepository $m) {
                        return $m->createQueryBuilder('m')
                                ->where('m.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('m.descripcion', 'ASC');
                    },
                    'label' => 'Menu',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
        ])*/
                 ;
                
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Grupo::class,
        ]);
    }

}
