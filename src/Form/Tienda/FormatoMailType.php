<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\FormatoMail;
use App\Service\Tienda\FormatoMailService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FormatoMailType extends AbstractType {

    private $formatoMailService;

    function __construct(FormatoMailService $formatoMailService) {
        $this->formatoMailService = $formatoMailService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
                ->add('type', ChoiceType::class, [
                    'label' => 'Type',
                    'required' => true,
                    'choices' =>
                    $this->formatoMailService->getTypes(),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('title', TextareaType::class, ['label' => 'Title',
                    'attr' => [
                        'maxlength' => 4000,
                        'rows' => 3,
                        'class' => 'form-control'],
                    'required' => false])
                ->add('body', TextareaType::class, ['label' => 'Body',
                    'attr' => [
                        'maxlength' => 10000,
                        'rows' => 8,
                        'class' => 'form-control'],
                    'required' => false])
                ->add('footer', TextareaType::class, ['label' => 'Footer',
                    'attr' => [
                        'maxlength' => 4000,
                        'rows' => 3,
                        'class' => 'form-control'],
                    'required' => false])
                ->add('configuracion', EntityType::class, [
                    'class' => 'App\Entity\Tienda\Configuracion',
                    'query_builder' => function (\App\Repository\Tienda\ConfiguracionRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.id', 'ASC');
                    },
                    'label' => 'Configuration',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => FormatoMail::class,
        ]);
    }

}
