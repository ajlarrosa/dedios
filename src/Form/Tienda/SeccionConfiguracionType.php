<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\SeccionConfiguracion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class SeccionConfiguracionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('posicion', NumberType::class, ['label' => 'Position',
                    'attr' => [
                        'step' => 1,
                        'min' => 1,                        
                        'class' => 'form-control'],
                    'required' => true])
                ->add('titulo1', TextType::class,
                        ['label' => 'Title 1',
                            'attr' => ['class' => 'form-control'],
                            'required' => false
                        ]
                )
                ->add('titulo2', TextType::class,
                        ['label' => 'Title 2',
                            'attr' =>
                            ['class' => 'form-control'],
                            'required' => false
                        ]
                )
                ->add('subtitulo', TextType::class, ['label' => 'Subtitle',
                    'attr' => ['class' => 'form-control'],
                            'required' => false
                    ]
                )
                ->add('descripcion', TextType::class, ['label' => 'Description',
                    'attr' => [
                        'class' => 'form-control',
                        'minlength' => '2',
                        'maxlength' => '50'],
                            'required' => true
                    ])
                ->add('seccion', EntityType::class, [
                    'class' => 'App\Entity\Tienda\Seccion',
                    'query_builder' => function (\App\Repository\Tienda\SeccionRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Section type',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('configuracion', EntityType::class, [
                    'class' => 'App\Entity\Tienda\Configuracion',
                    'query_builder' => function (\App\Repository\Tienda\ConfiguracionRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.id', 'ASC');
                    },
                    'label' => 'Configuration',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('categoria1', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Category 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('categoria2', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Category 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('subcategoria1', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Subcategory 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno', 'disabled'=>'true', 'title'=>'Selecione Categoría asociada.']
                ])
                ->add('subcategoria2', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Subcategory 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno', 'disabled'=>'true', 'title'=>'Selecione Categoría asociada.']
                ])
                ->add('lineaProducto1', EntityType::class, [
                    'class' => 'App\Entity\Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Product line 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('lineaProducto2', EntityType::class, [
                    'class' => 'App\Entity\Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Product line 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('imagen1', FileType::class, ['label' => 'Image',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,                    
                            'required' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'seccionconfiguracion.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagen2', FileType::class, ['label' => 'Image',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'required' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'seccionconfiguracion.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('producto1', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                    
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')                                
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Product 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto2', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Product 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto3', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Product 3',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto4', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Product 4',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto5', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Producto 5',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto6', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Producto 6',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => SeccionConfiguracion::class,
        ]);
    }

}
