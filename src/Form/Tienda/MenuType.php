<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Menu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class MenuType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', TextType::class, ['label' => 'Descripción',
                    'attr' => [
                        'class' => 'form-control',
                        'minlength' => '2',
                        'maxlength' => '15']])
                ->add('texto1', TextareaType::class, ['label' => 'Texto 1',
                    'attr' => [
                        'maxlength' => 100,
                        'rows' => 3,
                        'class' => 'form-control'],
                    'required' => false])
                ->add('texto2', TextareaType::class, ['label' => 'Texto 2',
                    'attr' => [
                        'maxlength' => 100,
                        'rows' => 3,
                        'class' => 'form-control'],
                    'required' => false])
                ->add('imagen', FileType::class, ['label' => 'Imagen',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
                        ]])
                ->add('categoria1', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Categoría 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('categoria2', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c2) {
                        return $c2->createQueryBuilder('c2')
                                ->where('c2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c2.descripcion', 'ASC');
                    },
                    'label' => 'Categoría 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 dos']
                ])
                ->add('categoriaMenu', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c2) {
                        return $c2->createQueryBuilder('c2')
                                ->where('c2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c2.descripcion', 'ASC');
                    },
                    'label' => 'Categoría del Menu',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria1', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s1) {
                        return $s1->createQueryBuilder('s1')
                                ->where('s1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s1.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria2', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s2) {
                        return $s2->createQueryBuilder('s2')
                                ->where('s2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s2.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 dos']
                ])
                ->add('subcategoriaMenu', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s2) {
                        return $s2->createQueryBuilder('s2')
                                ->where('s2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s2.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría del Menú',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('producto1', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p1) {
                        return $p1->createQueryBuilder('p1')
                                ->where('p1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p1.descripcion', 'ASC');
                    },
                    'label' => 'Producto 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('producto2', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p2) {
                        return $p2->createQueryBuilder('p2')
                                ->where('p2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p2.descripcion', 'ASC');
                    },
                    'label' => 'Producto 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 dos']
                ])
                ->add('lineaProducto1', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l1) {
                        return $l1->createQueryBuilder('l1')
                                ->where('l1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l1.descripcion', 'ASC');
                    },
                    'label' => 'Product Line 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('lineaProducto2', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l2) {
                        return $l2->createQueryBuilder('l2')
                                ->where('l2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l2.descripcion', 'ASC');
                    },
                    'label' => 'Product Line 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 dos']
                ])
                ->add('lineaProductoMenu', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l2) {
                        return $l2->createQueryBuilder('l2')
                                ->where('l2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l2.descripcion', 'ASC');
                    },
                    'label' => 'Product Line of the menu',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('grupos', EntityType::class, [
                    'class' => 'App:Tienda\Grupo',
                    'query_builder' => function (\App\Repository\Tienda\GrupoRepository $g) {
                        return $g->createQueryBuilder('g')
                                ->where('g.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('g.descripcion', 'ASC');
                    },
                    'label' => 'Groups',
                    'multiple' => true,
                    'by_reference' => false,
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('orden', IntegerType::class, ['label' => 'Orden',
                    'empty_data' => 100,
                    'attr' => [
                        'class' => 'form-control'],
                    'required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }

}
