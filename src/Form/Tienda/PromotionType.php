<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PromotionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', TextType::class, ['label' => 'Nombre',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => '100'],
                    'required' => true])
                ->add('tipoDescuento', ChoiceType::class, ['label' => 'Tipo de Descuento',
                    'choices' => [
                        Promotion::BENEFICIO_2x1 => Promotion::BENEFICIO_2x1,
                        Promotion::BENEFICIO_3x2 => Promotion::BENEFICIO_3x2,
                        Promotion::DESCUENTO_CANTIDAD => Promotion::DESCUENTO_CANTIDAD,
                        Promotion::ENVIO_CERO => Promotion::ENVIO_CERO
                    ],
                    'attr' => [
                        'class' => 'form-control'],
                    'choice_translation_domain' => true,
                ])
                ->add('cantidadMinima', NumberType::class, ['label' => 'Cantidad Mínima (Descuento por cantidad/Monto Mínimo en envíos)',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'html5' => true,
                    'required' => false])
                ->add('porcentaje', NumberType::class, ['label' => 'Porcentaje de descuento(Descuento por cantidad)',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'max' => 100,
                        'class' => 'form-control'],
                    'html5' => true,
                    'required' => true])
                ->add('fechaDesde', DateType::class, ['label' => 'Fecha Desde (Vacío para uso sin límite de fecha)',
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('fechaHasta', DateType::class, ['label' => 'Fecha Hasta (Vacío para uso sin límite de fecha)',
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('producto', EntityType::class, [
                    'class' => 'App:Producto',
                    'query_builder' => function (\App\Repository\ProductoRepository $p) {
                        return $p->createQueryBuilder('p')
                                ->where('p.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p.descripcion', 'ASC');
                    },
                    'label' => 'Producto',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('categoria', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c) {
                        return $c->createQueryBuilder('c')
                                ->where('c.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Categoría',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])
                ->add('subcategoria', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s) {
                        return $s->createQueryBuilder('s')
                                ->where('s.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('lineaProducto', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l) {
                        return $l->createQueryBuilder('l')
                                ->where('l.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l.descripcion', 'ASC');
                    },
                    'label' => 'Línea de Productos',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2 uno']
                ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Promotion::class,
        ]);
    }

}
