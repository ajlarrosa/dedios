<?php

namespace App\Form\Tienda;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Service\General\GeoService;

class UserWebType extends AbstractType {

    private $geoService;

    function __construct(GeoService $geoService) {
        $this->geoService = $geoService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', TextType::Class, [
                    'label' => 'Name',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 200,
                        'placeholder' => 'Please enter a name'
                    ]
                ])
                ->add('apellido', TextType::Class, [
                    'label' => 'Surname',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 200,
                        'placeholder' => 'Please enter a surname'
                    ]
                ])
                ->add('mail', EmailType::Class, [
                    'label' => 'Contact email',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 100,
                        'placeholder' => 'Enter your e-mail'
                    ]
                ])
                ->add('telefono', TextType::Class, [
                    'label' => 'Telephone contact',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 100,
                        'placeholder' => 'Enter your phone'
                    ]
                ])
                ->add('dni', TextType::Class, [
                    'label' => 'DNI',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 50,
                        'placeholder' => 'Enter your dni'
                    ]
                ])
                ->add('provincia', ChoiceType::Class, [
                    'label' => 'Province',
                    'mapped' => false,
                    'required' => true,
                    'choices' => $this->getArrayProvincias(),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('calleNumero', TextType::Class, [
                    'label' => 'Número de la calle',
                    'mapped' => false,
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Enter your street number'
                    ]
                ])
                ->add('zipCode', IntegerType::Class, [
                    'label' => 'Código Postal',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your zip code'
                    ]
                ])
                ->add('piso', TextType::Class, [
                    'label' => 'Piso',
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your floot'
                    ]
                ])
                ->add('unidadFuncional', TextType::Class, [
                    'label' => 'Unidad Funcional',
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your functional unit'
                    ]
                ])
        ;
    }

    private function getArrayProvincias() {
        $provincias = [];
        $provinciaArray = $this->geoService->getProvincias();
        foreach ($provinciaArray as $provincia) {
            $provincias[$provincia->nombre] = $provincia->id;
        }
        return $provincias;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

}
