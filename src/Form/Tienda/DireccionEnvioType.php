<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\DireccionEnvio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Service\General\GeoService;
use App\Service\Tienda\DireccionEnvioService;

class DireccionEnvioType extends AbstractType {

    private $geoService;
    private $direccionEnvioService;

    function __construct(GeoService $geoService, DireccionEnvioService $direccionEnvioService) {
        $this->geoService = $geoService;
        $this->direccionEnvioService = $direccionEnvioService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
                ->add('clienteWeb')
                ->add('user')
                ->add('tipoDireccion', ChoiceType::Class, [
                    'label' => 'Address Type',
                    'required' => true,
                    'choices' => $this->direccionEnvioService->getTiposDireccion($options['data']->getTipoDireccion()),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('provincia', ChoiceType::Class, [
                    'label' => 'Province',
                    'mapped' => false,
                    'required' => true,
                    'choices' => $this->getArrayProvincias(),
                    'attr' => [
                        'class' => 'form-control',
                        'data-id' => $options['data']->getId()
                    ]
                ])
                ->add('calle_numero', TextType::Class, [
                    'label' => 'Número de la calle',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => 255,
                        'placeholder' => 'Enter your street number'
                    ]
                ])
                ->add('zipCode', IntegerType::Class, [
                    'label' => 'Código Postal',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your zip code'
                    ]
                ])
                ->add('piso', TextType::Class, [
                    'label' => 'Piso',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your floot'
                    ]
                ])
                ->add('unidadFuncional', TextType::Class, [
                    'label' => 'Unidad Funcional',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Enter your functional unit'
                    ]
                ])
        ;
    }

    private function getArrayProvincias() {
        $provincias = [];
        $provinciaArray = $this->geoService->getProvincias();
        foreach ($provinciaArray as $provincia) {
            $provincias[$provincia->nombre] = $provincia->id;
        }
        return $provincias;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => DireccionEnvio::class,
            'provincia' => 2
        ]);
    }

}
