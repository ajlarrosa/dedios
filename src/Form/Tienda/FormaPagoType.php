<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\FormaPago;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FormaPagoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            -> add('descripcion', TextType::class, ['label' => 'Descripción', 
                    'attr' => [  
                        'class' => 'form-control', 
                        'maxlength' => '50']])
            ->add('porcentajeDescuento', NumberType::class, ['label' => 'Porcentaje de descuento',
                'attr' => [
                    'step' => 0.01,
                    'min' => 0,
                    'class' => 'form-control'],
                'required' => true])
            ->add('porcentajeRecargo', NumberType::class, ['label' => 'Porcentaje de recargo',
                'attr' => [
                    'step' => 0.01,
                    'min' => 0,
                    'class' => 'form-control'],
                'required' => true])    
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormaPago::class,
        ]);
    }
}
