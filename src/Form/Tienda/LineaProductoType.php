<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\LineaProducto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class LineaProductoType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', TextType::class, ['label' => 'Description',
                    'attr' => [
                        'class' => 'form-control',
                        'maxlength' => '255']])
                ->add('titleWeb', TextType::class,
                        array('label' => 'Web title',
                            'required' => false,
                            'attr' => array('class' => 'form-control',
                                'maxlength' => '100')))
                ->add('imagen', FileType::class, ['label' => 'Image',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])


        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => LineaProducto::class,
        ]);
    }

}
