<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Promocion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PromocionType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                -> add('descripcion', TextType::class, array('label' => 'Descripción',
                    'attr' => array('class' => 'form-control', 
                                    'maxlength' => '255')))
                -> add('color', ColorType::class, ['label' => 'Color:', 
                    'attr' => [  
                        'class' => 'form-control',
                        'required' => true ]])   
                 ->add('visibleMayorista', CheckboxType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'required' => false
                ))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Promocion::class,
        ]);
    }
}
