<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Pedido;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PedidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('costoEnvio')
            ->add('fecha')
            ->add('total')
            ->add('observacion')
            ->add('formapago')
            ->add('idpago')
            ->add('estado')
            ->add('user')
            ->add('envio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pedido::class,
        ]);
    }
}
