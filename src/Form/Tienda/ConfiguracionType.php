<?php

namespace App\Form\Tienda;

use App\Entity\Tienda\Configuracion;
use App\Entity\Common\Moneda;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ConfiguracionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('monedaMayorista', ChoiceType::class, [
                    'label' => 'Currency for wholesalers',
                    'required' => true,
                    'choices' => ['Pesos' => Moneda::COD_PESOS, 'Dolar' => Moneda::COD_DOLARES],
                    'attr' => [
                        'class' => 'form-control select2'
                    ]
                ])
                ->add('monedaMinorista', ChoiceType::class, [
                    'label' => 'Currency for individuals',
                    'required' => true,
                    'choices' => ['Pesos' => Moneda::COD_PESOS, 'Dolar' => Moneda::COD_DOLARES],
                    'attr' => [
                        'class' => 'form-control select2'
                    ]
                ])
                ->add('promocion', EntityType::class, [
                    'class' => 'App:Tienda\Promocion',
                    'query_builder' => function (\App\Repository\Tienda\PromocionRepository $p) {
                        return $p->createQueryBuilder('p')
                                ->where('p.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('p.descripcion', 'ASC');
                    },
                    'label' => 'Special offer',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('menus', EntityType::class, [
                    'class' => 'App:Tienda\Menu',
                    'query_builder' => function (\App\Repository\Tienda\MenuRepository $m) {
                        return $m->createQueryBuilder('m')
                                ->where('m.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('m.descripcion', 'ASC');
                    },
                    'label' => 'Menues',
                    'multiple' => true,
                    'by_reference' => false,
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('logo', FileType::class, ['label' => 'Logo',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenResultado', FileType::class, ['label' => 'Imagen Resultado',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSlider1', FileType::class, ['label' => 'Image Main Slider 1',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSlider2', FileType::class, ['label' => 'Image Main Slider 2',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSlider3', FileType::class, ['label' => 'Image Main Slider 3',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSliderMobile1', FileType::class, ['label' => 'Image Main Slider Mobile 1',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSliderMobile2', FileType::class, ['label' => 'Image Main Slider Mobile 2',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('imagenMainSliderMobile3', FileType::class, ['label' => 'Image Main Slider Mobile 3',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])
                ->add('videoMainSlider', TextType::class,
                        array('label' => 'Url Video Main Slider',
                            'required' => false,
                            'attr' => array('class' => 'form-control')))
                ->add('categoria1', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c1) {
                        return $c1->createQueryBuilder('c1')
                                ->where('c1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c1.descripcion', 'ASC');
                    },
                    'label' => 'Categoría 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('categoria2', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c2) {
                        return $c2->createQueryBuilder('c2')
                                ->where('c2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c2.descripcion', 'ASC');
                    },
                    'label' => 'Categoría 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('categoria3', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $c3) {
                        return $c3->createQueryBuilder('c3')
                                ->where('c3.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c3.descripcion', 'ASC');
                    },
                    'label' => 'Categoría 3',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria1', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s1) {
                        return $s1->createQueryBuilder('s1')
                                ->where('s1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s1.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria2', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s2) {
                        return $s2->createQueryBuilder('s2')
                                ->where('s2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s2.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria3', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $s3) {
                        return $s3->createQueryBuilder('s3')
                                ->where('s3.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('s3.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría 3',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('lineaProducto1', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l1) {
                        return $l1->createQueryBuilder('l1')
                                ->where('l1.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l1.descripcion', 'ASC');
                    },
                    'label' => 'Product Line 1',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('lineaProducto2', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l2) {
                        return $l2->createQueryBuilder('l2')
                                ->where('l2.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l2.descripcion', 'ASC');
                    },
                    'label' => 'Product Line 2',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('lineaProducto3', EntityType::class, [
                    'class' => 'App:Tienda\LineaProducto',
                    'query_builder' => function (\App\Repository\Tienda\LineaProductoRepository $l3) {
                        return $l3->createQueryBuilder('l3')
                                ->where('l3.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('l3.descripcion', 'ASC');
                    },
                    'label' => 'Product Line 3',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('costoFijoShipNow', IntegerType::class, array('label' => 'Costo fijo Ship Now',
                    'attr' => array('class' => 'form-control', 'min' => '0'),
                    'required' => true
                        )
                )
                ->add('demoraShipNow', IntegerType::class, array('label' => 'Demora Ship Now',
                    'attr' => array('class' => 'form-control', 'min' => '0'),
                    'required' => true
                        )
                )
                ->add('demoraGrabado', IntegerType::class, array('label' => 'Demora Grabado',
                    'attr' => array('class' => 'form-control', 'min' => '0'),
                    'required' => true
                        )
                )
                ->add('mostrarFechaShipNow', ChoiceType::class, [
                    'label' => 'Should the shipping date be shown in Ship now?',
                    'required' => true,
                    'choices' => [
                        'Yes' => true,
                        'No' => false
                    ],
                    'attr' => [
                        'class' => 'form-control select2'
                    ]
                ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Configuracion::class,
        ]);
    }

}
