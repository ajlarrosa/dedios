<?php

namespace App\Form;

use App\Entity\Consignacion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsignacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha')
            ->add('fechaVencimiento')
            ->add('descuento')
            ->add('bonificacion')
            ->add('importe')
            ->add('estado')
            ->add('unidadNegocio')
            ->add('clienteProveedor')
            ->add('listaPrecio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Consignacion::class,
        ]);
    }
}
