<?php

namespace App\Form;

use App\Entity\Documento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha')
            ->add('fechaRegistracion')
            ->add('importe')
            ->add('efectivo')
            ->add('nrocomprobante')
            ->add('nrofactura')
            ->add('nroremito')
            ->add('observacion')
            ->add('descripcion')
            ->add('descuento')
            ->add('bonificacion')
            ->add('tipoDocumento')
            ->add('moneda')
            ->add('nroreciboproveedor')
            ->add('fechareciboproveedor')
            ->add('estado')
            ->add('oro')
            ->add('plata')
            ->add('movimientocc')
            ->add('tipoGasto')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Documento::class,
        ]);
    }
}
