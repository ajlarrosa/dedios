<?php

namespace App\Form;

use App\Entity\ReciboCliente;
use App\Entity\ListaPrecio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ReciboClienteType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fecha', DateType::class, [
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('importe', NumberType::class, ['label' => 'Efectivo',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control total valor'],
                    'required' => true])
                ->add('observacion', TextareaType::class, ['label' => 'Observación',
                    'attr' => [
                        'maxlength' => 1999,
                        'rows' => 6,
                        'placeholder' => 'Add some details to the collect',
                        'class' => 'form-control'],
                    'required' => false])
                ->add('moneda', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control total'),
                    'choices' => array(
                        ListaPrecio::PESOS => 1,
                        ListaPrecio::DOLARES => 2
            )))
                ->add('oro', NumberType::class, ['label' => 'Oro',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('plata', NumberType::class, ['label' => 'Plata',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => ReciboCliente::class,
        ]);
    }

}
