<?php

namespace App\Form;

use App\Entity\Banco;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BancoType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                -> add('descripcion', TextType::class, array('label' => 'Descripción', 'attr' => array('class' => 'form-control')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Banco::class,
        ]);
    }

}
