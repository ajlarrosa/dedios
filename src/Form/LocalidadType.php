<?php

namespace App\Form;

use App\Entity\Localidad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LocalidadType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', TextType::class, [
                    'label' => 'Descripción',
                    'attr' => ['class' => 'form-control']
                ])
                ->add('provincia', EntityType::class, [
                    'class' => 'App:Provincia',
                    'query_builder' => function (\App\Repository\ProvinciaRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Provincia',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('cp', IntegerType::class, array('label' => 'Código Postal', 'attr' => array('class' => 'form-control', 'min' => '0')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Localidad::class,
        ]);
    }

}
