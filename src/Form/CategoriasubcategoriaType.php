<?php

namespace App\Form;

use App\Entity\Categoriasubcategoria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoriasubcategoriaType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('categoria', EntityType::class, [
                    'class' => 'App:Categoria',
                    'query_builder' => function (\App\Repository\CategoriaRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Categoría',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('subcategoria', EntityType::class, [
                    'class' => 'App:Subcategoria',
                    'query_builder' => function (\App\Repository\SubcategoriaRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.enabled =:enabled')
                                ->setParameter('enabled', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Subcategoría',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('metal', EntityType::class, [
                    'class' => 'App:Metal',
                    'query_builder' => function (\App\Repository\MetalRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Metal',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
                ])
                ->add('titleWeb', TextType::class,
                        array('label' => 'Web title',
                            'required' => false,
                            'attr' => array('class' => 'form-control',
                                'maxlength' => '100')))
                ->add('imagen', FileType::class, ['label' => 'Image',
                    'mapped' => false,
                    'required' => false,
                    'error_bubbling' => false,
                    'constraints' => [
                        new Image([
                            'mimeTypes' => [
                                'image/png',
                                'image/jpg',
                                'image/jpeg'],
                            'mimeTypesMessage' => 'menu.imagen.type.error', // traduccion en validators.yaml  
                                ])
            ]])

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Categoriasubcategoria::class,
        ]);
    }

}
