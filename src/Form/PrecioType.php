<?php

namespace App\Form;

use App\Entity\Precio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PrecioType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('valor', NumberType::class, ['label' => 'Precio',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('listaPrecio', EntityType::class, [
                    'class' => 'App:ListaPrecio',
                    'query_builder' => function (\App\Repository\ListaPrecioRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Lista de Precios',
                    'required' => true,
                    'attr' => ['class' => 'form-control select2']
        ]);        
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Precio::class,
        ]);
    }

}
