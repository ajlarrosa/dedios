<?php

namespace App\Form;

use App\Entity\Producto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProductoStockType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('tipoStock', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        'Units' => 'U',
                        'Silver Grams' => 'P',
                        'Gold Grams' => 'O'
            )))
            ->add('stock', TextType::class, array(
                'label' => 'Stock', 
                'attr' => array('class' => 'form-control'
            )));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Producto::class,
        ]);
    }

}
