<?php

namespace App\Form;

use App\Entity\Cotizacion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CotizacionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dolar', NumberType::class, ['label' => 'Dólar',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('oro', NumberType::class, ['label' => 'Dólar',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('plata', NumberType::class, ['label' => 'Dólar',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Cotizacion::class,
        ]);
    }

}
