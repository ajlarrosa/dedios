<?php

namespace App\Form;

use App\Entity\ListaPrecio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ListaPrecioType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', TextType::class, array('label' => 'Descripción', 'attr' => array('class' => 'form-control')))
                ->add('precioGrabado', NumberType::class, ['label' => 'Precio Grabado',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('moneda', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        ListaPrecio::PESOS => 1,
                        ListaPrecio::DOLARES => 2
            )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => ListaPrecio::class,
        ]);
    }

}
