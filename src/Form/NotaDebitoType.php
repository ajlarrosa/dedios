<?php

namespace App\Form;

use App\Entity\NotaDebito;
use App\Entity\FacturaProveedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class NotaDebitoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', DateType::class, [
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => true
                ])
            ->add('importe', NumberType::class, ['label' => 'Importe',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
            ->add('observacion', TextareaType::class, ['label' => 'Observación',
                    'attr' => [
                        'maxlength' => 999,
                        'rows' => 6,
                        'placeholder' => 'Add some details to the debit note',
                        'class' => 'form-control'],
                    'required' => false])
            ->add('moneda', ChoiceType::class, array(
                    'attr' => array('class' => 'form-control select2'),
                    'choices' => array(
                        FacturaProveedor::PESOS => FacturaProveedor::COD_PESOS,
                        FacturaProveedor::DOLARES => FacturaProveedor::COD_DOLARES
                    )
                ))
            ->add('oro', NumberType::class, ['label' => 'Oro',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
            ->add('plata', NumberType::class, ['label' => 'Plata',
                'attr' => [
                    'step' => 0.01,
                    'min' => 0,
                    'class' => 'form-control'],
                'required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NotaDebito::class,
        ]);
    }
}
