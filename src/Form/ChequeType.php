<?php

namespace App\Form;

use App\Entity\Cheque;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChequeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fechaEmision', DateType::class, [
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('fechaCobro', DateType::class, [
                    'widget' => 'single_text',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('importe', NumberType::class, ['label' => 'Amount',
                    'attr' => [
                        'step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'],
                    'required' => true])
                ->add('nroCheque', TextType::class, ['label' => 'Check Number',
                    'attr' => ['class' => 'form-control'],
                    'required' => true
                ])
                ->add('cuit', TextType::class, ['label' => 'CUIT',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('firmantes', TextType::class, ['label' => 'Signatories',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ])
                ->add('banco', EntityType::class, [
                    'class' => 'App:Banco',
                    'query_builder' => function (\App\Repository\BancoRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Bank',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])
              ->add('tipoCheque', EntityType::class, [
                    'class' => 'App:TipoCheque',
                    'query_builder' => function (\App\Repository\TipoChequeRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo =:activo')
                                ->setParameter('activo', '1')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'label' => 'Check type',
                    'required' => false,
                    'attr' => ['class' => 'form-control select2']
                ])              
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Cheque::class,
        ]);
    }

}
