<?php

namespace App\Form;

use App\Entity\CondicionFiscal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CondicionFiscalType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                -> add('descripcion', TextType::class, array('label' => 'Description', 'attr' => array('class' => 'form-control', 'maxlength' => '100')))
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CondicionFiscal::class,
        ]);
    }
}
