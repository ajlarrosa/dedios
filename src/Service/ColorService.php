<?php

namespace App\Service;

use App\Repository\ColorRepository;
use Twig\Environment;

class ColorService {

    private $colorRepository;
    private $twig;

    public function __construct(ColorRepository $colorRepository, Environment $twig) {
        $this->colorRepository = $colorRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $color = $this->colorRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('color/select.html.twig', ['color' => $color]);
    }

}
