<?php

namespace App\Service;

use App\Repository\ProvinciaRepository;
use Twig\Environment;

class ProvinciaService {

    private $provinciaRepository;
    private $twig;

    public function __construct(ProvinciaRepository $provinciaRepository, Environment $twig) {
        $this->provinciaRepository = $provinciaRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo) {
        $provincias = $this->provinciaRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('provincia/select.html.twig', ['provincias' => $provincias]);
    }

}
