<?php

namespace App\Service;

use App\Repository\ProductoRepository;
use App\Entity\Producto;
use App\Repository\CotizacionRepository;
use App\Service\General\GeneralService;
use Twig\Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductoService {

    const PESOS = '$';
    const DOLARES = 'u$s';

    private $productoRepository;
    private $generalService;
    private $twig;
    private $cotizacion;
    private $showCosto;
    private $router;

    public function __construct($showCosto, GeneralService $generalService, ProductoRepository $productoRepository, CotizacionRepository $cotizacionRepository, Environment $twig, UrlGeneratorInterface $router) {
        $this->productoRepository = $productoRepository;
        $cotizacionRepository;
        $this->twig = $twig;
        $this->showCosto = $showCosto;
        $this->cotizacion = $cotizacionRepository->findOneBy(['activo' => true], ['id' => 'DESC'], 1);
        $this->router = $router;
        $this->generalService= $generalService;
    }

    /*
     * Show Costo
     */

    public function getMonedaShowCosto() {
        if ($this->showCosto == 'US') {
            echo self::DOLARES;
        } else {
            echo self::PESOS;
        }
    }

    /*
     * Costo Moneda
     */

    public function getCostoMoneda($id, $print = true) {
        $producto = $this->productoRepository->find($id);
        $costo = 0;
        if ($producto->getMoneda() == 1) {
            ;
            if ($producto->getTipoStock() == Producto::COD_UNIDAD) {
                $costo = round($producto->getCosto() + ($producto->getOro() * $this->cotizacion->getOro()) + $producto->getPlata() * $this->cotizacion->getPlata(), 2);
            } else if ($producto->getTipoStock() == Producto::COD_GRAMO_PLATA) {
                $costo = round($producto->getPlata() * $producto->getCosto() / $this->cotizacion->getDolar(), 2);
            } else {
                $costo = round($producto->getOro() * $producto->getCosto() / $this->cotizacion->getDolar(), 2);
            }
        } else {
            if ($producto->getTipoStock() == Producto::COD_UNIDAD) {
                $costo = round($producto->getCosto() + ($producto->getOro() * $this->cotizacion->getOro() / $this->cotizacion->getDolar()) + $producto->getPlata() * $this->cotizacion->getPlata() / $this->cotizacion->getDolar(), 2);
            } else if ($producto->getTipoStock() == Producto::COD_GRAMO_PLATA) {
                $costo = round($producto->getPlata() * $producto->getCosto(), 2);
            } else {
                $costo = round($producto->getOro() * $producto->getCosto(), 2);
            }
        }

        if (($this->showCosto == 'US' && $producto->getMoneda() == Producto::COD_PESOS) || ($this->showCosto == 'ARG' && $producto->getMoneda() == Producto::COD_DOLAR)) {
            $costo = round($costo / $this->cotizacion->getDolar(), 2);
        }
        if ($print) {
            echo $costo;
        } else {
            return $costo;
        }
    }

    /*
     * Get producto by id
     */

    public function getProducto($id) {
        return $this->productoRepository->find($id);
    }

    /*
     * Get Route
     */

    public function getRoute($id) {

        $producto = $this->productoRepository->find($id);

        if ($producto) {
            return $this->router->generate('producto_tienda_show', [
                        'categoria' => $producto->getCategoriasubcategoria()->getCategoria()->getDescripcion(),
                        'subcategoria' => $producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion(),
                        'id' => $producto->getId(),
                        'descripcion' => $this->generalService->slugify($producto->getDescripcion())
            ]);
        }

        return '#';
    }

    

}
