<?php

namespace App\Service;

use App\Repository\BancoRepository;
use Twig\Environment;

class BancoService {

    private $bancoRepository;
    private $twig;

    public function __construct(BancoRepository $bancoRepository, Environment $twig) {
        $this->bancoRepository = $bancoRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo,$array=null) {
        $bancos = $this->bancoRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('banco/select.html.twig', ['bancos' => $bancos,'array'=>$array]);
    }


    
}
