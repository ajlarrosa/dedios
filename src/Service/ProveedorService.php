<?php

namespace App\Service;

use App\Entity\ClienteProveedor;
use App\Repository\ProveedorRepository;
use Twig\Environment;

class ProveedorService {

    private $proveedorRepository;
    private $twig;

    public function __construct(ProveedorRepository $proveedorRepository, Environment $twig) {
        $this->proveedorRepository = $proveedorRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo) {
        $proveedores = $this->proveedorRepository->findBy(['enabled' => $activo,'clienteProveedor'=> ClienteProveedor::PROVEEDOR_COD], ['razonSocial' => 'ASC']);
        echo $this->twig->render('proveedor/select.html.twig', ['proveedores' => $proveedores]);
    }
     public function getProveedorById($id) {
         return $this->proveedorRepository->find($id);        
     }
}
