<?php

namespace App\Service;

class UserService {
    
    private $userRols;

    public function __construct($userRols) {       
        $this->userRols=$userRols;
    }

    public function getUserRols() {
        return $this->userRols;
        
    }

}
