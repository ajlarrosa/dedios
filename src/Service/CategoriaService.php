<?php

namespace App\Service;

use App\Repository\CategoriaRepository;
use Twig\Environment;

class CategoriaService {

    private $categoriaRepository;
    private $twig;

    public function __construct(CategoriaRepository $categoriaRepository, Environment $twig) {
        $this->categoriaRepository = $categoriaRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $categorias = $this->categoriaRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('categoria/select.html.twig', ['categorias' => $categorias]);
    }

}
