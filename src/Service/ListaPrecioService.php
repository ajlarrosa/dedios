<?php

namespace App\Service;

use App\Repository\ListaPrecioRepository;
use App\Entity\ListaPrecio;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;

class ListaPrecioService {

    private $listaPrecioRepository;
    private $twig;
    private $security;

    public function __construct(ListaPrecioRepository $listaPrecioRepository, Environment $twig, Security $security) {
        $this->listaPrecioRepository = $listaPrecioRepository;
        $this->twig = $twig;
        $this->security = $security;
    }

    public function getSymbol($moneda) {
        if (ListaPrecio::COD_PESOS == $moneda) {
            return ListaPrecio::PESOS;
        }
        return ListaPrecio::DOLARES;
    }

    public function getSelect($activo, $array = null) {
        $listasPrecio = $this->listaPrecioRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('listaprecio/select.html.twig', ['listasPrecio' => $listasPrecio, 'array' => $array]);
    }

    /*
     * Get Precio grabado
     */

    public function getPrecioGrabado() {
        if ($this->security->isGranted("ROLE_USER_MAYORISTA")) {
            $precioGrabado = $this->listaPrecioRepository->findOneBy(['id' => ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB]);
        } else {
            $precioGrabado = $this->listaPrecioRepository->findOneBy(['id' => ListaPrecio::LISTA_PRECIO_MINORISTA_WEB]);
        }
        return $precioGrabado->getPrecioGrabado();
    }
    /*
     * Get Symbol by lista
     */
      public function getSymbolByLista($listaPrecio) {
          $lista=$this->listaPrecioRepository->find($listaPrecio);          
          return $lista->getMonedaSimbolo();        
    }
    
}
