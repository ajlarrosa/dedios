<?php

namespace App\Service;

use App\Repository\CondicionFiscalRepository;
use Twig\Environment;

class CondicionFiscalService {

    private $condicionFiscalRepository;
    private $twig;

    public function __construct(CondicionFiscalRepository $condicionFiscalRepository, Environment $twig) {
        $this->condicionFiscalRepository = $condicionFiscalRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $condicionesFiscales=$this->condicionFiscalRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/condicionFiscal/select.html.twig', ['condicionesFiscales' => $condicionesFiscales]);
    }

}
