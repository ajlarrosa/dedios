<?php

namespace App\Service;

use App\Entity\Factura;
use App\Entity\Metal;
use App\Entity\NotaCredito;
use App\Entity\NotaDebito;
use App\Entity\ReciboCliente;
use App\Entity\OrdenPago;
use App\Entity\ClienteProveedor;
use App\Entity\FacturaProveedor;
use App\Entity\MovimientoCC;
use App\Repository\MovimientoRepository;
use App\Repository\ClienteProveedorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class MovimientoCCService {

    private $movimientoRepository;
    private $clienteProveedorRepository;
    private $twig;
    private $em;

    public function __construct(MovimientoRepository $movimientoRepository, ClienteProveedorRepository $clienteProveedorRepository, Environment $twig, EntityManagerInterface $em) {
        $this->movimientoRepository = $movimientoRepository;
        $this->clienteProveedorRepository = $clienteProveedorRepository;
        $this->twig = $twig;
        $this->em = $em;
    }

    /*
     * Create
     */

    public function create(ClienteProveedor $clienteProveedor, Factura $factura = null, NotaCredito $notaCredito = null, NotaDebito $notaDebito = null, ReciboCliente $reciboCliente = null, OrdenPago $ordenPago = null, FacturaProveedor $facturaProveedor = null) {
        $movimientoCC = new MovimientoCC();
        $movimientoCC->setClienteProveedor($clienteProveedor);
        $movimientoCC->setFactura($factura);
        $movimientoCC->setNotaCredito($notaCredito);
        $movimientoCC->setNotaDebito($notaDebito);
        $movimientoCC->setReciboCliente($reciboCliente);
        $movimientoCC->setOrdenPago($ordenPago);
        $movimientoCC->setFacturaProveedor($facturaProveedor);

        //$this->em->persist($movimientoCC);
        //$this->em->flush();       
        return $movimientoCC;
    }

    /*
     * Get Parcial Balance
     */

    public function getParcialBalance($id, $moneda) {
        $movimiento = $this->movimientoRepository->find($id);

        $saldoInicial = $movimiento->getClienteProveedor()->getSaldoInicial($moneda);
        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;

        $sumasNotaCredito = $this->movimientoRepository->getSumas($movimiento, MovimientoCC::NOTA_CREDITO, $moneda);
        $sumasNotaDebito = $this->movimientoRepository->getSumas($movimiento, MovimientoCC::NOTA_DEBITO, $moneda);

        if ($movimiento->getClienteProveedor()->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

            $movimientosFacturas = $this->movimientoRepository->getFacturas($movimiento, $moneda);
            foreach ($movimientosFacturas as $movimientoFactura) {
                $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getTotal();
            }
            $sumasReciboCliente = $this->movimientoRepository->getSumas($movimiento, MovimientoCC::RECIBO_CLIENTE, $moneda);
            $total = $saldoInicial - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
        } else {
            $movimientosFacturas = $this->movimientoRepository->getFacturasProveedor($movimiento, $moneda);
            foreach ($movimientosFacturas as $movimientoFactura) {
                $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getTotal();
            }

            $sumasOrdenPago = $sumasFacturaProveedor + $this->movimientoRepository->getSumas($movimiento, MovimientoCC::ORDEN_PAGO, $moneda);
            $total = $saldoInicial - $sumasFacturaProveedor + $sumasOrdenPago + $sumasNotaCredito - $sumasNotaDebito;
        }

        return $total;
    }

    /*
     * Get Parcial Balance Metal
     */

    public function getParcialBalanceMetal($id, $metal) {
        $movimiento = $this->movimientoRepository->find($id);

        $saldoInicial = $movimiento->getClienteProveedor()->getSaldoInicialMetal($metal);
        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;

        $sumasNotaCredito = $this->movimientoRepository->getSumasMetal($movimiento, MovimientoCC::NOTA_CREDITO, $metal);
        $sumasNotaDebito = $this->movimientoRepository->getSumasMetal($movimiento, MovimientoCC::NOTA_DEBITO, $metal);

        if ($movimiento->getClienteProveedor()->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

            $movimientosFacturas = $this->movimientoRepository->getFacturasMetal($movimiento, $metal);
            foreach ($movimientosFacturas as $movimientoFactura) {
                if ($metal == Metal::GOLD) {
                    $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getOro();
                }
                if ($metal == Metal::SILVER) {
                    $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getPlata();
                }
            }
            $sumasReciboCliente = $this->movimientoRepository->getSumasMetal($movimiento, MovimientoCC::RECIBO_CLIENTE, $metal);
            $total = $saldoInicial - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
        } else {
            $movimientosFacturas = $this->movimientoRepository->getFacturasProveedorMetal($movimiento, $metal);
            foreach ($movimientosFacturas as $movimientoFactura) {
                if ($metal == Metal::GOLD) {
                    $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getOro();
                }
                if ($metal == Metal::SILVER) {
                    $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getPlata();
                }
            }

            $sumasOrdenPago = $sumasFacturaProveedor + $this->movimientoRepository->getSumasMetal($movimiento, MovimientoCC::ORDEN_PAGO, $metal);
            $total = $saldoInicial - $sumasFacturaProveedor + $sumasOrdenPago + $sumasNotaCredito - $sumasNotaDebito;
        }

        return $total;
    }

    /*
     * Get Balance
     */

    public function getBalance($clienteProveedorId, $moneda) {
        $clienteProveedor = $this->clienteProveedorRepository->find($clienteProveedorId);
      
        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;
        $sumasNotaCredito = 0;
        $sumasNotaDebito = 0;
        $total = $clienteProveedor->getSaldoInicial($moneda);
        $ultimoMovimiento = $this->movimientoRepository->getLastMovimiento($clienteProveedor);

        if ($ultimoMovimiento) {

            $sumasNotaCredito = floatval($this->movimientoRepository->getSumas($ultimoMovimiento, MovimientoCC::NOTA_CREDITO, $moneda));
            $sumasNotaDebito = floatval($this->movimientoRepository->getSumas($ultimoMovimiento, MovimientoCC::NOTA_DEBITO, $moneda));

            if ($clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

                $movimientosFacturas = $this->movimientoRepository->getFacturas($ultimoMovimiento, $moneda);
                foreach ($movimientosFacturas as $movimientoFactura) {
                    $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getTotal();
                }

                $sumasReciboCliente = floatval($this->movimientoRepository->getSumas($ultimoMovimiento, MovimientoCC::RECIBO_CLIENTE, $moneda));

                $total = $total - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
            } else {
                $movimientosFacturas = $this->movimientoRepository->getFacturasProveedor($ultimoMovimiento, $moneda);
                foreach ($movimientosFacturas as $movimientoFactura) {
                    $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getTotal();
                }

                $sumasOrdenPago = $sumasFacturaProveedor + $this->movimientoRepository->getSumas($ultimoMovimiento, MovimientoCC::ORDEN_PAGO, $moneda);
                $total = $total - $sumasFacturaProveedor + $sumasOrdenPago + $sumasNotaCredito - $sumasNotaDebito;
            }
        }

        return $total;
    }

    /*
     * Get Balance
     */

    public function getBalanceMetal($clienteProveedorId, $metal) {
        $clienteProveedor = $this->clienteProveedorRepository->find($clienteProveedorId);
  
        $sumasFactura = 0;
        $sumasFacturaProveedor = 0;
        $sumasOrdenPago = 0;
        $sumasNotaCredito = 0;
        $sumasNotaDebito = 0;
        $total = $clienteProveedor->getSaldoInicial($metal);
        $ultimoMovimiento = $this->movimientoRepository->getLastMovimiento($clienteProveedor);

        if ($ultimoMovimiento) {

            $sumasNotaCredito = $this->movimientoRepository->getSumasMetal($ultimoMovimiento, MovimientoCC::NOTA_CREDITO, $metal);
            $sumasNotaDebito = $this->movimientoRepository->getSumasMetal($ultimoMovimiento, MovimientoCC::NOTA_DEBITO, $metal);

            if ($clienteProveedor->getClienteProveedor() == ClienteProveedor::CLIENTE_COD) {

                $movimientosFacturas = $this->movimientoRepository->getFacturasMetal($ultimoMovimiento, $metal);
                foreach ($movimientosFacturas as $movimientoFactura) {
                    if ($metal == Metal::GOLD) {
                        $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getOro();
                    }
                    if ($metal == Metal::SILVER) {
                        $sumasFactura = $sumasFactura + $movimientoFactura->getFactura()->getPlata();
                    }
                }
                $sumasReciboCliente = $this->movimientoRepository->getSumasMetal($ultimoMovimiento, MovimientoCC::RECIBO_CLIENTE, $metal);
                $total = $total - $sumasFactura + $sumasReciboCliente + $sumasNotaCredito - $sumasNotaDebito;
            } else {
                $movimientosFacturas = $this->movimientoRepository->getFacturasProveedor($ultimoMovimiento, $metal);
                foreach ($movimientosFacturas as $movimientoFactura) {
                    if ($metal == Metal::GOLD) {
                        $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getOro();
                    }
                    if ($metal == Metal::SILVER) {
                        $sumasFacturaProveedor = $sumasFacturaProveedor + $movimientoFactura->getFacturaProveedor()->getPlata();
                    }
                }

                $sumasOrdenPago = $sumasFacturaProveedor + $this->movimientoRepository->getSumasMetal($ultimoMovimiento, MovimientoCC::ORDEN_PAGO, $metal);
                $total = $total - $sumasFacturaProveedor + $sumasOrdenPago + $sumasNotaCredito - $sumasNotaDebito;
            }
        }

        return $total;
    }

}
