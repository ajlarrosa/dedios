<?php

namespace App\Service;

use App\Repository\MetalRepository;
use Twig\Environment;

class MetalService {

    private $metalRepository;
    private $twig;

    public function __construct(MetalRepository $metalRepository, Environment $twig) {
        $this->metalRepository = $metalRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo) {
        $metales = $this->metalRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('metal/select.html.twig', ['metales' => $metales]);
    }

}
