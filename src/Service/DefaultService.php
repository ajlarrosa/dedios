<?php

namespace App\Service;


use Twig\Environment;


class DefaultService {

   
    private $twig;

    public function __construct( Environment $twig) {        
        $this->twig = $twig;
    }

    public function getStatusSelect() {
        echo $this->twig->render('default/statusSelect.html.twig');
    }

}
