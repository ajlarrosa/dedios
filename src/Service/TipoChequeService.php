<?php

namespace App\Service;

use App\Repository\TipoChequeRepository;
use Twig\Environment;

class TipoChequeService {

    private $tipoChequeRepository;
    private $twig;

    public function __construct(TipoChequeRepository $tipoChequeRepository, Environment $twig) {
        $this->tipoChequeRepository = $tipoChequeRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo,$array=null) {
        $tipoCheques = $this->tipoChequeRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('tipocheque/select.html.twig', ['tipoCheques' => $tipoCheques,'array'=>$array]);
    }

}
