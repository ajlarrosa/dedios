<?php

namespace App\Service;

use App\Repository\ProductoFacturaRepository;
use App\Entity\ProductoFactura;
use Doctrine\ORM\EntityManagerInterface;

use Twig\Environment;

class ProductoFacturaService {

    private $productoFacturaRepository;
    private $twig;
    private $em;

    public function __construct(ProductoFacturaRepository $productoFacturaRepository,  Environment $twig,EntityManagerInterface $em) {
        $this->productoFacturaRepository = $productoFacturaRepository;       
        $this->twig = $twig;  
        $this->em =$em;
    }

    /*
     * Create
     */
    public function create($factura,$producto,$cantidad,$descripcion,$precio) {
        $productoFactura = new ProductoFactura();
        $productoFactura->setFactura($factura);
        $productoFactura->setCantidad($cantidad);
        $productoFactura->setDescripcion($descripcion);
        $productoFactura->setPrecio(floatval($precio));
        $productoFactura->setProducto($producto);
        
        //$this->em->persist($productoFactura);
        //$this->em->flush();
        return $productoFactura;        
    }

}
