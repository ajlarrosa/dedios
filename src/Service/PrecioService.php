<?php

namespace App\Service;

use App\Repository\PrecioRepository;
use App\Repository\AtributosProductoRepository;
use App\Repository\PrecioMedidaProductoRepository;
use App\Repository\ListaPrecioRepository;
use Twig\Environment;
use App\Entity\ListaPrecio;
use Symfony\Component\Security\Core\Security;

class PrecioService {

    private $precioRepository;
    private $precioMedidaProductoRepository;
    private $atributosProductoRepository;
    private $listaPrecioRepository;
    private $twig;
    private $security;

    public function __construct(PrecioRepository $precioRepository, ListaPrecioRepository $listaPrecioRepository, AtributosProductoRepository $atributosProductoRepository, PrecioMedidaProductoRepository $precioMedidaProductoRepository, Environment $twig, Security $security) {
        $this->precioRepository = $precioRepository;
        $this->atributosProductoRepository = $atributosProductoRepository;
        $this->listaPrecioRepository = $listaPrecioRepository;
        $this->precioMedidaProductoRepository = $precioMedidaProductoRepository;
        $this->twig = $twig;
        $this->security = $security;
    }

    /*
     * Get select precios
     */

    public function getSelect($productoId) {
        echo $this->twig->render('precio/select.html.twig', [
            'precios' =>
            $this->getPrecios($productoId)
                ]
        );
    }

    /*
     * Get Precios
     */

    public function getPrecios($productoId) {
        return $this->precioRepository->findPreciosBy(
                        ['producto' => $productoId,
                            'activo' => '1'
        ]);
    }

    /*
     * Get List Prices
     */

    public function getListPrecios($productoId) {
        echo $this->twig->render('precio/list.html.twig', [
            'precios' =>
            $this->getPrecios($productoId)
                ]
        );
    }

    /*
     * Verificar Precio Producto
     * 
     */

    public function verificarPrecioProducto($productoId) {
        $cantidadPrecios = count($this->getPrecios($productoId));
        $cantidadListas = count($this->listaPrecioRepository->findBy(['activo' => true]));
        if ($cantidadPrecios == $cantidadListas) {
            return true;
        }
        return false;
    }

    /*
     * Get Price
     */

    public function getPrice($id) {

        if ($this->security->isGranted("ROLE_USER_MAYORISTA")) {
            $precio = $this->precioRepository->getPrecioBase([
                'producto' => $id,
                'listaPrecio' => ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB,
                'activo' => '1']);
        } else {
            $precio = $this->precioRepository->getPrecioBase([
                'producto' => $id,
                'listaPrecio' => ListaPrecio::LISTA_PRECIO_MINORISTA_WEB,
                'activo' => '1']);
        }

        if ($precio) {
            return $precio->getValor();
        } else {
            return null;
        }
    }

    /*
     * Get Precio según medida
     */

    public function getPrecioMedida($productoId, $medidaId, $listaPrecioId) {

        $atributoProducto = $this->atributosProductoRepository->findOneBy(['producto' => $productoId, 'medida' => $medidaId]);

        if ($atributoProducto) {
            foreach ($atributoProducto->getPreciosMedidaProducto() as $precioMedidaProducto) {
                if ($precioMedidaProducto->getEnabled() && $precioMedidaProducto->getPrecio()->getActivo() && $precioMedidaProducto->getPrecio()->getListaPrecio()->getId() == $listaPrecioId) {
                    return $precioMedidaProducto->getPrecio()->getValor();
                }
            }
        }
        return null;
    }

    /*
     * Get Precio según medida
     */

    public function getIdPrecioMedida($productoId, $medidaId, $listaPrecioId) {

        $atributoProducto = $this->atributosProductoRepository->findOneBy(['producto' => $productoId, 'medida' => $medidaId]);

        if ($atributoProducto) {
            foreach ($atributoProducto->getPreciosMedidaProducto() as $precioMedidaProducto) {
                if ($precioMedidaProducto->getEnabled() && $precioMedidaProducto->getPrecio()->getActivo() && $precioMedidaProducto->getPrecio()->getListaPrecio()->getId() == $listaPrecioId) {
                    return $precioMedidaProducto->getPrecio()->getId();
                }
            }
        }
        return null;
    }

    /*
     * Get Precio según lista precio
     */

    public function getPrecioPorListaPrecio($productoId, $listaPrecioId) {

        $precio = $this->precioRepository->findOneBy(['activo' => true, 'producto' => $productoId, 'listaPrecio' => $listaPrecioId]);

        if ($precio) {
            return $precio->getValor();
        }
        return null;
    }

    /*
     * Get Precio según medida final
     */

    public function getPrecioMedidaFinal($productoId,$medida,$listaPrecioId) {
        $atributoProducto = $this->precioMedidaProductoRepository->precioFinal([
            'producto' => $productoId,
            'medida' => $medida,
            'listaPrecio' => $listaPrecioId
        ]);

        if (isset($atributoProducto[0])) {
            return $atributoProducto[0]->getPrecio()->getValor();
        }
        
        $precio = $this->precioRepository->findPreciosBy([
            'activo' => true,
            'producto' => $productoId,
            'listaPrecio' => $listaPrecioId
        ]);
        
        return ($precio) ? $precio[0]->getValor() : null;        
    }

    /*
     * Get Period
     */

    public function getPeriod($filtros) {
        $maxPrice = round($this->precioRepository->getMaxPrice($filtros) / 4);
        $round = (strlen($maxPrice) - 1) * -1;
        return round($maxPrice, $round);
    }

}
