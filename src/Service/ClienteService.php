<?php

namespace App\Service;

use App\Repository\ClienteRepository;
use App\Service\Tienda\DireccionEnvioService;
use App\Entity\Tienda\DireccionEnvio;
use App\Entity\ClienteProveedor;
use App\Entity\User;
use Twig\Environment;
use Doctrine\ORM\EntityManagerInterface;

class ClienteService {

    private $clienteRepository;
    private $direccionEnvioService;
    private $twig;
    private $em;

    public function __construct(DireccionEnvioService $direccionEnvioService,ClienteRepository $clienteRepository, Environment $twig, EntityManagerInterface $em) {
        $this->clienteRepository = $clienteRepository;
        $this->direccionEnvioService= $direccionEnvioService;
        $this->twig = $twig;
        $this->em = $em;
    }

    /*
     * Get Select
     */
    public function getSelect($enabled) {
        $clientees = $this->clienteRepository->findBy(['enabled' => $enabled, 'clienteProveedor' => ClienteProveedor::CLIENTE_COD], ['razonSocial' => 'ASC']);
        echo $this->twig->render('cliente/select.html.twig', ['clientes' => $clientees]);
    }

    /*
     * 
     */

    public function getClienteById($id) {
        return $this->clienteRepository->find($id);
    }

    /*
     * Create Cliente from user
     */

    public function createClienteFromUser(User $user) {
        if (sizeof($this->clienteRepository->findBy(['mail' => $user->getMail()])) == 0) {

            $cliente = new ClienteProveedor();
            $cliente->setRazonSocial($user->getNombre());
            $cliente->setTelefono($user->getTelefono());
            $cliente->setMail($user->getMail());
            if ($user->hasRole('ROLE_USER_MAYORISTA')) {
                $cliente->setCondicionFiscal($user->getCondicionFiscal());
                $cliente->setCuit($user->getCuit());
                $direccionesEnvio = $this->direccionEnvioService->getDireccionesEnvio(['user'=>$user,'enabled'=>true,'tipo_direccion'=>DireccionEnvio::CASA]);
                $direccion=$direccionesEnvio[0];
                $cliente->setDireccion($direccion->getProvincia().', '.$direccion->getLocalidad().','.$direccion->getCalle().','.$direccion->getCalleNumero());
            }

            $this->em->persist($cliente);
            $this->em->flush($cliente);

            return $cliente;
        } else {
            return false;
        }
    }

}
