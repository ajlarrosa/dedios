<?php

namespace App\Service;

use App\Entity\ReciboCliente;
use App\Entity\MovimientoCC;
use Doctrine\ORM\EntityManagerInterface;

class ReciboClienteService {

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function create($cobranza) {
        try {
            $reciboCliente = new ReciboCliente();
            $movimientoCC = new MovimientoCC();

            $movimientoCC->setClienteProveedor($cobranza->cliente);
            $movimientoCC->setReciboCliente($reciboCliente);

            $reciboCliente->setFecha($cobranza->fecha);
            $reciboCliente->setImporte($cobranza->importe);
            $reciboCliente->setMoneda($cobranza->moneda);            
            $reciboCliente->setMovimientocc($movimientoCC);
            $reciboCliente->setFormaPago($cobranza->formaPago);

            $this->em->persist($movimientoCC);
            $this->em->persist($reciboCliente);
            $this->em->flush();

            return $reciboCliente;
        } catch (\Exception $e) {
            return false;
        }
    }

}
