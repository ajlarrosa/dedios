<?php

namespace App\Service;

use App\Repository\MovimientoChequeRepository;
use App\Entity\Cheque;
use App\Entity\MovimientoCheque;
use Doctrine\ORM\EntityManagerInterface;

class MovimientoChequeService {

    private $movimientoChequeRepository;    
    private $em;

    public function __construct(MovimientoChequeRepository $movimientoChequeRepository,EntityManagerInterface $em) {
        $this->movimientoChequeRepository = $movimientoChequeRepository;           
        $this->em =$em;
    }

    /*
     * Create
     */    
    public function create($tipoMovimiento,Cheque $cheque) {
        $movimientoCheque = new MovimientoCheque();
        $movimientoCheque->setCheque($cheque);
        $movimientoCheque->setTipoMovimiento($tipoMovimiento);
        
        //$this->em->persist($movimientoCC);
        //$this->em->flush();       
        return $movimientoCheque;        
    }

}
