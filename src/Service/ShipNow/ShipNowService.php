<?php

namespace App\Service\ShipNow;

use App\Service\General\CurlService;
use App\Service\ImagenProductoService;
use App\Entity\Tienda\Pedido;
use App\Entity\Tienda\DetallePedido;
use App\Entity\ShipNow\ShipNow;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\Tienda\ConfiguracionService;

class ShipNowService extends CurlService {

    private $shipNowUser;
    private $imagenProductoService;
    private $shipNowPassword;
    private $shipNowUrl;
    private $rootDirLogo;
    private $rootDirImages;
    private $em;
    private $configuracion;

    public function __construct(
            $rootDirImages,
            $rootDirLogo,
            $shipNowUser,
            $shipNowPassword,
            $shipNowUrl,
            EntityManagerInterface $em,
            ConfiguracionService $configuracionService,
            ImagenProductoService $imagenProductoService
    ) {
        $this->shipNowUser = $shipNowUser;
        $this->shipNowPassword = $shipNowPassword;
        $this->shipNowUrl = $shipNowUrl;
        $this->rootDirLogo = $rootDirLogo;
        $this->rootDirImages = $rootDirImages;
        $this->em = $em;
        $this->configuracion = $configuracionService->getLastConfiguration();
        $this->imagenProductoService = $imagenProductoService;
        parent::__construct($this->shipNowUrl);
    }

    /*
     * Show Token
     */

    private function returnToken() {
        return $this->getToken('users/authentication', ['user' => $this->shipNowUser, 'password' => $this->shipNowPassword]);
    }

    /*
     * Calculate price
     */

    public function calculatePrice($data) {

        $user = json_decode($this->returnToken());

        if (!array_key_exists('weight', $data)) {
            $data['weight'] = '0.5';
        }

        $method = "shipping_options?weight=" . $data['weight'] . "&to_zip_code=" . $data['zipCode'] . "&types=ship_pap%2Cship_pas";

        $response = json_decode($this->getData($method, $user->token));
        return $response->results;
    }

    /*
     * Simplify Options Price
     */

    public function simplifyOptionsPrice($data, $grabado) {
        $response = [];
        $options = $this->calculatePrice($data);

        foreach ($options as $key => $option) {
            $element = [];
            $element['price'] = $option->price;
            $element['shipping_service'] = $option->shipping_service->description;
            $element['ship_to_type'] = $option->ship_to_type;
            $delay = $this->configuracion->getDemoraShipNow();
            if ($grabado) {
                $delay += $this->configuracion->getDemoraGrabado();
            }
            $date = new \Datetime($option->minimum_delivery);
            date_add($date, date_interval_create_from_date_string($delay . ' days'));
            $element['minimum_delivery'] = date_format($date, DATE_ATOM);
            //if($element['shipping_service']=='BMBA - Entrega puerta a puerta'){
            $date = new \Datetime($option->maximum_delivery);


            date_add($date, date_interval_create_from_date_string($delay . ' days'));
            //  $option->maximum_delivery = date_format($date,DATE_ATOM).' ' .date_format($dateBefore,DATE_ATOM);
            //}
            //$element['maximum_delivery'] = $option->maximum_delivery;//date_format($date,DATE_ATOM);
            $element['maximum_delivery'] = date_format($date, DATE_ATOM);
            $element['ship_to'] = $option->ship_to;
            $element['shipping_service_id'] = $option->shipping_service->id;
            $response[] = (object) $element;
        }

        return $response;
    }
    
    
    /*
     * Find ShipNowOptionById
     */
    public function findShipNowOptionById($data,$grabado,$id){
        $shipNowOptions =$this->simplifyOptionsPrice($data, $grabado);
        
        $key=array_search($id, array_column(json_decode(json_encode($shipNowOptions),TRUE), 'shipping_service_id'));        
        return $shipNowOptions[$key];
    }

    /*
     * New Ship now
     */

    public function new(Pedido $pedido, $response) {
        $shipNow = new ShipNow();
        $shipNow->setPedido($pedido);
        $shipNow->setRespuesta($response);

        $this->em->persist($shipNow);
        $this->em->flush();
        return true;
    }

     /*
     * Update Variant
     */

    public function updateVariant($id,DetallePedido $detallePedido) {
        $data = $this->generarDataVariant($detallePedido);

        $user = json_decode($this->returnToken());

        $method = "variants/".$id;

        $response = $this->setData($method, $user->token, $data,'PUT');

        $result = json_decode($response);

        if (property_exists($result, 'errors')) {
            return false;
        }
        return $result;
    }

    
    
    /*
     * Create Variant
     */

    public function createVariant(DetallePedido $detallePedido) {
        $data = $this->generarDataVariant($detallePedido);

        $user = json_decode($this->returnToken());

        $method = "variants";

        $response = $this->setData($method, $user->token, $data);

        $result = json_decode($response);

        if (property_exists($result, 'errors')) {
            return false;
        }
        return $result;
    }

    /*
     * Generar Data Variant
     */

    public function generarDataVariant(DetallePedido $detallePedido) {
        
        $data['external_reference'] = $detallePedido->getExternalReference();
        $data['external_reference_user'] = $detallePedido->getExternalReference();
        $data['title'] = $detallePedido->getProductodesc();
        $data['price']['retail'] = $detallePedido->getPrecio();
        $data['currency'] = 'ARS';
        $path = $this->imagenProductoService->getFirstImage($detallePedido->getProducto()->getId());
        $data['image_url'] = $this->rootDirImages . $detallePedido->getProducto()->getId() . '/' . $path;
        $data['thumbnail_url'] = $this->rootDirImages . $detallePedido->getProducto()->getId() . '/' . $path;
        return $data;
    }

    /*
     * Create order
     */

    public function createOrder(Pedido $pedido) {
        $data = $this->generarData($pedido);

        $user = json_decode($this->returnToken());

        $method = "orders";

        $response = $this->setData($method, $user->token, $data);

        $this->new($pedido, $response);

        $result = json_decode($response);

        if (property_exists($result, 'errors')) {
            return false;
        }

        $pedido->setEnvioGenerado(true);
        $this->em->persist($pedido);
        $this->em->flush();
        return true;
    }

    /*
     * Generar data
     * {
      "external_reference": "1",
      "ship_to": {
      "name": "Javier",
      "last_name": "Larrosa",
      "zip_code": 1702,
      "address_line": "",
      "city": "Ciudadela",
      "state": "Buenos Aires",
      "email": "aj_larrosahotmail.com",
      "phone": "1168237621",
      "street_name": "Av. Gaona",
      "street_number": "3724",
      "unit": "",
      "floor": "",
      "post_office_id": ""
      },
      "items": [
      {
      "id": 2275682,
      "quantity": 1,
      "external_reference": "dedios-generico",
      "unit_price": 1,
      "title": "Anillo 1234",
      "image_url": ""
      }
      ],
      "comment": "Algun comenario",
      "shipping_option": {
      "service_code": "shipnow_pap",
      "carrier_code": "shipnow"
      },
      "store_id": "6002",
      "store_type": "manual"

      }

     */

    public function generarData(Pedido $pedido) {
        
        $data['external_reference'] = $pedido->getId() . '-' . bin2hex(openssl_random_pseudo_bytes(10));
        $data['ship_to']['name'] = $pedido->getEntity()->getNombre();
        $data['ship_to']['last_name'] = $pedido->getEntity()->getApellido();
        $data['ship_to']['zip_code'] = $pedido->getDireccionEnvio()->getZipCode();
        $data['ship_to']['address_line'] = $pedido->getDireccionEnvio()->getCalle() . ' ' . $pedido->getDireccionEnvio()->getCalleNumero();
        $data['ship_to']['state'] = $pedido->getDireccionEnvio()->getProvincia();
        $data['ship_to']['city'] = $pedido->getDireccionEnvio()->getLocalidad();
        $data['ship_to']['email'] = $pedido->getEmail();
        $data['ship_to']['phone'] = $pedido->getEntity()->getTelefono();
        $data['ship_to']['street_name'] = $pedido->getDireccionEnvio()->getCalle();
        $data['ship_to']['street_number'] = $pedido->getDireccionEnvio()->getCalleNumero();
        $data['ship_to']['unit'] = $pedido->getDireccionEnvio()->getUnidadFuncional();
        $data['ship_to']['floor'] = $pedido->getDireccionEnvio()->getPiso();
        //$data['ship_to']['post_office_id'] =;

        foreach ($pedido->getDetallespedido() as $detallePedido) {
            $variant = $this->getVariants($detallePedido->getExternalReference());
            
            if (sizeof($variant)>0){
                $v=$variant[0];
                $this->updateVariant($v->id,$detallePedido);
            }else {
                $v=$this->createVariant($detallePedido);
            }
            $item['id'] = $v->id;    
            $item['quantity'] = $detallePedido->getCantidad();    
            $item['unit_price']= $detallePedido->getprecio();
            $data['items'][] = $item;
        }

        //$data['comment'] =;

        $shippingService = $this->getShippingService($pedido->getShipNowServiceId());

        $data['shipping_option']['service_code'] = $shippingService->code;
        $data['shipping_option']['carrier_code'] = $shippingService->carrier->code;
        $data['store_id'] = "6002";
        $data['store_type'] = "manual";

        return $data;
    }

    /*
     * Get Post Office
     */

    public function getPostOffice($id) {

        $user = json_decode($this->returnToken());

        $method = "post_offices/" . $id;

        $response = json_decode($this->getData($method, $user->token));

        return $response->results;
    }

    /*
     * Get Shipping Service     
     */

    public function getShippingService($id) {
        $user = json_decode($this->returnToken());

        $method = "shipping_services/" . $id;

        return json_decode($this->getData($method, $user->token));
    }

    /*
     * Get Order     
     */

    public function getOrder($id) {
        $user = json_decode($this->returnToken());

        $method = "orders/" . $id;

        return json_decode($this->getData($method, $user->token));
    }

    /*
     * Get Variants     
     */

    public function getVariants($externalReference) {
        $user = json_decode($this->returnToken());

        $parameter = '';
        if (isset($externalReference)) {            
            $externalReference=str_replace(' ','%20',$externalReference);
            $parameter = '?external_reference=' . $externalReference;
        }
        $method = "variants" . $parameter;
        
        $response = json_decode($this->getData($method, $user->token));

        if (!isset($response)){
            return [];
        }
        
        return $response->results;
    }

}
