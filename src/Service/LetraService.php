<?php

namespace App\Service;

use App\Repository\LetraRepository;
use Twig\Environment;

class LetraService {

    private $letraRepository;
    private $twig;

    public function __construct(MedidaRepository $letraRepository, Environment $twig) {
        $this->letraRepository = $letraRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $letras = $this->letraRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('letra/select.html.twig', ['letras' => $letras]);
    }

}
