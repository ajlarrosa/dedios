<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\LineaProductoRepository;
use Twig\Environment;

class LineaProductoService {

    private $lineaProductoRepository;
    private $twig;

    public function __construct(LineaProductoRepository $lineaProductoRepository, Environment $twig) {
        $this->lineaProductoRepository = $lineaProductoRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $lineasProductos = $this->lineaProductoRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/linea_producto/select.html.twig', ['lineasProductos' => $lineasProductos]);
    }

}
