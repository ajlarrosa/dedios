<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\ConfiguracionRepository;
use App\Entity\Tienda\Configuracion;
use Twig\Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ConfiguracionService {

    private $configuracionRepository;
    private $twig;
    private $router;

    public function __construct(ConfiguracionRepository $configuracionRepository, Environment $twig, UrlGeneratorInterface $router) {
        $this->configuracionRepository = $configuracionRepository;
        $this->twig = $twig;
        $this->router = $router;
    }

    public function getLastConfiguration() {
        $configuration = $this->configuracionRepository->findOneBy(['enabled' => 1], ['id' => 'DESC'], 1);

        return $configuration;
    }

    /*
     * Get Route
     */

    public function getRoute($id, $first = true, $second = true) {
        $configuracion = $this->configuracionRepository->find($id);
        return $this->route($configuracion, $first, $second);
    }

    /*
     * Get route with configuracion
     */

    public function getWithConfiguracionRoute(Configuracion $configuracion, $first = true, $second = true) {
        return $this->route($configuracion, $first, $second);
    }

    /*
     * Route
     */

    private function route(Configuracion $configuracion, $first, $second) {

        if ($first === true) {
            $categoria = $configuracion->getCategoria1();
            $subcategoria = $configuracion->getSubcategoria1();
            $lineaProducto = $configuracion->getLineaProducto1();
        } elseif ($first === true) {

            $categoria = $configuracion->getCategoria2();
            $subcategoria = $configuracion->getSubcategoria2();
            $lineaProducto = $configuracion->getLineaProducto2();
        } else {
            $categoria = $configuracion->getCategoria3();
            $subcategoria = $configuracion->getSubcategoria3();
            $lineaProducto = $configuracion->getLineaProducto3();
        }

        if ($subcategoria && $categoria) {
            return $this->router->generate('subcategoria_tienda_index', [
                        'category' => $categoria->getDescripcion(),
                        'subcategory' => $subcategoria->getDescripcion(),
                        'category_id' => $categoria->getId(),
                        'id' => $subcategoria->getId()
            ]);
        }
        if ($categoria) {
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $categoria->getDescripcion(),
                        'id' => $categoria->getId()
            ]);
        }

        if ($lineaProducto) {
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $lineaProducto->getDescripcion(),
                        'id' => $lineaProducto->getId()
            ]);
        }

        return '#';
    }

    /*
     * 
     */
    public function getMonedaMayorista() {
        $configuracion = $this->getLastConfiguration();
        return $configuracion->getMonedaMayorista();
    }

    public function getMonedaMinorista() {
        $configuracion = $this->getLastConfiguration();
        return $configuracion->getMonedaMinorista();
    }

}
