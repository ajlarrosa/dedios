<?php

namespace App\Service\Tienda;

use App\Entity\Tienda\FormaPago;
use App\Repository\Tienda\FormaPagoRepository;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;

class FormaPagoService {

    private $formaPagoRepository;
    private $twig;
    private $security;

    public function __construct(            
            FormaPagoRepository $formaPagoRepository, 
            Environment $twig, 
            Security $security) {
        $this->formaPagoRepository = $formaPagoRepository;
        $this->twig = $twig;
        $this->security = $security;
    }

    public function getSelect($enabled) {
        $formasPago = $this->formaPagoRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/forma_pago/select.html.twig', ['formasPago' => $formasPago]);
    }

    public function getListWebCheckout() {
        $filtros['activo'] = true;
        
        $filtros['formasPago'] = ($this->security->isGranted("ROLE_USER_MAYORISTA")) ? FormaPago::TRANSFERENCIA_ID . ',' . FormaPago::ARREGLA_DEDIOS_ID . ',' . FormaPago::EFECTIVO_ID : FormaPago::TRANSFERENCIA_ID . ',' . FormaPago::MERCADOPAGO_ID;

        echo $this->twig->render('tienda/forma_pago/listaWebCheckout.html.twig', [
            'formasPago' => $this->formaPagoRepository->filterResults($filtros,'DESC')
                ]);
    }

    public function getTransferDiscount() {
        $filtros['formasPago'] = FormaPago::TRANSFERENCIA_ID;
        $formasPago = $this->formaPagoRepository->filterResults($filtros);
        return $formasPago[0]->getPorcentajeDescuento();
    }
    
    public function getFormaPagoById(int $id): FormaPago{
        return $this->formaPagoRepository->find($id);        
    }

}
