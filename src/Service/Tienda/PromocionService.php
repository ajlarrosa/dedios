<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\PromocionRepository;
use Twig\Environment;

class PromocionService {

    private $promocionRepository;
    private $twig;

    public function __construct(PromocionRepository $promocionRepository, Environment $twig) {
        $this->promocionRepository = $promocionRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $promociones = $this->promocionRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/promocion/select.html.twig', ['promociones' => $promociones]);
    }

}
