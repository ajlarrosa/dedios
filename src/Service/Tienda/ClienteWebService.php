<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\PedidoRepository;
use App\Repository\ColorRepository;
use App\Repository\MedidaRepository;
use App\Repository\LetraRepository;
use App\Repository\ProductoRepository;
use App\Service\PrecioService;
use App\Service\ListaPrecioService;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;

class ClienteWebService {

    private $pedidoRepository;
    private $colorRepository;
    private $letraRepository;
    private $medidaRepository;
    private $productoRepository;
    private $security;
    private $twig;
    private $em;

    public function __construct(ListaPrecioService $listaPrecioService, PrecioService $precioService, ProductoRepository $productoRepository, PedidoRepository $pedidoRepository, ColorRepository $colorRepository, MedidaRepository $medidaRepository, LetraRepository $letraRepository, Environment $twig, Security $security, EntityManagerInterface $em) {
        $this->pedidoRepository = $pedidoRepository;
        $this->productoRepository = $productoRepository;
        $this->colorRepository = $colorRepository;
        $this->medidaRepository = $medidaRepository;
        $this->letraRepository = $letraRepository;
        $this->twig = $twig;
        $this->security = $security;
        $this->precioService = $precioService;
        $this->listaPrecioService = $listaPrecioService;
        $this->em = $em;
    }

    /*
     * Add product to pedido
     */

    public function addProduct(Request $request, $product) {

        $cookies = $request->cookies;

        if ($cookies->has('detallePedido')) {
            $detallePedido = json_decode($cookies->get('detallePedido'));
        }

        $product->colorDescripcion = '';
        if (isset($product->color)) {
            $color = $this->colorRepository->find($product->color);
            if ($color) {
                $product->colorDescripcion = $color->getDescripcion();
                $product->color = $color->getId();
            }
        }

        $product->medidaDescripcion = '';
        if (isset($product->medida)) {
            $medida = $this->medidaRepository->find($product->medida);
            if ($medida) {
                $product->medidaDescripcion = $medida->getDescripcion();
                $product->medida = $medida->getId();
            }
        }

        $product->letraDescripcion = '';
        if (isset($product->letra)) {
            $letra = $this->letraRepository->find($product->letra);
            if ($letra) {
                $product->letraDescripcion = $letra->getDescripcion();
                $product->letra = $letra->getId();
            }
        }

        $producto = $this->productoRepository->find($product->producto);

        $product->productodesc = $producto->getDescripcion() . ' (' . $producto->getCodigo() . ')';
        $product->producto = $producto->getId();
                
        $detallePedido[] = $product;              
        $product->id = $this->findNextId($detallePedido);

        //Validez de las cookies 6hs
        //Para resetear cookies dev
        //$response['detallePedido'] = new Cookie("detallePedido", '', time() + 3600, '/', null, false, false);
        //$response['cantidadProductos'] = new Cookie("cantidadProductos", '', time() + 3600, '/', null, false, false);
        
        $response['detallePedido'] = new Cookie("detallePedido", json_encode($detallePedido), time() + 86400, '/', null, false, false);
        $response['cantidadProductos'] = new Cookie("cantidadProductos", sizeof($detallePedido), time() + 86400, '/', null, false, false);
        return $response;
    }

    /*
     * Find max Id
     */

    private function findNextId($detallesPedido) {
        $id = 0;
        foreach ($detallesPedido as $detallePedido) {
            if (isset($detallePedido->id)) {
                if ($detallePedido->id > $id) {
                    $id = $detallePedido->id;
                }
            }
        }

        return $id+1;
    }

    /*
     * Get detalle pedido from Cookie
     */

    public function getDetallePedido(Request $request) {
        $cookies = $request->cookies;

        if ($cookies->has('detallePedido')) {
            $detallePedido = json_decode($cookies->get('detallePedido'));
        } else {
            $detallePedido = [];
        }
        return $detallePedido;
    }

    /*
     * Get cookie pedido confirmado
     */

    public function pedidoConfirmado($idPedido) {

        //Cookie 24hs
        return new Cookie("idPedido", $idPedido, time() + 86400, '/', null, false, false);
    }

}
