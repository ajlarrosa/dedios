<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\SeccionRepository;
use Twig\Environment;

class SeccionService {

    private $seccionRepository;
    private $twig;

    public function __construct(SeccionRepository $seccionRepository, Environment $twig) {
        $this->seccionRepository = $seccionRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $secciones = $this->seccionRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/seccion/select.html.twig', ['secciones' => $secciones]);
    }

}
