<?php

namespace App\Service\Tienda;

use App\Entity\Tienda\FormatoMail;
use Twig\Environment;
use App\Repository\Tienda\FormatoMailRepository;

class FormatoMailService {

    private $twig;
    private $formatoMailRepository;

    public function __construct(Environment $twig, FormatoMailRepository $formatoMailRepository) {
        $this->twig = $twig;
        $this->formatoMailRepository = $formatoMailRepository;
    }

    /*
     * Get select type
     */

    public function getSelectType() {
        echo $this->twig->render('tienda/formato_mail/selectType.html.twig', ['types' => $this->getTypes()]);
    }

    /*
     * Get formato mail
     */

    public function getFormatoMail($filter) {
        return $this->formatoMailRepository->findBy($filter);
    }

    /*
     * Get types
     */

    public function getTypes() {
        return [FormatoMail::CONFIRM_USER => FormatoMail::COD_CONFIRM_USER,
            FormatoMail::CONFIRM_MINORISTA => FormatoMail::COD_CONFIRM_MINORISTA,
            FormatoMail::CONFIRM_CLIENTE_WEB => FormatoMail::COD_CONFIRM_CLIENTE_WEB,
            FormatoMail::CONFIRM_MAYORISTA => FormatoMail::COD_CONFIRM_MAYORISTA,
            FormatoMail::CONFIRM_MAIL_RETAIL => FormatoMail::COD_CONFIRM_MAIL_RETAIL,
            FormatoMail::CONFIRM_MAIL_WHOLESALER => FormatoMail::COD_CONFIRM_MAIL_WHOLESALER,
            FormatoMail::CONFIRM_PAYMENT_CLIENT => FormatoMail::COD_CONFIRM_PAYMENT_CLIENT,
            FormatoMail::CONFIRM_ORDER_TAKE_AWAY => FormatoMail::COD_CONFIRM_ORDER_TAKE_AWAY,
            FormatoMail::REMINDER_UNCONFIRMED_ORDER => FormatoMail::COD_REMINDER_UNCONFIRMED_ORDER];
    }

}
