<?php

namespace App\Service\Tienda;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Tienda\MercadoPago;
use App\Repository\Tienda\PedidoRepository;
use App\Repository\Tienda\MercadoPagoRepository;
use App\Entity\Tienda\Pedido;

class MercadoPagoService {

    private $requestStack;
    private $accessToken;
    private $preference;
    private $pedidoRepository;
    private $mercadoPagoRepository;
    private $pedidoId;
    private $router;
    private $em;

    public function __construct(MercadoPagoRepository $mercadoPagoRepository, PedidoRepository $pedidoRepository, EntityManagerInterface $entityManager, $accessToken, RequestStack $requestStack, UrlGeneratorInterface $router) {
        \MercadoPago\SDK::setAccessToken($accessToken);
        $this->accessToken = $accessToken;
        $this->preference = new \MercadoPago\Preference();
        $this->pedidoRepository = $pedidoRepository;
        $this->mercadoPagoRepository = $mercadoPagoRepository;
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->em = $entityManager;
    }

    public function setPedidoId($pedidoId) {
        $this->pedidoId = $pedidoId;
    }

    public function getPreference() {

        $route = $this->router->generate('mercadopago_procesar_pago', [
            'id' => $this->pedidoId
        ]);

        $this->preference->back_urls = array(
            "success" => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . $route,
            "failure" => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . $route,
            "pending" => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . $route
        );
        //Obtener pedido y forma el external_reference
        $pedido = $this->pedidoRepository->find($this->pedidoId);
        $this->preference->external_reference = $pedido->getExternalReference();
        $this->preference->auto_return = "approved";
        $this->preference->save();
        return $this->preference;
    }

    /*
     * Seteo de los productos a cobrar
     */

    public function setProductos($productos) {
        $items = [];
        foreach ($productos as $producto) {
            $item = new \MercadoPago\Item();
            $item->title = $producto->title;
            $item->description = $producto->description;
            $item->category_id = $producto->category_id;
            $item->picture_url = $producto->picture_url;
            $item->quantity = $producto->quantity;
            $item->unit_price = $producto->unit_price;
            $item->currency_id = $producto->currency_id;
            $items[] = $item;
        }

        $this->preference->items = $items;

        return true;
    }

    /*
     * Seteo del pagador
     */

    public function setPayer($pagador) {
        $payer = new \MercadoPago\Payer();
        $payer->name = $pagador->name;
        $payer->surname = $pagador->surname;
        $payer->email = $pagador->email;
        $payer->date_created = $pagador->date_created;
        $payer->phone = $pagador->phone;
        $payer->identification = $pagador->identification;
        $payer->address = $pagador->address;
        $this->preference->payer = $payer;

        return true;
    }

    /*
     * Create
     * 
     */

    public function create($preference, Pedido $pedido) {

        $mercadoPago = new MercadoPago();

        $mercadoPago->setPreferenceId($preference->id);
        $mercadoPago->setExternalReference($preference->external_reference);
        $mercadoPago->setSiteId($preference->site_id);
        $mercadoPago->setPedido($pedido);

        $this->em->persist($mercadoPago);
        $this->em->flush();

        return $mercadoPago;
    }

    /*
     * 
     */

    public function getPedidoByExternalReference($externalReference) {

        //Armar un método en el servicio mercadoPago para lo siguiente
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mercadopago.com/checkout/preferences/search?external_reference=" . urlencode($externalReference) . "&access_token=" . $this->accessToken,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if (!$err) {
            $response = json_decode($response);
            return $response->elements[0];
        }
        return false;
    }
    
    public function getPaymentStatus($paymentId) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mercadopago.com/v1/payments/" . $paymentId . "?access_token=" . $this->accessToken,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err) {
            return json_decode($response);
        }
        return false;
    }

    /*
     * Get Active MercadoPago
     */

    public function getActiveMercadoPago($pedidoId) {
        return $this->mercadoPagoRepository->findOneBy(['pedido' => $pedidoId, 'enabled' => true]);
    }

    /*
     * Get Payment Methods
     */

    public function getPaymentMethods() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mercadopago.com/v1/payment_methods?access_token=" . $this->accessToken,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err) {
            return json_decode($response);
        }
        
        return false;
    }

}
