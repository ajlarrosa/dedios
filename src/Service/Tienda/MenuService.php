<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\MenuRepository;
use App\Entity\Tienda\Menu;
use Twig\Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\General\GeneralService;

class MenuService {

    private $menuRepository;
    private $twig;
    private $generalService;

    /*
     * 
     */

    public function __construct(GeneralService $generalService, MenuRepository $menuRepository, Environment $twig, UrlGeneratorInterface $router) {
        $this->menuRepository = $menuRepository;
        $this->twig = $twig;
        $this->router = $router;
        $this->generalService = $generalService;
    }

    /*
     * 
     */

    public function getSelect($enabled) {
        $menus = $this->menuRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('tienda/menu/select.html.twig', ['menus' => $menus]);
    }

    public function getRoute($id, $first = true) {

        $menu = $this->menuRepository->find($id);

        if ($first === true) {
            $producto = $menu->getProducto1();
            $categoria = $menu->getCategoria1();
            $subcategoria = $menu->getSubcategoria1();
            $lineaProducto = $menu->getLineaProducto1();
        } else {
            $producto = $menu->getProducto2();
            $categoria = $menu->getCategoria2();
            $subcategoria = $menu->getSubcategoria2();
            $lineaProducto = $menu->getLineaProducto2();
        }

        if ($producto) {
            return $this->router->generate('producto_tienda_show', [
                        'categoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getCategoria()->getDescripcion()),
                        'subcategoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion()),
                        'id' => $producto->getId(),
                        'descripcion' => $this->generalService->slugify($producto->getDescripcion())
            ]);
        }

        if ($subcategoria && $categoria) {
            return $this->router->generate('subcategoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'subcategory' => $this->generalService->slugify($subcategoria->getDescripcion()),
                        'category_id' => $categoria->getId(),
                        'id' => $subcategoria->getId()
            ]);
        }
        if ($categoria) {
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'id' => $categoria->getId()
            ]);
        }

        if ($lineaProducto) {
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $this->generalService->slugify($lineaProducto->getDescripcion()),
                        'id' => $lineaProducto->getId()
            ]);
        }

        return '#';
    }

    /*
     * Get Menu with Menu Route
     */

    public function getMenuWithMenuRoute(Menu $menu, $first = true) {
        return $this->menuRoute($menu, $first);
    }

    /*
     * Get Menu Route
     */

    public function getMenuRoute($id, $first = null) {

        $menu = $this->menuRepository->find($id);
        return $this->menuRoute($menu, $first);
    }

    /*
     * Menu Route
     */

    private function menuRoute(Menu $menu, $first) {
        $categoria = $menu->getCategoriaMenu();
        $subcategoria = $menu->getSubcategoriaMenu();
        $lineaProducto = $menu->getLineaProductoMenu();

        if ($subcategoria && $categoria) {
            return $this->router->generate('subcategoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'subcategory' => $this->generalService->slugify($subcategoria->getDescripcion()),
                        'category_id' => $categoria->getId(),
                        'id' => $subcategoria->getId()
            ]);
        }
        if ($categoria) {
                        
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'id' => $categoria->getId()
            ]);
        }

        if ($lineaProducto) {
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $this->generalService->slugify($lineaProducto->getDescripcion()),
                        'id' => $lineaProducto->getId()
            ]);
        }

        return '#';
    }

}
