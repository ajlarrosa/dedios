<?php

namespace App\Service\Tienda;

use Twig\Environment;
use App\Repository\ProductoRepository;
use App\Repository\Tienda\PromotionRepository;
use App\Repository\Tienda\PedidoRepository;
use App\Service\Tienda\ClienteWebService;
use App\Service\PrecioService;
use App\Service\ListaPrecioService;
use App\Entity\Tienda\Promotion;

class PromotionService {

    private $twig;
    private $productoRepository;
    private $pedidoRepository;
    private $promotionRepository;
    private $clienteWebService;
    private $precioService;
    private $listaPrecioService;

    public function __construct(
            Environment $twig,
            ProductoRepository $productoRepository,
            PedidoRepository $pedidoRepository,
            PromotionRepository $promotionRepository,
            ClienteWebService $clienteWebService,
            PrecioService $precioService,
            ListaPrecioService $listaPrecioService
    ) {
        $this->twig = $twig;
        $this->productoRepository = $productoRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->promotionRepository = $promotionRepository;
        $this->clienteWebService = $clienteWebService;
        $this->precioService = $precioService;
        $this->listaPrecioService = $listaPrecioService;
    }

    public function getTipoDescuentoSelect() {
        echo $this->twig->render('tienda/promotion/tipoDescuentoSelect.html.twig');
    }

    public function getActivePromotion($productoId, $detallePedidoId = null) {
        $promotions = [];

        $producto = $this->productoRepository->find($productoId);

        $hoyString = (new \DateTime())->format('Y-m-d');

        $type = [];

        $promotion = $this->promotionRepository->filterResults([
            'activo' => true,
            'fechaDesde' => $hoyString,
            'fechaHasta' => $hoyString,
            'type' => 'all'
        ]);

        if (!$promotion) {
            $promotion = $this->promotionRepository->filterResults([
                'activo' => true,
                'fechaDesde' => $hoyString,
                'fechaHasta' => $hoyString,
                'producto' => $producto,
                'type' => 'promos'
            ]);

            if ($promotion) {
                $type = [
                    'type' => 'PRODUCTO',
                    'id' => $promotion[0]->getProducto()->getId(),
                ];
            }
        }

        if (!$promotion) {
            //Categoría
            $promotion = $this->promotionRepository->filterResults([
                'activo' => true,
                'fechaDesde' => $hoyString,
                'fechaHasta' => $hoyString,
                'categoria' => $producto->getCategoriasubcategoria()->getCategoria(),
                'type' => 'promos'
            ]);

            if ($promotion) {
                $type = [
                    'type' => 'CATEGORIA',
                    'id' => $promotion[0]->getCategoria()->getId(),
                ];
            }
        }

        if (!$promotion) {
            //Subcategoría                        
            $promotion = $this->promotionRepository->filterResults([
                'activo' => true,
                'fechaDesde' => $hoyString,
                'fechaHasta' => $hoyString,
                'subcategoria' => $producto->getCategoriasubcategoria()->getSubcategoria(),
                //'categoria' => $producto->getCategoriasubcategoria()->getCategoria(),
                'type' => 'promos'
            ]);

            if ($promotion) {
                $type = [
                    'type' => 'SUBCATEGORIA',
                    'id' => $promotion[0]->getSubcategoria()->getId(),
                ];
            }
        }

        if (!$promotion) {
            $promotion = $this->promotionRepository->filterResults([
                'activo' => true,
                'fechaDesde' => $hoyString,
                'fechaHasta' => $hoyString,
                'lineaProductos' => $producto->getIdsLineaProducto(),
                'type' => 'promos'
            ]);

            if ($promotion) {
                $type = [
                    'type' => 'LINEA PRODUCTO',
                    'id' => $promotion[0]->getLineaProducto()->getId(),
                ];
            }
        }

        if ($promotion) {
            $promotions['producto']['tipoDescuento'] = $promotion[0]->getTipoDescuento();
            $promotions['producto']['cantidadMinima'] = $promotion[0]->getCantidadMinima();
            $promotions['producto']['id'] = $productoId;
            $promotions['producto']['detallePedidoId'] = $detallePedidoId;
            $promotions['producto']['porcentaje'] = $promotion[0]->getPorcentaje();
            $promotions['producto']['types'] = [$type]; //$promotion[0]->getTypes();
        }

        $flag = false;

        $promotion = $this->promotionRepository->filterResults([
            'activo' => true,
            'fechaDesde' => $hoyString,
            'fechaHasta' => $hoyString,
            'tipoDescuento' => Promotion::ENVIO_CERO,
            'type' => 'all'
        ]);

        if ($promotion) {
            $flag = true;
        }

        if (!$flag) {
            foreach ($producto->getProductosLineaProducto() as $productoLineaProducto) {
                if ($productoLineaProducto->getEnabled() && $productoLineaProducto->getLineaProducto()->getEnabled()) {
                    $flag = true;
                    $promotion = $this->promotionRepository->filterResults([
                        'activo' => true,
                        'fechaDesde' => $hoyString,
                        'fechaHasta' => $hoyString,
                        'tipoDescuento' => Promotion::ENVIO_CERO,
                        'producto' => $producto,
                        'categoria' => $producto->getCategoriasubcategoria()->getCategoria(),
                        'subcategoria' => $producto->getCategoriasubcategoria()->getSubcategoria(),
                        'lineaProducto' => $productoLineaProducto->getLineaProducto()
                    ]);

                    if ($promotion) {
                        break;
                    }
                }
            }
        }

        if (!$flag) {
            $promotion = $this->promotionRepository->filterResults([
                'activo' => true,
                'fechaDesde' => $hoyString,
                'fechaHasta' => $hoyString,
                'tipoDescuento' => Promotion::ENVIO_CERO,
                'producto' => $producto,
                'categoria' => $producto->getCategoriasubcategoria()->getCategoria(),
                'subcategoria' => $producto->getCategoriasubcategoria()->getSubcategoria(),
                'lineaProducto' => null
            ]);
        }

        if ($promotion) {
            $promotions['envio']['cantidadMinima'] = $promotion[0]->getCantidadMinima();
            $promotions['envio']['producto']['id'] = $productoId;
            $promotions['envio']['types'] = $promotion[0]->getTypes();
        }

        return $promotions;
    }

    public function getActivePedidoPromotion($request, $pedido) {
        return ($pedido) ? $this->getPromotion($pedido->getDetallespedido()) : $this->getPromotion($this->clienteWebService->getDetallePedido($request));
    }

    private function getPromotion($detallesPedido) {

        $totales[Promotion::TYPE_CATEGORIA] = [];
        $totales[Promotion::TYPE_SUBCATEGORIA] = [];
        $totales[Promotion::TYPE_LINEA_PRODUCTO] = [];
        $totales[Promotion::TYPE_PRODUCTO] = [];
        $totales[Promotion::TYPE_ALL] = [];
        $totales[Promotion::TYPE_ALL]['cantidad'] = 0;
        $totales[Promotion::TYPE_ALL]['monto'] = 0;
        $totales[Promotion::TYPE_GRUPO] = [];

        $promo['2x1'] = [];
        $promo['3x2'] = [];
        $montos['2x1'] = 0;
        $montos['3x2'] = 0;

        $promotion = [];

        $hoyString = (new \DateTime())->format('Y-m-d');

        $promociones = $this->promotionRepository->filterResults([
            'activo' => true,
            'fechaDesde' => $hoyString,
            'fechaHasta' => $hoyString,
            'type' => 'promos'
        ]);

        $promocion = is_array($promociones) && (count($promociones) > 0) ? $promociones[0] : null;

        foreach ($detallesPedido as $detallePedido) {

            //Por aca entran los clientesWeb
            if ($detallePedido instanceof \stdClass) {
                $producto = $this->productoRepository->find($detallePedido->producto);

                $id = $detallePedido->producto;

                $detallePedidoId = $detallePedido->id;

                $cantidad = $detallePedido->cantidad;

                //Quizas hay que descontar si tiene alguna promocion veremos
                $monto = (is_null($detallePedido->medida)) ? $this->precioService->getPrice($detallePedido->producto) : $this->precioService->getPrecioMedidaFinal($detallePedido->producto, $detallePedido->medida, 1);

                if (!is_null($detallePedido->grabado)) {
                    $monto = $monto + $this->listaPrecioService->getPrecioGrabado();
                }
            } else {
                //Por aca entran los usuarios registrados
                $producto = $detallePedido->getProducto();
                $id = $detallePedido->getProducto()->getId();
                $detallePedidoId = $detallePedido->getId();
                $cantidad = $detallePedido->getCantidad();
                $monto = $this->precioService->getPrecioMedidaFinal($detallePedido->getProducto()->getId(), $detallePedido->getMedida(), 1);
            }

            $activePromotion = $this->getActivePromotion($id, $detallePedidoId);

            if ($activePromotion) {

                if (array_key_exists('envio', $activePromotion)) {
                    $promotion['productos']['envio'][] = $activePromotion['envio'];
                }

                if (array_key_exists('producto', $activePromotion)) {
                    $promotion['productos']['producto'][] = $activePromotion['producto'];

                    for ($i = 1; $i <= $cantidad; $i++) {
                        $promo[$activePromotion['producto']['tipoDescuento']][] = $monto;
                    }
                }
            }


            $totales = $this->getTotales(
                    $producto,
                    $totales,
                    $cantidad,
                    $monto,
                    $promocion
            );
        }

        rsort($promo['2x1']);
        rsort($promo['3x2']);

        $cantidad2x1 = intdiv(sizeof($promo['2x1']), 2);
        $sumaMonto2x1 = 0;
        $sumaMonto3x2 = 0;

        for ($i = 0; $i < $cantidad2x1; $i++) {
            $sumaMonto2x1 += $promo['2x1'][$i];
        }

        if (sizeof($promo['2x1']) % 2 != 0) {
            $sumaMonto2x1 += $promo['2x1'][$cantidad2x1];
        }

        $cantidad3x2 = intdiv(sizeof($promo['3x2']), 3);

        for ($i = 0; $i < $cantidad3x2 * 2; $i++) {
            $sumaMonto3x2 += $promo['3x2'][$i];
        }

        $resto3x2 = sizeof($promo['3x2']) % 3;

        for ($i = 0; $i < $resto3x2; $i++) {
            $sumaMonto3x2 += $promo['3x2'][$cantidad3x2 + $i];
        }

        $montos['2x1'] = array_sum($promo['2x1']) - $sumaMonto2x1;

        $montos['3x2'] = array_sum($promo['3x2']) - $sumaMonto3x2;

        $promotion['totales'] = $totales;

        $promotion['montos'] = $montos;
       
        return $promotion;
    }

    private function getTotales($producto, $totales, $cantidad, $monto, $promotion) {

        if ($promotion) {
            //Categoria
            if (array_key_exists($producto->getCategoriasubcategoria()->getCategoria()->getId(), $totales[Promotion::TYPE_CATEGORIA])) {
                $totales[Promotion::TYPE_CATEGORIA][$producto->getCategoriasubcategoria()->getCategoria()->getId()]['cantidad'] += $cantidad;
                $totales[Promotion::TYPE_CATEGORIA][$producto->getCategoriasubcategoria()->getCategoria()->getId()]['monto'] += $cantidad * $monto;
            } else {
                $totales[Promotion::TYPE_CATEGORIA][$producto->getCategoriasubcategoria()->getCategoria()->getId()]['cantidad'] = $cantidad;
                $totales[Promotion::TYPE_CATEGORIA][$producto->getCategoriasubcategoria()->getCategoria()->getId()]['monto'] = $cantidad * $monto;
            }
            //Subcategoria
            if (array_key_exists($producto->getCategoriasubcategoria()->getSubCategoria()->getId(), $totales[Promotion::TYPE_SUBCATEGORIA])) {
                $totales[Promotion::TYPE_SUBCATEGORIA][$producto->getCategoriasubcategoria()->getSubCategoria()->getId()]['cantidad'] += $cantidad;
                $totales[Promotion::TYPE_SUBCATEGORIA][$producto->getCategoriasubcategoria()->getSubCategoria()->getId()]['monto'] += $cantidad * $monto;
            } else {
                $totales[Promotion::TYPE_SUBCATEGORIA][$producto->getCategoriasubcategoria()->getSubcategoria()->getId()]['cantidad'] = $cantidad;
                $totales[Promotion::TYPE_SUBCATEGORIA][$producto->getCategoriasubcategoria()->getSubcategoria()->getId()]['monto'] = $cantidad * $monto;
            }
            //Producto
            if (array_key_exists($producto->getId(), $totales[Promotion::TYPE_PRODUCTO])) {
                $totales[Promotion::TYPE_PRODUCTO][$producto->getId()]['cantidad'] += $cantidad;
                $totales[Promotion::TYPE_PRODUCTO][$producto->getId()]['monto'] += $cantidad * $monto;
            } else {
                $totales[Promotion::TYPE_PRODUCTO][$producto->getId()]['cantidad'] = $cantidad;
                $totales[Promotion::TYPE_PRODUCTO][$producto->getId()]['monto'] = $cantidad * $monto;
            }

            //Línea de Producto
            foreach ($producto->getProductosLineaProducto() as $productoLineaProducto) {
                if ($productoLineaProducto->getLineaProducto()->getEnabled()) {
                    if (array_key_exists($productoLineaProducto->getLineaProducto()->getId(), $totales[Promotion::TYPE_LINEA_PRODUCTO])) {
                        $totales[Promotion::TYPE_LINEA_PRODUCTO][$productoLineaProducto->getLineaProducto()->getId()]['cantidad'] += $cantidad;
                        $totales[Promotion::TYPE_LINEA_PRODUCTO][$productoLineaProducto->getLineaProducto()->getId()]['monto'] += $cantidad * $monto;
                    } else {
                        $totales[Promotion::TYPE_LINEA_PRODUCTO][$productoLineaProducto->getLineaProducto()->getId()]['cantidad'] = $cantidad;
                        $totales[Promotion::TYPE_LINEA_PRODUCTO][$productoLineaProducto->getLineaProducto()->getId()]['monto'] = $cantidad * $monto;
                    }
                }
            }

            //Todos
            $totales[Promotion::TYPE_ALL]['cantidad'] += $cantidad;
            $totales[Promotion::TYPE_ALL]['monto'] += $cantidad * $monto;

            //Conjunto
            $product = $promotion->getProducto();
            $categoria = $promotion->getCategoria();
            $subcategoria = $promotion->getSubcategoria();
            $lineaProducto = $promotion->getLineaProducto();

            $cuentaGrupo = (is_null($product) ? 0 : 1) +
                    (is_null($categoria) ? 0 : 1) +
                    (is_null($subcategoria) ? 0 : 1) +
                    (is_null($lineaProducto) ? 0 : 1);


            if ($cuentaGrupo > 1) {

                if (($product && $producto->getId() === $product->getId()) ||
                        ($categoria && $producto->getCategoriasubcategoria()->getCategoria()->getId() === $categoria->getId()) ||
                        ($subcategoria && $producto->getCategoriasubcategoria()->getSubcategoria()->getId() === $subcategoria->getId()) ||
                        ($lineaProducto && $producto->isInLineaProducto($lineaProducto->getId()))
                ) {
                    if ($product) {
                        if (array_key_exists('PRODUCTO', $totales[Promotion::TYPE_GRUPO])) {
                            $totales[Promotion::TYPE_GRUPO]['PRODUCTO'][$product->getId()]['cantidad'] += $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['PRODUCTO'][$product->getId()]['monto'] += $cantidad * $monto;
                        } else {
                            $totales[Promotion::TYPE_GRUPO]['PRODUCTO'][$product->getId()]['cantidad'] = $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['PRODUCTO'][$product->getId()]['monto'] = $cantidad * $monto;
                        }
                    }

                    if ($categoria) {
                        if (array_key_exists('CATEGORIA', $totales[Promotion::TYPE_GRUPO])) {
                            $totales[Promotion::TYPE_GRUPO]['CATEGORIA'][$categoria->getId()]['cantidad'] += $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['CATEGORIA'][$categoria->getId()]['monto'] += $cantidad * $monto;
                        } else {
                            $totales[Promotion::TYPE_GRUPO]['CATEGORIA'][$categoria->getId()]['cantidad'] = $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['CATEGORIA'][$categoria->getId()]['monto'] = $cantidad * $monto;
                        }
                    }

                    if ($subcategoria) {
                        if (array_key_exists('SUBCATEGORIA', $totales[Promotion::TYPE_GRUPO])) {
                            $totales[Promotion::TYPE_GRUPO]['SUBCATEGORIA'][$subcategoria->getId()]['cantidad'] += $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['SUBCATEGORIA'][$subcategoria->getId()]['monto'] += $cantidad * $monto;
                        } else {
                            $totales[Promotion::TYPE_GRUPO]['SUBCATEGORIA'][$subcategoria->getId()]['cantidad'] = $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['SUBCATEGORIA'][$subcategoria->getId()]['monto'] = $cantidad * $monto;
                        }
                    }

                    if ($lineaProducto) {
                        if (array_key_exists('LINEA PRODUCTO', $totales[Promotion::TYPE_GRUPO])) {
                            $totales[Promotion::TYPE_GRUPO]['LINEA PRODUCTO'][$lineaProducto->getId()]['cantidad'] += $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['LINEA PRODUCTO'][$lineaProducto->getId()]['monto'] += $cantidad * $monto;
                        } else {
                            $totales[Promotion::TYPE_GRUPO]['LINEA PRODUCTO'][$lineaProducto->getId()]['cantidad'] = $cantidad;
                            $totales[Promotion::TYPE_GRUPO]['LINEA PRODUCTO'][$lineaProducto->getId()]['monto'] = $cantidad * $monto;
                        }
                    }
                }
            }
        }

        return $totales;
    }

}
