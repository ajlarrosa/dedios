<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\CuponRepository;

class CuponService {

    private $cuponRepository;

    public function __construct(
            CuponRepository $cuponRepository
    ) {
        $this->cuponRepository = $cuponRepository;
    }

    public function getActiveCupon(string $cuponCode) {
        if (empty($cuponCode)){
            return [];
        }
                
        $today = new \DateTime();

        return $this->cuponRepository->filter([
                    'activo' => true,
                    'codigo' => $cuponCode,
                    'fechaDesde' => $today->format('Y-m-d H:i:s'),
                    'fechaHasta' => $today->format('Y-m-d'),
                ])->getQuery()->getResult();
    }
}
