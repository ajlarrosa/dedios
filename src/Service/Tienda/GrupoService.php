<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\GrupoRepository;
use App\Entity\Tienda\Grupo;
use App\Repository\Tienda\LineaProductoRepository;
use App\Repository\CategoriaRepository;
use App\Repository\SubcategoriaRepository;
use App\Repository\CategoriasubcategoriaRepository;
use App\Repository\ProductoRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\General\GeneralService;

class GrupoService {

    private $grupoRepository;
    private $productoRepository;
    private $categoriaRepository;
    private $subcategoriaRepository;
    private $categoriasubcategoriaRepository;
    private $lineaProductoRepository;
    private $generalService;
    private $router;

    /*
     * 
     */

    public function __construct(
            GeneralService $generalService, 
            GrupoRepository $grupoRepository, 
            ProductoRepository $productoRepository, 
            CategoriaRepository $categoriaRepository, 
            SubcategoriaRepository $subcategoriaRepository, 
            CategoriasubcategoriaRepository $categoriasubcategoriaRepository, 
            LineaProductoRepository $lineaProductoRepository, 
            UrlGeneratorInterface $router) {
        $this->grupoRepository = $grupoRepository;
        $this->productoRepository = $productoRepository;
        $this->categoriaRepository = $categoriaRepository;
        $this->subcategoriaRepository = $subcategoriaRepository;
        $this->categoriasubcategoriaRepository = $categoriasubcategoriaRepository;
        $this->lineaProductoRepository = $lineaProductoRepository;
        $this->router = $router;
        $this->generalService = $generalService;
    }

    /*
     * 
     */

    public function getWithGrupoRoute(Grupo $grupo, $idItem) {
        return $this->route($grupo, $idItem);
    }

    /*
     * Get routes
     */

    public function getRoute($id, $idItem) {

        $grupo = $this->grupoRepository->find($id);
        return $this->route($grupo, $idItem);
    }

    /*
     * Route
     */

    private function route(Grupo $grupo, $idItem) {
        if (sizeof($grupo->getCategoriasubcategoriasGrupo()) > 0) {

            $categoriaSubcategoria = $this->categoriasubcategoriaRepository->find($idItem);

            return $this->router->generate('subcategoria_tienda_index', [
                        'categoriaSubcategoria' => $categoriaSubcategoria,
                        'category' => $this->generalService->slugify($categoriaSubcategoria->getCategoria()->getDescripcion()),
                        'subcategory' => $this->generalService->slugify($categoriaSubcategoria->getSubcategoria()->getDescripcion()),
                        'category_id' => $categoriaSubcategoria->getCategoria()->getId(),
                        'id' => $categoriaSubcategoria->getSubcategoria()->getId()
            ]);
        }

        if (sizeof($grupo->getCategoriasGrupo()) > 0) {
            $categoria = $this->categoriaRepository->find($idItem);
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'id' => $categoria->getId()
            ]);
        }

        if (sizeof($grupo->getLineaProductosGrupo()) > 0) {
            $lineaProducto = $this->lineaProductoRepository->find($idItem);
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $this->generalService->slugify($lineaProducto->getDescripcion()),
                        'id' => $lineaProducto->getId()
            ]);
        }
        if (sizeof($grupo->getProductosGrupo()) > 0) {
            $producto = $this->productoRepository->find($idItem);

            return $this->router->generate('producto_tienda_show', [
                        'categoria' => $producto->getCategoriasubcategoria()->getCategoria()->getDescripcion(),
                        'subcategoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion()),
                        'id' => $producto->getId(),
                        'descripcion' => $this->generalService->slugify($producto->getDescripcion())
            ]);

            return '#';
        }
    }

}
