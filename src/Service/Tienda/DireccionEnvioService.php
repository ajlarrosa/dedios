<?php

namespace App\Service\Tienda;

use App\Entity\Tienda\DireccionEnvio;
use App\Form\Tienda\DireccionEnvioType;
use App\Service\General\GeneralService;
use App\Service\General\GeoService;
use App\Service\Tienda\PedidoService;
use App\Repository\Tienda\DireccionEnvioRepository;
use App\Repository\Tienda\PedidoRepository;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class DireccionEnvioService {

    private $pedidoRepository;
    private $form;
    private $generalService;
    private $pedidoService;
    private $direccionEnvioRepository;
    private $security;
    private $twig;
    private $router;
    private $serializer;
    private $translator;

    public function __construct(
            PedidoRepository $pedidoRepository,
            DireccionEnvioRepository $direccionEnvioRepository,
            Environment $twig,
            Security $security,
            GeneralService $generalService,
            PedidoService $pedidoService,
            FormFactoryInterface $form,
            UrlGeneratorInterface $router,
            SerializerInterface $serializer,
            TranslatorInterface $translator,
            GeoService $geoService
    ) {
        $this->twig = $twig;
        $this->security = $security;
        $this->direccionEnvioRepository = $direccionEnvioRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->generalService = $generalService;
        $this->pedidoService = $pedidoService;
        $this->form = $form;
        $this->router = $router;
        $this->serializer = $serializer;
        $this->translator = $translator;
        $this->geoService = $geoService;
    }

    public function getTiposDireccion($tipoDireccion = null) {
        return ($tipoDireccion === DireccionEnvio::DIRECCION_FACTURACION) ?
                [DireccionEnvio::DIRECCION_FACTURACION => DireccionEnvio::DIRECCION_FACTURACION] :
                [
            DireccionEnvio::CASA => DireccionEnvio::CASA,
            DireccionEnvio::OFICINA => DireccionEnvio::OFICINA,
            DireccionEnvio::DIRECCION_1 => DireccionEnvio::DIRECCION_1,
            DireccionEnvio::DIRECCION_2 => DireccionEnvio::DIRECCION_2,
            DireccionEnvio::DIRECCION_3 => DireccionEnvio::DIRECCION_3
        ];
    }

    public function create(Request $request, $type = null) {
        $direccionEnvio = new DireccionEnvio();

        if (is_string($type)) {
            $direccionEnvio->setTipoDireccion($type);
        }

        return $this->setData($request, 'create', $direccionEnvio);
    }

    /* public function edit(Request $request, DireccionEnvio $direccionEnvio) {
      return $this->setData($request, $direccionEnvio);
      } */

    /* public function setData($data, $direccionEnvio) {
      if (!$this->generalService->setIfPropertyExists($direccionEnvio, $data, 'clienteWeb')) {
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'user');
      }
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'calle');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'calle_id');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'calleNumero');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'departamento');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'departamento_id');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'localidad');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'localidad_id');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'provincia');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'provincia_id');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'unidadFuncional');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'piso');
      $this->generalService->setIfPropertyExists($direccionEnvio, $data, 'zipCode');

      return $direccionEnvio;
      } */

    public function renderFormAlta(DireccionEnvio $direccionEnvio) {
        $form = $this->form->create(DireccionEnvioType::class, $direccionEnvio, [
            'action' => $this->router->generate('direccion_envio_new'),
            'method' => 'POST'
                ]
        );

        echo $this->twig->render('tienda/direccion_envio/modalNuevaDireccion.html.twig', [
            'formDireccionEnvio' => $form->createView()
        ]);
    }

    public function renderFormEdit(DireccionEnvio $direccionEnvio) {
        $form = $this->form->create(DireccionEnvioType::class, $direccionEnvio, [
            'action' => $this->router->generate('direccion_envio_edit', ['id' => $direccionEnvio->getId()]),
            'method' => 'POST'
                ]
        );

        echo $this->twig->render('tienda/direccion_envio/modalEditarDireccion.html.twig', [
            'formDireccionEnvio' => $form->createView(),
            'direccionEnvio' => $direccionEnvio
        ]);
    }

    public function getDireccionesEnvio($filter) {
        return $this->direccionEnvioRepository->findBy($filter);
    }

    public function getDireccionesByPedido($pedidoId) {
        $pedido = $this->pedidoRepository->find($pedidoId);

        $clienteWeb = $pedido->getClienteWeb();

        return (isset($clienteWeb)) ? $this->direccionEnvioRepository->findBy(['clienteWeb' => $clienteWeb->getId(), 'enabled' => true]) : $this->direccionEnvioRepository->findBy(['user' => $pedido->getUser()->getId(), 'enabled' => true]);
    }

    public function getBillingAddressByPedido($pedidoId) {
        $pedido = $this->pedidoRepository->find($pedidoId);

        $clienteWeb = $pedido->getClienteWeb();

        if (isset($clienteWeb)) {
            return $this->direccionEnvioRepository->findOneBy(['clienteWeb' => $clienteWeb->getId(), 'enabled' => true, 'tipo_direccion' => DireccionEnvio::CASA]);
        }

        return $this->direccionEnvioRepository->findOneBy(['user' => $pedido->getUser()->getId(), 'enabled' => true, 'tipo_direccion' => DireccionEnvio::CASA]);
    }

    public function getZipCode() {
        $pedido = $this->pedidoService->getPedidoPendiente();
        $direccionEnvio = $this->getBillingAddressByPedido($pedido->getId());

        return ['zipCode' => $direccionEnvio->getZipCode()];
    }

    public function setData(Request $request, string $action, DireccionEnvio $direccionEnvio) {

        $isDireccionEnvio = array_key_exists('direccion_envio', $request->request->all());
        
        $provinciaId = $isDireccionEnvio ? $request->request->all()['direccion_envio']['provincia'] : $request->request->all()['cliente_web']['provincia'] ;

        $provincia = $this->geoService->getProvincia($provinciaId);

        if (is_null($provincia)) {
            return $this->redirectToRoute('tienda_pedido_checkout',['errorMessage'=>$this->translator->trans('It was not possible to {{ action }} the address. Province not found.', ['{{ action }}' => $action])]);
        }

        $localidad = $this->geoService->getLocalidad($request->request->all()['localidad']);
        if (is_null($localidad)) {
            return $this->redirectToRoute('tienda_pedido_checkout',['errorMessage'=>$this->translator->trans('It was not possible to {{ action }} the address. Locality not found.', ['{{ action }}' => $action])]);
        }

        $departamento = $this->geoService->getDepartamento($request->request->get('departamento'));

        $direccionEnvio->setProvincia($provincia->nombre);
        $direccionEnvio->setProvinciaId($provincia->id);
        $direccionEnvio->setLocalidad($localidad->nombre);
        $direccionEnvio->setLocalidadId($localidad->id);

        $direccionEnvio->setDepartamento(is_object($departamento) ? $departamento->nombre : null);
        $direccionEnvio->setDepartamentoId(is_object($departamento) ? $departamento->id : null);

        $direccionEnvio->setCalle($request->request->get('calle'));
        $direccionEnvio->setCalleId($request->request->get('calle_id', null));    
        
        $calleNumero = $isDireccionEnvio ?  $request->request->all()['direccion_envio']['calle_numero'] : $request->request->all()['cliente_web']['calleNumero'] ;
        $direccionEnvio->setCalleNumero($calleNumero);
        
        $zipCode = $isDireccionEnvio ? $request->request->all()['direccion_envio']['zipCode'] : $request->request->all()['cliente_web']['zipCode'];
        $direccionEnvio->setZipCode($zipCode);

        return $direccionEnvio;
    }

}
