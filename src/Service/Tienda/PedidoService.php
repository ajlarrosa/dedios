<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\PedidoRepository;
use App\Entity\Tienda\Pedido;
use App\Entity\Tienda\FormatoMail;
use App\Entity\ListaPrecio;
use App\Entity\Common\Email;
use App\Entity\Tienda\DetallePedido;
use App\Service\PrecioService;
use App\Service\ListaPrecioService;
use App\Service\General\MailerService;
use App\Service\Tienda\ConfiguracionService;
use App\Service\Tienda\FormatoMailService;
use App\Service\ImagenProductoService;
use App\Repository\ClienteRepository;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PedidoService {

    private $clienteRepository;
    private $email_receiver;
    private $pedidoRepository;
    private $security;
    private $twig;
    private $em;
    private $mailerService;
    private $formatoMailService;
    private $translator;
    private $imagenProductoService;

    public function __construct($email_receiver, FormatoMailService $formatoMailService, ConfiguracionService $configuracionService, ClienteRepository $clienteRepository, TranslatorInterface $translator, MailerService $mailerService, ListaPrecioService $listaPrecioService, PrecioService $precioService, PedidoRepository $pedidoRepository, Environment $twig, Security $security, EntityManagerInterface $em, ImagenProductoService $imagenProductoService) {
        $this->clienteRepository = $clienteRepository;
        $this->pedidoRepository = $pedidoRepository;
        $this->email_receiver = $email_receiver;
        $this->twig = $twig;
        $this->security = $security;
        $this->precioService = $precioService;
        $this->listaPrecioService = $listaPrecioService;
        $this->mailerService = $mailerService;
        $this->formatoMailService = $formatoMailService;
        $this->translator = $translator;
        $this->em = $em;
        $this->configuracion = $configuracionService->getLastConfiguration();
        $this->imagenProductoService = $imagenProductoService;
    }

    /*
     * Create
     */

    public function create($user, $clienteWeb = null) {
        $pedido = new Pedido();
        $pedido->setHash(hash("sha256", serialize($pedido)));

        if ($user) {
            $pedido->setUser($user);
        } else {
            $pedido->setClienteWeb($clienteWeb);
        }

        return $pedido;
    }

    /*
     * get pedido pendiente
     */

    public function getPedidoPendiente() {

        $user = $this->security->getUser();

        return $this->pedidoRepository->findOneBy(['user' => $user, 'enabled' => 1, 'estado' => Pedido::PENDIENTE]);
    }

    /*
     * Get pedido by Id
     */

    public function getPedidoPendienteById($id) {

        return $this->pedidoRepository->find(
                        [
                            'id' => $id
                        ]
        );
    }

    /*
     * get mis pedidos
     */

    public function getMisPedidos() {

        $user = $this->security->getUser();

        return $this->pedidoRepository->getMisPedidos($user);
    }

    /*
     * Count productos en pedido pendiente
     */

    public function countProductosPedidoPendiente() {
        $pedido = $this->getPedidoPendiente();
        if ($pedido) {
            return sizeof($pedido->getDetallespedido());
        } else {
            return 0;
        }
    }

    /*
     * Add product to pedido
     */

    public function addProduct($product) {

        try {
            $detallePedido = new DetallePedido();
            $detallePedido->setCantidad($product->cantidad);

            $detallePedido->setGrabado($product->grabado);
            $detallePedido->setColor($product->color);
            $detallePedido->setLetra($product->letra);
            $detallePedido->setMedida($product->medida);
            $detallePedido->setProducto($product->producto);
            $detallePedido->setProductodesc($product->producto->getDescripcion());
            $detallePedido->setPedido($this->getPedidoPendiente());

            $this->em->persist($detallePedido);
            $this->em->flush($detallePedido);
            return $detallePedido;
        } catch (\Exception $e) {
            return false;
        }
    }

    /*
     * Add product to pedido
     */

    public function addProductPedido($product, Pedido $pedido) {

        try {
            $detallePedido = new DetallePedido();
            $detallePedido->setCantidad($product->cantidad);

            $detallePedido->setGrabado($product->grabado);
            $detallePedido->setColor($product->color);
            $detallePedido->setLetra($product->letra);
            $detallePedido->setMedida($product->medida);
            $detallePedido->setProducto($product->producto);
            $detallePedido->setProductodesc($product->producto->getDescripcion());
            $detallePedido->setPedido($pedido);

            $this->em->persist($detallePedido);
            $this->em->flush($detallePedido);
            return $detallePedido;
        } catch (\Exception $e) {
            return false;
        }
    }

    /*
     * Get total para pedidos pendientes, toma los precios actuales. No los precios guardados
     * una vez confirmado el pedido
     */

    public function getSubTotal($idPedido = null) {

        if (!$idPedido) {
            $pedido = $this->getPedidoPendiente();
        } else {
            $pedido = $this->pedidoRepository->find($idPedido);
        }

        $subtotal = 0;

        if ($pedido->getUser() != null && $pedido->getUser()->hasRole("ROLE_USER_MAYORISTA")) {
            $listaPrecioId = ListaPrecio::LISTA_PRECIO_MAYORISTA_WEB;
        } else {
            $listaPrecioId = ListaPrecio::LISTA_PRECIO_MINORISTA_WEB;
        }

        foreach ($pedido->getDetallespedido() as $detallePedido) {
            if (!is_null($detallePedido->getPrecio())) {
                $precio = $detallePedido->getPrecio();
            } else {
                $grabado = 0;
                if ($detallePedido->getGrabado() != '') {
                    $grabado = $this->listaPrecioService->getPrecioGrabado();
                }
                $precio = $this->precioService->getPrecioMedidaFinal($detallePedido->getProducto()->getId(), $detallePedido->getMedida(), $listaPrecioId) + $grabado;
            }
            $subtotal = $subtotal + round($detallePedido->getCantidad() * $precio, 2);
        }

        return $subtotal;
    }

    /*
     * Get total
     */

    public function getTotal($idPedido = null) {

        if (!$idPedido) {
            $pedido = $this->getPedidoPendiente();
        } else {
            $pedido = $this->pedidoRepository->find($idPedido);
        }

        //$total = $this->getSubTotal($idPedido);        
        $total = $pedido->getTotal() + $pedido->getValorRecargo() - $pedido->getValorDescuento() - $pedido->getMontoPromocion();
        $costoEnvio = $pedido->getCostoEnvio();

        if (isset($costoEnvio)) {
            $total = $total + $pedido->getCostoEnvio();
        }
        return $total;
    }

    /*
     * Send Email Order confirmation
     */

    public function sendEmailOrderConfirmation(Pedido $pedido) {
        $sendMail = $pedido->getEmail();
        $imagenesPedido = $this->getImagenesPedido($pedido);
        
        $stores = $this->clienteRepository->getStores();
        $emailDedios = new Email();
        $emailDedios->setTitle($this->translator->trans("Order confirmation #") . $pedido->getId());

        $emailDedios->setSendTo($this->email_receiver);
        $emailDedios->setTemplate('email/confirmDedios.html.twig');

        $emailDedios->setParameters(['pedido' => $pedido, 'stores' => $stores, 'configuracion' => $this->configuracion, 'imagenesPedido' => $imagenesPedido]);
        $response = $this->mailerService->enviarMail($emailDedios);

        $email = new Email();
        $email->setTitle($this->translator->trans("Order confirmation #") . $pedido->getId());
        $email->setSendTo($sendMail);

        $email->setTemplate('email/confirm.html.twig');
        
        $fMail = (is_null($pedido->getUser())) ? FormatoMail::COD_CONFIRM_CLIENTE_WEB : ($pedido->getUser()->hasRole("ROLE_USER_MAYORISTA") ? FormatoMail::COD_CONFIRM_MAYORISTA : FormatoMail::COD_CONFIRM_MINORISTA);

        $formatoEmail = $this->formatoMailService->getFormatoMail([
            'type' => $fMail,
            'enabled' => true,
            'configuracion' => $this->configuracion
        ]);

        if (sizeof($formatoEmail) > 0) {
            $email->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'stores' => $stores, 'configuracion' => $this->configuracion, 'imagenesPedido' => $imagenesPedido]);
            $response = $this->mailerService->enviarMail($email);
            
            return true;
        }
    
        return false;
    }

    /*
     * Get Imagenes pedido
     */

    public function getImagenesPedido(Pedido $pedido) {
        $imagenesPedido = [];
        foreach ($pedido->getDetallespedido() as $detallePedido) {
            $imagenesPedido[] = '/public/uploads/productos/' . $detallePedido->getProducto()->getId() . '/' . $this->imagenProductoService->getFirstImage($detallePedido->getProducto()->getId());
        }
        return $imagenesPedido;
    }

    /*
     * Send Email Order confirmation
     */

    public function sendEmailConfirmTakeAway(Pedido $pedido) {
        $sendMail = $pedido->getEmail();
        $imagenesPedido = $this->getImagenesPedido($pedido);

        //Envío de mail al cliente
        $email = new Email();
        $email->setTitle($this->translator->trans("You can now withdraw your order #") . $pedido->getId());
        $email->setSendTo($sendMail);

        $email->setTemplate('email/confirmOrderTakeAway.html.twig');

        $formatoEmail = $this->formatoMailService->getFormatoMail([
            'type' => FormatoMail::COD_CONFIRM_ORDER_TAKE_AWAY,
            'enabled' => true,
            'configuracion' => $this->configuracion
        ]);
        if (sizeof($formatoEmail) > 0) {
            $email->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'configuracion' => $this->configuracion, 'imagenesPedido' => $imagenesPedido]);
            $response = $this->mailerService->enviarMail($email);
            if ($response === true) {
                return true;
            }
        }
        return false;
    }

    /*
     * Send Client Payment confirmation
     */

    public function sendClientPaymentConfirmation(Pedido $pedido) {
        $sendMail = $pedido->getEmail();
        $imagenesPedido = $this->getImagenesPedido($pedido);

        //Envío de mail al cliente
        $email = new Email();
        $email->setTitle($this->translator->trans("Payment Confirmed - Order #") . $pedido->getId());
        $email->setSendTo($sendMail);

        $email->setTemplate('email/confirmPagoCliente.html.twig');

        $formatoEmail = $this->formatoMailService->getFormatoMail([
            'type' => FormatoMail::COD_CONFIRM_PAYMENT_CLIENT,
            'enabled' => true,
            'configuracion' => $this->configuracion
        ]);
        if (sizeof($formatoEmail) > 0) {
            $email->setParameters(['title' => $formatoEmail[0]->getTitle(), 'body' => $formatoEmail[0]->getBody(), 'footer' => $formatoEmail[0]->getFooter(), 'pedido' => $pedido, 'configuracion' => $this->configuracion, 'imagenesPedido' => $imagenesPedido]);
            $response = $this->mailerService->enviarMail($email);
            if ($response === true) {
                return true;
            }
        }
        return false;
    }

    /*
     * Send Reminder Unconfirmed Order Payment
     */

    public function sendReminderUnconfirmedOrder(Pedido $pedido) {
        $sendMail = $pedido->getEmail();
        $imagenesPedido = $this->getImagenesPedido($pedido);

        //Envío de mail al cliente
        $email = new Email();
        $email->setTitle($this->translator->trans("Finish your order - Order #") . $pedido->getId());
        $email->setSendTo($sendMail);

        $email->setTemplate('email/reminder.html.twig');

        $formatoEmail = $this->formatoMailService->getFormatoMail([
            'type' => FormatoMail::COD_REMINDER_UNCONFIRMED_ORDER,
            'enabled' => true,
            'configuracion' => $this->configuracion
        ]);
        if (sizeof($formatoEmail) > 0) {
            $email->setParameters([
                'title' => $formatoEmail[0]->getTitle(),
                'body' => $formatoEmail[0]->getBody(),
                'footer' => $formatoEmail[0]->getFooter(),
                'pedido' => $pedido,
                'configuracion' => $this->configuracion,
                'imagenesPedido' => $imagenesPedido
            ]);
            $response = $this->mailerService->enviarMail($email);
            if ($response === true) {
                return true;
            }
        }
        return false;
    }

    /*
     * Get status select
     */

    public function getStatusSelect() {
        echo $this->twig->render('pedido/statusSelect.html.twig');
    }

    public function tieneGrabado($idPedido = null) {
        if (!$idPedido) {
            $pedido = $this->getPedidoPendiente();
        } else {
            $pedido = $this->pedidoRepository->find($idPedido);
        }

        foreach ($pedido->getDetallespedido() as $detallePedido) {
            if ($detallePedido->getGrabado() != '') {
                return true;
            }
        }

        return false;
    }

}
