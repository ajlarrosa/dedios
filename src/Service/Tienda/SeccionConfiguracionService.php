<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\SeccionConfiguracionRepository;
use App\Entity\Tienda\SeccionConfiguracion;
use Twig\Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Service\General\GeneralService;

class SeccionConfiguracionService {

    private $seccionConfiguracionRepository;
    private $twig;
    private $router;
    private $generalService;

    public function __construct(GeneralService $generalService, SeccionConfiguracionRepository $seccionConfiguracionRepository, Environment $twig, UrlGeneratorInterface $router) {
        $this->seccionConfiguracionRepository = $seccionConfiguracionRepository;
        $this->twig = $twig;
        $this->router = $router;
        $this->generalService = $generalService;
    }

    public function getRouteWithSeccionConfiguracion(SeccionConfiguracion $seccionConfiguracion, $first = true) {
        return $this->route($seccionConfiguracion, $first);
    }

    public function getRoute($id, $first = true) {
        $seccionConfiguracion = $this->seccionConfiguracionRepository->find($id);
        
        return $this->route($seccionConfiguracion, $first);
    }

    private function route(SeccionConfiguracion $seccionConfiguracion, $first) {
        if ($first === true) {
            $producto = $seccionConfiguracion->getProducto1();
            $categoria = $seccionConfiguracion->getCategoria1();
            $subcategoria = $seccionConfiguracion->getSubcategoria1();
            $lineaProducto = $seccionConfiguracion->getLineaProducto1();
        } else {
            $producto = $seccionConfiguracion->getProducto2();
            $categoria = $seccionConfiguracion->getCategoria2();
            $subcategoria = $seccionConfiguracion->getSubcategoria2();
            $lineaProducto = $seccionConfiguracion->getLineaProducto2();
        }

        if ($subcategoria && $categoria) {
            return $this->router->generate('subcategoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'subcategory' => $this->generalService->slugify($subcategoria->getDescripcion()),
                        'category_id' => $categoria->getId(),
                        'id' => $subcategoria->getId()
            ]);
        }
        if ($categoria) {
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $this->generalService->slugify($categoria->getDescripcion()),
                        'id' => $categoria->getId()
            ]);
        }

        if ($lineaProducto) {
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $this->generalService->slugify($lineaProducto->getDescripcion()),
                        'id' => $lineaProducto->getId()
            ]);
        }

        /* El producto debe ser el ultimo en  verificarse1 */
        
        if ($producto) {
            return $this->router->generate('producto_tienda_show', [
                        'categoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getCategoria()->getDescripcion()),
                        'subcategoria' => $this->generalService->slugify($producto->getCategoriasubcategoria()->getSubcategoria()->getDescripcion()),
                        'id' => $producto->getId(),
                        'descripcion' => $this->generalService->slugify($producto->getDescripcion())
            ]);
        }
        return '#';
    }

}
