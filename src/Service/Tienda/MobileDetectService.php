<?php

namespace App\Service\Tienda;

use App\Entity\Tienda\Pedido;

class MobileDetectService {

    private $mobileDetect;

    public function __construct() {
        $this->mobileDetect = new \Mobile_Detect();
    }

    public function setDevicePedido(Pedido $pedido): void {
        $pedido->setDeviceCheckout($this->mobileDetect->isMobile() ? 'Móvil' : 'Web');
    }

}
