<?php

namespace App\Service\Tienda;

use App\Repository\Tienda\MenuRepository;
use Twig\Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class VerMasService {

    private $menuRepository;
    private $twig;

    /*
     * 
     */

    public function __construct(MenuRepository $menuRepository, Environment $twig, UrlGeneratorInterface $router) {
        $this->menuRepository = $menuRepository;
        $this->twig = $twig;
        $this->router = $router;
    }

    /*
     * 
     */
    public function getRoute($element,$categoria=null,$search=null) {
        if($element!=null){
            $class = get_class($element);
        }else{
            $class = '';
        }
        
        if (strpos($class,'Subcategoria')) {
            return $this->router->generate('subcategoria_tienda_index', [
                        'category' => $categoria->getDescripcion(),
                        'subcategory' => $element->getDescripcion(),
                        'category_id' => $categoria->getId(),
                        'id' => $element->getId()
            ]);
        }
        if (strpos($class,'Categoria')) {
            return $this->router->generate('categoria_tienda_index', [
                        'category' => $element->getDescripcion(),
                        'id' => $element->getId()
            ]);
        }

        if (strpos($class,'LineaProducto')) {
            return $this->router->generate('linea_producto_tienda_index', [
                        'descripcion' => $element->getDescripcion(),
                        'id' => $element->getId()
            ]);
        }

        if($class==''){
            $palabrasClaves = explode(' ',trim($search));
        
            return $this->router->generate('producto_tienda_index',[
                'palabrasClaves'=>$palabrasClaves
            ]);
        }
        
        return '#';
    }

}
