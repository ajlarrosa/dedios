<?php

namespace App\Service;

use App\Repository\ImagenProductoRepository;
use App\Entity\ImagenProducto;
use Twig\Environment;

class ImagenProductoService {


    private $imagenProductoRepository;
    private $twig;

    public function __construct(ImagenProductoRepository $imagenProductoRepository, Environment $twig) {
        $this->imagenProductoRepository = $imagenProductoRepository;
        $this->twig = $twig;
    }

    /*
     * Get first image
     */

    public function getFirstImage($productId) {
        $imagenProducto = $this->imagenProductoRepository->findFirstImage($productId); 
       
        if ($imagenProducto){
            return $imagenProducto->getPath();
        }else {
            return null;
        }
        
    }

    
    
}
