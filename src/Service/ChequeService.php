<?php

namespace App\Service;

use App\Repository\ChequeRepository;
use App\Entity\Cheque;
use App\Repository\BancoRepository;
use App\Repository\TipoChequeRepository;
use Twig\Environment;

class ChequeService {

    private $chequeRepository;
    private $bancoRepository;
    private $tipoChequeRepository;
    private $twig;

    public function __construct(ChequeRepository $chequeRepository, Environment $twig, BancoRepository $bancoRepository, TipoChequeRepository $tipoChequeRepository) {
        $this->chequeRepository = $chequeRepository;
        $this->bancoRepository = $bancoRepository;
        $this->tipoChequeRepository = $tipoChequeRepository;
        $this->twig = $twig;
    }

    public function getStatusSelect() {
        $estados = [Cheque::COD_ACTIVO => Cheque::ACTIVO, Cheque::COD_ELIMINADO => Cheque::ELIMINADO, Cheque::COD_RECHAZADO => Cheque::RECHAZADO, Cheque::COD_USADO => Cheque::USADO];
        echo $this->twig->render('cheque/selectStatus.html.twig', [
            'estados' => $estados
                ]
        );
    }

    /*
     * Create
     */

    public function create($data) {
        $cheque = new Cheque();
        $cheque->setBanco($this->bancoRepository->find($data->banco));
        $cheque->setCuit($data->cuit);        
        $cheque->setFechaEmision(\DateTime::createFromFormat('Y-m-d', $data->fechaEmision));
        if ($data->fechaCobro!=''){
            $cheque->setFechaCobro(\DateTime::createFromFormat('Y-m-d', $data->fechaCobro));
        }
        
        $cheque->setFirmantes($data->firmantes);
        $cheque->setImporte(floatval($data->importe));
        $cheque->setNroCheque($data->nroCheque);
        $cheque->setTipoCheque($this->tipoChequeRepository->find($data->tipoCheque));


        return $cheque;
    }

}
