<?php

namespace App\Service;

use App\Repository\TipoGastoRepository;
use Twig\Environment;

class TipoGastoService {

    private $tipoGastoRepository;
    private $twig;

    public function __construct(TipoGastoRepository $tipoGastoRepository, Environment $twig) {
        $this->tipoGastoRepository = $tipoGastoRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo,$array=null) {
        $tipoGastos = $this->tipoGastoRepository->findBy(['activo' => $activo], ['nombre' => 'ASC']);
        echo $this->twig->render('tipogasto/select.html.twig', ['tipoGastos' => $tipoGastos,'array'=>$array]);
    }

}
