<?php

namespace App\Service;

use App\Repository\SubcategoriaRepository;
use Twig\Environment;

class SubcategoriaService {

    private $subcategoriaRepository;
    private $twig;

    public function __construct(
            SubcategoriaRepository $subcategoriaRepository,
            Environment $twig
    ) {
        $this->subcategoriaRepository = $subcategoriaRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $subcategorias = $this->subcategoriaRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('subcategoria/select.html.twig', ['subcategorias' => $subcategorias]);
    }

    public function getSubcategories($filtros) {
        $filtros['activo'] = 1;
        $filtros['order'] = 'subcategoria';
        $subcategorias = $this->subcategoriaRepository->getQueryFilter($filtros);
        return $subcategorias;
    }

}
