<?php

namespace App\Service;

use App\Repository\MedidaRepository;
use Twig\Environment;

class MedidaService {

    private $medidaRepository;
    private $twig;

    public function __construct(MedidaRepository $medidaRepository, Environment $twig) {
        $this->medidaRepository = $medidaRepository;
        $this->twig = $twig;
    }

    public function getSelect($enabled) {
        $medidas = $this->medidaRepository->findBy(['enabled' => $enabled], ['descripcion' => 'ASC']);
        echo $this->twig->render('medida/select.html.twig', ['medidas' => $medidas]);
    }

}
