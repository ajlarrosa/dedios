<?php

namespace App\Service\General;

class GeneralService {
     
    public function isImage($filetype) {
        return in_array($filetype, ['image/jpg', 'image/png', 'image/jpeg']);
    }

    
    /*
     * Set if property exists
     */
    public function setIfPropertyExists($entity, $object, $attribute) {

        if (property_exists($object, $attribute)) {
            $method = "set" . $this->underscore2Camelcase($attribute);
            $entity->{$method}($object->{$attribute});
            
            return true;
        }
        
        return false;
    }

    /*
     * Undescored to Camelcase
     */
    private function underscore2Camelcase($str) {
        // Split string in words.
        $words = explode('_', strtolower($str));

        $return = '';
        foreach ($words as $word) {
            $return .= ucfirst(trim($word));
        }

        if ($return == '') {
            return $str;
        }
        return $return;
    }

    public function slugify(string $text): string {

        $text = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove;', $text);
        $text = str_replace("/", "-", $text);

        $text = str_replace(" ", "_", $text);
        $text = str_replace('"', "", $text);
        $text = str_replace("(", "", $text);
        $text = str_replace(")", "", $text); 
        $text = str_replace("&", "", $text);

        return empty($text) ? 'n-a' : $text;            
    }
    
}
