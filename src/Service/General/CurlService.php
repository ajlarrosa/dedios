<?php

namespace App\Service\General;

class CurlService {

    private $result;
    private $url;

    public function __construct($url) {
        $this->result = [];
        $this->result['message'] = '';
        $this->result['code'] = 0;
        $this->url = $url;
    }

    /*
     * Get Token
     */

    public function getToken($methodUrl, $data) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $methodUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $data['user'] . ":" . $data['password']
        ));

        $result = curl_exec($curl);

        if ($result === false) {
            $this->result['code'] = 500;
            $this->result['data'] = null;
            $this->result['message'] = 'No ha respondido el servidor. Pruebe nuevamente o contacte al administrador';
            return (object) $this->result;
        } else {
            return $result;
        }
    }

    /*
     * Get Data
     */

    public function getData($methodUrl, $token = null) {
        $curl = curl_init();

        $opciones = [
            CURLOPT_URL => $this->url . $methodUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ];

        if (isset($token)) {
            $opciones[CURLOPT_POSTFIELDS] = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_consumer_key\"\r\n\r\ntoken\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_token\"\r\n\r\n1HbjxjY5wNwJ7wDMbZWCZ0XliK6F57XiLIVjau1anIN5M_yFaA\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_signature_method\"\r\n\r\nHMAC-SHA1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_timestamp\"\r\n\r\n1607703488\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_nonce\"\r\n\r\nQu8Km0\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_version\"\r\n\r\n1.0\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"oauth_signature\"\r\n\r\nh8GjgsjGZi/GKrz4oVtnnw5H7P4=\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
            $opciones[CURLOPT_HTTPHEADER] = [
                "authorization: Token token=" . $token,
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ];
        } else {
            $opciones[CURLOPT_HTTPHEADER] = [
                "cache-control: no-cache"
            ];
        }

        curl_setopt_array($curl, $opciones);
        $result = curl_exec($curl);
       
        if ($result === false) {
            $this->result['code'] = 500;
            $this->result['data'] = null;
            $this->result['message'] = 'No ha respondido el servidor. Pruebe nuevamente o contacte al administrador';
            return (object) $this->result;
        }
        
        return $result;
    }

    /*
     * Get Data
     */

    public function setData($methodUrl, $token, $data, $method = 'POST') {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url . $methodUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "authorization: Token token=" . $token,
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));
        $result = curl_exec($curl);

        if ($result === false) {
            $this->result['code'] = 500;
            $this->result['data'] = null;
            $this->result['message'] = 'No ha respondido el servidor. Pruebe nuevamente o contacte al administrador';
            return (object) $this->result;
        } else {
            return $result;
        }
    }

}
