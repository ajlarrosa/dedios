<?php

namespace App\Service\General;

use Twig\Environment;
use App\Entity\Common\Moneda;
use App\Service\Tienda\ConfiguracionService;
use App\Repository\UserRepository;

class MonedaService {

    private $twig;
    private $configuracionService;
    private $userRepository;

    public function __construct(Environment $twig, ConfiguracionService $configuracionService, UserRepository $userRepository) {
        $this->twig = $twig;
        $this->configuracionService = $configuracionService;
        $this->userRepository = $userRepository;
    }

    /*
     * Get Moneda String
     */

    public function getMonedaString($codMoneda) {

        if ($codMoneda == Moneda::COD_PESOS) {
            return Moneda::SIMBOLO_PESOS;
        } else {
            return Moneda::SIMBOLO_DOLARES;
        }
    }

    /*
     * Get Moneda String
     */

    public function getMonedaIso($codMoneda) {

        if ($codMoneda == Moneda::COD_PESOS) {
            return Moneda::ISO_PESOS;
        } else {
            return Moneda::ISO_DOLARES;
        }
    }

    /*
     * get Moneda Usuario
     */

    public function getMonedaUsuario($usuarioId) {
        $usuario = $this->userRepository->find($usuarioId);

        if (strpos($usuario->getRolesHtml(), 'USUARIO MAYORISTA')) {

            $codMoneda = $this->configuracionService->getMonedaMayorista();
        } else {

            $codMoneda = $this->configuracionService->getMonedaMinorista();
        }
        return $this->getMonedaString($codMoneda);
    }

    /*
     * get Moneda Usuario
     */

    public function getCodMonedaUsuario($usuarioId) {
        $usuario = $this->userRepository->find($usuarioId);

        if (strpos($usuario->getRolesHtml(), 'USUARIO MAYORISTA')) {

            return $this->configuracionService->getMonedaMayorista();
        }

        return $this->configuracionService->getMonedaMinorista();
    }

}
