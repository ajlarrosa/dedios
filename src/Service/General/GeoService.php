<?php

namespace App\Service\General;

use App\Service\General\CurlService;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class GeoService extends CurlService {

    private $geoUrl;
    private $cache;

    public function __construct($geoUrl) {
        $this->geoUrl = $geoUrl;
        $this->cache = new FilesystemAdapter();
        parent::__construct($this->geoUrl);
    }

    public function getProvincias() {
        return $this->cache->get('provincias', function (ItemInterface $item) {
                    $item->expiresAfter(3200);
                    $response = $this->getData("provincias?1=1");

                    if (is_object($response)) {
                        return [];
                    }

                    $provincias = json_decode($response);

                    return $provincias->{'hydra:member'};
                });
    }

    public function getProvincia($id) {
        return $this->cache->get('provincia_id_' . $id, function (ItemInterface $item) use ($id) {
                    $item->expiresAfter(3200);
                    return json_decode($this->getData("provincias/" . $id));
                });
    }

    public function getLocalidades($idProvincia, $idDepartamento, $page = 1) {
        if (empty($idProvincia)) {
            return $this->cache->get('localidades_departamento_id_' . $idDepartamento . '_page_' . $page, function (ItemInterface $item) use ($idDepartamento, $page) {
                        $item->expiresAfter(3200);
                        $localidades = json_decode($this->getData("localidads?departamentoId=" . $idDepartamento . '&page=' . $page));
                        return $localidades->{'hydra:member'};
                    });
        }

        return $this->cache->get('localidades_provincia_id_' . $idProvincia . '_page_' . $page, function (ItemInterface $item) use ($idProvincia, $page) {
                    $item->expiresAfter(3200);
                    $localidades = json_decode($this->getData("localidads?provinciaId=" . $idProvincia . '&page=' . $page));
                    return $localidades->{'hydra:member'};
                });
    }

    public function getLocalidad($id) {
        return $this->cache->get('localidad_id_' . $id, function (ItemInterface $item) use ($id) {
                    $item->expiresAfter(3200);
                    return json_decode($this->getData("localidads/" . $id));
                });
    }

    public function getDepartamentos($idProvincia, $page = 1) {
        return $this->cache->get('departamentos_provincia_id_' . $idProvincia . '_page_' . $page, function (ItemInterface $item) use ($idProvincia, $page) {
                    $item->expiresAfter(3200);
                    $departamentos = json_decode($this->getData("departamentos?provinciaId=" . $idProvincia . '&page=' . $page));
                    return $departamentos->{'hydra:member'};
                });
    }

    public function getDepartamento($id) {
        return $this->cache->get('departamento_id_' . $id, function (ItemInterface $item) use ($id) {
                    $item->expiresAfter(3200);
                    return json_decode($this->getData("departamentos/" . $id));
                });
    }

    public function getCalles(bool &$more, $idLocalidadCensal, int $page = 1,string $nombre = null) {  
        $response = [];
        $nombreCalles = [];

        $calles = $this->cache->get('calle_localidacensal_id_' . $idLocalidadCensal . '_page_' . $page.'_nombre_'.(is_string($nombre) ? $nombre  :''), function (ItemInterface $item) use ($idLocalidadCensal, $page, $nombre) {
            $item->expiresAfter(3200);
                    
            $method ="calles?localidadCensalId=" . $idLocalidadCensal . '&page=' . $page;            
            
            $response = json_decode($this->getData(is_string($nombre) ? $method.'&nombre='.$nombre : $method));
            
            return $response->{'hydra:member'};
        }); 
               
        $more = (count($calles) > 0) ? true : false;        
        
        foreach ($calles as $calle) {
            if (in_array($calle->nombre, $nombreCalles)) {
                continue;
            }
            
            $response[] = $calle;
            $nombreCalles[] = $calle->nombre;
            
        }
        
        return $response;
    }

    public function getCalle($id) {
        $response = $this->cache->get('calle_id_' . $id, function (ItemInterface $item) use ($id) {
            $item->expiresAfter(3200);
            return json_decode($this->getData("calles/" . $id));
        });

        return (property_exists($response, 'id')) ? $response : null;
    }

}
