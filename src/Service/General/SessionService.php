<?php

namespace App\Service\General;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\Tienda\ConfiguracionRepository;
use App\Service\Tienda\ConfiguracionService;
use App\Service\ListaPrecioService;
use App\Service\Tienda\SeccionConfiguracionService;
use App\Service\ImagenProductoService;
use App\Service\Tienda\GrupoService;
use App\Service\Tienda\MenuService;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class SessionService {

    private $session;
    private $configuracionRepository;
    private $configuracionService;
    private $listaPrecioService;
    private $imagenProductoService;
    private $grupoService;
    private $menuService;
    private $cache;

    public function __construct(
            SessionInterface $session,
            ConfiguracionRepository $configuracionRepository,
            ConfiguracionService $configuracionService,
            ListaPrecioService $listaPrecioService,
            SeccionConfiguracionService $seccionConfiguracionService,
            ImagenProductoService $imagenProductoService,
            GrupoService $grupoService,
            MenuService $menuService
    ) {
        $this->session = $session;
        $this->configuracionRepository = $configuracionRepository;
        $this->configuracionService = $configuracionService;
        $this->seccionConfiguracionService = $seccionConfiguracionService;
        $this->imagenProductoService = $imagenProductoService;
        $this->listaPrecioService = $listaPrecioService;
        $this->grupoService = $grupoService;
        $this->menuService = $menuService;
        $this->cache = new FilesystemAdapter();

        //if (!$this->cache->hasItem('configuracion')){          
            $this->setConfiguration();
        //}        
    }

    public function getConfiguration() {
        return $this->session->get('configuration');
    }

    public function getImageMainSlider1() {
        return $this->session->get('imagenMainSlider1');
    }

    public function setConfiguration() {
        $configuracionActual = $this->configuracionRepository->findOneBy(['enabled' => 1], ['id' => 'DESC'], 1);
                
        $configuracion = $this->cache->get('configuracion', function (ItemInterface $item) use ($configuracionActual) {
            $item->expiresAfter(3200);          
            return $configuracionActual;
        });
        
        $this->session->set('configurationId', $configuracion->getId());
        $this->session->set('imagenMainSlider1', $configuracion->getImagenMainSlider1());
        $this->session->set('imagenMainSlider2', $configuracion->getImagenMainSlider2());
        $this->session->set('imagenMainSlider3', $configuracion->getImagenMainSlider3());

        $this->session->set('imagenMainSliderMobile1', $configuracion->getImagenMainSliderMobile1());
        $this->session->set('imagenMainSliderMobile2', $configuracion->getImagenMainSliderMobile2());
        $this->session->set('imagenMainSliderMobile3', $configuracion->getImagenMainSliderMobile3());

        $this->session->set('ruta1', $this->configuracionService->getWithConfiguracionRoute($configuracion));
        $this->session->set('ruta2', $this->configuracionService->getWithConfiguracionRoute($configuracion, false));
        $this->session->set('ruta3', $this->configuracionService->getWithConfiguracionRoute($configuracion, false, false));

        $this->session->set('videoMainSlider', $configuracion->getVideoMainSlider());

        $menus = [];
        foreach ($configuracionActual->getMenus() as $menu) {
            $m = [];
            $m['descripcion'] = $menu->getDescripcion();
            $m['cantidad'] = sizeof($menu->getGrupos());
            foreach ($menu->getGrupos() as $grupo) {
                $g = [];
                $g['descripcion'] = $grupo->getDescripcion();
                foreach ($grupo->getListaActiva() as $lista) {
                    $l = [];
                    $l['title'] = $lista->getItem()->getTitleMenu();
                    $l['ruta'] = $this->grupoService->getWithGrupoRoute($grupo, $lista->getItem()->getId());
                    $g['listaActiva'][] = $l;
                }
                $m['grupos'][] = $g;
            }
            $m['ruta1'] = $this->menuService->getMenuWithMenuRoute($menu);

            $m['ruta2'] = $this->menuService->getMenuWithMenuRoute($menu, false);
            $m['texto1'] = $menu->getTexto1();
            $m['texto2'] = $menu->getTexto2();

            $m['imagen'] = $menu->getImagen();

            $menus[] = $m;
        }

        $this->session->set('menus', json_encode($menus));
        $this->session->set('logo', $configuracion->getLogo());

        $secciones = [];
        foreach ($configuracionActual->getSeccionConfiguracion() as $seccionConfiguracion) {
            $s = [];
            if ($seccionConfiguracion->getEnabled()) {
                $s['descripcion'] = $seccionConfiguracion->getSeccion()->getDescripcion();
                $s['titulo1'] = $seccionConfiguracion->getTitulo1();
                $s['imagen1'] = $seccionConfiguracion->getImagen1();
                $s['imagen2'] = $seccionConfiguracion->getImagen2();
                
                $s['ruta'] = $this->seccionConfiguracionService->getRouteWithSeccionConfiguracion($seccionConfiguracion);

                $s['ruta2'] = $this->seccionConfiguracionService->getRouteWithSeccionConfiguracion($seccionConfiguracion, false);

                if (!is_null($seccionConfiguracion->getProducto1())) {
                    $s['categoriaProducto1'] = $seccionConfiguracion->getProducto1()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto1'] = $seccionConfiguracion->getProducto1()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto1'] = $seccionConfiguracion->getProducto1()->getId();
                    $s['descripcionProducto1'] = $seccionConfiguracion->getProducto1()->getSlug();
                    $s['imagenProducto1'] = $seccionConfiguracion->getProducto1()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto1()->getId());
                }

                if (!is_null($seccionConfiguracion->getProducto2())) {
                    $s['categoriaProducto2'] = $seccionConfiguracion->getProducto2()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto2'] = $seccionConfiguracion->getProducto2()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto2'] = $seccionConfiguracion->getProducto2()->getId();
                    $s['descripcionProducto2'] = $seccionConfiguracion->getProducto2()->getSlug();
                    $s['imagenProducto2'] = $seccionConfiguracion->getProducto2()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto2()->getId());
                }

                if (!is_null($seccionConfiguracion->getProducto3())) {
                    $s['categoriaProducto3'] = $seccionConfiguracion->getProducto3()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto3'] = $seccionConfiguracion->getProducto3()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto3'] = $seccionConfiguracion->getProducto3()->getId();
                    $s['descripcionProducto3'] = $seccionConfiguracion->getProducto3()->getSlug();
                    $s['imagenProducto3'] = $seccionConfiguracion->getProducto3()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto3()->getId());
                }

                if (!is_null($seccionConfiguracion->getProducto4())) {
                    $s['categoriaProducto4'] = $seccionConfiguracion->getProducto4()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto4'] = $seccionConfiguracion->getProducto4()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto4'] = $seccionConfiguracion->getProducto4()->getId();
                    $s['descripcionProducto4'] = $seccionConfiguracion->getProducto4()->getSlug();
                    $s['imagenProducto4'] = $seccionConfiguracion->getProducto4()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto4()->getId());
                }

                if (!is_null($seccionConfiguracion->getProducto5())) {
                    $s['categoriaProducto5'] = $seccionConfiguracion->getProducto5()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto5'] = $seccionConfiguracion->getProducto5()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto5'] = $seccionConfiguracion->getProducto5()->getId();
                    $s['descripcionProducto5'] = $seccionConfiguracion->getProducto5()->getSlug();
                    $s['imagenProducto5'] = $seccionConfiguracion->getProducto5()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto5()->getId());
                }

                if (!is_null($seccionConfiguracion->getProducto6())) {
                    $s['categoriaProducto6'] = $seccionConfiguracion->getProducto6()->getCategoriasubcategoria()->getCategoria()->getSlug();
                    $s['subcategoriaProducto6'] = $seccionConfiguracion->getProducto6()->getCategoriasubcategoria()->getSubcategoria()->getSlug();
                    $s['idProducto6'] = $seccionConfiguracion->getProducto6()->getId();
                    $s['descripcionProducto6'] = $seccionConfiguracion->getProducto6()->getSlug();
                    $s['imagenProducto6'] = $seccionConfiguracion->getProducto6()->getId() . '/' . $this->imagenProductoService->getFirstImage($seccionConfiguracion->getProducto6()->getId());
                }
            }
            $secciones[] = $s;
        }
        $this->session->set('secciones', json_encode($secciones));

        $monedaMayorista = $configuracion->getMonedaMayorista();
        $simboloMayorista = $this->cache->get('simboloMayorista', function (ItemInterface $item) use ($monedaMayorista) {
            $item->expiresAfter(3200);          
            return $this->listaPrecioService->getSymbol($monedaMayorista);
        });
        
        $this->session->set('simboloMayorista', $simboloMayorista);
        
        $monedaMinorista = $configuracion->getMonedaMinorista();
        $simboloMinorista = $this->cache->get('simboloMinorista', function (ItemInterface $item) use ($monedaMinorista) {
            $item->expiresAfter(3200);          
            return $this->listaPrecioService->getSymbol($monedaMinorista);
        });
                
        $this->session->set('simboloMinorista', $simboloMinorista);
        
        $this->session->set('costoFijoShipNow', $configuracion->getCostoFijoShipNow());
        $this->session->set('mostrarFechaShipNow', $configuracion->getMostrarFechaShipNow());        
        $this->session->set('promocion', $configuracionActual->getPromocion());
        $this->session->set('promocionVisibleMayorista', $configuracion->getPromocion()->getVisibleMayorista());
    }

}
