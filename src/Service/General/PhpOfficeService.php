<?php

namespace App\Service\General;

use Twig\Environment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PhpOfficeService {

    private $twig;
    private $entityManager;
    private $translator;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager, Environment $twig, TranslatorInterface $translator) {
        $this->twig = $twig;
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    public function exportarExcel($spreadSheet, $titulo) {

        $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        $writer = new Xlsx($spreadSheet);
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $titulo . '.xlsx"');
        $response->setPrivate();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('must-revalidate', true);

        $response->setCallback(function() use ($writer) {
            $writer->save('php://output');
        });
        return $response;
    }
    /*
     * Exportar csv
     */
    public function exportCsv($spreadSheet, $titulo) {
        $writer = new Csv($spreadSheet);
        $writer->setDelimiter(",");
        $writer->setEnclosure('"');
        $writer->setUseBOM(true);

        $response = new StreamedResponse();

        $contentType = 'text/csv;charset=utf-8';
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $titulo . '.csv"');
        $response->setPrivate();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('must-revalidate', true);        

        $response->setCallback(function() use ($writer) {
            $writer->save('php://output');
        });

        return $response;
    }

}
