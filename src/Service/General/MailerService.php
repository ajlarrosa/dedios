<?php

namespace App\Service\General;

use Twig\Environment;
use App\Entity\Common\Email;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class MailerService {

    protected $mailer;
    private $twig;
    private $emailSender;
    private $translator;

    public function __construct($email_sender, \Swift_Mailer $mailer, Environment $twig, TranslatorInterface $translator, ContainerBagInterface $container) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->container = $container;
        $this->emailSender = $email_sender;
        $this->translator = $translator;
    }

    public function enviarMail(Email $email) {
        try {
            $message = new \Swift_Message($email->getTitle());
                    $message->setFrom([$this->emailSender => $this->translator->trans('De Dios Jewels')])
                    ->setTo($email->getSendTo());
                   
                    $parameters = $email->getParameters();
                    
                    $logo = $message->embed(
                        \Swift_Image::fromPath(
                            realpath(
                                $this->container->get('kernel.project_dir') . "/public/tienda/img/logo.jpg"
                            )
                    ));
                    
                    $parameters['logo_src']=$logo;
                    
                    $imagenesPedido=array();
                    
                    if(isset($parameters['imagenesPedido'])){
                        foreach($parameters['imagenesPedido'] as $imagenPedido){
                            $imagenesPedido[] = $message->embed(
                                \Swift_Image::fromPath(
                                    realpath(
                                        $this->container->get('kernel.project_dir') . $imagenPedido
                                    )
                            ));
                        }
                        $parameters['imagen_pedido_src']=$imagenesPedido;
                    }
                                       
                    $message->setBody(
                    $this->twig->render(
                            $email->getTemplate(),
                            $parameters,
                            $logo,
                            $imagenesPedido
                    ),
                    'text/html'
            );
                                        
            $this->mailer->send($message);
                            
            return true;
        } catch (\Swift_TransportException $e) {
            return $this->translator("The message could not be sent.");
        }
    }

}
