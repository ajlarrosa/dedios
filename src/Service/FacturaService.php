<?php

namespace App\Service;

use Twig\Environment;

class FacturaService {
 
    private $twig;

    public function __construct( Environment $twig) {
       
        $this->twig = $twig;
    }

    public function addFacturaRow($line) {      
        echo $this->twig->render('factura/facturaRow.html.twig', ['line' => $line]);
    }   
}
