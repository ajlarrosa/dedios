<?php

namespace App\Service;

use App\Repository\CotizacionRepository;
use Twig\Environment;

class CotizacionService {

    private $cotizacionRepository;
  
    public function __construct(CotizacionRepository $cotizacionRepository) {
        $this->cotizacionRepository = $cotizacionRepository;
       
    }
    
    public function getLastCotizacion(){
        return $this->cotizacionRepository->findOneBy(['activo' => true], ['id' => 'DESC'], 1);
    }

}
