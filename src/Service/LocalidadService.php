<?php

namespace App\Service;

use App\Repository\LocalidadRepository;
use Twig\Environment;

class LocalidadService {

    private $localidadRepository;
    private $twig;

    public function __construct(LocalidadRepository $localidadRepository, Environment $twig) {
        $this->localidadRepository = $localidadRepository;
        $this->twig = $twig;
    }

    public function getSelect($activo) {
        $localidades = $this->localidadRepository->findBy(['activo' => $activo], ['descripcion' => 'ASC']);
        echo $this->twig->render('localidad/select.html.twig', ['localidades' => $localidades]);
    }

    public function getSelectByProvincia($activo,$id) {
        $localidades = $this->localidadRepository->findBy(['activo' => $activo,'provincia'=>$id], ['descripcion' => 'ASC']);
        echo $this->twig->render('localidad/select.html.twig', ['localidades' => $localidades]);
    }

}
