# Se cambio en doctrine.yaml
orm:
    auto_generate_proxy_classes: false
#Para generar proxies
doctrine orm:generate-proxies

#Para deploy en prod
composer install --no-dev --optimize-autoloader

y copiar los archivos modificados de vendor/composer

# Variables de entorno
__APP_SECERET__ #Token de la app
__DATABASE_URL__ #Url de la base de datos
__MAILER_URL__ #Url de mailer
__MAILER_DSN__ #DSN de mailer
__EMAIL_SENDER__ #E-mail de envios del sistema
__EMAIL_RECEIVER__  #E-mail donde se van a recibir los mensajes
__MERCADO_PAGO_PUBLIC_KEY__ #Public key de mercadopago
__ACCESS_TOKEN_MERCADOPAGO__ #access token de mercadopago
__TRANSLATE__ #Idioma (es)
__WEB_IS_UP__ # '1' si esta activa '0' para inactiva y muestra la pantalla de en construccion


#MercadoPago Development

# Vendedor
      {
      "id": 561830437,
      "nickname": "TETE8070987",
      "password": "qatest2023",
      "site_status": "active",
      "email": "test_user_66283220@testuser.com"
      }
     
#Comprador
 {
      "id": 561829019,
      "nickname": "TESTVQFVBARO",
      "password": "qatest5536",
      "site_status": "active",
      "email": "test_user_46692669@testuser.com"
      }

#Tarjetas de créditos
      Tarjeta           Número                  Código de seguridad	Fecha de vencimiento
      Mastercard	5031 7557 3453 0604	123                     11/25
      Visa              4509 9535 6623 3704     123                     11/25
      American Express	3711 803032 57522	1234                    11/25

Shipnow:
    Usuario: mcerovaz@gmail.com
    Contraseña: Dedios2020

Eventos de Facebook

 - Ver Pagina se captura siempre
1 -AddPaymentInfo
2- Search
3- InitiateCheckout
4- ViewContent
5- CompleteRegistration
5- Contact