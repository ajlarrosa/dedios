$(function () {   
    if (pedidoIdCupon != 0 && cupon.codigo !='') {
        
        calcularTotalCupon(cupon);
        
        calcular();

        return;
    }
});

$('#cupon').blur(function () {
    $('#errorCupon').html('');

    path = path_alta_pedido_cupon.replace('cuponReplace', $('#cupon').val());
    path = path.replace('amp;', '');

    $.get(path).done(function (response) {
        if (response.status && !response.hasOwnProperty('cupon')) {
            $('.loader-web').css('display', 'block');

            window.location.reload(true);

            return;
        }

        if (response.status && response.hasOwnProperty('cupon')) {
            cupon = response.cupon;

            $('#cuponCode').val(cupon.codigo);

            calcularTotalCupon(cupon);

            calcular();

            return;
        }

        totalCuponDescuento = 0;
        envioCuponDescuento = 0;
        
        $('#descripcionPromocion').parent().removeClass('resta');

        let promoDescription = $('#descripcionPromocion').html();
        $('#descripcionPromocion').html(promoDescription.replace(/ CUPON /g, ''));
        $('#promociones').html(formatearConSeparadorMiles(totalMontoPromocionesParaCupon + "", conDecimales));

        calcular();

        $('#cupon').val('');

        $('#errorCupon').html(response.message);
    });
});

function calcularTotalCupon(cupon) {     
    if (cupon.montoMinimo > totalParaCupon){
        $('#errorCupon').html('Aún no alcanzas el monto mínimo para poder utilizar este cupón');
        return;
    }    
    
    let descuentoPorCupon = totalMontoPromocionesParaCupon;
    switch (cupon.tipoDescuento) {
        case tiposCupon['porcentajeTotal']:
            totalCuponDescuento = totalParaCupon * cupon.descuento / 100;
            descuentoPorCupon += totalCuponDescuento;
            break;

        case tiposCupon['montoFijo']:
            totalCuponDescuento = cupon.montoFijo;
            descuentoPorCupon += totalCuponDescuento;
            break;

        case tiposCupon['envio']:
            if ($('#shipping').length) {
                envioCuponDescuento = parseFloat(obtenerValor($('#shipping').html())) * cupon.descuento / 100 - cupon.montoFijo;
                descuentoPorCupon += envioCuponDescuento;
            }
            break;

    }

    let promoDescription = $('#descripcionPromocion').html();

    if (!promoDescription.includes('CUPON')) {
        $('#descripcionPromocion').html(promoDescription + ' CUPON ');
        $('#descripcionPromocion').parent().addClass('resta');
    }

    $('#promociones').html(formatearConSeparadorMiles(descuentoPorCupon + "", conDecimales));
    
}
