/*
 * Obtener Valor
 */
function obtenerValor(valor) {
    return valor.replace(/[.]/g, '');
}
/*
 * Formater Con Separador de Miles
 */
function formatearConSeparadorMiles(valor, conDecimales = false) {
    let respuesta;
    if (conDecimales !== false) {
        respuesta = valor.replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1,$2') //si quiero agregar decimales con punto
                .replace(/\B(?=(\d{3})+(?!\d)\,?)/g, ".");
    } else {
        respuesta = valor.replace(/\D/g, "")
                //.replace(/([0-9])([0-9]{2})$/, '$1.$2') //si quiero agregar decimales con punto
                .replace(/\B(?=(\d{3})+(?!\d)\,?)/g, ".");
    }
    return respuesta;

}
               