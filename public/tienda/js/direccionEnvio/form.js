$(function () {
    (typeof provinciaInicialId !== 'undefined' && provinciaInicialId !== '') ? $('#direccion_envio_provincia').val(provinciaInicialId).change() : $('#direccion_envio_provincia').change();
});

$('#direccion_envio_provincia').change(function () {
    loadDepartamento($(this).val());
    loadLocalidad($(this).val());
});

function loadDepartamento(idProvincia) {
    $('.loading_departamento').show();

    $('#direccion_envio_departamento').attr('disabled', true);

    $.get(geo_option_select_departamentos.replace('idProvinciaReplace', idProvincia)).done(function (data) {
        $('#direccion_envio_departamento').html(data);

        if (firstLoad && typeof departamentoInicialId !== 'undefined' && departamentoInicialId !== '') {
            $('#direccion_envio_departamento').val(departamentoInicialId).change();
        }

        $('#direccion_envio_departamento').removeAttr('disabled');

        $('.loading_departamento').hide();
    });
}

$('#direccion_envio_localidad').change(function () {
    localidadCensalId = $('#direccion_envio_localidad').find(':selected').data('localidadcensalid');
    
    $('#direccion_envio_calle,#direccion_envio_calle_id').val('');

    if (firstLoad && typeof calleInicial !== 'undefined') {
        $('#direccion_envio_calle').val(calleInicial);
        $('#direccion_envio_calle_id').val(calleInicialId);
    }else {
        $('#localidad').val(localidadCensalId);
    }

    $('#direccion_envio_calle').prop('disabled', false);
    firstLoad = false;
});

function loadLocalidad(idProvincia) {
    $('.loading_localidad').show();

    $('#direccion_envio_localidad,#direccion_envio_calle').attr('disabled', true);

    localidadCensalId = '';

    $.get(geo_option_select_localidades.replace('idProvinciaReplace', idProvincia)).done(function (data) {
        $('#direccion_envio_localidad').html(data);

        if (firstLoad && typeof localidadInicialId !== 'undefined' && localidadInicialId !== '') {            
            $('#direccion_envio_localidad').val(localidadInicialId).change();
        }

        $('#direccion_envio_localidad,#direccion_envio_calle').removeAttr('disabled');

        $('.loading_localidad').hide();
    });
}

function loadCalle(nombreCalle) {    
    let path = geo_option_select_calles.replace('idLocalidadCensalReplace', localidadCensalId);

    path = path.replace('amp;', '');
    path = path.replace('dataReplace', nombreCalle);
    path = path.replace('amp;', '');

    $.get(path).done(function (data) {
        $('#direccion_envio_sugerencia').fadeIn(1000).html(data);

        $('.suggest-element').click(function () {
            $('#direccion_envio_calle').val($(this).data('nombre'));
            $('#direccion_envio_calle_id').val($(this).data('id'));
            $('#direccion_envio_sugerencia').fadeOut();
        });
    });
}

$('#direccion_envio_calle').keyup(function () {
    $('#direccion_envio_calle_id').val('');
    let data = $(this).val();

    if (data.length >= 3) {
        loadCalle(data);
        return;
    }

    $('#direccion_envio_sugerencia').fadeOut();
});

$('#aceptar').click(function () {
    $(this).attr('disabled', 'true');

    (typeof provinciaInicialId !== 'undefined' && provinciaInicialId !== '') ? $('#editarDireccionModal').modal('hide') : $('#newDireccionEnvioModal').modal('hide');

    $('.loader-web').css('display', 'block');
    $('.error-message').css('display', 'none');

    if ($('#direccion_envio_localidad').val().trim() === '') {
        $('#localityError').css('display', 'block');
        $('#direccion_envio_localidad').focus();
    } else if ($('#direccion_envio_calle').val().trim() === '') {
        $('#streetError').css('display', 'block');
        $('#direccion_envio_calle').focus();
    } else if ($('#direccion_envio_calle_numero').val().trim() === '') {
        $('#streetNumberError').css('display', 'block');
        $('#direccion_envio_calle_numero').focus();
    } else if ($('#direccion_envio_zipCode').val().trim() === '') {
        $('#zipCodeError').css('display', 'block');
        $('#direccion_envio_zipCode').focus();
    } else {
        $('form[name="direccion_envio"]').submit();
        return;
    }

    $(this).removeAttr('disabled');
    $('.loader-web').css('display', 'none');

    if (typeof provinciaInicialId !== 'undefined' && provinciaInicialId !== '') {
        $('#editarDireccionModal').modal('show');
    } else {
        $('#newDireccionEnvioModal').modal('show');
    }
});

$("#editarDireccionModal,#newDireccionEnvioModal").on("hidden.bs.modal", function () {
    $('#formAltaDireccion,#formEditarDireccion').html('');
});

