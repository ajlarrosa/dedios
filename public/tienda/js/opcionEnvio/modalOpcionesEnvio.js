console.log(1);
$('#saveCloseFormaEnvio').click(function () {
    $('.loader-web').css('display', 'block');

    let path = tienda_pedido_modify_envio.replace('replaceId', pedidoId);

    path = path.replace('amp;', '');
    path = path.replace('replaceShipNowServiceId', $(".shipNowOptionRadio:checked").val());
    path = path.replace('amp;', '');
    path = path.replace('replaceDireccionId', $(".customRadioDir:checked").val());
    path = path.replace('amp;', '');
    path = path.replace('replaceCostoEnvio', $(".shipNowOptionRadio:checked").data('price'));
    path = path.replace('amp;', '');

    $.get(path).done(function (result) {
        if (typeof result === 'object' && result !== null) {
            if (result.code === 200) {
                window.location.reload(true);
                return;
            }
            $('#errorMessage').html(result.resultado);
        } else {
            $('#errorMessage').html(traducciones['Error trying to update shipping mnethod']);
        }
        $('#errorMessage').css('display', 'block');
    });
});


function handleClick(zipCode) {
    shipNowServiceId = '';
    obtenerOpcionesShipNow(zipCode);
}

