$('#cliente_web_provincia').change(function () {
    let idProvincia = $(this).val();
    let path = path_geo_option_select_departamentos.replace('idProvinciaReplace', idProvincia);

    $('#loading_departamento').show();
    $('#cliente_web_departamento').attr('disabled', true);
    $.get(path).done(function (data) {
        $('#cliente_web_departamento').html(data);
    });

    path = path_geo_option_select_localidades.replace('idProvinciaReplace', idProvincia);
    $('#loading_localidad').show();
    $('#cliente_web_localidad').attr('disabled', true);

    $.get(path).done(function (data) {
        $('#cliente_web_localidad').html(data);
        $('#loading_departamento').hide();
        $('#cliente_web_departamento').removeAttr('disabled');
        $('#loading_localidad').hide();
        $('#cliente_web_localidad').removeAttr('disabled');
    });
});

$('#cliente_web_departamento').change(function () {
    let path = path_geo_option_select_localidades_by_departamento.replace('idDepartamentoReplace', $(this).val());

    $('#loading_localidad').show();
    $('#cliente_web_localidad').attr('disabled', true);

    $.get(path).done(function (data) {
        $('#cliente_web_localidad').html(data);
        $('#loading_localidad').hide();
        $('#cliente_web_localidad').removeAttr('disabled');
    });
});

$('#cliente_web_localidad').change(function () {
    let localidadcensalid = $(this).find(':selected').data('localidadcensalid');
    $('#cliente_web_calle').prop('disabled', false);
    $('#cliente_web_calle').data('idlocalidadcensal', localidadcensalid);
});

$('#cliente_web_calle').keyup(function () {
    let data = $(this).val();
    if (data.length >= 3) {
        let idLocalidadCensal = $(this).data('idlocalidadcensal');

        let path = path_geo_option_select_calles.replace('idLocalidadcensalReplace', idLocalidadCensal);
        path = path.replace('amp;', '');
        path = path.replace('dataReplace', data);
        path = path.replace('amp;', '');
        $('#loading_calle').show();
        $.get(path).done(function (data) {
            $('#loading_calle').hide();
            if (document.activeElement.id == 'cliente_web_calle') {
                $('#sugerencia').fadeIn(1000).html(data);
                $('.suggest-element').click(function () {
                    let id = $(this).data('id');
                    let nombre = $(this).data('nombre');
                    $('#cliente_web_calle').val(nombre);
                    $('#cliente_web_calle_id').val(id);
                    $('#sugerencia').fadeOut();
                });
            }
        });
    } else {
        $('#sugerencia').fadeOut();
    }
});

$('#cliente_web_calle').blur(function () {
    $('#sugerencia').fadeOut();
});

$('#cliente_web_provincia').change();
