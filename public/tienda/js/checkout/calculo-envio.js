$(function () {
    initializeCustomRadioClick();
});

$('#startPurchase').click(function () {
    let zipCode = Cookies.get('zipCode');
    $('#divCalculoEnvio').css('display', 'none');
    if (!grantRoleUserMinorista) {
        $('#cliente_web_zipCode').val(zipCode);
        $('#sectionRegistro').css('display', 'block');
    } else {
        $('#sinSeleccionEnvio').css('display', 'flex');
        let shipNowId = $('.shipNowOption:checked').val();
        if (shipNowId !== undefined) {
            $('#mostrarEnvio').click();
            $('.shipNowOptionConfirm[value="' + shipNowId + '"]').prop('checked', true);
            $(".shipNowOptionConfirm:checked").click();
        }
    }
});

function initializeCustomRadioClick() {
    $('.custom-radio').click(function () {
        let element = $(this).find('input:first');
        let price = element.data('price');

        $('.shipNowOptionRadio').prop('checked', false);
        element.prop('checked', true);
        $('#shipping').html(price);
        $('#shippingPrice').val(price);
        setPromos();
        $('#shipNowId').val(element.val());
        calcular();
    });
}

$('#calculateShipping').click(function () {
    let zipCode = $('#zipCode').val();
    $('#noShippingMessage').css('display', 'none');

    if (Math.floor(zipCode) == zipCode && $.isNumeric(zipCode)) {
        Cookies.set('zipCode', zipCode, {expires: 1});
        path = pathShipNowSimplifyOptionsPrice.replace('zipCodeReplace', zipCode);
        path = path.replace('tieneGrabado', hasGrabado());

        $.get(path).done(function (data) {
            $('.trShipNow').remove();
            let opcionesShipNow = JSON.parse(data);
            let html = '';
            let hayResultado = false;
            $.each(opcionesShipNow, function (i, opcionShipNow) {
                if (opcionShipNow.ship_to_type != 'PostOffice') {
                    hayResultado = true;
                    let price = calcularPrecioEnvioShipNow(opcionShipNow.price);
                    let priceValue = price;

                    if (envioGratisFlag) {
                        price = 'Gratis!';
                        priceValue = 0;
                    }

                    html += '<tr class="trShipNow">';
                    html += '   <td>';
                    html += '       <div class="custom-control custom-radio">';
                    html += '           <input type="radio" name="shipNowOption[]" class="custom-control-input shipNowOption"  value="' + opcionShipNow.shipping_service_id + '" data-price="' + priceValue + '">';
                    html += '           <label class="custom-control-label" for="shipNowOption[]"><strong>' + traducciones['Send it to my home.'] + '</strong> ' + opcionShipNow.shipping_service + '</label>';
                    html += '<br>' + obtenerPlazo(opcionShipNow);
                    html += '       </div>';
                    html += '   </td>';
                    html += '   <td>$ ' + price + '</td>';
                    html += '</tr>';
                }
            });

            if (hayResultado) {
                $('#bodyOpcionesEnvio').append(html);
                initializeCustomRadioClick();
            } else {
                $('#noShippingMessage').css('display', 'block');
            }
        });

    } else {
        $('#zipCode').val('');
    }
});

function calcularPrecioEnvioShipNow(priceShipNow) {
    return Math.round(parseFloat(priceShipNow) * 1.21 + parseFloat(costoFijoShipNow));
}

function obtenerPlazo(opcionShipNow) {

    let plazo = '';
    if (mostrarFecha) {
        let d = new Date(opcionShipNow.minimum_delivery);
        let h = new Date(opcionShipNow.maximum_delivery);
        let dString = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
        let hString = h.getDate() + "-" + (h.getMonth() + 1) + "-" + h.getFullYear();

        if (dString == hString) {
            plazo += traducciones['Arrives on '] + dString;
        } else {
            plazo += traducciones['Arrives between '] + dString + traducciones[' and '] + hString;
        }
    }
    return plazo;
}
