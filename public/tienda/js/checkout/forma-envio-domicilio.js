$(function () {
    verificarTipoEnvio(tipoEnvio, shipNowServiceId);
});

$('#linkAltaDireccionEnvio').click(function () {
    $('.loader-web').css('display', 'block');
    $.get(path_form_alta_direccion_envio).done(function (formulario) {
        $('#formAltaDireccion').html(formulario);
        $('#newDireccionEnvioModal').modal('show');
        $('.loader-web').css('display', 'none');
    });
});

$('#linkEditarDireccionFacturacion').click(function () {
    $('.loader-web').css('display', 'block');

    $.get(path_form_edit_direccion_envio).done(function (formulario) {
        $('#formEditarDireccion').html(formulario);
        $('#editarDireccionModal').modal('show');
        $('.loader-web').css('display', 'none');
    });
});

function verificarTipoEnvio(tipoEnvio, shipNowServiceId) {
    if (tipoEnvio === SHIP_NOW) {
        $('.shippingAdressNew').css('display', 'block');
        $('#leyendaDireccionEnvio').html(direccion_envio);
        obtenerServicioShipNow(shipNowServiceId);

        return;
    }

    if (tipoEnvio === OFFICE) {
        $('#leyendaTipoEnvio').html('<strong>' + traducciones['Pick up at showroom.'] + '</strong>' + traducciones['Pick up at De Dios Jewels'] + ' ' + traducciones['(obelisk area)']);

        return;
    }
}

function obtenerServicioShipNow(id) {
    let path = shipnow_find_ship_now_option_by_id.replace('zipCodeReplace', direccion_envio_zip_code);
    let leyenda = '';

    path = path.replace('replaceId', id);
    path = path.replace('amp;', '');
    path = path.replace('tieneGrabado', hasGrabado());
    path = path.replace('amp;', '');
    $.get(path).done(function (opcionShipNow) {        
        let costo = 0;
        if (!envioGratisFlag) {
            costo = calcularPrecioEnvioShipNow(opcionShipNow.price);
        }

        leyenda = '<strong >' + traducciones['Send it to my home.'] + '</strong> ' + opcionShipNow.shipping_service + '. ' + obtenerPlazo(opcionShipNow) + '. Costo $ ' + costo;
        $('#leyendaTipoEnvio').html(leyenda);

        $('#shipping').html(costo);
        $('#shippingPrice').val(costo);
        calcular();

        return;
    });

}

$('#modificarFormaEnvio').click(function (e) {
    obtenerOpcionesShipNow($(".customRadioDir:checked").data('zipcode'));
    $('#opcionesDeEnvio').modal('show');
});

function initializeShipNowOptionRadio() {
    $('.custom-shipNowOptionRadio').click(function () {
        let element = $(this).find('input:first');
        $('.shipNowOptionRadio').prop('checked', false);
        element.prop('checked', true);
    });
}

function obtenerOpcionesShipNow(zipCode) {
    let html = '';

    $('.loader-web').css('display', 'block');

    path = pathShipNowSimplifyOptionsPrice.replace('zipCodeReplace', zipCode);
    path = path.replace('tieneGrabado', hasGrabado());

    $.get(path).done(function (data) {
        $('.trShipNow').remove();

        let opcionesShipNow = JSON.parse(data);
        let hayResultado = false;

        $.each(opcionesShipNow, function (i, opcionShipNow) {
            if (opcionShipNow.ship_to_type != 'PostOffice') {
                hayResultado = true;
                let price = calcularPrecioEnvioShipNow(opcionShipNow.price);
                let priceValue = price;

                if (envioGratisFlag) {
                    price = 'Gratis!';
                    priceValue = 0;
                }

                html += '<tr class="trShipNow">';
                html += '   <td>';
                html += '       <div class="custom-control custom-radio custom-shipNowOptionRadio opcionDeEnvioShipNow">';
                html += '           <input type="radio" name="shipNowOption[]" class="custom-control-input shipNowOptionRadio"  value="' + opcionShipNow.shipping_service_id + '" data-price="' + priceValue + '">';
                html += '           <label class="custom-control-label" for="shipNowOption[]"><strong>' + traducciones['Send it to my home.'] + '</strong> ' + opcionShipNow.shipping_service + '</label>';
                html += obtenerPlazo(opcionShipNow);
                html += '       </div>';
                html += '   </td>';
                html += '   <td>$ ' + price + '</td>';
                html += '</tr>';
            }
        });

        if (hayResultado) {
            $('#bodyOpcionesEnvio').append(html);
            initializeShipNowOptionRadio();

            (shipNowServiceId === '') ? $(".shipNowOptionRadio[value=" + OFFICE + "]").click() : $(".shipNowOptionRadio[value=" + shipNowServiceId + "]").click();
        }

        $('.loader-web').css('display', 'none');
    });
}

