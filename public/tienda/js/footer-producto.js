
// icons
feather.replace();

$(function () {
    $('.mas').click(function () {
        var cantidad = parseInt($('#cantidad').val()) + 1;
        $('#cantidad').val(cantidad);
        $('#quantity').html(cantidad);
    });
    $('.menos').click(function () {
        var cantidad = parseInt($('#cantidad').val()) - 1;
        if (cantidad > 0) {
            $('#cantidad').val(cantidad);
            $('#quantity').html(cantidad);
        }
    });

    var ancho = $(window).width();

    if (ancho <= 975) {

// Menu Offcanvas - Mobile			

        $("[data-trigger]").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var offcanvas_id = $(this).attr('data-trigger');
            $(offcanvas_id).toggleClass("show");
            $('body').toggleClass("offcanvas-active");
            $(".screen-overlay").toggleClass("show");

        });

        // Close menu when pressing ESC
        $(document).on('keydown', function (event) {
            if (event.keyCode === 27) {
                $(".offcanvas").removeClass("show");
                $("body").removeClass("overlay-active");
            }
        });

        $(".btn-close, .screen-overlay").click(function (e) {
            $(".screen-overlay").removeClass("show");
            $(".offcanvas").removeClass("show");
            $("body").removeClass("offcanvas-active");
        });

        // 1st carousel, main
        $('.producto-carousel').flickity({
            contain: true,
            cellAlign: 'left',
            prevNextButtons: false,
            imagesLoaded: true
        });

        // Tooltip colores de cubic
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })


    } else {
        const $dropdown = $(".dropdown");
        const $dropdownToggle = $(".dropdown-toggle");
        const $dropdownMenu = $(".dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                        function () {
                            const $this = $(this);
                            $this.addClass(showClass);
                            $this.find($dropdownToggle).attr("aria-expanded", "true");
                            $this.find($dropdownMenu).addClass(showClass);
                            $(".darkness").removeClass("d-none");
                        },
                        function () {
                            const $this = $(this);
                            $this.removeClass(showClass);
                            $this.find($dropdownToggle).attr("aria-expanded", "false");
                            $this.find($dropdownMenu).removeClass(showClass);
                            $(".darkness").addClass("d-none");
                        }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });

        // Tooltip colores de cubic
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        // Carousel de fotos del producto
        var $carousel = $('.producto-carousel').flickity({
            contain: true,
            cellAlign: 'left',
            prevNextButtons: false,
            imagesLoaded: true
        });

        var $carouselNav = $('.producto-nav');
        var $carouselNavCells = $carouselNav.find('.carousel-cell');

        $carouselNav.on('click', '.carousel-cell', function (event) {
            var index = $(event.currentTarget).index();
            $carousel.flickity('select', index);
        });

        var flkty = $carousel.data('flickity');
        var navTop = $carouselNav.position().top;
        var navCellHeight = $carouselNavCells.height();
        var navHeight = $carouselNav.height();

        $carousel.on('select.flickity', function () {
            // set selected nav cell
            $carouselNav.find('.is-nav-selected').removeClass('is-nav-selected');
            var $selected = $carouselNavCells.eq(flkty.selectedIndex)
                    .addClass('is-nav-selected');
            // scroll nav
            var scrollY = $selected.position().top +
                    $carouselNav.scrollTop() - (navHeight + navCellHeight) / 2;
            $carouselNav.animate({
                scrollTop: scrollY
            });
        });

    }

}); //end document ready
$(window).resize(function () {
    var ancho = $(window).width();
    if (ancho <= 975) {

        $('.dropdown').off('show.bs.dropdown');
        $('.dropdown').off('hide.bs.dropdown');
// Menu Offcanvas - Mobile			

        $("[data-trigger]").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var offcanvas_id = $(this).attr('data-trigger');
            $(offcanvas_id).toggleClass("show");
            $('body').toggleClass("offcanvas-active");
            $(".screen-overlay").toggleClass("show");

        });

        // Close menu when pressing ESC
        $(document).on('keydown', function (event) {
            if (event.keyCode === 27) {
                $(".offcanvas").removeClass("show");
                $("body").removeClass("overlay-active");
            }
        });

        $(".btn-close, .screen-overlay").click(function (e) {
            $(".screen-overlay").removeClass("show");
            $(".offcanvas").removeClass("show");
            $("body").removeClass("offcanvas-active");
        });

        // 1st carousel, main
        $('.producto-carousel').flickity({
            contain: true,
            cellAlign: 'left',
            prevNextButtons: false,
            imagesLoaded: true
        });

        // Tooltip colores de cubic
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })



    } else {
        const $dropdown = $(".dropdown");
        const $dropdownToggle = $(".dropdown-toggle");
        const $dropdownMenu = $(".dropdown-menu");
        const showClass = "show";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                        function () {
                            const $this = $(this);
                            $this.addClass(showClass);
                            $this.find($dropdownToggle).attr("aria-expanded", "true");
                            $this.find($dropdownMenu).addClass(showClass);
                            $(".darkness").removeClass("d-none");
                        },
                        function () {
                            const $this = $(this);
                            $this.removeClass(showClass);
                            $this.find($dropdownToggle).attr("aria-expanded", "false");
                            $this.find($dropdownMenu).removeClass(showClass);
                            $(".darkness").addClass("d-none");
                        }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });

        // Tooltip colores de cubic
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        // Carousel de fotos del producto
        var $carousel = $('.producto-carousel').flickity({
            contain: true,
            cellAlign: 'left',
            prevNextButtons: false,
            imagesLoaded: true
        });

        var $carouselNav = $('.producto-nav');
        var $carouselNavCells = $carouselNav.find('.carousel-cell');

        $carouselNav.on('click', '.carousel-cell', function (event) {
            var index = $(event.currentTarget).index();
            $carousel.flickity('select', index);
        });

        var flkty = $carousel.data('flickity');
        var navTop = $carouselNav.position().top;
        var navCellHeight = $carouselNavCells.height();
        var navHeight = $carouselNav.height();

        $carousel.on('select.flickity', function () {
            // set selected nav cell
            $carouselNav.find('.is-nav-selected').removeClass('is-nav-selected');
            var $selected = $carouselNavCells.eq(flkty.selectedIndex)
                    .addClass('is-nav-selected');
            // scroll nav
            var scrollY = $selected.position().top +
                    $carouselNav.scrollTop() - (navHeight + navCellHeight) / 2;
            $carouselNav.animate({
                scrollTop: scrollY
            });
        });

    }
});





