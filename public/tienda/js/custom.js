$(function () {
    var ancho = $(window).width();

    if (ancho <= 975) {
        // Menu Offcanvas - Mobile			s
        startMobile();
    } else {
        
        const $dropdown = $(".dropdown");
        const $dropdownToggle = $(".dropdown-toggle");
        const $dropdownMenu = $(".dropdown-menu");
        const showClass = "show";
        const displayNone = "d-none";

        $(window).on("load resize", function () {
            if (this.matchMedia("(min-width: 768px)").matches) {
                $dropdown.hover(
                        function () {
                            const $this = $(this);
                            $this.addClass(showClass);
                            $this.find($dropdownToggle).attr("aria-expanded", "true");
                            $this.find($dropdownMenu).addClass(showClass);
                            $(".darkness").removeClass(displayNone);
                        },
                        function () {
                            const $this = $(this);                            
                            $this.removeClass(showClass);
                            
                            $this.find($dropdownToggle).attr("aria-expanded", "false");
                            $this.find($dropdownMenu).removeClass(showClass);
                            $(".darkness").addClass(displayNone);
                        }
                );
            } else {
                $dropdown.off("mouseenter mouseleave");
            }
        });

        $('.dropdown').on('show.bs.dropdown', function () {
            $(".darkness").removeClass("d-none");

        });
        $('.dropdown').on('hide.bs.dropdown', function () {
            $(".darkness").addClass("d-none");
        });
    }
});
$(window).resize(function () {
    var ancho = $(window).width();
    if (ancho <= 975) {
        $('.dropdown').off('show.bs.dropdown');
        $('.dropdown').off('hide.bs.dropdown');
        startMobile();
    }
});

/*
 * Inicia controles mobile
 */
function startMobile() {
    $("[data-trigger]").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        var offcanvas_id = $(this).attr('data-trigger');
        $(offcanvas_id).toggleClass("show");
        $('body').toggleClass("offcanvas-active");
        $(".screen-overlay").toggleClass("show");

    });

    // Close menu when pressing ESC
    $(document).on('keydown', function (event) {
        if (event.keyCode === 27) {
            $(".offcanvas").removeClass("show");
            $("body").removeClass("overlay-active");
        }
    });

    $(".btn-close, .screen-overlay").click(function (e) {
        $(".screen-overlay").removeClass("show");
        $(".offcanvas").removeClass("show");
        $("body").removeClass("offcanvas-active");
    });
}


