//WebSocket settings
JSPM.JSPrintManager.auto_reconnect = true;
JSPM.JSPrintManager.start();
JSPM.JSPrintManager.WS.onStatusChanged = function () {
    if (jspmWSStatus()) {
        //get client installed printers
        JSPM.JSPrintManager.getPrinters().then(function (myPrinters) {
            var options = '';
            for (var i = 0; i < myPrinters.length; i++) {
                if (myPrinters[i].indexOf('Datamax') >= 0) {
                    options += '<option>' + myPrinters[i] + '</option>';
                }
            }
            $('#installedPrinterName').html(options);
        });
    }
};

//Check JSPM WebSocket status
function jspmWSStatus() {
    if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Open)
        return true;
    else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Closed) {
        alert('JSPrintManager (JSPM) is not installed or not running! Download JSPM Client App from https://neodynamic.com/downloads/jspm');
        return false;
    } else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.BlackListed) {
        alert('JSPM has blacklisted this website!');
        return false;
    }
}

//Do printing...,
function print(cpj, codigo, cantidad, precio) {

    if (jspmWSStatus() && cantidad > 0) {

        //Set content to print...      
        var impresion = '';

        for (var i = 0; i < cantidad; i++) {
            impresion += "^XA";
            impresion += "^FO445,20^ADN,4,10^FDDeDios Joyas^FS";
            impresion += "^FO445,45^ADN,2,10^FD" + codigo + "^FS";
            impresion += "^FO500,65^ADN,2,10^FD" + precio.toString() + "^FS";
            impresion += "^BY1^FS";
            impresion += "^FO645,30^BCN,55,Y,N,N^FD" + codigo + "^FS";
            impresion += "^XZ";
        }

        cpj.printerCommands += impresion;
      
    } else {
        alert('No se pudo realizar la impresión');
    }
}



