$(function () {
    //Tables
    $('th').addClass('text-center');
    $('td').addClass('text-center');
    //Initialize select 2
    $('.select2').addClass('col-xs-12');
    $('.select2').select2();
    $('.showFilters').click(function () {
        if ($(this).data('option') == 1) {
            $('#seekerFilters').css('display', 'none');
        } else {
            $('#seekerFilters').css('display', 'block');
        }
    });
    $('input,select,textbox,checkbox').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });
    // jQuery plugin to prevent double submission of forms
    $.fn.preventDoubleSubmission = function () {
        $(this).on('submit', function (e) {
            var $form = $(this);
            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                e.preventDefault();
            } else {
                // Mark it so that the next submit can be ignored
                $form.data('submitted', true);
            }
        });
        // Keep chainability
        return this;
    };
    $('form').preventDoubleSubmission();
});

function goBack() {
    window.history.back();
}